package net.spigotutils.placeholder;

import org.bukkit.entity.Player;

import lombok.Getter;

/**
 * Représente un "Placeholder"
 * En français, un espace réservé.
 * Sur minecraft, ce terme est utilisé pour désigné des mots qui vont être remplacé au moment
 * ou le joueur va regarder ces mots.
 * 
 * <blockquote>
 * "Bonjour à toi %playerName% !" --> "Bonjour à toi Fabcc"
 * </blockquote>
 * 
 * @author Fabien CAYRE (Computer)
 *
 * @date 19/01/2021
 */
public abstract class Placeholder {

	@Getter
	private String name;

	/**
	 * Constructeur
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param name
	 * @date 19/01/2021
	 */
	public Placeholder(String name) {
		super();
		this.name = name;
	}


	/**
	 * Retourne la chaîne de caractères à la place du placeholder
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param player
	 * @return
	 * @date 19/01/2021
	 */
	public abstract String compute(Player player);
	
}
