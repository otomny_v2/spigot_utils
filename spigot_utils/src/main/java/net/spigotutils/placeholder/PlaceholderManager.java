package net.spigotutils.placeholder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;

import lombok.Getter;

public class PlaceholderManager {

	private static PlaceholderManager instance;

	public static PlaceholderManager getInstance() {
		return instance;
	}
	
	@Getter
	private final List<Placeholder> placeholderList;
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @date 19/01/2021
	 */
	public PlaceholderManager() {
		this.placeholderList = new ArrayList<>();
		instance = this;
	}
	
	/**
	 * Ajoute un placeholder
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param holder
	 * @date 19/01/2021
	 */
	public void addPlaceHolder(Placeholder holder) {
		this.placeholderList.add(holder);
	}
	
	/**
	 * Supprime un placeholder
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param holder
	 * @date 19/01/2021
	 */
	public void removePlaceHolder(Placeholder holder) {
		Iterator<Placeholder> iterator = this.placeholderList.iterator();
		while (iterator.hasNext()) {
			Placeholder placeholder = (Placeholder) iterator.next();
			if(placeholder.getName().equals(holder.getName())) {
				iterator.remove();
				break;
			}
		}
	}
	
	/**
	 * Remplace toutes les occurences des places holders
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param player
	 * @param placeholder
	 * @return
	 * @date 19/01/2021
	 */
	public String compute(Player player, String text) {
		String finalText = text;
		for(Placeholder placeholder : this.placeholderList) {
			finalText = finalText.replaceAll(placeholder.getName(), placeholder.compute(player));
		}
		return finalText;
	}
	
}
