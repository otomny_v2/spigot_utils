package net.spigotutils.scoreboard;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import net.kyori.adventure.text.Component;

public class ScoreboardSignBukkit extends ScoreboardHandler
{
    private List<ScoreboardText> list = Arrays.asList(new ScoreboardText[15]);
    private Scoreboard scoreBoard;
    private Objective objective;
    private String tag;
    private int lastSentCount;
    private boolean created = false;

    public ScoreboardSignBukkit(Player player, String title){
        super(player, title);
        this.tag = "PlaceHolder";
        Preconditions.checkState(title.length() <= 32, (Object)"title can not be more than 32");
        this.lastSentCount = -1;
        this.tag = ChatColor.translateAlternateColorCodes('&', title);
        if(player.getScoreboard() == null){
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            this.scoreBoard = scoreboard;
            player.setScoreboard(this.scoreBoard);
        }else{
            this.scoreBoard = player.getScoreboard();
        }
        create();
    }
    

    public ScoreboardText create(String input) {
        input = ChatColor.translateAlternateColorCodes('&', input);
        ScoreboardText text = null;
        if (input.length() <= 16) {
            text = new ScoreboardText(input, "");
        }
        else {
            String first = input.substring(0, 16);
            String second = input.substring(16, input.length());
            if (first.endsWith(String.valueOf('§'))) {
                first = first.substring(0, first.length() - 1);
                second = String.valueOf(String.valueOf('§')) + second;
            }
            final String lastColors = ChatColor.getLastColors(first);
            second = String.valueOf(String.valueOf(lastColors)) + second;
            text = new ScoreboardText(first, StringUtils.left(second, 16));
        }
        return text;
    }

    private ScoreboardText getOrCreate(int i){
        ScoreboardText txt = this.list.get(i);
        return txt == null ? new ScoreboardText("", "") : txt;
    }

    private int scoreForIndex(int index){
        return 15 - index;
    }

    public void clear() {
        this.list.clear();
    }

    public void remove(final int index) {
        final String name = this.getNameForTeamEntry(index);
        this.scoreBoard.resetScores(name);
        final Team team = this.getOrCreateTeam(getNameForTeam(index), index);
        team.unregister();
    }

    public void update(final Player player) {

        player.setScoreboard(this.scoreBoard);
        /*
        List<Integer> killTeams = new ArrayList<>();
        */

        for (int index = 0; index < this.list.size(); ++index) {
            if(this.list.get(index) == null)continue;
            final Team i = this.getOrCreateTeam(getNameForTeam(index), index);
            final ScoreboardText str = this.list.get(index);
            if(str != null){
                i.setPrefix(str.getLeft());
                i.setSuffix(str.getRight());
                this.objective.getScore(this.getNameForTeamEntry(index)).setScore(scoreForIndex(index));
            }else{
                //killTeams.add(index);
            }
        }

        // More lines were send last update
        // We need to remove
        /*
        for(int index : killTeams){
            remove(index);
        }
        */
        

        this.lastSentCount = this.list.size();
    }

    public String getNameForTeam(int index){
        return String.valueOf(String.valueOf(ChatColor.stripColor(StringUtils.left(this.tag, 14)))) + index;
    }

    public Team getOrCreateTeam(final String team, final int i) {
        Team value = this.scoreBoard.getTeam(team);
        if (value == null) {
            value = this.scoreBoard.registerNewTeam(team);
            value.addEntry(this.getNameForTeamEntry(i));
        }
        return value;
    }

    public Objective getOrCreateObjective(final String objective) {
        Objective value = this.scoreBoard.getObjective("dummyhubobj");
        if (value == null) {
            value = this.scoreBoard.registerNewObjective("dummyhubobj", "dummy");
        }
        value.setDisplayName(objective);
        return value;
    }

    public String getNameForTeamEntry(final int index) {
        return String.valueOf(String.valueOf(ChatColor.values()[index].toString())) + ChatColor.RESET;
    }


    private static class ScoreboardText
    {
        private String left;
        private String right;

        public ScoreboardText(final String left, final String right) {
            this.left = left;
            this.right = right;
        }

        public String getLeft() {
            return this.left;
        }

        public String getRight() {
            return this.right;
        }
    }

    @Override
    public void destroy() {
        if(created){
            this.objective.unregister();
        }
    }

    @Override
    public void create() {
        if(!created){
            (this.objective = this.getOrCreateObjective(this.tag)).setDisplaySlot(DisplaySlot.SIDEBAR);
            this.created = true;
        }
    }

    @Override
    public void update() {
        update(super.player);
    }

    @Override
    public boolean isCreated() {
        return this.created;
    }

    @Override
    public String getLine(int i) {
        ScoreboardText txt = this.list.get(i);
        if(txt == null) return "__null__";
        return txt.getLeft() + txt.getRight();
    }

    @Override
    public void setLine(int i, String val) {
        ScoreboardText text = create(val);
        list.set(i, text);
        update();
    }


    @Override
    public void setObjectiveName(String name) {
        this.boardName = name;
        this.objective.displayName(Component.text(name));
    }
}
