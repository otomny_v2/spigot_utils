package net.spigotutils.scoreboard;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;

public abstract class ScoreboardHandler {
    
    protected Player player;
    protected String boardName;
	@Getter
	@Setter
	protected int oldLine;
	@Getter
	@Setter
	protected int line;

    public ScoreboardHandler (Player player, String boardName){
        this.player = player;
        this.boardName = boardName;
    }

    public abstract void destroy();

    public abstract void create();

    public abstract void update();

    public abstract boolean isCreated();

    public abstract String getLine(int i);

    public abstract void setObjectiveName(String name);

    public abstract void setLine(int i, String val);
}
