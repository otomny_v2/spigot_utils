package net.spigotutils.scoreboard;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import lombok.Getter;
import net.api.process.TaskDispatcher;
import net.spigotutils.listener.StringChatEditTransformListener;
import net.spigotutils.utils.Ex;
import net.spigotutils.utils.Pair;

/**
 * Singleton pour pouvoir modifier les scoreboards.
 *
 * @author ComminQ_Q (Computer)
 *
 * @date 16/10/2019
 */
public class CustomBoards {

  public static int REFRESH_IN_TICK = 40;
  private static CustomBoards instance;

  public synchronized static CustomBoards getInstance() { return instance; }

  private static ScoreboardHandler createInstance(Player player,
                                                  String boardName) {
    return new ScoreboardSign(player, boardName);
  }

  public static void registerEvent(Plugin plugin) {
    Bukkit.getServer().getPluginManager().registerEvents(new Listener() {
      @EventHandler
      public void onQuit(PlayerQuitEvent event) {
        killScoreboard(event.getPlayer());
      }

      @EventHandler
      public void onKick(PlayerKickEvent event) {
        killScoreboard(event.getPlayer());
      }
    }, plugin);
    Bukkit.getServer().getPluginManager().registerEvents(
        new StringChatEditTransformListener(), plugin);
  }

  public static ScoreboardHandler getOrCreateScoreboard(Player player,
                                                        String boardName) {
    CustomBoards cb = getInstance();
    if (cb.exist(player))
      return cb.get(player);
    return cb.add(player, boardName);
  }

  public static void updateBoardName(Player player, String newBoardName) {
    CustomBoards cb = getInstance();
    if (cb.exist(player)) {
      cb.get(player).setObjectiveName(newBoardName);
    }
  }

  public static void addScoreboardProvider(Player player,
                                           ScoreboardProvider provider) {
    CustomBoards cb = getInstance();
    if (cb.exist(player)) {
      cb.addScoreboardProviders(player, provider);
    }
  }

  public static void killScoreboard(Player player) {
    CustomBoards cb = getInstance();
    if (cb.exist(player)) {
      cb.remove(player);
    }
  }

  public synchronized static CustomBoards getInstance(Plugin plugin) {
    if (instance == null)
      instance = new CustomBoards(plugin);
    registerEvent(plugin);
    return instance;
  }

  private Map<UUID, Pair<ScoreboardHandler, LinkedList<ScoreboardProvider>>>
      boards;
  @Getter private Plugin plugin;

  private CustomBoards(Plugin plugin) {
    this.boards = new HashMap<>();
    this.plugin = plugin;
    TaskDispatcher.run(this::ensureScoreboards, 0,
                       (int)(((double)REFRESH_IN_TICK / 20) * 1000));
  }

  public void forceScoreboard(Player player) {
    if (exist(player)) {
      ScoreboardHandler sign = get(player);
      if (!sign.isCreated()) {
        sign.create();
      }
      List<String> lines = new LinkedList<>();
      for (ScoreboardProvider provider : getProvider(player)) {
        for (String line : provider.getDisplay(player)) {
          if (lines.size() < 16) {
            lines.add(line);
          }
        }
      }
      sign.update();

      int i = 0;
      for (String line : lines) {
        sign.setLine(i, line);
        i++;
      }
      sign.setLine(lines.size());
    }
  }

  private void ensureScoreboards() {
    Bukkit.getOnlinePlayers().parallelStream().forEach(player -> {
      Ex.grab(() -> {
        if (exist(player)) {
          ScoreboardHandler sign = get(player);
          if (!sign.isCreated()) {
            sign.create();
          }
          List<String> lines = new LinkedList<>();
          for (ScoreboardProvider provider : getProvider(player)) {
            for (String line : provider.getDisplay(player)) {
              if (lines.size() < 16) {
                lines.add(line);
              }
            }
          }

          sign.update();

          int i = 0;
          for (String line : lines) {
            int localIndex = i++;

            sign.setLine(localIndex, line);
          }
          sign.setLine(lines.size());
        }
      });
    });
  }

  private ScoreboardHandler add(Player player, String boardName) {
    ScoreboardHandler ss = createInstance(player, boardName);
    this.boards.put(player.getUniqueId(), new Pair<>(ss, new LinkedList<>()));
    return ss;
  }

  private boolean exist(Player player) {
    return this.boards.containsKey(player.getUniqueId());
  }

  private void clearScoreboardProviders(Player player) {
    if (exist(player)) {
      this.boards.get(player.getUniqueId()).getSecondValue().clear();
    }
  }

  private void addScoreboardProviders(Player player,
                                      ScoreboardProvider provider) {
    if (exist(player)) {
      this.boards.get(player.getUniqueId()).getSecondValue().add(provider);
    }
  }

  private ScoreboardHandler get(Player player) {
    return this.boards.get(player.getUniqueId()).getFirstValue();
  }

  private List<ScoreboardProvider> getProvider(Player player) {
    return this.boards.get(player.getUniqueId()).getSecondValue();
  }

  private void remove(Player player) {
    ScoreboardHandler sign = get(player);
    sign.destroy();
    this.boards.remove(player.getUniqueId());
  }
}
