package net.spigotutils.scoreboard;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundSetDisplayObjectivePacket;
import net.minecraft.network.protocol.game.ClientboundSetObjectivePacket;
import net.minecraft.network.protocol.game.ClientboundSetPlayerTeamPacket;
import net.minecraft.network.protocol.game.ClientboundSetScorePacket;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import net.minecraft.world.scores.Objective;
import net.minecraft.world.scores.PlayerTeam;
import net.minecraft.world.scores.Scoreboard;
import net.minecraft.world.scores.Team.Visibility;
import net.minecraft.world.scores.criteria.ObjectiveCriteria;
import net.spigotutils.chat.ColorUtils;

/**
 * 
 * @author zyuiop
 * @author Fabcc
 *
 * @date 12/01/2020 (Modifié pour notre version de serveur)
 */
public class ScoreboardSign extends ScoreboardHandler {
	private boolean created = false;
	protected final VirtualTeam[] lines = new VirtualTeam[15];

	@Getter
	@Setter
	private int updating;

	private Scoreboard scoreboard;

	/**
	 * Create a scoreboard sign for a given player and using a specifig objective
	 * name
	 * 
	 * @param player        the player viewing the scoreboard sign
	 * @param objectiveName the name of the scoreboard sign (displayed at the top of
	 *                      the scoreboard)
	 */
	public ScoreboardSign(Player player, String objectiveName) {
		super(player, objectiveName);
		this.updating = 2;
		this.scoreboard = new Scoreboard();
	}

	/**
	 * Send the initial creation packets for this scoreboard sign. Must be called at
	 * least once.
	 */
	@Override
	public void create() {
		if (created)
			return;

		ServerGamePacketListenerImpl player = getPlayer();
		player.send(createObjectivePacket(0, super.boardName));
		player.send(setObjectiveSlot());
		int i = 0;
		while (i < lines.length)
			sendLine(i++);

		created = true;
	}

	/**
	 * Send the packets to remove this scoreboard sign. A destroyed scoreboard sign
	 * must be recreated using {@link ScoreboardSign#create()} in order to be used
	 * again
	 */
	@Override
	public void destroy() {
		if (!created)
			return;

		getPlayer().send(createObjectivePacket(1, null));
		for (VirtualTeam team : lines)
			if (team != null)
				getPlayer().send(team.removeTeam());

		created = false;
	}

	/**
	 * Change the name of the objective. The name is displayed at the top of the
	 * scoreboard.
	 * 
	 * @param name the name of the objective, max 32 char
	 */
	@Override
	public void setObjectiveName(String name) {
		super.boardName = name;
		if (created)
			getPlayer().send(createObjectivePacket(2, name));
	}

	/**
	 * Change a scoreboard line and send the packets to the player. Can be called
	 * async.
	 * 
	 * @param line  the number of the line (0 <= line < 15)
	 * @param value the new value for the scoreboard line
	 */
	@Override
	public void setLine(int line, String value) {
		VirtualTeam team = getOrCreateTeam(line);
		String old = team.getCurrentPlayer();

		if (old != null && created)
			getPlayer().send(removeLine(old));

		team.setValue(value);
		sendLine(line);
	}

	/**
	 * Remove a given scoreboard line
	 * 
	 * @param line the line to remove
	 */
	public void removeLine(int line) {
		VirtualTeam team = getOrCreateTeam(line);
		String old = team.getCurrentPlayer();

		if (old != null && created) {
			getPlayer().send(removeLine(old));
			getPlayer().send(team.removeTeam());
		}

		lines[line] = null;
	}

	/**
	 * Get the current value for a line
	 * 
	 * @param line the line
	 * @return the content of the line
	 */
	@Override
	public String getLine(int line) {
		if (line > 14)
			return null;
		if (line < 0)
			return null;
		return getOrCreateTeam(line).getValue();
	}

	/**
	 * Get the team assigned to a line
	 * 
	 * @return the {@link VirtualTeam} used to display this line
	 */
	public VirtualTeam getTeam(int line) {
		if (line > 14)
			return null;
		if (line < 0)
			return null;
		return getOrCreateTeam(line);
	}

	@Override
	public void update() {
		for (VirtualTeam team : this.lines)
			if (team != null) {
				this.getPlayer().send(team.removeTeam());
			}
	}

	protected ServerGamePacketListenerImpl getPlayer() {
		return ((CraftPlayer) player).getHandle().connection;
	}

	@SuppressWarnings("rawtypes")
	private void sendLine(int line) {
		if (line > 14)
			return;
		if (line < 0)
			return;
		if (!created)
			return;

		int score = (15 - line);
		VirtualTeam val = getOrCreateTeam(line);
		for (Packet packet : val.sendLine())
			getPlayer().send(packet);
		getPlayer().send(sendScore(val.getCurrentPlayer(), score));
		val.reset();
	}

	private VirtualTeam getOrCreateTeam(int line) {
		if (lines[line] == null)
			lines[line] = new VirtualTeam("__fakeScore" + line);

		return lines[line];
	}

	/*
	 * Factories
	 */
	private ClientboundSetObjectivePacket createObjectivePacket(int mode, String displayName) {

		Objective objective = createObjective(player.getName(), displayName);
		ClientboundSetObjectivePacket packet = new ClientboundSetObjectivePacket(objective, mode);
		// Nom de l'objectif

		/*
		 * PacketPlayOutScoreboardObjective packet = new
		 * PacketPlayOutScoreboardObjective(); setField(packet, "a", player.getName());
		 * 
		 * // Mode // 0 : créer // 1 : Supprimer // 2 : Mettre à jour setField(packet,
		 * "d", mode);
		 * 
		 * if (mode == 0 || mode == 2) { setField(packet, "b",
		 * IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + displayName + "\"}"));
		 * setField(packet, "c",
		 * IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER); }
		 */
		return packet;
	}

	private ClientboundSetDisplayObjectivePacket setObjectiveSlot() {
		ClientboundSetDisplayObjectivePacket packet = new ClientboundSetDisplayObjectivePacket(1,
				createObjective(player.getName(), player.getName()));

		return packet;
	}

	private ClientboundSetScorePacket sendScore(String line, int score) {
		// return new PacketPlayOutScoreboardScore(Action.CHANGE, player.getName(),
		// line, score);
		/*
		 * constructor class ClientboundSetScorePacket this.owner = var2;
		 * this.objectiveName = var1; this.score = var3; this.method = var0;
		 */
		return new ClientboundSetScorePacket(net.minecraft.server.ServerScoreboard.Method.CHANGE, player.getName(),
				line, score);
	}

	private ClientboundSetScorePacket removeLine(String line) {
		return new ClientboundSetScorePacket(net.minecraft.server.ServerScoreboard.Method.REMOVE, null, line, 0);
	}

	private Objective createObjective(String objectiveName, String displayName) {
		// var0 = scoreboard
		// var1 = name
		// var2 = ObjectiveCriteria
		// var3 = Component DisplayName
		// var4 = RenderType
		return new Objective(ScoreboardSign.this.scoreboard, objectiveName, ObjectiveCriteria.DUMMY,
				Component.Serializer.fromJson("{\"text\":\"" + displayName + "\"}"),
				ObjectiveCriteria.RenderType.INTEGER);
	}

	/**
	 * This class is used to manage the content of a line. Advanced users can use it
	 * as they want, but they are encouraged to read and understand the code before
	 * doing so. Use these methods at your own risk.
	 */
	public class VirtualTeam {
		private final String name;
		private String prefix;
		private String suffix;
		private String currentPlayer;
		private String oldPlayer;

		private boolean prefixChanged, suffixChanged, playerChanged = false;
		private boolean first = true;

		private VirtualTeam(String name, String prefix, String suffix) {
			this.name = name;
			this.prefix = prefix;
			this.suffix = suffix;
		}

		private VirtualTeam(String name) {
			this(name, "", "");
		}

		public String getName() {
			return name;
		}

		public String getPrefix() {
			return prefix;
		}

		public void setPrefix(String prefix) {
			if (this.prefix == null || !this.prefix.equals(prefix))
				this.prefixChanged = true;
			this.prefix = prefix;
		}

		public String getSuffix() {
			return suffix;
		}

		public void setSuffix(String suffix) {
			if (this.suffix == null || !this.suffix.equals(prefix))
				this.suffixChanged = true;
			this.suffix = suffix;
		}

		/**
		 * Following the https://wiki.vg/Protocol#Teams FOR MODE - 0: create team - 1:
		 * remove team - 2: update team info - 3: add entities to team - 4: remove
		 * entities from team
		 * 
		 * 
		 * @param mode
		 * @return
		 */
		private ClientboundSetPlayerTeamPacket createPacket(int mode) {
			ClientboundSetPlayerTeamPacket packet = null;

			PlayerTeam team = createPacketTeam();

			/*
			 * private static final int METHOD_ADD = 0; CREATE TEAM private static final int
			 * METHOD_REMOVE = 1; REMOVE TEAM private static final int METHOD_CHANGE = 2;
			 * private static final int METHOD_JOIN = 3; ADD PLAYER private static final int
			 * METHOD_LEAVE = 4; REMOVE A PLAYER
			 */

			switch (mode) {
				case 0: // CREATE
					// var0 => the team object
					/*
					 * var1 => IF TRUE: Mode is ADD (adding team) We are adding the player (the list
					 * provided contains the playername) IF FALSE: Mode is CHANGE () Empty list is
					 * provided
					 */
					packet = ClientboundSetPlayerTeamPacket.createAddOrModifyPacket(team, true);
					break;
				case 1: // REMOVE
					packet = ClientboundSetPlayerTeamPacket.createRemovePacket(team);
					break;
				case 2: // UPDATE TEAM INFO
					packet = ClientboundSetPlayerTeamPacket.createAddOrModifyPacket(team, false);
					break;
				case 3: // ADD ENTRIES
					// var2 == Action.ADD ? 3 : 4
					// var0 => team object
					// var1 => player name (string)
					/*
					 * var2 => Action tag IF Action.ADD Mode IS 3 (JOIN PLAYER) ELSE Mode is 4
					 * (LEAVE PLAYER)
					 */
					packet = ClientboundSetPlayerTeamPacket.createPlayerPacket(team, "",
							ClientboundSetPlayerTeamPacket.Action.ADD);
					break;
				case 4: // REMOVE ENTRIES
					packet = ClientboundSetPlayerTeamPacket.createPlayerPacket(team, "",
							ClientboundSetPlayerTeamPacket.Action.REMOVE);
					break;
				default:
					throw new IllegalArgumentException("Illegal mode provided");
			}

			/*
			 * Spigot Mapping fields PacketPlayOutScoreboardTeam packet = new
			 * PacketPlayOutScoreboardTeam(); setField(packet, "a", name); setField(packet,
			 * "b", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + "" + "\"}"));
			 * setField(packet, "c", IChatBaseComponent.ChatSerializer.a("{\"text\":\"" +
			 * prefix + "\"}")); setField(packet, "d",
			 * IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + suffix + "\"}"));
			 * setField(packet, "e", "always"); setField(packet, "g", EnumChatFormat.RESET);
			 * setField(packet, "i", mode);
			 */

			return packet;
		}

		private PlayerTeam createPacketTeam() {
			PlayerTeam team = new PlayerTeam(ScoreboardSign.this.scoreboard, this.name); // name is field A
			team.setDisplayName(Component.Serializer.fromJson("{\"text\":\"\"}")); // field B (empty string)
			team.setPlayerPrefix(Component.Serializer.fromJson("{\"text\":\"" + prefix + "\"}")); // field C
			team.setPlayerSuffix(Component.Serializer.fromJson("{\"text\":\"" + suffix + "\"}")); // field D
			team.setNameTagVisibility(Visibility.ALWAYS); // field E
			team.setColor(ChatFormatting.RESET); // field G
			return team;
		}

		public ClientboundSetPlayerTeamPacket createTeam() {
			PlayerTeam team = createPacketTeam();
			return ClientboundSetPlayerTeamPacket.createAddOrModifyPacket(team, true);
		}

		public ClientboundSetPlayerTeamPacket updateTeam() {
			PlayerTeam team = createPacketTeam();
			return ClientboundSetPlayerTeamPacket.createAddOrModifyPacket(team, false);
		}

		public ClientboundSetPlayerTeamPacket removeTeam() {
			ClientboundSetPlayerTeamPacket p = ClientboundSetPlayerTeamPacket
					.createRemovePacket(ScoreboardSign.createTeam(this.name, ""));
			first = true;
			return p;
		}

		public void setPlayer(String name) {
			if (this.currentPlayer == null || !this.currentPlayer.equals(name))
				this.playerChanged = true;
			this.oldPlayer = this.currentPlayer;
			this.currentPlayer = name;
		}

		public Iterable<ClientboundSetPlayerTeamPacket> sendLine() {
			List<ClientboundSetPlayerTeamPacket> packets = new ArrayList<>();

			if (first) {
				packets.add(createTeam());
			} else if (prefixChanged || suffixChanged) {
				packets.add(updateTeam());
			}

			if (first || playerChanged) {
				if (oldPlayer != null) // remove these two lines ?
				{
					// THIS IS CAUSING SERIOUS CLIENT SHIT
					// packets.add(addOrRemovePlayer(ClientboundSetPlayerTeamPacket.Action.REMOVE,
					// oldPlayer));
				}
				// And then add the player
				packets.add(changePlayer());
			}

			if (first)
				first = false;

			return packets;
		}

		public void reset() {
			prefixChanged = false;
			suffixChanged = false;
			playerChanged = false;
			oldPlayer = null;
		}

		public ClientboundSetPlayerTeamPacket changePlayer() {
			return addOrRemovePlayer(ClientboundSetPlayerTeamPacket.Action.ADD, currentPlayer);
		}

		@SuppressWarnings("unchecked")
		public ClientboundSetPlayerTeamPacket addOrRemovePlayer(ClientboundSetPlayerTeamPacket.Action action,
				String playerName) {
			PlayerTeam team = createPacketTeam();

			var packet = ClientboundSetPlayerTeamPacket.createPlayerPacket(team, playerName, action);

			return packet;
		}

		public String getCurrentPlayer() {
			return currentPlayer;
		}

		public String getValue() {
			return getPrefix() + getCurrentPlayer() + getSuffix();
		}

		public void setValueOld(String value) {
			if (value.length() <= 16) {
				setPrefix("");
				setSuffix("");
				setPlayer(value);
			} else if (value.length() <= 32) {
				setPrefix(value.substring(0, 16));
				setPlayer(value.substring(16));
				setSuffix("");
			} else if (value.length() <= 48) {
				setPrefix(value.substring(0, 16));
				setPlayer(value.substring(16, 32));
				setSuffix(value.substring(32));
			} else {
				throw new IllegalArgumentException(
						"Too long value ! Max 48 characters, value was " + value.length() + " !");
			}
		}

		public void setValue(String value) {
			if (value.length() <= 16) {
				setPrefix("");
				setPlayer(value);
				setSuffix("");
			} else if (value.length() <= 32) {
				String first = value.substring(0, 16);
				ChatColor color = ChatColor.RESET;
				ChatColor temp = null;
				if ((temp = ColorUtils.lastColorOfString(first)) != null)
					color = temp;

				// La couleur se situe a la frontière du string gauche et du string milieu
				int i = first.lastIndexOf('§');
				if (i == 15) {
					// On prend le caractère juste après
					char colorChar = value.charAt(16);
					color = ChatColor.getByChar(colorChar);
					// On réduit la taille du string du coup
					first = first.substring(0, 15);
				}

				setPrefix(first);
				setPlayer(color + value.substring(i == 15 ? 17 : 16));
				setSuffix("");
			} else if (value.length() <= 48) {
				String s = value.substring(0, 16);
				ChatColor color = ChatColor.RESET;
				ChatColor temp = null;
				if ((temp = ColorUtils.lastColorOfString(s)) != null)
					color = temp;

				setPrefix(s);
				String ss = value.substring(16, 32);

				setPlayer(color + ss);

				color = ChatColor.RESET;
				temp = null;
				if ((temp = ColorUtils.lastColorOfString(ss)) != null)
					color = temp;

				setSuffix(color + value.substring(32));
			} else {
				throw new IllegalArgumentException(
						"Too long value ! Max 48 characters, value was " + value.length() + " !");
			}
		}
	}

	private static PlayerTeam createTeam(String name, String player) {
		PlayerTeam team = new PlayerTeam(null, name);
		team.getPlayers().add(player);
		return team;
	}

	private static void setField(Object edit, String fieldName, Object value) {
		try {
			Field field = edit.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(edit, value);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isCreated() {
		return this.created;
	}
}