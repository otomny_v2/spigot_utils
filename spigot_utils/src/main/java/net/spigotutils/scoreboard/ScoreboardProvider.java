package net.spigotutils.scoreboard;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public abstract class ScoreboardProvider {

	protected List<String> list;
	
	public ScoreboardProvider() {
		this.list = new ArrayList<String>();
	}
	
	public abstract void setLine(Player player);
	
	protected void add(String string) {
		this.list.add(string);
	}
	
	public List<String> getDisplay(Player player){
		this.list.clear();
		this.setLine(player);
		return this.list;
	}
	
}
