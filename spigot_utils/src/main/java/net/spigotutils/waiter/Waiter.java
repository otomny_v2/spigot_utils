package net.spigotutils.waiter;

import java.util.function.Consumer;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Builder;

@Builder
public class Waiter {

	@Builder.Default
	private int time = 30;
	private Plugin plugin;
	@Builder.Default
	private Consumer<Integer> each = i -> {};
	@Builder.Default
	private Runnable end = () -> {};
	
	public void launch() {
		new BukkitRunnable() {

			int timeMax = time;
			
			@Override
			public void run() {
				each.accept(timeMax);
				timeMax--;
				if(timeMax == 0) {
					Waiter.this.end.run();
					cancel();
				}
			}
		}.runTaskTimerAsynchronously(this.plugin, 0, 20);
	}
	
}
