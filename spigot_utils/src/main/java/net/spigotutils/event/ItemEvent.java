package net.spigotutils.event;

import org.bukkit.Material;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;

public class ItemEvent {

  public static int shiftClickItem(CraftItemEvent event) {
    int itemsChecked = 0;
    int possibleCreations = 1;
    if (event.isShiftClick()) {
      for (ItemStack item : event.getInventory().getMatrix()) {
        if (item != null && !item.getType().equals(Material.AIR)) {
          if (itemsChecked == 0)
            possibleCreations = item.getAmount();
          else
            possibleCreations = Math.min(possibleCreations, item.getAmount());
          itemsChecked++;
        }
      }
    }
    return event.getRecipe().getResult().getAmount() * possibleCreations;
  }
}
