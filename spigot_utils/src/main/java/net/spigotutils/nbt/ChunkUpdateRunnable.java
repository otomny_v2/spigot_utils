package net.spigotutils.nbt;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import net.minecraft.network.protocol.game.ClientboundForgetLevelChunkPacket;
import net.minecraft.network.protocol.game.ClientboundLevelChunkWithLightPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ThreadedLevelLightEngine;
import net.minecraft.world.level.chunk.LevelChunk;
import net.spigotutils.nms.PacketUtils;

public class ChunkUpdateRunnable implements Runnable {

  private static ChunkUpdateRunnable instance;
  public static int CHUNK_UPDATE_PER_PERIOD = 2;

  public static void init(Plugin plugin) {
    if (instance != null)
      throw new IllegalStateException(
          "ChunkUpdateRunnable already been initialized");
    instance = new ChunkUpdateRunnable();
    Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, instance, 0, 2L);
  }

  public static void upload(ChunkBatch chunkBatch) {}

  public record ChunkBatch(World bukkitWorld, ServerLevel world,
                           List<LevelChunk> chunks, Runnable onEnd) {}

  public ConcurrentLinkedQueue<ChunkBatch> chunkBatchs =
      new ConcurrentLinkedQueue<>();

  @Override
  public void run() {
    if (!this.chunkBatchs.isEmpty()) {
      int processed = 0;
      var next = this.chunkBatchs.peek();
      var world = next.world();
      ThreadedLevelLightEngine engine = world.getChunkSource().getLightEngine();
      var iterator = next.chunks().iterator();
      while (iterator.hasNext()) {
        var chunk = iterator.next();

        ClientboundForgetLevelChunkPacket unload =
            new ClientboundForgetLevelChunkPacket(chunk.getPos().x,
                                                  chunk.getPos().z);

        ClientboundLevelChunkWithLightPacket lightAndLoad =
            new ClientboundLevelChunkWithLightPacket(chunk, engine, null, null,
                                                     true);

        for (Player p : Bukkit.getOnlinePlayers()) {
          if (!p.getWorld().equals(next.bukkitWorld()))
            continue;

          ServerPlayer ep = ((CraftPlayer)p).getHandle();
          int dist = Bukkit.getViewDistance() + 1;
          int chunkX = ep.chunkPosition().x;
          int chunkZ = ep.chunkPosition().z;
          if (chunk.getPos().x < chunkX - dist ||
              chunk.getPos().x > chunkX + dist ||
              chunk.getPos().z < chunkZ - dist ||
              chunk.getPos().z > chunkZ + dist)
            continue;
          PacketUtils.sendPacket(p, unload);
          PacketUtils.sendPacket(p, lightAndLoad);
        }
        processed++;
				iterator.remove();
        if (processed > CHUNK_UPDATE_PER_PERIOD) {
          return;
        }
      }
    }
  }
}
