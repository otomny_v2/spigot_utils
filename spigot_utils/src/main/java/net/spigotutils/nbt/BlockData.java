package net.spigotutils.nbt;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.BlockPos;
import net.spigotutils.utils.Triplet;

public class BlockData {
  private static final Pattern pattern =
      Pattern.compile("(?<=minecraft:)[a-z]\\w+");

  private static final Set<String> KEEP_BLOCK_DATAS =
      Set.of("STAIRS", "SLAB", "TRAPDOOR", "BUTTON", "DOOR", "LOG", "STEM");

  @Getter @Setter private String world;
  @Getter @Setter private Triplet<Integer, Integer, Integer> blockPosition;
  @Getter @Setter private Material fastMaterial;
  @Getter @Setter private org.bukkit.block.data.BlockData blockData;

  public BlockData(String blockData, Location location) {
    this.blockPosition = new Triplet<>(
        location.getBlockX(), location.getBlockY(), location.getBlockZ());
    this.world = location.getWorld().getName();
    this.blockData = location.getBlock().getBlockData();
    this.fastMaterial = location.getBlock().getType();
  }

  public BlockData(String blockData, int x, int y, int z) {
    this(blockData, "world", x, y, z);
  }

  public BlockData(Material data, String world, int x, int y, int z) {
    this.blockPosition = new Triplet<>(x, y, z);
    this.world = world;
    this.fastMaterial = data;
    this.blockData =
        Bukkit.createBlockData("minecraft:" + data.toString().toLowerCase());
  }

  public BlockData(String blockData, String world, int x, int y, int z) {
    this.blockPosition = new Triplet<>(x, y, z);
    this.world = world;
    Matcher m = pattern.matcher(blockData);
    m.find();
    var blockString = m.group().toUpperCase();
    if (blockString.equals("GRASS_PATH")) {
      blockString = "DIRT_PATH";
      this.blockData = Bukkit.createBlockData("minecraft:dirt_path");
    } else {
      try {
        this.blockData = Bukkit.createBlockData(blockData);
      } catch (Exception e) {
        this.blockData = Bukkit.createBlockData(
            blockData.substring(0, blockData.indexOf('[')));
      }
    }
    this.fastMaterial = Material.valueOf(blockString);
  }

  public BlockData(org.bukkit.block.data.BlockData blockData,
                   @NotNull String world, int x, int y, int z) {
    this.blockData = blockData;
    this.world = world;
    this.blockPosition = new Triplet<>(x, y, z);
    this.fastMaterial = blockData.getMaterial();
  }

  /**
   * Replace with the current material Mutate the object If
   * new & old materials are both SLAB / STAIRS, we try to
   * keep blockData
   *
   * @param material
   */
  public void swapMaterial(Material material) {
    var replace = material.toString();
    var current = fastMaterial.toString();
    if (KEEP_BLOCK_DATAS.stream().anyMatch(
            keep -> replace.contains(keep) && current.contains(keep))) {

      // On essaye de remplacer le matérial à l'intérieur de la
      // chaîne de caracètre pour
      // garder les mêmes propriétés de blocks
      var rawBlockData = this.blockData.getAsString();
      int end = rawBlockData.indexOf("[");
      if (end == -1) {
        end = rawBlockData.length();
      }

      String newBlockData = "minecraft:" + material.toString().toLowerCase() +
                            rawBlockData.substring(end, rawBlockData.length());

      this.blockData = Bukkit.createBlockData(newBlockData);
      this.fastMaterial = material;
    } else {
      this.fastMaterial = material;
      this.blockData = Bukkit.createBlockData(
          "minecraft:" + material.toString().toLowerCase());
    }
  }

  public BlockPos getBlockPos() {
    return new BlockPos(this.blockPosition.get1(), this.blockPosition.get2(),
                        this.blockPosition.get3());
  }
}
