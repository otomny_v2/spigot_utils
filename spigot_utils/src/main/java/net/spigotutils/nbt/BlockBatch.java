package net.spigotutils.nbt;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class BlockBatch {

  @Getter @Setter private String world;

  @Getter @Setter private List<BlockData> blockDatas;

  @Getter @Setter private Runnable onEnd;

  /**
   * @param world
   * @param blockDatas
   * @param onEnd
   */
  public BlockBatch(String world, List<BlockData> blockDatas, Runnable onEnd) {
    this.world = world;
    this.blockDatas = blockDatas;
    this.onEnd = onEnd;
  }
}
