package net.spigotutils.nbt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import org.apache.commons.lang.Validate;
import org.bson.Document;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import lombok.Getter;
import lombok.Setter;
import net.api.process.TaskDispatcher;
import net.spigotutils.data.Adaptable;
import net.spigotutils.location.LocationUtils;
import net.spigotutils.utils.Pair;
import net.spigotutils.utils.number.NumberUtils;

@Getter
@Setter
/**
 * Represent a zone in a world
 */
public class Area implements Adaptable {

	private Location start;
	private Location end;

	/**
	 * @param start
	 * @param end
	 */
	public Area(Location start, Location end) {
		Objects.requireNonNull(start, "Error: Area, start is null");
		Objects.requireNonNull(end, "Error: Area, end is null");
		Validate.isTrue(start.getWorld() == end.getWorld(),
				"Location must be in same worlds");
		this.start = start;
		this.end = end;
	}

	public Area(Document document) {
		this.start = LocationUtils
				.fromDocument(document.get("start", new Document()));
		this.end = LocationUtils
				.fromDocument(document.get("end", new Document()));
	}

	public void asyncBlockData(
			Consumer<List<BlockData>> consumer) {
		this.asyncBlockData(false, consumer);
	}

	public void asyncBlockData(boolean ignoreAir,
			Consumer<List<BlockData>> consumer) {
		Objects.requireNonNull(this.start,
				"Error: Area, this.start is null");
		Objects.requireNonNull(this.end,
				"Error: Area, this.end is null");

		var minX = Math.min(this.start.getBlockX(),
				this.end.getBlockX());
		var minZ = Math.min(this.start.getBlockZ(),
				this.end.getBlockZ());
		var minY = Math.min(this.start.getBlockY(),
				this.end.getBlockY());
		var maxX = Math.max(this.start.getBlockX(),
				this.end.getBlockX());
		var maxZ = Math.max(this.start.getBlockZ(),
				this.end.getBlockZ());
		var maxY = Math.max(this.start.getBlockY(),
				this.end.getBlockY());

		var world = this.start.getWorld().getName();

		// load chunk snapshots
		var minChunkX = minX >> 4;
		var minChunkZ = minZ >> 4;
		var maxChunkX = maxX >> 4;
		var maxChunkZ = maxZ >> 4;

		Map<Pair<Integer, Integer>, ChunkSnapshot> snapshots = new HashMap<>();

		for (int cx = minChunkX; cx <= maxChunkX; cx++) {
			for (int cz = minChunkZ; cz <= maxChunkZ; cz++) {
				var snapshot = this.start.getWorld().getChunkAt(cx, cz)
						.getChunkSnapshot();
				snapshots.put(Pair.of(cx, cz), snapshot);
			}
		}

		TaskDispatcher.run(() -> {
			List<BlockData> datas = new LinkedList<>();
			for (int x = minX; x < maxX + 1; x++) {
				for (int z = minZ; z < maxZ + 1; z++) {
					for (int y = maxY; y >= minY; y--) {
						var chunkSnapshot = snapshots
								.get(Pair.of(x >> 4, z >> 4));
						// Le Block à la position X, Y , Z
						var blockAt = chunkSnapshot.getBlockData(
								NumberUtils.mod(x, 16), y,
								NumberUtils.mod(z, 16));
						if (ignoreAir
								&& blockAt.getMaterial() == Material.AIR)
							continue;
						// Le type de bloc actuel (pour le //undo)
						BlockData old = new BlockData(blockAt, world, x, y,
								z);
						datas.add(old);
					}
				}
			}
			consumer.accept(datas);
		});
	}

	public List<BlockData> blockDatasSnapshot() {
		return blockDatasSnapshot(false);
	}

	/**
	 * @return Chunk snapshots
	 */
	public Map<Pair<Integer, Integer>, ChunkSnapshot> getChunksSnapshots() {
		var minX = Math.min(this.start.getBlockX(),
				this.end.getBlockX());
		var minZ = Math.min(this.start.getBlockZ(),
				this.end.getBlockZ());
		var maxX = Math.max(this.start.getBlockX(),
				this.end.getBlockX());
		var maxZ = Math.max(this.start.getBlockZ(),
				this.end.getBlockZ());

		// load chunk snapshots
		var minChunkX = minX >> 4;
		var minChunkZ = minZ >> 4;
		var maxChunkX = maxX >> 4;
		var maxChunkZ = maxZ >> 4;

		Map<Pair<Integer, Integer>, ChunkSnapshot> snapshots = new HashMap<>();

		for (int cx = minChunkX; cx <= maxChunkX; cx++) {
			for (int cz = minChunkZ; cz <= maxChunkZ; cz++) {
				var snapshot = this.start.getWorld().getChunkAt(cx, cz)
						.getChunkSnapshot();
				snapshots.put(Pair.of(cx, cz), snapshot);
			}
		}
		return snapshots;
	}

	/**
	 * Utilisation des ChunkSnapshots pour éviter de
	 * synchroniser h24 sur le main thread
	 * 
	 * @return Les données
	 */
	public List<BlockData> blockDatasSnapshot(boolean ignoreAir) {
		Objects.requireNonNull(this.start,
				"Error: Area, this.start is null");
		Objects.requireNonNull(this.end,
				"Error: Area, this.end is null");

		var minX = Math.min(this.start.getBlockX(),
				this.end.getBlockX());
		var minZ = Math.min(this.start.getBlockZ(),
				this.end.getBlockZ());
		var minY = Math.min(this.start.getBlockY(),
				this.end.getBlockY());
		var maxX = Math.max(this.start.getBlockX(),
				this.end.getBlockX());
		var maxZ = Math.max(this.start.getBlockZ(),
				this.end.getBlockZ());
		var maxY = Math.max(this.start.getBlockY(),
				this.end.getBlockY());
		List<BlockData> datas = new ArrayList<>();

		var world = this.start.getWorld().getName();

		// load chunk snapshots
		var minChunkX = minX >> 4;
		var minChunkZ = minZ >> 4;
		var maxChunkX = maxX >> 4;
		var maxChunkZ = maxZ >> 4;

		Map<Pair<Integer, Integer>, ChunkSnapshot> snapshots = new HashMap<>();

		for (int cx = minChunkX; cx <= maxChunkX; cx++) {
			for (int cz = minChunkZ; cz <= maxChunkZ; cz++) {
				var snapshot = this.start.getWorld().getChunkAt(cx, cz)
						.getChunkSnapshot();
				snapshots.put(Pair.of(cx, cz), snapshot);
			}
		}

		for (int x = minX; x <= maxX + 1; x++) {
			for (int z = minZ; z <= maxZ + 1; z++) {
				for (int y = maxY; y >= minY; y--) {
					var chunkSnapshot = snapshots
							.get(Pair.of(x >> 4, z >> 4));
					// Le Block à la position X, Y , Z

					var blockAt = chunkSnapshot.getBlockData(
							NumberUtils.mod(x, 16), y, NumberUtils.mod(z, 16));
					if (ignoreAir && blockAt.getMaterial() == Material.AIR)
						continue;
					// Le type de bloc actuel (pour le //undo)
					BlockData old = new BlockData(blockAt, world, x, y, z);
					datas.add(old);
				}
			}
		}
		return datas;
	}

	/**
	 * All the blocks in the zone
	 * 
	 * @return
	 */
	public List<BlockData> blockDatas() {
		return blockDatas(false);
	}

	/**
	 * @return The blocks located in the zone
	 */
	public List<BlockData> blockDatas(boolean ignoreAir) {

		Objects.requireNonNull(this.start,
				"Error: Area, this.start is null");
		Objects.requireNonNull(this.end,
				"Error: Area, this.end is null");

		var minX = Math.min(this.start.getBlockX(),
				this.end.getBlockX());
		var minZ = Math.min(this.start.getBlockZ(),
				this.end.getBlockZ());
		var minY = Math.min(this.start.getBlockY(),
				this.end.getBlockY());
		var maxX = Math.max(this.start.getBlockX(),
				this.end.getBlockX());
		var maxZ = Math.max(this.start.getBlockZ(),
				this.end.getBlockZ());
		var maxY = Math.max(this.start.getBlockY(),
				this.end.getBlockY());

		var world = this.start.getWorld().getName();

		List<BlockData> datas = new ArrayList<>();

		for (int x = minX; x < maxX + 1; x++) {
			for (int z = minZ; z < maxZ + 1; z++) {
				for (int y = maxY; y >= minY; y--) {
					// Le Block à la position X, Y , Z
					Block blockAt = this.start.getWorld().getBlockAt(x, y,
							z);
					if (ignoreAir && blockAt.getType() == Material.AIR)
						continue;
					// Le type de bloc actuel (pour le //undo)
					BlockData old = new BlockData(blockAt.getBlockData(),
							world, x, y, z);
					datas.add(old);
				}
			}
		}

		return datas;
	}

	@Override
	public Document adapt() {
		return new Document()
				.append("start", LocationUtils.toDocument(start))
				.append("end", LocationUtils.toDocument(end));
	}
}
