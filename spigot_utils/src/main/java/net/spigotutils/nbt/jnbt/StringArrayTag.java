package net.spigotutils.nbt.jnbt;

import java.util.Arrays;

public class StringArrayTag extends Tag {

	/**
	 * The value.
	 */
	private final String[] value;

	public StringArrayTag(String name, String[] value) {
		super(name);
		this.value = value;
	}

	@Override
	public String[] getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder();
		for (final String string : value) {
			stringBuilder.append(string).append(" ");
		}
		final String name = getName();
		String append = "";
		if ((name != null) && !name.equals("")) {
			append = "(\"" + getName() + "\")";
		}
		return "TAG_String_Array" + append + ": " + stringBuilder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + Arrays.hashCode(value);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {

		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof StringArrayTag)) {
			return false;
		}
		final StringArrayTag other = (StringArrayTag) obj;
		if (!Arrays.equals(value, other.value)) {
			return false;
		}
		return true;
	}

}
