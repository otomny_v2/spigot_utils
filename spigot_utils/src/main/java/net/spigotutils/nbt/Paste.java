package net.spigotutils.nbt;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;

import lombok.Getter;
import net.spigotutils.schematics.FastSchematic;
import net.spigotutils.utils.Triplet;


public class Paste {

  @Getter private List<BlockData> blockDatas;
  @Getter private Schematic schematic;
  @Getter private Triplet<Double, Double, Double> relative;

  public Paste(Schematic schematic) {
    this.blockDatas = fromSchematic(schematic);
    this.schematic = schematic;
  }

  public Paste(FastSchematic f) {
    this.blockDatas = fromSchematic(f);
    this.relative = new Triplet<>(
        f.getRelativeOffSetX(), f.getRelativeOffSetY(), f.getRelativeOffSetZ());
  }

  private List<BlockData> fromSchematic(FastSchematic fastSchematic) {
    List<BlockData> datas = new ArrayList<>();
    for (int y = 0; y < fastSchematic.getHeight(); y++) {
      for (int x = 0; x < fastSchematic.getWidth(); x++) {
        for (int z = 0; z < fastSchematic.getLength(); z++) {
          int index = y * fastSchematic.getLength() * fastSchematic.getWidth() +
                      x * fastSchematic.getLength() + z;
          datas.add(new BlockData(fastSchematic.getBlocks()[index], "world", x,
                                  y, z));
        }
      }
    }
    return datas;
  }

  private List<BlockData> fromSchematic(Schematic schematic) {
    List<BlockData> datas = new ArrayList<>();
    for (int y = 0; y < schematic.height; y++) {
      for (int x = 0; x < schematic.length; x++) {
        for (int z = 0; z < schematic.width; z++) {
          int index =
              y * schematic.width * schematic.length + x * schematic.width + z;
          datas.add(new BlockData(schematic.blocks[index], "world", x, y, z));
        }
      }
    }
    return datas;
  }

  public List<BlockData> toBlockDatas(Location relative, boolean skipAir) {
    final Location fRelative = relative.clone();
    if (this.schematic != null) {
      fRelative.subtract(this.schematic.offset.getBlockX(),
                         this.schematic.offset.getBlockY(),
                         this.schematic.offset.getBlockZ());
    }
    if (this.relative != null) {
      fRelative.subtract(this.relative.getFirstValue(),
                         this.relative.getSecondValue(),
                         this.relative.getThirdValue());
    }
    return this.blockDatas.stream()
        .filter(b -> skipAir ? b.getFastMaterial() != Material.AIR : true)
        .map(blockData -> {
          Triplet<Integer, Integer, Integer> t = blockData.getBlockPosition();
          int x = t.getFirstValue(), y = t.getSecondValue(),
              z = t.getThirdValue();
          x += fRelative.getBlockX();
          y += fRelative.getBlockY();
          z += fRelative.getBlockZ();

          BlockData b = new BlockData(blockData.getBlockData(),
                                      fRelative.getWorld().getName(), x, y, z);
          return b;
        })
        .collect(Collectors.toList());
  }

  public List<BlockData> toBlockDatas(Location relative) {
    return toBlockDatas(relative, false);
  }
}
