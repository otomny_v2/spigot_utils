package net.spigotutils.nbt;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

import lombok.Getter;
import net.spigotutils.utils.Triplet;

public class BlockPasteRunnable implements Runnable {

  private static BlockPasteRunnable instance;
  public static final String UNKNOWN = "__unknown__";
  @Getter private static AtomicLong submitedBlocks = new AtomicLong(0L);

  public static BlockPasteRunnable init(Plugin plugin) {
    if (instance == null) {
      instance = new BlockPasteRunnable();
      Bukkit.getScheduler().runTaskTimer(plugin, instance, 0, 1);
    }
    return instance;
  }

  public static void addBlock(BlockData blockData) {
    getInstance().blockData.add(blockData);
    submitedBlocks.incrementAndGet();
  }

  public static void addBlocks(List<BlockData> blockData) {
    addBlocks(() -> {}, blockData);
  }

  public static void addBlocks(BlockBatch batch) {
    getInstance().blocksDatas.add(batch);
    submitedBlocks.addAndGet(batch.getBlockDatas().size());
  }

  public static void addBlocks(Runnable onEnd, List<BlockData> blockData) {
    addBlocks(new BlockBatch(UNKNOWN, blockData, onEnd));
  }

  public static BlockPasteRunnable getInstance() { return instance; }

  public static int BLOCK_PASTE_PER_TICKS = 50;
  public static boolean OPTIMIZED_BLOCKPASTE = false;
  public static boolean CHECK_BLOCK_VALUE_BEFORE = true;

  private ConcurrentLinkedQueue<BlockData> blockData;
  private ConcurrentLinkedQueue<BlockBatch> blocksDatas;
  private Map<String, EditSession> nativeSessions = new HashMap<>();

  private BlockPasteRunnable() {
    this.blockData = new ConcurrentLinkedQueue<>();
    this.blocksDatas = new ConcurrentLinkedQueue<>();
  }

  @Override
  public void run() {
    try {
      int currentBlock = 0;
      if (!this.blockData.isEmpty()) {
        Iterator<BlockData> blockIterator = this.blockData.iterator();
        while (blockIterator.hasNext()) {
          BlockData blockDataAtomic = blockIterator.next();
          World world = Bukkit.getWorld(blockDataAtomic.getWorld());
          Triplet<Integer, Integer, Integer> triplet =
              blockDataAtomic.getBlockPosition();

          Block block = world.getBlockAt(triplet.getFirstValue(),
                                         triplet.getSecondValue(),
                                         triplet.getThirdValue());
          if (CHECK_BLOCK_VALUE_BEFORE &&
              block.getType() == blockDataAtomic.getFastMaterial()) {
            // Test rapidité
            continue;
          }

          block.setBlockData(blockDataAtomic.getBlockData());

          currentBlock++;
          blockIterator.remove();
          if (currentBlock > BLOCK_PASTE_PER_TICKS) {
            return;
          }
        }
        return;
      }
      if (!this.blocksDatas.isEmpty()) {
        BlockBatch batch = this.blocksDatas.peek();
        if (batch == null)
          return;
        EditSession nativeSession = null;
        if (OPTIMIZED_BLOCKPASTE && !batch.getWorld().equals(UNKNOWN)) {
          // Un monde a été donné au batch, on connais donc ce monde.
          if (!this.nativeSessions.containsKey(batch.getWorld())) {
            this.nativeSessions.put(
                batch.getWorld(),
                new EditSession(Bukkit.getWorld(batch.getWorld())));
          }
          nativeSession = this.nativeSessions.get(batch.getWorld());
        }
        var isNative = nativeSession != null;
        Iterator<BlockData> blockIterator = batch.getBlockDatas().iterator();
        while (blockIterator.hasNext()) {
          BlockData blockDataAtomic = blockIterator.next();
          if (isNative) {
            nativeSession.setBlock(blockDataAtomic);
          } else {
            World world = Bukkit.getWorld(blockDataAtomic.getWorld());
            Triplet<Integer, Integer, Integer> triplet =
                blockDataAtomic.getBlockPosition();

            Block block = world.getBlockAt(triplet.getFirstValue(),
                                           triplet.getSecondValue(),
                                           triplet.getThirdValue());
            if (block.getType() == blockDataAtomic.getFastMaterial()) {
              // Test rapidité
              continue;
            }
            block.setBlockData(blockDataAtomic.getBlockData());
          }
          currentBlock++;
          blockIterator.remove();
          if (currentBlock > BLOCK_PASTE_PER_TICKS) {
            return;
          }
        }
        // Quand on a plus de blocks à placer
        this.blocksDatas.poll();
        batch.getOnEnd().run();
        if (isNative) {
          nativeSession.update();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
