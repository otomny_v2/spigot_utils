package net.spigotutils.nbt;

import java.util.function.BiConsumer;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import net.spigotutils.item.ItemStackBuilder;

public class AreaSelector implements Listener{
	
	public static AreaSelectorBuilder Builder(Plugin plugin) {
		return new AreaSelectorBuilder(plugin);
	}

	private static final ItemStack WAND = new ItemStackBuilder(Material.GOLDEN_HOE)
			.withDisplayName("§eSelection")
			.description(
					"",
					"§7Clique-droit pour la §6première sélèction",
					"§7Clique-gauche pour la §6deuxième sélèction",
					"§7Drop pour §cannuler",
					"§7Sneak-clique pour §avalider",
					"")
			.get();
	private static final String XYZ_FORMAT = ChatColor.GREEN + "Position n°%numeroSelection% => X: %x%, Y: %y%, Z: %z%";

	private Location start;
	private Location end;
	private Plugin plugin;
	private Runnable onEnd;
	private BiConsumer<Location, Location> onComplete;
	private Player player;

	public AreaSelector(Plugin plugin, Runnable onEnd, BiConsumer<Location, Location> onComplete, Player player) {
		super();
		this.plugin = plugin;
		this.onEnd = onEnd;
		this.onComplete = onComplete;
		this.player = player;
		setup();
	}

	public void end() {
		this.onEnd.run();
		clear();
	}

	public void complete() {
		this.onComplete.accept(this.start, this.end);
		clear();
	}

	public void setup() {
		this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
		this.player.getInventory().addItem(WAND.clone());
	}

	public void clear() {
		HandlerList.unregisterAll(this);
		this.player.getInventory().remove(WAND.getType());
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		if (event.getPlayer() == this.player && event.getItemDrop().getItemStack().isSimilar(WAND)) {
			this.end();
			event.getItemDrop().remove();
		}
	}

	@EventHandler
	public void onBlockClick(PlayerInteractEvent event) {
		if (event.getPlayer() == this.player) {
			if (event.getItem() != null && event.getPlayer().isSneaking()) {
				if(this.start != null && this.end != null) {
					this.complete();
				}
				return;
			}
			if (event.getItem() != null && event.getClickedBlock() != null) {
				if (event.getItem().equals(WAND)) {
					Location location = event.getClickedBlock().getLocation();
					String newFormat = XYZ_FORMAT
							.replaceAll("%x%", String.valueOf(location.getBlockX()))
							.replaceAll("%y%", String.valueOf(location.getBlockY()))
							.replaceAll("%z%", String.valueOf(location.getBlockZ()));

					// On vérifie quel clique il a fait
					if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
						newFormat = newFormat.replaceAll("%numeroSelection%", String.valueOf(1));
						this.start = location;
					} else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
						newFormat = newFormat.replaceAll("%numeroSelection%", String.valueOf(2));
						this.end = location;
					}
					event.setCancelled(true);
					this.player.sendMessage(newFormat);
					return;
				}
			}
		}
	}

	public static class AreaSelectorBuilder {

		private Plugin plugin;
		private Runnable onEnd;
		private BiConsumer<Location, Location> onComplete;

		public AreaSelectorBuilder(Plugin plugin) {
			this.plugin = plugin;
		}

		public AreaSelectorBuilder end(Runnable onEnd) {
			this.onEnd = onEnd;
			return this;
		}

		public AreaSelectorBuilder complete(BiConsumer<Location, Location> onComplete) {
			this.onComplete = onComplete;
			return this;
		}

		public AreaSelector build(Player player) {
			return new AreaSelector(this.plugin, this.onEnd, this.onComplete, player);
		}

	}


}
