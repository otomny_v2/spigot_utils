package net.spigotutils.nbt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.Document;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

import lombok.Getter;
import lombok.Setter;
import net.mongoutils.Adaptator;
import net.mongoutils.MongoUtils;
import net.spigotutils.location.LocationUtils;
import net.spigotutils.nbt.jnbt.CompoundTag;
import net.spigotutils.nbt.jnbt.IntTag;
import net.spigotutils.nbt.jnbt.NBTInputStream;
import net.spigotutils.nbt.jnbt.NBTOutputStream;
import net.spigotutils.nbt.jnbt.ShortTag;
import net.spigotutils.nbt.jnbt.StringArrayTag;
import net.spigotutils.nbt.jnbt.StringTag;
import net.spigotutils.nbt.jnbt.Tag;


public class Schematic implements Adaptator<Schematic> {

  public static final String DB_COLLECTION = "schematics";
  public static final String DEFAULT_NAMESPACE = "default";

  /**
   * Create a schematic from ingame location
   * @param offset Offset
   * @param start Start location
   * @param end End location
   * @param dataName Data name
   * @return The schematic
   */
  public static Schematic createSchematic(Location offset, Location start,
                                          Location end, String dataName) {
    return createSchematic(offset, start, end, dataName, DEFAULT_NAMESPACE);
  }


  /**
   * Create a schematic from ingame location
   * @param offset Offset
   * @param start Start location
   * @param end End location
   * @param dataName Data name
   * @param namespace Namespace
   * @return
   */
  public static Schematic createSchematic(Location offset, Location start,
                                          Location end, String dataName,
                                          String namespace) {

    int minX = Math.min(start.getBlockX(), end.getBlockX());
    int minY = Math.min(start.getBlockY(), end.getBlockY());
    int minZ = Math.min(start.getBlockZ(), end.getBlockZ());
    int maxX = Math.max(start.getBlockX(), end.getBlockX());
    int maxY = Math.max(start.getBlockY(), end.getBlockY());
    int maxZ = Math.max(start.getBlockZ(), end.getBlockZ());

    Location newStart = new Location(start.getWorld(), minX, minY, minZ);
    Location newEnd = new Location(end.getWorld(), maxX, maxY, maxZ);

    int lenght = (short)((maxX - minX) + 1);
    int height = (short)((maxY - minY) + 1);
    int width = (short)((maxZ - minZ) + 1);

    String[] blocks = new String[lenght * width * height];
    Arrays.fill(blocks, "minecraft:air");

    Area area = new Area(newStart, newEnd);
    area.blockDatasSnapshot(true).parallelStream().forEach(blockData -> {
      var pos = blockData.getBlockPosition();
      int relativeX = (pos.get1() - minX);
      int relativeY = (pos.get2() - minY);
      int relativeZ = (pos.get3() - minZ);
      int index = relativeY * width * lenght + relativeX * width + relativeZ;
      blocks[index] = blockData.getBlockData().getAsString();
    });

    // Calcul du offset :
    // Location du joueur - Location du min (donc du start)

    Location realOffset = offset.clone().subtract(
        new Location(start.getWorld(), minX, minY, minZ));

    Schematic schems =
        new Schematic(blocks, width, lenght, height, realOffset, dataName);
    schems.namespace = namespace;
    return schems;
  }

  /**
   * Retourne la liste des schematics
   *
   * @author ComminQ_Q (Computer)
   *
   * @param plugin
   * @return
   * @date 28/10/2020
   */
  public static Map<String, List<String>> listSchematicDBSorted(Plugin p) {
    Map<String, List<String>> ids = new HashMap<>();
    MongoUtils.getClient()
        .getDatabase(System.getProperty("dbName"))
        .getCollection(DB_COLLECTION)
        .find()
        .projection(Projections.fields(Projections.include("_id", "namespace")))
        .forEach(d -> {
          String namespace = d.get("namespace", Schematic.DEFAULT_NAMESPACE);
          if (ids.containsKey(namespace)) {
            ids.get(namespace).add(d.getString("_id"));
          } else {
            List<String> idsList = new ArrayList<>();
            idsList.add(d.getString("_id"));
            ids.put(namespace, idsList);
          }
        });
    return ids;
  }

  /**
   * Retourne la liste des schematics
   *
   * @author ComminQ_Q (Computer)
   *
   * @param plugin
   * @return
   * @date 28/10/2020
   */
  public static List<String> listSchematicDB(Plugin p) {
    return MongoUtils.loadIdFrom(System.getProperty("dbName"), DB_COLLECTION);
  }

  public static List<String> listSchematic(Plugin plugin) {
    File dataFolder = plugin.getDataFolder();
    if (!dataFolder.exists()) {
      return new ArrayList<String>();
    }
    File schems = new File(dataFolder, "schems");
    if (!schems.exists()) {
      schems.mkdir();
      return new ArrayList<String>();
    }
    return Arrays.asList(schems.list())
        .stream()
        .filter(fileName -> fileName.endsWith(".schem"))
        .collect(Collectors.toList());
  }

  public static Schematic fromDocument(Document document) {
    return new Schematic(document);
  }

  public static boolean saveSchematic(Plugin plugin, Schematic schems,
                                      String name) throws IOException {
    File dataFolder = plugin.getDataFolder();
    schems.dataName = name;
    File schemFolder = new File(dataFolder.getAbsolutePath() + "/schems");
    if (!schemFolder.exists()) {
      schemFolder.mkdir();
    }
    File file = new File(schemFolder.getAbsolutePath() + "/" +
                         String.format("%s.schem", name));
    if (file.exists()) {
      throw new IOException("File already exist");
    }
    file.createNewFile();
    FileOutputStream stream = new FileOutputStream(file);
    NBTOutputStream nbtStream = new NBTOutputStream(stream);

    Map<String, Tag> newBlocksValue = new HashMap<String, Tag>();

    IntTag width = new IntTag("Width", schems.width);
    IntTag length = new IntTag("Length", schems.length);
    IntTag height = new IntTag("Height", schems.height);

    newBlocksValue.put(width.getName(), width);
    newBlocksValue.put(length.getName(), length);
    newBlocksValue.put(height.getName(), height);

    IntTag offsetX = new IntTag("WEOffsetX", schems.offset.getBlockX());
    IntTag offsetY = new IntTag("WEOffsetY", schems.offset.getBlockY());
    IntTag offsetZ = new IntTag("WEOffsetZ", schems.offset.getBlockZ());

    newBlocksValue.put(offsetX.getName(), offsetX);
    newBlocksValue.put(offsetY.getName(), offsetY);
    newBlocksValue.put(offsetZ.getName(), offsetZ);

    List<String> materials = Arrays.asList(schems.blocks);

    StringArrayTag array = new StringArrayTag(
        "Blocks", materials.toArray(new String[materials.size()]));

    StringTag dataName = new StringTag("Name", name);

    newBlocksValue.put(array.getName(), array);
    newBlocksValue.put("Name", dataName);

    CompoundTag schemTag = new CompoundTag("OSchematic", newBlocksValue);

    nbtStream.writeTag(schemTag);
    nbtStream.close();
    return true;
  }

  /**
   * Charge un schematic depuis la bdd
   *
   * @author ComminQ_Q (Computer)
   *
   * @param schematicId
   * @return
   * @date 28/10/2020
   */
  public static Schematic loadDbSchematic(String schematicId) {
    Document document = MongoUtils.loadRaw(System.getProperty("dbName"),
                                           DB_COLLECTION, schematicId);
    return document == null ? null : fromDocument(document);
  }

  /**
   * Met à jour les schematics qui ne sont pas mis à jours car la première
   * conception prend trop de place
   *
   * @author Fabien CAYRE (Computer)
   *
   * @date 18/02/2021
   */
  public static void updateAllSchematic() {
    List<Document> documents = MongoUtils.loadRaws(
        System.getProperty("dbName"), DB_COLLECTION, Filters.lt("version", 4),
        Projections.include("_id", "version", "blocks", "length", "width",
                            "height"));

    System.out.println("[UPDATE SCHEMATIC] Updating " + documents.size() +
                       " schematics...");

    for (Document doc : documents) {
      String identifier = doc.getString("_id");
      int version = doc.getInteger("version", 1);
      System.out.println("[UPDATE SCHEMATIC] "
                         + "\t"
                         + "- Updating " + identifier + "(version " + version +
                         ", to version 3) ...");
      if (version == 1) {
        List<String> blocs = doc.getList("blocks", String.class);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updating " + blocs.size() + " blocs...");
        int width = doc.getInteger("width", -1);
        int length = doc.getInteger("length", -1);
        int height = doc.getInteger("height", -1);

        List<Document> compressed =
            compressSchematicData(blocs.toArray(new String[blocs.size()]),
                                  width, length, height, false);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updated to around " + compressed.size() +
                           " composed data");

        MongoUtils.updatePart(
            System.getProperty("dbName"), DB_COLLECTION, identifier,
            new Document("$set", new Document("blocks", compressed)
                                     .append("version", version + 1)));
      } else if (version == 2) {
        int width = doc.getInteger("width", -1);
        int length = doc.getInteger("length", -1);
        int height = doc.getInteger("height", -1);

        List<Document> blockDatas = doc.getList("blocks", Document.class);
        String[] blocks =
            uncompressSchematicData(blockDatas, width * length * height, false);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updating " + blockDatas.size() +
                           " composed data...");
        List<Document> compressed =
            compressSchematicData(blocks, width, length, height, true);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updated to around " + compressed.size() +
                           " composed data");
        MongoUtils.updatePart(
            System.getProperty("dbName"), DB_COLLECTION, identifier,
            new Document("$set", new Document("blocks", compressed)
                                     .append("version", version + 1)));
      } else if (version == 3) {
        int width = doc.getInteger("width", -1);
        int length = doc.getInteger("length", -1);
        int height = doc.getInteger("height", -1);

        List<Document> blockDatas = doc.getList("blocks", Document.class);
        String[] blocks =
            uncompressSchematicData(blockDatas, width * length * height, true);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updating " + blockDatas.size() +
                           " composed data...");
        List<Document> compressed =
            compressSchematicData(blocks, width, length, height, false);
        System.out.println("[UPDATE SCHEMATIC] "
                           + "\t"
                           + "- Updated to around " + compressed.size() +
                           " composed data");
        MongoUtils.updatePart(
            System.getProperty("dbName"), DB_COLLECTION, identifier,
            new Document("$set", new Document("blocks", compressed)
                                     .append("version", version + 1)));
      }
    }
  }

  public static Schematic loadSchematic(Plugin plugin, String fileName)
      throws IOException {
    File dataFolder = plugin.getDataFolder();
    File schemFolder = new File(dataFolder.getAbsolutePath() + "/schems");
    if (!schemFolder.exists()) {
      schemFolder.mkdir();
      throw new IllegalArgumentException(
          "Schematic folder doesn't exist, creating...");
    }
    if (!fileName.endsWith(".schem")) {
      fileName += ".schem";
    }
    File file = new File(schemFolder, fileName);
    if (!file.exists()) {
      throw new IllegalArgumentException("Schematic fileName doesn't exist");
    }
    FileInputStream stream = new FileInputStream(file);
    NBTInputStream nbtStream = new NBTInputStream(stream);

    CompoundTag schematicTag = (CompoundTag)nbtStream.readTag();
    if (!schematicTag.getName().equals("OSchematic")) {
      nbtStream.close();
      throw new IllegalArgumentException(
          "Tag \"Schematic\" does not exist or is not first");
    }

    Map<String, Tag> schematic = schematicTag.getValue();
    if (!schematic.containsKey("Blocks")) {
      nbtStream.close();
      throw new IllegalArgumentException(
          "Schematic file is missing a \"Blocks\" tag");
    }

    short width = getChildTag(schematic, "Width", ShortTag.class).getValue();
    short length = getChildTag(schematic, "Length", ShortTag.class).getValue();
    short height = getChildTag(schematic, "Height", ShortTag.class).getValue();

    String schematicName =
        getChildTag(schematic, "Name", StringTag.class).getValue();

    String[] blockMaterial =
        getChildTag(schematic, "Blocks", StringArrayTag.class).getValue();
    for (int i = 0; i < blockMaterial.length; i++) {
      if (!blockMaterial[i].contains("minecraft:")) {
        blockMaterial[i] = "minecraft:" + blockMaterial[i].toLowerCase();
      }
    }

    int offsetX = getChildTag(schematic, "WEOffsetX", IntTag.class).getValue();
    int offsetY = getChildTag(schematic, "WEOffsetY", IntTag.class).getValue();
    int offsetZ = getChildTag(schematic, "WEOffsetZ", IntTag.class).getValue();

    Location relative = new Location(null, offsetX, offsetY, offsetZ);
    Schematic schem = new Schematic(blockMaterial, width, length, height,
                                    relative, schematicName);
    nbtStream.close();
    System.out.println("Loaded schematic : " + schem);
    return schem;
  }

  private static <T extends Tag> T getChildTag(Map<String, Tag> items,
                                               String key, Class<T> expected)
      throws IllegalArgumentException {
    if (!items.containsKey(key)) {
      throw new IllegalArgumentException("Schematic file is missing a \"" +
                                         key + "\" tag");
    }
    Tag tag = items.get(key);
    if (!expected.isInstance(tag)) {
      throw new IllegalArgumentException(key + " tag is not of tag type " +
                                         expected.getName());
    }
    return expected.cast(tag);
  }

  private static String[] uncompressSchematicData(List<Document> documents,
                                                  int size, boolean skipAir) {
    String[] res = new String[size];
    Arrays.fill(res, "minecraft:air");
    int index = 0;
    for (Document document : documents) {
      String type = document.getString("type");
      if (document.containsKey("index")) {
        index = document.getInteger("index");
      }
      for (int i = 0; i < document.getInteger("count"); i++) {
        res[index] = type;
        index++;
      }
    }
    return res;
  }

  private static List<Document> compressSchematicData(String[] blocks,
                                                      int width, int length,
                                                      int height,
                                                      boolean skipAir) {
    LinkedList<Document> res = new LinkedList<>();
    boolean createForNext = false;
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < length; x++) {
        for (int z = 0; z < width; z++) {
          int index = y * width * length + x * width + z;

          if (index > 0) {
            Document document = res.peekLast();
            if (skipAir && document != null &&
                document.getString("type").equalsIgnoreCase("minecraft:air")) {
              createForNext = true;
              continue;
            }
            if (document != null &&
                document.getString("type").equals(blocks[index])) {
              // Les blocs précèdent étaient de type air, on doit garder la
              // trace de l'index à mettre, car sinon le vide entre ces blocs de
              // sera pas respecté
              if (skipAir && createForNext) {
                document = new Document()
                               .append("type", blocks[index])
                               .append("count", 1)
                               .append("index", index);
                res.add(document);
                // On reset le drapeau, il n'y a plus d'espace à respecter
                createForNext = false;
              } else {
                // on augmente le nombre de bloc de cette tranche & de ce type
                document.put("count", document.getInteger("count") + 1);
              }
            } else {
              // Si le bloc est de type "AIR" et qu'on skip l'air alors on
              // continue vers la case suivante
              if (skipAir && blocks[index].equalsIgnoreCase("minecraft:air")) {
                createForNext = true;
                continue;
              }
              document = new Document()
                             .append("type", blocks[index])
                             .append("count", 1);
              // Si on est en mode skipAir, il faut une trace de l'index
              if (skipAir) {
                document.append("index", index);
              }
              res.add(document);
              createForNext = false;
            }
          } else {
            // Si on skip air ET que le materiel est de l'air alors on fait rien
            // Non (skip air ET material == air)
            if (!(skipAir && blocks[index].equalsIgnoreCase("minecraft:air"))) {
              Document data = new Document()
                                  .append("type", blocks[index])
                                  .append("count", 1)
                                  .append("index", index);
              res.add(data);
            }
          }
        }
      }
    }

    return res;
  }

  @Getter protected String dataName;
  protected String[] blocks;
  protected int width;
  protected int length;
  protected int height;

  @Getter @Setter private int version;
  @Getter @Setter protected Location offset;
  @Getter @Setter protected String namespace;

  protected Schematic(Document document) {
    this.width = document.getInteger("width");
    this.length = document.getInteger("length");
    this.height = document.getInteger("height");
    this.offset = LocationUtils.fromDocument((Document)document.get("offset"));
    this.dataName = document.getString("data_name");
    this.version = document.getInteger("version", 1);
    if (document.containsKey("blocks")) {
      if (this.version == 1) {
        List<String> blockDatas = document.getList("blocks", String.class);
        for (int i = 0; i < blockDatas.size(); i++) {
          String string = blockDatas.get(i);
          if (!string.contains("minecraft:")) {
            blockDatas.set(i, "minecraft:" + string.toLowerCase());
          }
        }
        this.blocks = blockDatas.toArray(new String[blockDatas.size()]);
        this.version = 3;
      } else if (this.version == 2) {
        List<Document> blockDatas = document.getList("blocks", Document.class);
        this.blocks = uncompressSchematicData(
            blockDatas, this.width * this.length * this.height, false);
        this.version = 3;
      } else if (this.version == 3) {
        List<Document> blockDatas = document.getList("blocks", Document.class);
        this.blocks = uncompressSchematicData(
            blockDatas, this.width * this.length * this.height, true);
        this.version = 4;
      } else {
        List<Document> blockDatas = document.getList("blocks", Document.class);
        this.blocks = uncompressSchematicData(
            blockDatas, this.width * this.length * this.height, false);
      }
    }
    if (document.containsKey("namespace")) {
      this.namespace = document.getString("namespace");
    } else {
      this.namespace = Schematic.DEFAULT_NAMESPACE;
    }
  }

  private Schematic(String[] blocks, int width, int length, int height,
                    Location offset, String schematicName) {
    this.blocks = new String[blocks.length];
    for (int i = 0; i < blocks.length; i++) {
      this.blocks[i] = blocks[i];
    }
    this.width = width;
    this.length = length;
    this.height = height;
    this.offset = offset;
    this.dataName = schematicName;
    this.version = 4;
  }

  public void setDataName(String dataName) { this.dataName = dataName; }

  public Paste asPaste() { return new Paste(this); }

  public Document toDocument() {
    Document doc =
        new Document()
            .append("_id", this.dataName)
            .append("width", this.width)
            .append("length", this.length)
            .append("height", this.height)
            .append("offset", LocationUtils.toDocument(this.offset))
            .append("blocks",
                    compressSchematicData(this.blocks, this.width, this.length,
                                          this.height, false))
            .append("data_name", this.dataName)
            .append("version", this.version);

    if (this.namespace != null) {
      doc.append("namespace", this.namespace);
    }

    return doc;
  }

  @Override
  public Document adapt(Schematic schematic) {
    return schematic.toDocument();
  }

  @Override
  public String getUniqueID(Schematic schematic) {
    return schematic.dataName;
  }
}
