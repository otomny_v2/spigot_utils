package net.spigotutils.nbt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.block.data.CraftBlockData;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_19_R1.util.CraftMagicNumbers;
import org.bukkit.entity.Player;

import net.api.process.TaskDispatcher;
import net.minecraft.core.BlockPos;
import net.minecraft.network.protocol.game.ClientboundForgetLevelChunkPacket;
import net.minecraft.network.protocol.game.ClientboundLevelChunkWithLightPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ThreadedLevelLightEngine;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.spigotutils.nms.PacketUtils;

public class EditSession {

  private final org.bukkit.World bukkitWorld;
  private final ServerLevel world;
  private final Map<BlockPos, BlockState> modified = new HashMap<>();

  public EditSession(org.bukkit.World bukkitWorld) {
    this.bukkitWorld = bukkitWorld;
    this.world = ((CraftWorld)bukkitWorld).getHandle();
  }

  public void setBlock(int x, int y, int z, Material material) {
    modified.put(new BlockPos(x, y, z),
                 CraftMagicNumbers.getBlock(material).defaultBlockState());
  }

  public void setBlock(BlockData blockData) {
    modified.put(blockData.getBlockPos(),
                 ((CraftBlockData)blockData.getBlockData()).getState());
  }

  public void update() {

    // modify blocks
    Set<LevelChunk> chunks = new HashSet<>();
    for (Map.Entry<BlockPos, BlockState> entry : modified.entrySet()) {
      LevelChunk chunk = world.getChunkSource().getChunk(
          entry.getKey().getX() >> 4, entry.getKey().getZ() >> 4, true);
      chunks.add(chunk);
      chunk.setBlockState(entry.getKey(), entry.getValue(), false);
    }

    // update lights
    ThreadedLevelLightEngine engine = world.getChunkSource().getLightEngine();
    for (BlockPos pos : modified.keySet()) {
      engine.checkBlock(pos);
    }

    // unload & load chunk data
    TaskDispatcher.run(() -> {
      for (LevelChunk chunk : chunks) {
        ClientboundForgetLevelChunkPacket unload =
            new ClientboundForgetLevelChunkPacket(chunk.getPos().x,
                                                  chunk.getPos().z);

        ClientboundLevelChunkWithLightPacket lightAndLoad =
            new ClientboundLevelChunkWithLightPacket(chunk, engine, null, null,
                                                     true);

        for (Player p : Bukkit.getOnlinePlayers()) {
          if (!p.getWorld().equals(this.bukkitWorld))
            continue;

          ServerPlayer ep = ((CraftPlayer)p).getHandle();
          int dist = Bukkit.getViewDistance() + 1;
          int chunkX = ep.chunkPosition().x;
          int chunkZ = ep.chunkPosition().z;
          if (chunk.getPos().x < chunkX - dist ||
              chunk.getPos().x > chunkX + dist ||
              chunk.getPos().z < chunkZ - dist ||
              chunk.getPos().z > chunkZ + dist)
            continue;
          PacketUtils.sendPacket(p, unload);
          PacketUtils.sendPacket(p, lightAndLoad);
        }
      }
    });

    // clear modified blocks
    modified.clear();
  }
}
