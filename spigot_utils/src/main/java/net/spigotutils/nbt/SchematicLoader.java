package net.spigotutils.nbt;

import org.bson.Document;

import net.mongoutils.Loader;

public class SchematicLoader implements Loader<Schematic> {

	@Override
	public Class<Schematic> getAnalyzedClass() {
		return Schematic.class;
	}

	@Override
	public Schematic loadFrom(Document dbObject) {
		return new Schematic(dbObject);
	}

}
