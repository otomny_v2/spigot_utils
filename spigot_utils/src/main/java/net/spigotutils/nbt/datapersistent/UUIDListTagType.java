package net.spigotutils.nbt.datapersistent;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;

public class UUIDListTagType
		implements PersistentDataType<byte[], UUID[]> {

	public static final UUIDListTagType INSTANCE = new UUIDListTagType();

	private UUIDListTagType() {
	}

	@Override
	public Class<byte[]> getPrimitiveType() {
		return byte[].class;
	}

	@Override
	public UUID[] fromPrimitive(byte[] input,
			PersistentDataAdapterContext ctx) {
		ByteBuffer bb = ByteBuffer.wrap(input);
		int length = bb.getInt();
		var array = new UUID[length];
		for(int i = 0; i < length; i++){
			long most = bb.getLong();
			long least = bb.getLong();
			array[i] = new UUID(most, least);
		}
		return array;
	}

	@Override
	public Class<UUID[]> getComplexType() {
		return UUID[].class;
	}

	@Override
	public byte[] toPrimitive(UUID[] input,
			PersistentDataAdapterContext ctx) {
		int uuidByteLength = 16;
		int length = input.length;
		int intLength = 4;
		ByteBuffer bb = ByteBuffer
				.wrap(new byte[(uuidByteLength * length) + intLength]);
		bb.putInt(length);
		for (int i = 0; i < length; i++) {
			UUID complex = input[i];
			bb.putLong(complex.getMostSignificantBits());
			bb.putLong(complex.getLeastSignificantBits());
		}
		return bb.array();
	}

}
