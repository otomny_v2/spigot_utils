package net.spigotutils.item;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackAdaptor {
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param itemStack L'item à transformé en Document
	 * @return
	 * @date 02/05/2020
	 */
	public static Document getDocument(ItemStack itemStack) {
		Document document = new Document();
		// Les infos générales
		document.append("material", itemStack.getType().toString());
		document.append("amount", itemStack.getAmount());
		// Les méta
		if(itemStack.hasItemMeta()) {
			Document meta = new Document();
			ItemMeta itemMeta = itemStack.getItemMeta();
			// Serialisation des enchantements
			if(itemMeta.hasEnchants()) {
				List<Document> docList = new ArrayList<>();
				for(Enchantment enchant : itemMeta.getEnchants().keySet()) {
					int level = itemMeta.getEnchantLevel(enchant);
					Document enchantment = new Document();
					enchantment.append("enchantName", enchant.getKey().getNamespace()+":"+enchant.getKey().getKey());
					enchantment.append("level", level);
					docList.add(enchantment);
				}
				meta.append("enchantments", docList);
			}
			// Les lores
			if(itemMeta.hasLore()) {
				List<String> lores = itemMeta.getLore();
				meta.append("lores", lores);
			}
			// L'affichage custom
			if(itemMeta.hasDisplayName()) {
				meta.append("customName", itemMeta.getDisplayName());
			}
			// Ajout de la meta
			document.append("meta", meta);
		}
		return document;
	}

	public static ItemStack getItemStack(Document document) {
		ItemStack item = new ItemStack(Material.STONE);
		// Les infos principales
		Material material = Material.valueOf(document.getString("material"));
		int amount = document.getInteger("amount", 1);
		
		ItemMeta itemMeta = item.getItemMeta();
		
		if(document.containsKey("meta")) {
			Document meta = document.get("meta", Document.class);
			if(meta.containsKey("enchantments")) {
				List<Document> enchants = meta.getList("enchantments", Document.class);
				for(Document enchant : enchants) {
					int level = enchant.getInteger("level", 1);
					String enchantKey = enchant.getString("enchantName");
					
					String namespace = enchantKey.split("\\:")[0];
					String key = enchantKey.split("\\:")[1];
					
					Enchantment enchantment = Enchantment.getByKey(new NamespacedKey(namespace, key));
					itemMeta.addEnchant(enchantment, level, true);
				}
			}
			if(meta.containsKey("lores")) {
				List<String> lores = meta.getList("lores", String.class);
				itemMeta.setLore(lores);
			}
			if(meta.containsKey("customName")) {
				String customName = meta.getString("customName");
				itemMeta.setDisplayName(customName);
			}
		}
		
		item.setItemMeta(itemMeta);
		
		// Affectations
		item.setType(material);
		item.setAmount(amount);
		return item;
	}

}
