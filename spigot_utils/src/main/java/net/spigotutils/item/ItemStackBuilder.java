package net.spigotutils.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

/**
 * Classe wrapper pour creer des item stack plus rapidement
 * 
 * @author ComminQ_Q (Computer)
 *
 *         2019-10-02
 */
public class ItemStackBuilder {

	public static ItemStack of(Material material){
		return new ItemStack(material);
	}

	private ItemStack itemStack;

	public ItemStackBuilder(Material material) {
		this.itemStack = new ItemStack(material);
	}

	public ItemStackBuilder(Material material, int amount) {
		this.itemStack = new ItemStack(material, amount);
	}

	public ItemStackBuilder(ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	
	/**
	 * Cache les attributs de l'item
	 * @author ComminQ_Q (Computer)
	 *
	 * @param hide
	 * @return
	 * @date 06/10/2019
	 */
	public ItemStackBuilder setHideFlag(boolean hide) {
		ItemMeta meta = this.itemStack.getItemMeta();
		if (meta == null) return this;
		if (hide) {
			meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
			meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
			meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			this.itemStack.setItemMeta(meta);
		}else {
			meta.removeItemFlags(ItemFlag.HIDE_ATTRIBUTES,
								 ItemFlag.HIDE_DESTROYS,
								 ItemFlag.HIDE_PLACED_ON,
								 ItemFlag.HIDE_ENCHANTS,
								 ItemFlag.HIDE_POTION_EFFECTS);
			this.itemStack.setItemMeta(meta);
		}
		return this;
	}
	
	/**
	 * Ajoute un attribut sur l'item courrant
	 * @author fabien
	 * @date 20/10/2019
	 * @param attribute
	 * @param modifier
	 * @return
	 */
	public ItemStackBuilder setAttribute(Attribute attribute, AttributeModifier modifier) {
		ItemMeta meta = this.itemStack.getItemMeta();
		if (meta == null) return this;
		meta.addAttributeModifier(attribute, modifier);
		this.itemStack.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Permet à l'item de s'afficher en surbrillance <br>
	 * (Comme du shiny sur pokemon mais là c'est dans Minecraft)
	 * @author ComminQ_Q (Computer)
	 *
	 * @param glow
	 * @return
	 * @date 06/10/2019
	 */
	public ItemStackBuilder withGlow(boolean glow) {
		return setHideFlag(true).withEnchantment(Enchantment.ARROW_DAMAGE, 0);
	}

	/**
	 * Ajoute un nom a l'item
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param displayName
	 * @return 2019-10-02
	 */
	public ItemStackBuilder withDisplayName(String displayName) {
		ItemMeta meta = this.itemStack.getItemMeta();
		meta.setDisplayName(displayName);
		this.itemStack.setItemMeta(meta);
		return this;
	}

	/**
	 * Met � jour le nombre d'item dans le stack
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param amount
	 * @return 2019-10-02
	 */
	public ItemStackBuilder withAmount(int amount) {
		this.itemStack.setAmount(amount);
		return this;
	}

	/**
	 * Ajout d'un enchantement sur un item
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param enchantment
	 * @param level
	 * @return 2019-10-02
	 */
	public ItemStackBuilder withEnchantment(Enchantment enchantment, int level) {
		this.itemStack.addUnsafeEnchantment(enchantment, level);
		return this;
	}

	/**
	 * Ajout d'une description
	 * @author ComminQ_Q (Computer)
	 *
	 * @param strings La description
	 * @return le builder
	 * @date 13/06/2020
	 */
	public ItemStackBuilder description(List<String> strings) {
		ItemMeta meta = this.itemStack.getItemMeta();

		List<String> lores = new ArrayList<>();
		for(String string : strings)lores.add(string);
		
		if (!meta.hasLore()) {
			meta.setLore(lores);
		} else {
			meta.getLore().addAll(lores);
		}
		this.itemStack.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Ajout d'une description
	 * @author ComminQ_Q (Computer)
	 *
	 * @param strings La description
	 * @return le builder
	 * @date 13/06/2020
	 */
	public ItemStackBuilder description(String... strings) {
		return description(Arrays.asList(strings));
	}
	
	/**
	 * Ajout d'une description
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param lores
	 * @return 2019-10-02
	 */
	@Deprecated
	public ItemStackBuilder withLore(List<String> lores) {
		ItemMeta meta = this.itemStack.getItemMeta();
		if (meta.getLore() == null) {
			meta.setLore(lores);
		} else {
			meta.getLore().addAll(lores);
		}
		this.itemStack.setItemMeta(meta);
		return this;
	}

	/**
	 * Ajoute un enchantement sur un livre si et seulement si le type de l'item est
	 * un livre
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param enchantment
	 * @param level
	 * @return 2019-10-02
	 */
	public ItemStackBuilder withBookEnchantment(Enchantment enchantment, int level) {
		if (this.itemStack.getType() == Material.ENCHANTED_BOOK) {
			EnchantmentStorageMeta meta = (EnchantmentStorageMeta) this.itemStack.getItemMeta();
			if (meta.hasStoredEnchant(enchantment)) {
				meta.removeStoredEnchant(enchantment);
			}
			meta.addStoredEnchant(enchantment, level, true);
		}
		return this;
	}

	/**
	 * Ajoute un effect de potion
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param enchantment
	 * @param level
	 * @return 2019-10-02
	 */
	public ItemStackBuilder withPotionType(PotionType type, boolean upgrade, boolean elongated) {
		if (this.itemStack.getType().toString().contains("POTION")) {
			PotionMeta meta = (PotionMeta) this.itemStack.getItemMeta();
			meta.setBasePotionData(new PotionData(type, elongated, upgrade));
			this.itemStack.setItemMeta(meta);
		}
		return this;
	}

	/**
	 * Renvoie l'item con�u avec les m�thodes pr�c�dent
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return 2019-10-02
	 */
	public ItemStack get() {
		return this.itemStack;
	}

}
