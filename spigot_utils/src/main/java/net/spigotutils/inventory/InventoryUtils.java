package net.spigotutils.inventory;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.Damageable;

/**
 * Classe wrapper avec des méthodes sympa pour les inventaires
 *
 * @author ComminQ_Q (Computer)
 *
 * @date 11/05/2020
 */
public class InventoryUtils {

  private static final InventoryUtils inv = new InventoryUtils(null);

  /**
   * Retourne le nombre d'item du même type
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param inventory
   * @param material
   * @return
   * @date 15/06/2021
   */
  public static int count(Inventory inventory, Material material) {
    inv.inventory = inventory;
    return inv.itemCount(material);
  }

  /**
   * Vérifie si l'inventaire du joueur est pleins avant de lui give l'item
   * Si l'inventaire est plein, l'item apparaît par terre devant le joueur
   * @param player Le joueur
   * @param itemStack L'item à donner
   */
  public static void safeGive(Player player, ItemStack itemStack) {
    if (hasPlace(player.getInventory(), itemStack)) {
      player.getInventory().addItem(itemStack);
    } else {
      player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
      player.sendMessage(
          "§cIl n'y avait pas assez de place dans votre inventaire");
    }
  }

  public static boolean hasPlace(Inventory inventory, Material material) {
    inv.inventory = inventory;
    return inv.hasPlace(material);
  }

  public static boolean hasPlace(PlayerInventory inventory,
                                 ItemStack itemStack) {
    int tempAmount = itemStack.getAmount();
    int maxStack = itemStack.getType().getMaxStackSize();
    var itemMeta = itemStack.getItemMeta();
    for (int i = 0; i < 36; i++) {
      var item = inventory.getItem(i);
      if (item == null) {
        tempAmount -= maxStack;
        continue;
      }
      if (item.getType() != itemStack.getType())
        continue;
      var itemAmount = item.getAmount();
      if (itemStack.getAmount() >= maxStack)
        continue;
      var meta = item.getItemMeta();
      if (!meta.getDisplayName().equals(itemMeta.getDisplayName()))
        continue;
      int canAddOnThisItem = maxStack - itemAmount;
      tempAmount -= canAddOnThisItem;
      if (tempAmount <= 0)
        break;
    }
    return tempAmount <= 0;
  }

  public static boolean hasPlace(Inventory inventory, ItemStack itemStack) {
    int tempAmount = itemStack.getAmount();
    int maxStack = itemStack.getType().getMaxStackSize();
    var itemMeta = itemStack.getItemMeta();
    for (int i = 0; i < inventory.getSize(); i++) {
      var item = inventory.getItem(i);
      if (item == null) {
        tempAmount -= maxStack;
        continue;
      }
      if (item.getType() != itemStack.getType())
        continue;
      var itemAmount = item.getAmount();
      if (itemStack.getAmount() >= maxStack)
        continue;
      var meta = item.getItemMeta();
      if (!meta.getDisplayName().equals(itemMeta.getDisplayName()))
        continue;
      int canAddOnThisItem = maxStack - itemAmount;
      tempAmount -= canAddOnThisItem;
      if (tempAmount <= 0)
        break;
    }
    return tempAmount <= 0;
  }

  public static boolean hasPlace(PlayerInventory inventory, Material material,
                                 int amount) {
    int tempAmount = amount;
    int maxStack = material.getMaxStackSize();
    for (int i = 0; i < 36; i++) {
      var item = inventory.getItem(i);
      if (item == null) {
        tempAmount -= maxStack;
        continue;
      }
      if (item.getType() != material)
        continue;
      var itemAmount = item.getAmount();
      if (amount >= maxStack)
        continue;
      int canAddOnThisItem = maxStack - itemAmount;
      tempAmount -= canAddOnThisItem;
      if (tempAmount <= 0)
        break;
    }
    return tempAmount <= 0;
  }

  public static boolean hasPlace(Inventory inventory, Material material,
                                 int amount) {
    int tempAmount = amount;
    int maxStack = material.getMaxStackSize();
    for (int i = 0; i < inventory.getSize(); i++) {
      var item = inventory.getItem(i);
      if (item == null) {
        tempAmount -= maxStack;
        continue;
      }
      if (item.getType() != material)
        continue;
      var itemAmount = item.getAmount();
      if (amount >= maxStack)
        continue;
      int canAddOnThisItem = maxStack - itemAmount;
      tempAmount -= canAddOnThisItem;
      if (tempAmount <= 0)
        break;
    }
    return tempAmount <= 0;
  }

  private Inventory inventory;

  /**
   * Constructeur
   *
   * @author ComminQ_Q (Computer)
   *
   * @param inventory
   * @date 11/05/2020
   */
  public InventoryUtils(Inventory inventory) { this.inventory = inventory; }

  /**
   * Renvoie le nombre d'item du type Material
   *
   * @author ComminQ_Q (Computer)
   *
   * @param material
   * @return
   * @date 11/05/2020
   */
  public int itemCount(Material material) {
    return this.inventory.all(material)
        .values()
        .stream()
        .mapToInt(ItemStack::getAmount)
        .sum();
  }

  public void remove(Material material, int count, boolean onlyNoEnch,
                     boolean onlyFullRepair) {
    if (itemCount(material) < count)
      return;
    int deletedCount = 0;
    for (int slot = 0; slot < this.inventory.getSize(); slot++) {
      // Item à l'index slot
      ItemStack itemStack = this.inventory.getItem(slot);
      if (itemStack == null)
        continue;
      if (itemStack.getType() != material)
        continue;
      if (onlyNoEnch && !itemStack.getEnchantments().isEmpty())
        continue;
      // player.getItemInHand().getDurability() <=
      // player.getItemInHand().getType().getMaxDurability()
      Damageable damageable = (Damageable)itemStack.getItemMeta();

      if (onlyFullRepair && damageable.hasDamage() &&
          damageable.getDamage() < itemStack.getType().getMaxDurability()) {
        continue;
      }
      // Nombre d'item dans le stack
      int amount = itemStack.getAmount();
      // Nombre futur d'item supprimé
      int newDeletedCount = amount + deletedCount;
      if (newDeletedCount <= count) {
        this.inventory.clear(slot);
        // On ajoute le nombre d'item supprimé au nombre d'item supprimé total
        deletedCount = newDeletedCount;
        continue;
      } else {
        // Le nombre qui doit rester dans le stack
        int reste = newDeletedCount - count;
        itemStack.setAmount(reste);
        break;
      }
    }
  }

  /**
   * Supprime n item de type x
   *
   * @author ComminQ_Q (Computer)
   *
   * @param material Le type X de l'item à supprimer
   * @param count    Le nombre total de X à supprimer
   * @date 11/05/2020
   */
  public void remove(Material material, int count) {
    this.remove(material, count, true, true);
  }

  /**
   * Supprime tous les items de type X
   *
   * @author ComminQ_Q (Computer)
   *
   * @param material Le type X de l'item à supprimer
   * @date 11/05/2020
   */
  public void remove(Material material) {
    if (!hasItem(material))
      return;
    this.inventory.remove(material);
  }

  /**
   * Vérifie la disponibilité de l'inventaire du joueur selon un item X
   *
   * @author ComminQ_Q (Computer)
   *
   * @param material le type de l'item
   * @date 30/11/2021
   */
  public boolean hasPlace(Material material) {
    return this.inventory.firstEmpty() != -1 ||
        this.inventory.first(material) != -1;
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param material
   * @return
   * @date 11/05/2020
   */
  public boolean hasItem(Material material) {
    return this.inventory.first(material) != -1;
  }
}
