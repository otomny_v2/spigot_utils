package net.spigotutils.inventory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InventoryAdaptor{

	/**
	 * Transformer un inventaire en document capable d'être sauvegardé en base de données
	 * @author ComminQ_Q (Computer)
	 *
	 * @param inventory
	 * @return
	 * @date 13/10/2019
	 */
	public static Document adapt(Inventory inventory) {
		Document document = new Document();
		if(inventory instanceof PlayerInventory) {
			PlayerInventory playerInventory = (PlayerInventory) inventory;
			document.append("type", "player");
			// Objet dans l'armure
			Document armor = new Document();
			armor.append("helmet", playerInventory.getHelmet().serialize());
			armor.append("chestplate", playerInventory.getChestplate().serialize());
			armor.append("leggings", playerInventory.getLeggings().serialize());
			armor.append("boots", playerInventory.getBoots().serialize());
			
			// Objet dans les mains
			document.append("main-hand", playerInventory.getItemInMainHand().serialize());
			document.append("off-hand", playerInventory.getItemInOffHand().serialize());
			
			// Inventaire complet
			List<Document> items = Arrays.asList(playerInventory.getStorageContents())
											.stream()
											.map(itemStack -> new Document().append("item", itemStack.serialize()))
											.collect(Collectors.toList());
			
			document.append("item", items);
		}else {
			//TODO Faire le reste issouk ?
		}
		
		return document;
	}

	/**
	 * Charge un inventaire depuis un Document
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @param document
	 * @return
	 * @date 13/10/2019
	 */
	@SuppressWarnings("unchecked")
	public static Inventory loadFrom(@Nullable Player player, @Nonnull Document document) {
		Inventory inventory = null;
		if(document.getString("type").equals("player")) {
			// Requiert pour cete opération que le joueur ne soit pas null
			Objects.requireNonNull(player);
			
			// Chargement des données
			PlayerInventory playerInventory = player.getInventory();
			ItemStack helmet = ItemStack.deserialize((Map<String, Object>) document.get("helmet"));
			ItemStack chestplate = ItemStack.deserialize((Map<String, Object>) document.get("chestplate"));
			ItemStack leggings = ItemStack.deserialize((Map<String, Object>) document.get("leggings"));
			ItemStack boots = ItemStack.deserialize((Map<String, Object>) document.get("boots"));

			ItemStack mainHand = ItemStack.deserialize((Map<String, Object>) document.get("main-hand"));
			ItemStack offHand = ItemStack.deserialize((Map<String, Object>) document.get("off-hand"));
			
			List<Document> items = (List<Document>) document.get("item");
			
			// Affecte les données chargées dans l'inventaire du joueur
			playerInventory.setHelmet(helmet);
			playerInventory.setChestplate(chestplate);
			playerInventory.setLeggings(leggings);
			playerInventory.setBoots(boots);
			
			playerInventory.setItemInMainHand(mainHand);
			playerInventory.setItemInOffHand(offHand);
			
			ItemStack[] itemsArray = items.stream()
										  .map(doc -> ItemStack.deserialize((Map<String, Object>) doc.get("item")))
										  .collect(Collectors.toList())
										  .toArray(new ItemStack[items.size()]);
			
			playerInventory.setStorageContents(itemsArray);
			
			inventory = playerInventory;
		}
		
		return inventory;
	}

}
