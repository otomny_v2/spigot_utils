package net.spigotutils.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import net.minecraft.server.MinecraftServer;
import net.spigotutils.timings.CustomTimings;

public class TestEvent {

	public static void testEvent(Listener listener, String methodName) throws Exception {
		Method method = findByName(listener.getClass(), methodName);
		if (method == null)
			return;
		if (method.getParameterCount() != 1)
			return;
		if (method.isAnnotationPresent(EventTest.class)) {
			EventTest test = method.getAnnotationsByType(EventTest.class)[0];
			Plugin plugin = Bukkit.getPluginManager().getPlugin(test.plugin());
			if (plugin == null) {
				throw new IllegalStateException("Erreur : le plugin est nul");
			}
			EventTester<Event> tester = (EventTester<Event>) test.supplyEvent().newInstance();
			if (tester == null) {
				throw new IllegalStateException("Erreur : le testeur chargé est impossible");
			}
			if (test.sync()) {
				if (test.eventInstance()) {
					List<Double> tps = new ArrayList<>();
					for (int i = 0; i < test.count(); i++) {
						Event event = tester.getEvent();
						tps.add(MinecraftServer.getServer().recentTps[0]);
						testMethod(listener, methodName, method, event, tester);
						tps.add(MinecraftServer.getServer().recentTps[0]);
					}
					// calcul moyenne TPS
					double averageTps = tps.stream().mapToDouble(x -> x)
						.average()
						.getAsDouble();
					double percFrom = (averageTps / 20.0) * 100;
					
					
					System.out.println("[TESTEVENT] Average TPS during Test "+tester.getEvent().getClass().getSimpleName()+" = "+averageTps+".");
					System.out.println("[TESTEVENT] Consider this is during the test, others things can impact.");
					System.out.println("[TESTEVENT] This is "+percFrom+" less than 20TPS");
					tester.afterAll();
				} else {
					Event event = tester.getEvent();
					for (int i = 0; i < test.count(); i++) {
						testMethod(listener, methodName, method, event, tester);
					}
					tester.afterAll();
				}
			} else {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					try {
						if (test.eventInstance()) {
							List<Double> tps = new ArrayList<>();
							for (int i = 0; i < test.count(); i++) {
								tps.add(MinecraftServer.getServer().recentTps[0]);
								Event event = tester.getEvent();
								testMethod(listener, methodName, method, event, tester);
								tps.add(MinecraftServer.getServer().recentTps[0]);
							}
							double averageTps = tps.stream().mapToDouble(x -> x)
								.average()
								.getAsDouble();
							double percFrom = (averageTps / 20.0) * 100;
							
							
							System.out.println("[TESTEVENT] Average TPS during Test "+tester.getEvent().getClass().getSimpleName()+" = "+averageTps+".");
							System.out.println("[TESTEVENT] Consider this is during the test, others things can impact.");
							System.out.println("[TESTEVENT] This is "+percFrom+" less than 20TPS");
							tester.afterAll();
						} else {
							Event event = tester.getEvent();
							List<Double> tps = new ArrayList<>();
							for (int i = 0; i < test.count(); i++) {
								tps.add(MinecraftServer.getServer().recentTps[0]);
								testMethod(listener, methodName, method, event, tester);
								tps.add(MinecraftServer.getServer().recentTps[0]);
							}
							double averageTps = tps.stream().mapToDouble(x -> x)
								.average()
								.getAsDouble();
							double percFrom = (averageTps / 20.0) * 100;
							
							
							System.out.println("[TESTEVENT] Average TPS during Test "+tester.getEvent().getClass().getSimpleName()+" = "+averageTps+".");
							System.out.println("[TESTEVENT] Consider this is during the test, others things can impact.");
							System.out.println("[TESTEVENT] This is "+percFrom+" less than 20TPS");
							tester.afterAll();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
		}
	}

	private static <T extends Event> void testMethod(Listener listener, String methodName, Method method, T event, EventTester<T> tester) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		CustomTimings.doTimings(listener.getClass().getSimpleName()+"_"+methodName);
		method.invoke(listener, event);
		tester.after(event);
		CustomTimings.doTimings(listener.getClass().getSimpleName()+"_"+methodName);
	}
	
	public static void testEvent(Listener listener) throws Exception {
		Method[] methods = listener.getClass().getDeclaredMethods();
		for (Method method : methods) {
			testEvent(listener, method.getName());
		}
	}

	private static Method findByName(Class<?> klass, String methodName) {
		for (Method method : klass.getDeclaredMethods()) {
			if (method.getName().toLowerCase().equals(methodName.toLowerCase())) {
				return method;
			}
		}
		return null;
	}

}
