package net.spigotutils.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
/**
 * Classe pour testé les évènement / la fréquence en les lancant X fois
 * @author ComminQ_Q (Computer)
 *
 * @date 10/10/2019
 */
public @interface EventTest {

	public String plugin();
	
	public boolean sync() default true;
	
	public boolean eventInstance() default true;
	
	public int count() default 100;
	
	public Class<? extends EventTester<?>> supplyEvent();
	
}
