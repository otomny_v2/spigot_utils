package net.spigotutils.test;

import org.bukkit.event.Event;

public interface EventTester<T extends Event> {

	public T getEvent();
	
	public void afterAll();

	public void after(T event);
	
}
