package net.spigotutils.utils;

import net.mongoutils.MongoUtils;
import net.spigotutils.data.Adaptable;
import net.spigotutils.data.Identifiable;

/**
 * Version write only de la classe {@link DBObjectManager}
 */
public abstract class DBWriteObjectManager<T extends Adaptable & Identifiable> {

  private String collection;

  public DBWriteObjectManager(String collection) {
    this.collection = collection;
  }

  /**
   * Créer un objet dans la bdd
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param data
   * @date 07/05/2021
   */
  public void create(T data) {
    MongoUtils.insertInDb(System.getProperty("dbName"), this.collection,
                          data.getDatabaseId(), data.adapt());
  }
}
