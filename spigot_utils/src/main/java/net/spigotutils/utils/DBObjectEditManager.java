package net.spigotutils.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import net.spigotutils.data.Adaptable;
import net.spigotutils.data.Identifiable;
import net.spigotutils.data.Loadable;
import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.Guictify;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiTransformer;

public abstract class DBObjectEditManager<T extends Adaptable & Guictify &
                                                        Identifiable & Loadable>
    extends DBObjectManager<T> {

  protected String guiName = "§eEditeur d'objet";

  public DBObjectEditManager(Class<T> type, String collection) {
    super(type, collection);
  }

  public abstract T create();

  public void openEdit(Player player) { openEdit(player, 0); }

  public void openEdit(Player player, int page) {
    final int ITEM_PAGE_COUNT = 3 * 7;
    int maxPageCount = (int)Math.floor(Double.valueOf(this.data.size()) /
                                       Double.valueOf(ITEM_PAGE_COUNT)) +
                       1;
    int maxPage = maxPageCount - 1;

    var guiBuilder = new GuiBuilder().name(guiName).size(5 * 9).fillSide(
        Material.ORANGE_STAINED_GLASS_PANE);

    if (page < 0) {
      openEdit(player, 0);
    } else if (page > maxPageCount) {
      openEdit(player, maxPageCount);
    }
    int startPage = page * ITEM_PAGE_COUNT;
    int endPage = Math.min(this.data.size(), (page + 1) * ITEM_PAGE_COUNT);
    boolean lastPage = page == maxPage;

    for (int i = startPage; i < endPage; i++) {
      final int index = i;
      var item = this.data.get(index);
      guiBuilder.item(createEditItem(item)
                          .description("§aClique gauche pour éditer",
                                       "§cClique gauche pour supprimer")
                          .clickHandler((_1, _2, clickType, _3) -> {
                            if (clickType == ClickType.RIGHT) {
                              // remove
                              GuiAcceptItem.openAccept(
                                  "§cSupprimé ?", player,
                                  () -> openEdit(player, page), () -> {
                                    remove(item);
                                    openEdit(player, page);
                                  });
                            } else {
                              // edit
                              GuiTransformer.fast(item, player, () -> {
                                update(item);
                                openEdit(player, page);
                              });
                            }
                            return true;
                          })
                          .build());
    }

    if (lastPage) {
      guiBuilder.item(new GuiItemBuilder()
                          .name("§aAjouter un élément")
                          .icon(Material.LIME_CONCRETE)
                          .description("", "§7Cliquez pour ajouter un élément")
                          .clickHandler(() -> {
                            // Ajouter
                            var item = create();
                            GuiTransformer
                                .of(item,
                                    () -> {
                                      create(item);
                                      openEdit(player, page);
                                    })
                                .openGUI(player);
                          })
                          .build());
    }

    if (page > 0) {
      guiBuilder.item(39, new GuiItemBuilder()
                              .name("§7Page précèdente")
                              .icon(Material.ARROW)
                              .clickHandler(() -> openEdit(player, page - 1))
                              .build());
    }
    if (page < maxPageCount) {
      guiBuilder.item(41, new GuiItemBuilder()
                              .icon(Material.ARROW)
                              .name("§7Page suivante")
                              .clickHandler(() -> openEdit(player, page + 1))
                              .build());
    }

    guiBuilder.build().open(player);
  }

  protected GuiItemBuilder createEditItem(T data) { return data.item(); }
}
