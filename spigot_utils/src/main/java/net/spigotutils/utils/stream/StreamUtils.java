package net.spigotutils.utils.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Helper sur les streams
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 16/10/2019
 */
public class StreamUtils {

	/**
	 * Write a varint to the buffer
	 * 
	 * @param buffer the buffer to write to
	 * @param value  the int value to write to intvar
	 * @throws IOException
	 */
	public static void writeVarInt(OutputStream buffer, int value) throws IOException {
		while ((value & -128) != 0) {
			buffer.write(value & 127 | 128);
			value >>>= 7;
		}
		buffer.write(value);
	}

	/**
	 * Write a varint to the buffer
	 * 
	 * @param buffer the buffer to write to
	 * @param value  the short value to write to intvar
	 * @throws IOException
	 */
	public static void writeVarInt(OutputStream buffer, short value) throws IOException {
		while ((value & -128) != 0) {
			buffer.write(value & 127 | 128);
			value >>>= 7;
		}
		buffer.write(value);
	}

	/**
	 * Reads a varint from the given InputStream and returns the decoded value
	 * as an int.
	 *
	 * @param inputStream the InputStream to read from
	 */
	public static int readVarInt(InputStream inputStream) throws IOException {
		int result = 0;
		int shift = 0;
		int b;
		do {
			if (shift >= 32) {
				// Out of range
				throw new IndexOutOfBoundsException("varint too long");
			}
			// Get 7 bits from next byte
			b = inputStream.read();
			result |= (b & 0x7F) << shift;
			shift += 7;
		} while ((b & 0x80) != 0);
		return result;
	}

	/**
	 * Reads a varint from the given InputStream and returns the decoded value
	 * as an int.
	 *
	 * @param inputStream the InputStream to read from
	 */
	public static short readVarShort(InputStream inputStream) throws IOException {
		short result = 0;
		int shift = 0;
		int b;
		do {
			if (shift >= 16) {
				// Out of range
				throw new IndexOutOfBoundsException("varshort too long");
			}
			// Get 7 bits from next byte
			b = inputStream.read();
			result |= (b & 0x7F) << shift;
			shift += 7;
		} while ((b & 0x80) != 0);
		return result;
	}

	/**
	 * Mappe une Collection de type X vers une liste de type Y
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <FROM> Le type d'entrée
	 * @param <TO>   le type de sortie
	 * @param from   la collection à mappé
	 * @param func   la fonction de mappage
	 * @return
	 * @date 16/10/2019
	 */
	public static <FROM, TO> List<TO> map(Collection<FROM> from, Function<? super FROM, ? extends TO> func) {
		return from == null ? new ArrayList<>() : from.stream().map(func).collect(Collectors.toList());
	}

	/**
	 * Mappe une Collection de type X vers une liste de type Y
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <FROM> Le type d'entrée
	 * @param <TO>   le type de sortie
	 * @param from   la collection à mappé
	 * @param func   la fonction de mappage
	 * @return
	 * @date 16/10/2019
	 */
	public static <FROM, TO, TO2> List<TO2> map(
			Collection<FROM> from,
			Function<? super FROM, ? extends TO> func,
			Function<? super TO, ? extends TO2> func2) {
		return from == null ? new ArrayList<>() : from.stream().map(func).map(func2).collect(Collectors.toList());
	}

	/**
	 * Transforme une List 2D ( Map<K, List<V>> ) vers une liste à une dimension
	 * List<V>
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param <KEY>
	 * @param <VALUE>
	 * @param from
	 * @return
	 * @date 15/02/2021
	 */
	public static <KEY, VALUE> List<VALUE> flatMap(Map<KEY, List<VALUE>> from) {
		return from.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

	/**
	 * Mappe une collection de type X vers une map de type <X, Y>
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <FROM> Le type d'entrée
	 * @param <TO>   le type de sortie
	 * @param from   la collection à mappé
	 * @param func   la fonction de mappage
	 * @return {@link Map} la map <FROM, TO>
	 * @date 16/10/2019
	 */
	public static <FROM, TO> Map<FROM, TO> mapToMap(Collection<FROM> from, Function<? super FROM, ? extends TO> func) {
		return from == null ? new HashMap<>() : from.stream().collect(Collectors.toMap(f -> f, func));
	}

	/**
	 * Filtre une collection
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <FROM>
	 * @param from
	 * @param predicate
	 * @return
	 * @date 01/06/2020
	 */
	public static <FROM> List<FROM> filter(Collection<FROM> from, Predicate<? super FROM> predicate) {
		return from == null ? new ArrayList<>() : from.stream().filter(predicate).collect(Collectors.toList());
	}

	/**
	 * Filtre une collection et retourne le premier élément filtré;
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>        Le type
	 * @param collection la collection à filtré
	 * @param predicate  le prédicat de filtrage
	 * @return T Le type trouvé OU null si non trouvé
	 * @date 17/10/2019
	 */
	public static <T> T filterAndFind(Collection<? extends T> collection, Predicate<T> predicate) {
		return collection == null ? null : collection.stream().filter(predicate).findFirst().orElse(null);
	}

	/**
	 * Filtre une collection et renvoie s'il y à un élèment correspondant
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param <T>
	 * @param collection
	 * @param predicate
	 * @return true OU false
	 * @date 03/08/2021
	 */
	public static <T> boolean filterAndExist(Collection<? extends T> collection, Predicate<T> predicate) {
		return collection == null ? false : collection.stream().filter(predicate).findFirst().isPresent();
	}

	/**
	 * Filtre une collection et la map ensuite
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>        Le type
	 * @param collection la collection à filtré
	 * @param predicate  le prédicat de filtrage
	 * @return T Le type trouvé OU null si non trouvé
	 * @date 17/10/2019
	 */
	public static <T, U> List<U> filterAndMap(
			Collection<? extends T> collection,
			Predicate<T> predicate,
			Function<? super T, ? extends U> transform) {

		return collection == null ? null
				: collection.stream().filter(predicate).map(transform).collect(Collectors.toList());
	}

	/**
	 * Filtre une collection et retourne le premier élément filtré;
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>        Le type
	 * @param collection la collection à filtré
	 * @param predicate  le prédicat de filtrage
	 * @return T Le type trouvé OU null si non trouvé
	 * @date 17/10/2019
	 */
	public static <T, U> U filterMapFind(
			Collection<? extends T> collection,
			Predicate<T> predicate,
			Function<? super T, ? extends U> transform) {
		try {
			return collection == null ? null : collection.stream().filter(predicate).map(transform).findFirst().orElse(null);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * Vérifie la présence d'un ou plusieurs élèments respectant une règle
	 * 
	 * @param <T> le type d'élèment
	 * @param collection la collection
	 * @param predicate la règle
	 * @return
	 */
	public static <T> boolean anyMatch(Collection<? extends T> collection, Predicate<T> predicate) {
		try {
			return collection == null ? null : collection.stream().anyMatch(predicate);
		} catch (Exception e) {
			return false;
		}
	}

}
