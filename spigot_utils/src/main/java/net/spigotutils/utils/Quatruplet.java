package net.spigotutils.utils;

import lombok.Getter;

public class Quatruplet<T, R, S, U> {

	@Getter
	private T first;
	@Getter
	private R second;
	@Getter
	private S third;
	@Getter
	private U fourth;
	
	public Quatruplet(T first, R second, S third, U fourth) {
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
	}
	
	
	
}
