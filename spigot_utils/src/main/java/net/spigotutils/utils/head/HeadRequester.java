package net.spigotutils.utils.head;

import net.spigotutils.gui.item.GuiItem;

public interface HeadRequester {

  /**
   * Replaces the head in an open inventory panel with the supplied panel item
   * @param item - panel item, must be a player head
   */
  void setHead(GuiItem item);
}