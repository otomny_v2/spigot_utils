package net.spigotutils.utils.random;

import java.security.SecureRandom;

public class RandomUtils {
	
	private RandomUtils(){}

	public static SecureRandom SECURE_RANDOM = new SecureRandom();
}
