package net.spigotutils.utils.random;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import net.spigotutils.utils.Pair;

public class WeightedRandomUtils {

	/**
	 * 
	 */
	public interface WeightEntry extends Comparable<WeightEntry> {

		/**
		 * @return
		 */
		double getWeight();

		@Override
		default int compareTo(WeightEntry o) {
			if (this.getWeight() < o.getWeight()) {
				return 1;
			}
			if (this.getWeight() > o.getWeight()) {
				return -1;
			}
			return 0;
		}

	}

	/**
	 * @param <T>
	 * @param collection
	 * @return
	 */
	public static <T extends WeightEntry> double getTotalProbability(
			Collection<T> collection) {
		return collection.stream()
				.mapToDouble(WeightEntry::getWeight).sum();
	}

	/**
	 * @param <T>
	 * @param collection
	 * @return
	 * @throws NoSuchElementException
	 */
	public static <T extends WeightEntry> T getWeightedEntry(
			Collection<T> collection) throws NoSuchElementException {
		return getWeightedEntry(ThreadLocalRandom.current(),
				collection);
	}

	
	/**
	 * 
	 * @param <T>
	 * @return
	 */
	public static <T extends WeightEntry> Optional<T> getWeightedEntryOptional(Random random, Collection<T> collection){
		try {
			return Optional.of(getWeightedEntry(random, collection));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	/**
	 * 
	 * @param <T>
	 * @param random
	 * @param collection
	 * @return
	 * @throws NoSuchElementException
	 */
	public static <T extends WeightEntry> T getWeightedEntry(
			Random random, Collection<T> collection) throws NoSuchElementException {
		if (collection.isEmpty()) {
			throw new NoSuchElementException();
		}
		List<T> sorted = new ArrayList<>(collection);
		Collections.sort(sorted);
		double probabilitySum = getTotalProbability(collection);

		List<Pair<T, Double>> list = new ArrayList<>();

		double start = 0;
		double randomIndex = start;

		for (T type : sorted) {
			double currIndex = randomIndex
					+ (type.getWeight() / probabilitySum);
			list.add(Pair.of(type, currIndex));
			randomIndex = currIndex;
		}
		double randomValue = random.nextDouble();
		for (var pair : list) {
			if (pair.get2() >= randomValue) {
				return pair.get1();
			}
		}
		return sorted.get(0);

	}

	/**
	 * @param <T>
	 * @param random
	 * @param collection
	 * @param amount
	 * @return
	 * @throws NoSuchElementException
	 */
	public static <T extends WeightEntry> List<T> getWeightedEntry(
			Random random, Collection<T> collection, int amount)
			throws NoSuchElementException {
		if (collection.isEmpty()) {
			throw new NoSuchElementException();
		}
		List<T> sorted = new ArrayList<>(collection);
		Collections.sort(sorted);
		double probabilitySum = getTotalProbability(collection);

		List<Pair<T, Double>> list = new ArrayList<>();

		double start = 0;
		double randomIndex = start;

		for (T type : sorted) {
			double currIndex = randomIndex
					+ (type.getWeight() / probabilitySum);
			list.add(Pair.of(type, currIndex));
			randomIndex = currIndex;
		}
		List<T> generatedWeightedEntries = new ArrayList<>();
		MainLoop: for (int i = 0; i < amount; i++) {
			double randomValue = random.nextDouble();
			for (var pair : list) {
				if (pair.get2() >= randomValue) {
					generatedWeightedEntries.add(pair.get1());
					continue MainLoop;
				}
			}
			generatedWeightedEntries.add(sorted.get(0));
		}
		return generatedWeightedEntries;
	}

}
