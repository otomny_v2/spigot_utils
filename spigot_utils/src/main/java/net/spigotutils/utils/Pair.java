package net.spigotutils.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * Représente un couple de type <T, U>
 * 
 * @author ComminQ_Q (Computer)
 * @param <T>
 * @param <U>
 * @date 16/10/2019
 */
public class Pair<T, U> {

	public static <T, U> Pair<T, U> of(T first, U second) {
		return new Pair<>(first, second);
	}

	@Getter
	@Setter
	private T firstValue;
	@Getter
	@Setter
	private U secondValue;

	/**
	 * Constructeur
	 * 
	 * @author ComminQ_Q (Computer)
	 * @param firstValue
	 * @param secondValue
	 * @date 16/10/2019
	 */
	public Pair(T firstValue, U secondValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}

	public T get1() {
		return this.firstValue;
	}

	public U get2() {
		return this.secondValue;
	}

	public T getX() {
		return this.firstValue;
	}

	public U getY() {
		return this.secondValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstValue == null) ? 0 : firstValue.hashCode());
		result = prime * result
				+ ((secondValue == null) ? 0 : secondValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Pair<T, U> other = (Pair<T, U>) obj;
		if (firstValue == null) {
			if (other.firstValue != null)
				return false;
		} else if (!firstValue.equals(other.firstValue))
			return false;
		if (secondValue == null) {
			if (other.secondValue != null)
				return false;
		} else if (!secondValue.equals(other.secondValue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pair<" + this.firstValue + ":" + this.secondValue
				+ ">";
	}

}
