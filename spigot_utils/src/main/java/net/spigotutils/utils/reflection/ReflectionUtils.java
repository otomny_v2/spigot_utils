package net.spigotutils.utils.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;


/**
 *
 * @author ComminQ_Q (Computer)
 *
 * @date 10/11/2019
 */
public class ReflectionUtils {

  private static Reflections reflection;

  /**
   * Renvoie le type d'objet stocké dans une collection
   *
   * @author ComminQ_Q (Computer)
   *
   * @param collection
   * @return
   * @date 10/11/2019
   */
  @Deprecated
  public static Class<?> getTypeOfCollection(Collection<?> collection) {
    return null;
  }

  /**
   * Vérifie si une classe possède une méthode avec la signature en paramètre
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param methodName
   * @param args
   * @return
   * @date 15/03/2021
   */
  public static boolean hasMethod(Class<?> clazz, String methodName,
                                  Class<?> args) {
    Method method = null;
    try {
      method = clazz.getDeclaredMethod(methodName, args);
    } catch (Exception e) {
    }
    return method != null;
  }

  public static <T> Object callMethod(Class<?> clazz, T instance,
                                      String methodName, Class<?>[] argsType,
                                      Object[] args) throws Exception {
    Method method = clazz.getDeclaredMethod(methodName, argsType);
    return method.invoke(instance, args);
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param packageName
   * @return
   * @date 22/05/2020
   */
  public static Set<Class<?>> findClass(String packageName) {
    return findClassBySuperclass(packageName, Object.class);
  }

  /**
   * Récupère le type de la liste générique du champ d'une classe
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param listField
   * @return
   * @date 18/12/2020
   */
  public static Class<?> getTypeOfListField(Field listField) {
    return (Class<?>)((ParameterizedType)listField.getGenericType())
        .getActualTypeArguments()[0];
  }

  /**
   * Récupère le type de la liste générique du champ d'une classe
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param listField
   * @return
   * @date 18/12/2020
   */
  public static Class<?> getKeyTypeOfMapField(Field mapField) {
    return (Class<?>)((ParameterizedType)mapField.getGenericType())
        .getActualTypeArguments()[0];
  }

  /**
   * Récupère le type de la liste générique du champ d'une classe
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param listField
   * @return
   * @date 18/12/2020
   */
  public static Class<?> getValueTypeOfMapField(Field mapField) {
    return (Class<?>)((ParameterizedType)mapField.getGenericType())
        .getActualTypeArguments()[1];
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param <E>
   * @param enumClass
   * @return
   * @throws NoSuchFieldException
   * @throws IllegalAccessException
   * @date 22/05/2020
   */
  public static <E> E[] enumList(Class<E> enumClass)
      throws NoSuchFieldException, IllegalAccessException {
    E[] e = (E[])enumClass.getEnumConstants();
    return e;
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param <T>
   * @param packageName
   * @param clazz
   * @return
   * @date 01/05/2020
   */
  public static <T> Set<Class<? extends T>>
  findClassBySuperclass(String packageName, Class<T> clazz) {
    Reflections reflections = new Reflections(
        new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage(packageName))
            .filterInputsBy(new FilterBuilder().includePackage(packageName)));

    return reflections.getSubTypesOf(clazz);
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param <T>
   * @param packageName
   * @param clazz
   * @return
   * @date 01/05/2020
   */
  public static <T> Set<Class<? extends T>>
  findClassBySuperclass(ClassLoader loader, String packageName,
                        Class<T> clazz) {

    Reflections reflections = new Reflections(
        new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage(packageName, loader))
            .filterInputsBy(new FilterBuilder().includePackage(packageName)));

    return reflections.getSubTypesOf(clazz);
  }

  /**
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param field
   * @param object
   * @return
   * @date 15/07/2021
   */
  public static void setFieldValue(Field field, Object object, Object value) {
    try {
      field.setAccessible(true);
      field.set(object, value);
      field.setAccessible(false);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  /**
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param field
   * @param object
   * @return
   * @date 15/07/2021
   */
  public static Object getFieldValue(Field field, Object object) {
    try {
      field.setAccessible(true);
      Object o = field.get(object);
      field.setAccessible(false);
      return o;
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param <T>
   * @param <V>
   * @param fieldName
   * @param toGet
   * @return
   * @date 03/05/2020
   */
  public static <T, V> T getFieldValue(String fieldName, V toGet)
      throws ClassCastException {
    try {
      Class<?> clazzT = toGet.getClass();
      Field field = clazzT.getDeclaredField(fieldName);
      if (!field.isAccessible()) {
        field.setAccessible(true);
      }
      return (T)field.get(toGet);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Met un champ d'un objet T à une valeur V
   *
   * @author ComminQ_Q (Computer)
   *
   * @param <T>
   * @param <V>
   * @param fieldName
   * @param toSet
   * @param value
   * @date 10/11/2019
   */
  public static <T, V> void setFieldValue(String fieldName, T toSet, V value) {
    try {
      Class<?> clazzT = toSet.getClass();
      Field field = clazzT.getDeclaredField(fieldName);
      if (!field.isAccessible()) {
        field.setAccessible(true);
      }
      field.set(toSet, value);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void printFields(Object object) {
    printFields(object.getClass(), object);
  }

  public static void printFields(Class<?> clazz, Object object) {
    StringBuilder builder = new StringBuilder();
    for (Field field : clazz.getDeclaredFields()) {
      field.setAccessible(true);
      try {
        builder.append(Modifier.toString(field.getModifiers()) + " " +
                       field.getType().getSimpleName() + " = " +
                       field.get(object) + " \n");
      } catch (Exception e) {
        System.out.println(e);
      }
      field.setAccessible(false);
    }
    System.out.println(builder.toString());
  }

  public static List<Field> getFieldList(Object object) {
    Class<?> clazz = object.getClass();
    List<Field> fields = new ArrayList<>();
    for (Field field : clazz.getDeclaredFields()) {
      fields.add(field);
    }
    return fields;
  }

	public static final String getConstructors(Class<?> klass){
		var builder = new StringBuilder();
		for(var construkt : klass.getDeclaredConstructors()){
			builder.append(klass.getSimpleName()+"(");
			for(var params : construkt.getParameterTypes()){
				builder.append(params.getCanonicalName()+", ");
			}
			builder.append("), ");
		}
		return builder.toString();
	}
}
