package net.spigotutils.utils.reflection;

import java.util.UUID;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 03/05/2020
 */
public final class ObjectUtils {

	public static final UUID NULL_UUID = UUID.randomUUID();
	
	public static void dumpStackTrace(){
		int i = 0;
		for(StackTraceElement trace : Thread.currentThread().getStackTrace()){
			for(int y = 0; y < i; y++)
				System.out.print("  ");
			System.out.println(trace);
			i++;
		}
	}

	/**
	 * Renvoie un tableau d'objet
	 * @author ComminQ_Q (Computer)
	 *
	 * @param objects
	 * @return
	 * @date 03/05/2020
	 */
	public static Object[] from(Object ...objects) {
		return objects;
	}
	
	/**
	 * Renvoie un tableau d'objet vide
	 * @author ComminQ_Q (Computer)
	 *
	 * @param objects
	 * @return
	 * @date 03/05/2020
	 */
	public static Object[] from() {
		return new Object[] {};
	}
	
}
