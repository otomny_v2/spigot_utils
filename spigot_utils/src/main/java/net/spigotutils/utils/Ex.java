package net.spigotutils.utils;

public class Ex {

	/**
	 * Méthode pour catch les exceptions sans avoir à mettre
	 * un
	 * <br>
	 * <code>
	 * try{
	 *			...code 
	 * }catch(Exception e){
	 * }
	 * </code>
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param <T>
	 * @param func
	 * @return
	 * @date 03/08/2021
	 */
	public static <T> T grab(CSupplier<T> func) {
		return catch_(func);
	}
	
	/**
	 * Méthode pour catch les exceptions sans avoir à mettre
	 * un
	 * <br>
	 * <code>
	 * try{
	 *			...code 
	 * }catch(Exception e){
	 * }
	 * </code>
	 * @author Fabien
	 * @date 20 août 2021
	 * @param func
	 */
	public static void grab(CRunnable func) {
		catch_(func);
	}
	
	/**
	 * Plutôt utiliser la méthode grab
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param <T>
	 * @param func
	 * @return
	 * @date 03/08/2021
	 */
	public static void catch_(CRunnable func) {
		try {
			func.run();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Plutôt utiliser la méthode grab
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param <T>
	 * @param func
	 * @return
	 * @date 03/08/2021
	 */
	public static <T> T catch_(CSupplier<T> func) {
		T data = null;
		try {
			data = func.get();
			return data;
		}catch (Exception e) {
			e.printStackTrace();
			return data;
		}
	}
	
	public static interface CRunnable{
		
		public void run() throws Exception;
		
	}
	
	public static interface CSupplier<T>{
		
		public T get() throws Exception;
		
	}
	
}
