package net.spigotutils.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * Représente un couple de type <T, U>
 * @author ComminQ_Q (Computer)
 *
 * @param <T>
 * @param <U>
 * @date 16/10/2019
 */
public class Triplet<T, U, V> {

	@Getter @Setter private T firstValue;
	@Getter @Setter private U secondValue;
	@Getter @Setter private V thirdValue;
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param firstValue
	 * @param secondValue
	 * @date 16/10/2019
	 */
	public Triplet(T firstValue, U secondValue, V thirdValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
		this.thirdValue = thirdValue;
	}

	public T get1(){
		return this.firstValue;
	}

	public U get2(){
		return this.secondValue;
	}

	public V get3(){
		return this.thirdValue;
	}

	public T getX(){
		return this.firstValue;
	}

	public U getY(){
		return this.secondValue;
	}

	public V getZ(){
		return this.thirdValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstValue == null) ? 0 : firstValue.hashCode());
		result = prime * result + ((secondValue == null) ? 0 : secondValue.hashCode());
		result = prime * result + ((thirdValue == null) ? 0 : thirdValue.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return this.firstValue +" "+ this.secondValue + " "+this.thirdValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triplet other = (Triplet) obj;
		if (firstValue == null) {
			if (other.firstValue != null)
				return false;
		} else if (!firstValue.equals(other.firstValue))
			return false;
		if (secondValue == null) {
			if (other.secondValue != null)
				return false;
		} else if (!secondValue.equals(other.secondValue))
			return false;
		if (thirdValue == null) {
			if (other.thirdValue != null)
				return false;
		} else if (!thirdValue.equals(other.thirdValue))
			return false;
		return true;
	}
	
	


}