package net.spigotutils.utils;

public interface TriFunction<I1, I2, I3, OUT> {

	public OUT apply(I1 in1, I2 in2, I3 in3);

}
