package net.spigotutils.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bson.Document;

import net.mongoutils.MongoUtils;
import net.spigotutils.data.Adaptable;
import net.spigotutils.data.Identifiable;
import net.spigotutils.data.Loadable;
import net.spigotutils.utils.reflection.ReflectionUtils;

public abstract class DBObjectManager<T extends Adaptable & Identifiable &
                                                    Loadable> {

  protected List<T> data = new ArrayList<>();
  private Class<T> type;
  private String collection;

  public DBObjectManager(Class<T> type, String collection) {
    this.data = new CopyOnWriteArrayList<>();
    this.type = type;
    this.collection = collection;
  }

  /**
   * Charge les objets de la base de données
   *
   * @author Fabien CAYRE (Computer)
   *
   * @date 07/05/2021
   */
  public void load() {
    this.data.clear();
    MongoUtils
        .loadIndexed(System.getProperty("dbName"), this.collection,
                     this::createFromDatabase)
        .forEach(this.data::add);
  }

  /**
   *
   * @param dbId
   * @return
   */
  public T getByDatabaseId(String dbId) {
    return this.data.stream()
        .filter(obj -> obj.getDatabaseId().equals(dbId))
        .findFirst()
        .orElse(null);
  }

  public T createFromDatabase(Document document) {
    try {
      var instance = type.getConstructor(Document.class).newInstance(document);
      return instance;
    } catch (InstantiationException | IllegalAccessException |
             IllegalArgumentException | InvocationTargetException |
             NoSuchMethodException | SecurityException e) {
      e.printStackTrace();
      System.out.println("Error hydratation: cannot create instance of " +
                         type.getSimpleName() +
                         " from document, constructor: " +
                         ReflectionUtils.getConstructors(type));
    }
    return null;
  }

  /**
   * @param action Création, Suppression, Modification
   * @param objId L'identifiant de l'objet
   * @param updateField Champ a mettre à jour pour l'id de l'objet envoyé, "ALL"
   *     pour mettre à jour tous l'objet
   * @param data Les données de l'objet sous formats JSON
   */
  public void messageUpdate(ObjectAction action, String objId, Document data) {

    // On est susceptible de recevoir notre propre message
    // On doit regarder pour chaque action si on l'a pas déja fait
    switch (action) {
    case CREATE:
      if (getByDatabaseId(objId) == null) {
        var instance = createFromDatabase(data);
        this.data.add(instance);
      }
      break;
    case DELETE:
      if (getByDatabaseId(objId) != null) {
        this.data.remove(getByDatabaseId(objId));
      }
      break;
    case UPDATE:
      var obj = getByDatabaseId(objId);
      if (obj != null) {
        if (obj.supportAutoUpdate()) {
          List<Class<?>> allowedClasses =
              List.of(int.class, float.class, double.class, Integer.class,
                      Float.class, Double.class, String.class);

          for (String key : data.keySet()) {
            try {
              var field = obj.getClass().getDeclaredField(key);
              if (field != null) {
                if (allowedClasses.contains(field.getType())) {
                  field.set(obj, data.get(key));
                }
              }
            } catch (NoSuchFieldException | SecurityException |
                     IllegalArgumentException | IllegalAccessException e) {
              e.printStackTrace();
            }
          }
        } else {
          obj.update(data);
        }
      }
      break;
    default:
      break;
    }
  }

  /**
   * Sauvegarde les objets de la base de données
   *
   * @author Fabien CAYRE (Computer)
   *
   * @date 07/05/2021
   */
  public void save() { this.data.parallelStream().forEach(this::update); }

  public void update(T data) {
    // TODO Réécrire cette fonction pour faire appel au broken RabbitMQ / Kafka
    MongoUtils.update(System.getProperty("dbName"), this.collection,
                      data.getDatabaseId(), data.adapt());
  }

  /**
   * Créer un objet dans la bdd
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param data
   * @date 07/05/2021
   */
  public void create(T data) {
    this.data.add(data);
    // TODO Réécrire cette fonction pour faire appel au broken RabbitMQ / Kafka
    MongoUtils.insertInDb(System.getProperty("dbName"), this.collection,
                          data.getDatabaseId(), data.adapt(),
                          this.data.indexOf(data));
  }

  /**
   * Supprime un objet dans la bdd
   *
   * @author Fabien CAYRE (Computer)
   *
   * @date 19/04/2021
   */
  public void remove(T data) {
    // TODO Réécrire cette fonction pour faire appel au broken RabbitMQ / Kafka
    MongoUtils.remove(System.getProperty("dbName"), this.collection,
                      data.getDatabaseId());
    this.data.remove(data);
  }
}
