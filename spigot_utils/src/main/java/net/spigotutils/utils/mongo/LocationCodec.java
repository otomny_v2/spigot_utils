package net.spigotutils.utils.mongo;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bukkit.Location;

import net.spigotutils.location.LocationUtils;

public class LocationCodec implements Codec<Location> {

	private Codec<Document> documentCodec;

	public LocationCodec() {
		this.documentCodec = new DocumentCodec();
	}

	public LocationCodec(Codec<Document> codec) {
		this.documentCodec = codec;
	}

	@Override
	public void encode(BsonWriter writer, Location value, EncoderContext encoderContext) {
		this.documentCodec.encode(writer, LocationUtils.toDocument(value), encoderContext);
	}

	@Override
	public Class<Location> getEncoderClass() {
		return Location.class;
	}

	@Override
	public Location decode(BsonReader reader, DecoderContext decoderContext) {
		Document document = this.documentCodec.decode(reader, decoderContext);
		return LocationUtils.fromDocument(document);
	}

}
