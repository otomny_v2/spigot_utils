package net.spigotutils.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtils {

  public static final ZoneId EUROPE = ZoneId.of("Europe/Monaco");

  /**
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param millis
   * @return
   * @date 04/05/2021
   */
  public static String formatHourMinuteSecond(long millis) {
    return String.format(
        "%02d heures %02d minutes %02d secondes",
        TimeUnit.MILLISECONDS.toHours(millis),
        TimeUnit.MILLISECONDS.toMinutes(millis) -
            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
        TimeUnit.MILLISECONDS.toSeconds(millis) -
            TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(millis)));
  }

  public static String formatAdaptAllHours(long millis) {
    // Supérieur à 1 jour
    String s = "";
    // Supérieur à une heure
    if (millis > 60 * 60 * 1000) {
      s += ((millis / (60 * 60 * 1000))) + " h ";
    }
    return s + ((millis / (60 * 1000)) % 60) + " mins ";
  }

  public static String formatAdaptAll(long millis) {
    // Supérieur à 1 jour
    String s = "";
    if (millis > 24 * 60 * 60 * 1000) {
      s += (millis / (24 * 60 * 60 * 1000)) + " j ";
    }
    // Supérieur à une heure
    if (millis > 60 * 60 * 1000) {
      s += ((millis / (60 * 60 * 1000)) % 24) + " h ";
    }
    // Supérieur à 1 minute
    if (millis > 60 * 1000) {
      s += ((millis / (60 * 1000)) % 60) + " mins ";
    }
    return s + ((millis / 1000) % 60) + " secs";
  }

  public static String formatAdapt(long millis) {
    // Supérieur à 1 jour
    if (millis > 24 * 60 * 60 * 1000) {
      return (millis / (24 * 60 * 60 * 1000)) + " j ";
    }
    // Supérieur à une heure
    if (millis > 60 * 60 * 1000) {
      return ((millis / (60 * 60 * 1000)) % 24) + " h ";
    }
    // Supérieur à 1 minute
    if (millis > 60 * 1000) {
      return ((millis / (60 * 1000)) % 60) + " mins ";
    }
    return ((millis / 1000) % 60) + " secs";
  }

  /**
   * Retourne le jour de la semaine
   * @return le jour de la semaine (DE 1 A 7)
   */
  public static int dayOfWeek() {
    // LE PREMIER JOUR DE LA SEMAINE C'EST DIMANCHE
    // ??????????????????????????????? celui qui a écris cette spec est une
    // énorme merde
    int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
    return dayOfWeek == 0 ? 7 : dayOfWeek;
  }

  /**
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param millis
   * @return
   * @date 04/05/2021
   */
  public static String formatMinuteSecond(long millis) {
    return String.format(
        "%02d:%02d",
        TimeUnit.MILLISECONDS.toMinutes(millis) -
            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
        TimeUnit.MILLISECONDS.toSeconds(millis) -
            TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(millis)));
  }

  /**
   * Format d'une date
   * @param time epoch milliseconds
   * @return
   */
  public static String formatUTC2(long time) {
    return ZonedDateTime.ofInstant(Instant.ofEpochMilli(time), EUROPE)
        .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
  }

  public static LocalDate toLocalDateTime(Date date) {
    return Instant.ofEpochMilli(date.getTime()).atZone(EUROPE).toLocalDate();
  }
}
