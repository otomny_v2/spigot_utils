package net.spigotutils.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class StringUtils {

	private StringUtils() {
	}

	public static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static SecureRandom rnd = null;

	static {
		try {
			rnd = SecureRandom.getInstance("SHA1PRNG");
			;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static String replaceLast(String text, String regex, String replacement) {
		return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
	}

	public static String alphabet(int code) {
		String text = "";
		int currentIndex = 0;
		int textLength = code / 26 + 1;
		do {
			int maxRemove = Math.min(26, code);
			text += "" + (char) ((maxRemove % 26)
					+ 'a');
			code -= maxRemove;
			currentIndex++;
			if (currentIndex == textLength)
				break;
		} while (true);

		return text.toUpperCase();
	}

	public static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	public static String capitalize(String string) {
		if (string.length() < 0)
			return string;
		return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
	}

	public static String clamp(String string, int maxSize) {
		int max = Math.min(string.length(), maxSize);
		return string.substring(0, max);
	}

	public static void assertInside(String[] strings, String string) {
		if (!Arrays.asList(strings).contains(string)) {
			throw new IllegalArgumentException(
					"String must be contains in array (string='" + string + "', array=" + Arrays.toString(strings) + ")");
		}
	}

}
