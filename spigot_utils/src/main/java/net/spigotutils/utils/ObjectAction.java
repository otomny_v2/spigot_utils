package net.spigotutils.utils;

public enum ObjectAction {
	
	CREATE,
	UPDATE,
	DELETE;

}
