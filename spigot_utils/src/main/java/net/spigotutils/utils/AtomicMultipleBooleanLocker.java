package net.spigotutils.utils;

import java.util.Arrays;

import lombok.Getter;

public class AtomicMultipleBooleanLocker {
  
  @Getter
  private boolean[] lockers;
  private Runnable onLocked;

  public AtomicMultipleBooleanLocker(int size, Runnable onLocked){
    this.lockers = new boolean[size];
    this.onLocked = onLocked;
    Arrays.fill(lockers, false);
  }

  public synchronized void set(boolean val, int index){
    this.lockers[index] = val;

    for(boolean b : this.lockers){
      if(!b)
        return;
    }
    this.onLocked.run();
  }


}
