package net.spigotutils.utils;

public interface Into<T> {
	
	T get();

}
