package net.spigotutils.utils.number;

public class TimeFunction {

  private TimeFunction() {}

	/**
	 * Bound between 0 and 1
	 * @param x the value representing the progression of the animation from 0 to 1
	 * @return 
	 */
  public static final double easeOutQuart(double x) {
    return 1 - Math.pow(1 - x, 4);
  }
}
