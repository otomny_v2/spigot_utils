package net.spigotutils.utils;

public interface TriConsumer<F, S, T> {

	static <A,B,C> TriConsumer<A,B,C> none(){
		return (a,b,c) -> {};
	}

	void accept(F f, S s, T t);
	
}
