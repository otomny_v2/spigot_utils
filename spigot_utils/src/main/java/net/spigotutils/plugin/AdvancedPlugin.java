package net.spigotutils.plugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


/**
 * Classe wrapper qui permet d'ajouter des m�thodes
 * aux instances de "Plugins"
 * @author ComminQ_Q (Computer)
 *
 * 2019-10-03
 */
public abstract class AdvancedPlugin extends JavaPlugin{

	/**
	 * Est apel�e quand l'instance s'initialize
	 * @author ComminQ_Q (Computer)
	 *
	 * 2019-10-03
	 */
	public abstract void enable();
	
	/**
	 * Est apel�e quand l'instance se d�sactive
	 * @author ComminQ_Q (Computer)
	 *
	 * 2019-10-03
	 */
	public abstract void disable();
	
	@Override
	public void onEnable() {
		super.onEnable();
		enable();
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		disable();
	}
	
	/**
	 * Enregistre une liste de commande dans le serveur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param commands
	 * 2019-10-04
	 */
	public void loadCommands(Command... commands) {
		for(Command command : commands) {
			CommandMap map = this.getServer().getCommandMap();
			map.register(command.getName(), "", command);
		}
	}
	
	/**
	 * Enregistre une liste d'écouteur dans le serveur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param listeners
	 * 2019-10-04
	 */
	public void loadListeners(Listener... listeners) {
		for (Listener listener : listeners) {
			this.getServer().getPluginManager().registerEvents(listener, this);
		}
	}
	
	
	
	
}
