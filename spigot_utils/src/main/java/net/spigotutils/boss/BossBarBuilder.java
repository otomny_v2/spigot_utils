package net.spigotutils.boss;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

/**
 * Buildeur pour créer facilement des bossBars
 * @author fabien
 * @date 20/10/2019
 *
 */
public class BossBarBuilder {

	private transient BossBar bossBar;
	private String title; // Le titre de la bossbar
	private BarColor barColor; // La couleur de la bossBar
	private BarStyle barStyle; // Le style de la bossBar
	private Set<BarFlag> flags; // Les flags de la bossBar
	private boolean visible; // La visibilité de la bossBar
	
	/**
	 * Constructeur
	 * @author fabien
	 * @date 20/10/2019
	 * @param title
	 */
	public BossBarBuilder(String title) {
		this.title = title;
		this.flags = new HashSet<>();
		this.visible = true;
	}

	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @param invis
	 * @return
	 */
	public BossBarBuilder invinsible(boolean invis) {
		this.visible = !invis;
		return this;
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @param color
	 * @return
	 */
	public BossBarBuilder color(BarColor color) {
		this.barColor = color;
		return this;
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @param style
	 * @return
	 */
	public BossBarBuilder style(BarStyle style) {
		this.barStyle = style;
		return this;
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @return
	 */
	public BossBarBuilder darkSky() {
		return flags(BarFlag.DARKEN_SKY);
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @param barFlag
	 * @return
	 */
	public BossBarBuilder flags(BarFlag barFlag) {
		this.flags.add(barFlag);
		return this;
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @param players
	 */
	public void send(Player... players) {
		BossBar bossBar = getOrCreate();
		bossBar.setVisible(visible);
		for(Player player : players) {
			bossBar.addPlayer(player);
		}
	}
	
	/**
	 * 
	 * @author fabien
	 * @date 20/10/2019
	 * @return
	 */
	public BossBar getOrCreate() {
		if(this.bossBar == null) {
			BossBar bossBar = Bukkit.createBossBar(this.title,
					this.barColor,
					this.barStyle,
					this.flags.toArray(new BarFlag[this.flags.size()]));
			bossBar.setVisible(this.visible);
			this.bossBar = bossBar;
		}
		return this.bossBar;
	}

}
