package net.spigotutils.boss;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.Validate;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import net.spigotutils.utils.number.NumberUtils;

/**
 * Représente une boss bar qui va durée dans le temps
 * 
 * @author fabien
 * @date 20/10/2019
 *
 */
public class TimedBossBar {

	protected Plugin plugin;
	protected BossBarBuilder builder;
	protected int stayInSeconds;
	protected boolean relativePercentage;
	protected float rawPerc;
	@Getter protected boolean started;
	/**
	 * Si positionné à true la barre ne disparait pas à la fin du décompte
	 */
	protected boolean sticky;
	protected AtomicInteger currentTime;

	public TimedBossBar(Plugin plugin) {
		Validate.notNull(plugin, "Le plugin ne peut être null");
		this.plugin = plugin;
		this.relativePercentage = false;
		this.rawPerc = 0.5F;
		this.started = false;
		this.currentTime = new AtomicInteger(0);
		this.sticky = false;
	}

	public BossBar startAndSend(Player... players) {
		Validate.notNull(this.builder, "Le BossBarBuilder ne peut être null");
		final BossBar bar = this.builder.getOrCreate();
		if(this.started) {
			return bar;
		}
		if (relativePercentage) {
			bar.setProgress(0);
		} else {
			bar.setProgress(this.rawPerc);
		}
		this.builder.send(players);
		this.started = true;
		new BukkitRunnable() {
			@Override
			public void run() {
				float currentProgress = NumberUtils.map(currentTime.get(), 0, TimedBossBar.this.stayInSeconds, 0F, 1F);
				bar.setProgress(currentProgress);
				if (currentTime.incrementAndGet() >= stayInSeconds) {
					if (!sticky) bar.removeAll();
					else bar.setProgress(1.0F);
					cancel();
				}
			}
		}.runTaskTimer(this.plugin, 0, 20L);
		return bar;
	}
	
	public void send(Player... players) {
		this.builder.send(players);
	}

	public TimedBossBar bossBar(BossBarBuilder builder) {
		this.builder = builder;
		return this;
	}

	public TimedBossBar rawPercentage(float raw) {
		if (raw < 0.0 && raw > 1.0) {
			raw = 0.5F;
		}
		this.rawPerc = raw;
		return this;
	}

	public TimedBossBar relativePercentage(boolean rel) {
		this.relativePercentage = rel;
		return this;
	}

	public TimedBossBar second(int seconds) {
		if(seconds < 2) seconds = 2;
		this.stayInSeconds = seconds;
		return this;
	}
	
	/**
	 * Permet de conserver la barre à la fin du décompte
	 * @author d-gardes
	 * @param sticky
	 * @return
	 */
	public TimedBossBar sticky(boolean sticky) {
		this.sticky = sticky;
		return this;
	}

}
