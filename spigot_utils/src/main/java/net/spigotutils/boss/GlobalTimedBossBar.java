package net.spigotutils.boss;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bukkit.Bukkit;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;

import net.spigotutils.utils.number.NumberUtils;

public class GlobalTimedBossBar extends TimedBossBar {

	private Queue<UUID> affected = new ConcurrentLinkedQueue<>();
	private BossBar bar;
	private @NotNull BukkitTask task;

	public GlobalTimedBossBar(Plugin plugin) {
		super(plugin);
	}

	/**
	 * @author Fabien CAYRE (Computer)
	 * @param players
	 * @return
	 * @date 03/08/2021
	 */
	@Override
	@Deprecated
	public BossBar startAndSend(Player... players) {
		throw new IllegalAccessError("Deprecated");
	}

	public void kill() {
		if (!this.started) {
			return;
		}
		this.task.cancel();
		this.task = null;
		this.started = false;
		bar.removeAll();
	}

	public void start() {
		if (this.started) {
			return;
		}
		this.sticky = false;
		this.started = true;
		this.bar = this.builder.getOrCreate();

		this.task = new BukkitRunnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					if (!affected.contains(player.getUniqueId())) {
						affected.add(player.getUniqueId());
						bar.addPlayer(player);
					}
				}
				for (UUID uuid : affected) {
					Player player = null;
					if ((player = Bukkit.getPlayer(uuid)) == null
							|| !player.isOnline()) {
						affected.remove(uuid);
						if (player != null)
							bar.removePlayer(player);
					}
				}

				float currentProgress = NumberUtils
						.map(currentTime.get(), 0, stayInSeconds, 0F, 1F);
				bar.setProgress(currentProgress);
				if (currentTime.incrementAndGet() >= stayInSeconds) {
					if (!sticky)
						bar.removeAll();
					else
						bar.setProgress(1.0F);
					cancel();
				}
			}
		}.runTaskTimer(this.plugin, 0, 20L);
	}
}
