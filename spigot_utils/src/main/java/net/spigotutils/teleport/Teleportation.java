package net.spigotutils.teleport;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Teleportation {

	private Location from;
	private Location to;
	@Builder.Default private int time = 3;
	@Builder.Default private boolean instant = true;
	@Builder.Default private boolean cancel = true;
	
	/**
	 * Téléporte un joueur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player Le joueur à téléporter
	 * @date 12/07/2020
	 */
	public void teleport(Player player) {
	}
	
}
