package net.spigotutils.locale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.locale.Language;

public class I18N {

	private static final Set<String> ISO_LANGUAGES = Set.of(Locale.getISOLanguages()).stream()
			.map(String::toLowerCase).collect(Collectors.toSet());
	private static I18N instance;

	public static String t(String lang, String key){
		return getInstance().getTranslation(lang, key);
	}

	/**
	 * Traduit via le fichier de langue la clé
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param key
	 * @return
	 * @date 29/12/2020
	 */
	public static String getTranslation(NamespacedKey key) {
		return getTranslation(key.getKey());
	}

	/**
	 * Traduit via le fichier de langue la clé
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param key
	 * @return
	 * @date 29/12/2020
	 */
	public static String getTranslation(String key) {
		return Language.getInstance().getOrDefault(key);
	}

	/**
	 * Return the instance of i18N
	 * 
	 * @return instance The instance
	 */
	public static I18N getInstance() {
		if (instance == null) {
			instance = new I18N();
		}
		return instance;
	}

	/**
	 * Map containing all translations
	 */
	@Getter
	private Map<String, Map<String, String>> translations = new HashMap<>();
	
	/**
	 * Enable or disable the language feature inside GuiTransform and all
	 */
	@Getter
	@Setter
	private boolean toggled;

	/**
	 * Use this function to get lang from a player
	 * By default, it will always returns "en"
	 */
	@Getter
	@Setter
	private Function<Player, String> langProvider = (p -> "en");

	private I18N() {
		this.translations = new HashMap<>();
	}

	/**
	 * Return the translation of the specific key for a specific lang
	 * 
	 * @param lang The lang
	 * @param key  The key
	 * @return "no_lang" if lang not found, "no_translation" if translation not
	 *         found or the translation
	 */
	public String getTranslation(String lang, String key) {
		if (!this.translations.containsKey(lang))
			return "no_lang";
		if (!this.translations.get(lang).containsKey(key)) {
			return "no_translation";
		}
		return this.translations.get(lang).get(key);
	}

	/**
	 * Append translations to the existing one
	 * 
	 * @param lang         The lang
	 * @param translations Map of key (representing the key to get the translation)
	 *                     and the value (the translation)
	 * @throws IllegalArgumentException if {lang} param is not ISO 3166 compliant
	 */
	public void appendTranslation(String lang, Map<String, String> translations) {
		lang = lang.toLowerCase();
		if (!ISO_LANGUAGES.contains(lang)) {
			throw new IllegalArgumentException("Lang is not ISO compliant (value=" + lang + ")");
		}
		if (this.translations.containsKey(lang)) {
			this.translations.computeIfPresent(lang, (key, value) -> {
				value.putAll(translations);
				return value;
			});
		} else {
			this.translations.put(lang, translations);
		}
	}

  public void clear() {
		this.translations.clear();
  }

}
