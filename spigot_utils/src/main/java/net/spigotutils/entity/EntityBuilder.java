package net.spigotutils.entity;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

/**
 * Permet de construire une entité
 * @author ComminQ_Q (Computer)
 *
 * @date 07/10/2019
 */
public class EntityBuilder {

	private Location location; // Position de l'entitée dans l'espace
	private EntityType entityType; // type d'entitée construite
	private String displayName; // Nom de l'entitée
	private boolean invincible; // Invincibilité
	private boolean ai; // Active l'intelligence artificielle
	
	public EntityBuilder() {
		this.location = null;
		this.displayName = null;
		this.entityType = EntityType.PIG;
		this.invincible = false;
		this.ai = false;
	}
	
	/**
	 * 
	 * @param ai
	 * @return
	 */
	public EntityBuilder ai(boolean ai) {
		this.ai = ai;
		return this;
	}
	
	/**
	 * 
	 * @param invincible
	 * @return
	 */
	public EntityBuilder invincible(boolean invincible) {
		this.invincible = invincible;
		return this;
	}
	
	/**
	 * 
	 * @param entityType
	 * @return
	 */
	public EntityBuilder entity(EntityType entityType) {
		this.entityType = entityType;
		return this;
	}
	
	/**
	 * 
	 * @param location Position de l'entité
	 * @return l'instance courrante
	 */
	public EntityBuilder location(Location location) {
		this.location = location;
		return this;
	}
	
	/**
	 * 
	 * @param name Le nom d'affichage de l'entité
	 * @return l'instance courrante
	 */
	public EntityBuilder displayName(String name) {
		this.displayName = name;
		return this;
	}
	
	/**
	 * 
	 * @return l'entité créer à partir des paramètres passé dans le builder
	 */
	public Entity get() {
		Validate.notNull(this.location, "La position ne peux être nulle");
		Validate.notNull(this.entityType, "Le type d'entitée ne peux être nulle");
		Entity entity = this.location.getWorld()
							.spawnEntity(this.location, this.entityType);
		if(this.displayName != null) {
			entity.setCustomNameVisible(true);
			entity.setCustomName(this.displayName);
		}
		entity.setInvulnerable(this.invincible);
		if(entity instanceof LivingEntity) {
			LivingEntity livingEntity = (LivingEntity) entity;
			livingEntity.setAI(this.ai);
		}
		//TODO faire les checks pour les AI, invisible ET invincible
		return entity;
	}
	
	
}
