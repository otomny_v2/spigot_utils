package net.spigotutils.config;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import net.spigotutils.config.savable.ConfigSavable;

/**
 * Gère tous les fichiers de configuration de tous les plugins (.yml)
 * @author ComminQ_Q (Computer)
 *
 * @date 07/11/2019
 */
public class ConfigManager {

	private static ConfigManager instance; // Le singleton de l'instance
	
	/**
	 * Renvoie l'instance (ou là créer si elle n'existe pas)
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 07/11/2019
	 */
	public static synchronized ConfigManager getInstance() {
		if(instance == null) {
			instance = new ConfigManager();
		}
		return instance;
	}
	
	private List<ConfigFile> configFile; // La liste des fichiers de config
	
	/**
	 * Constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 07/11/2019
	 */
	public ConfigManager() {
		this.configFile = new ArrayList<>();
	}
	
	/**
	 * Charge un fichier .yml qui est intégré à l'archive .jar dans le répertoire /plugins/(NomDuPlugin)/
	 * @author ComminQ_Q (Computer)
	 *
	 * @param plugin
	 * @param fileName
	 * @return
	 * @date 07/11/2019
	 */
	public PluginConfigFile loadPluginConfigFile(Plugin plugin, String fileName) {
		PluginConfigFile pluginConfigFile = new PluginConfigFile(plugin, fileName, null);
		this.configFile.add(pluginConfigFile);
		return pluginConfigFile;
	}
	
	/**
	 * Créer un fichier .yml avec les paramètres par défaut dans le répertoire /plugins/(NomDuPlugin)/
	 * ou lit le fichier existant
	 * @author ComminQ_Q (Computer)
	 *
	 * @param plugin
	 * @param fileName
	 * @return
	 * @date 07/11/2019
	 */
	public BasicConfigFile loadBasicConfigFile(Plugin plugin, String fileName) {
		BasicConfigFile basicConfigFile = new BasicConfigFile(plugin, fileName, null);
		this.configFile.add(basicConfigFile);
		return basicConfigFile;
	}
	
	public ConfigurationSection getOrCreateConfigurationSection(String path, YamlConfiguration yamlConfiguration) {
		if(yamlConfiguration.isConfigurationSection(path)) {
			return yamlConfiguration.getConfigurationSection(path);
		}
		return yamlConfiguration.createSection(path);
	}
	
	public <T extends ConfigSavable> boolean saveToConfig(String fileName, String path, Plugin plugin, T param) {
		ConfigFile configFile = getConfigFile(fileName, plugin);
		if(configFile == null) {
			throw new NullPointerException("Le fichier n'existe pas");
		}
		YamlConfiguration yaml = configFile.getConfiguration();
		if(yaml == null) {
			throw new NullPointerException("Ce n'est pas un fichier de config");
		}
		return false;
	}
	
	/**
	 * Charge un {@link ConfigSavable} depuis un fichier fileName et depuis le chemin path
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T> le type de donnée qui étend {@link ConfigSavable}
	 * @param fileName le fichier ou chercher l'instance
	 * @param path le chemin dans le fichier .yml
	 * @return
	 * @date 07/11/2019
	 */
	@SuppressWarnings("unchecked")
	public <T extends ConfigSavable> T getObjFromConfig(String fileName, String path, Plugin plugin){
		ConfigFile configFile = getConfigFile(fileName, plugin);
		if(configFile == null) {
			throw new NullPointerException("Le fichier n'existe pas");
		}
		YamlConfiguration yaml = configFile.getConfiguration();
		if(yaml == null) {
			throw new NullPointerException("Ce n'est pas un fichier de config");
		}
		if(!yaml.contains(path)) {
			throw new NullPointerException("Il n'y a pas de chemin avec ce nom ("+path+")");
		}
		String classPath = path+".clazz";
		if(!yaml.contains(classPath)) {
			throw new NullPointerException("Il n'y a pas de chemin avec vers la classe ("+path+".clazz)");
		}
		try {
			Class<T> klazzT = (Class<T>) Class.forName(classPath);
			Constructor<T> constructor = klazzT.getConstructor(Document.class);
			Document savable = new Document();
			for(String string : yaml.getConfigurationSection(path).getKeys(false)) {
				if(!string.equals("clazz")) {
					savable.append(string, yaml.getConfigurationSection(path).get(string));
				}
			}
			return constructor.newInstance(savable);
		}catch (NoSuchMethodException e) {
			throw new RuntimeException("Il n'y a pas de constructeur déclaré avec un paramètre de type Document");
		}catch (ClassCastException e) {
			throw new RuntimeException("La classe n'étend pas ConfigSavable");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	/**
	 * Renvoie un objet de configuration lié au nom du fichier
	 * @author ComminQ_Q (Computer)
	 *
	 * @param fileName le nom du fichier
	 * @param plugin le plugin associé
	 * @return
	 * @date 07/11/2019
	 */
	public ConfigFile getConfigFile(String fileName, Plugin plugin) {
		if(!fileName.endsWith(".yml")) {
			fileName+=".yml";
		}
		final String finalName = fileName;
		return this.configFile
				.stream()
				.filter(config -> config.getFileName().equals(finalName) && config.getPlugin().getName().equals(plugin.getName()))
				.findFirst().orElse(null);
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return toutes les instances de fichiers de configuration
	 * @date 07/11/2019
	 */
	public List<ConfigFile> getConfigFile() {
		return configFile;
	}


	
}
