package net.spigotutils.config.loader;

import java.io.IOException;
import java.util.Map;

import org.bukkit.plugin.Plugin;

public abstract class MapConfigLoader<K, V> extends ConfigLoader{

	public MapConfigLoader(Plugin plugin, String file) throws IOException {
		super(plugin, file);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return La liste chargé
	 * @date 28/11/2019
	 */
	public abstract Map<V, K> getList();
	
}
