package net.spigotutils.config.loader;

import java.io.IOException;
import java.util.List;

import org.bukkit.plugin.Plugin;

public abstract class ListConfigLoader<T> extends ConfigLoader{

	public ListConfigLoader(Plugin plugin, String file) throws IOException {
		super(plugin, file);
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @return La liste chargé
	 * @date 28/11/2019
	 */
	public abstract List<T> getList();
	
}
