package net.spigotutils.config.loader;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.apache.commons.lang.Validate;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public abstract class ConfigLoader {

	protected YamlConfiguration yaml; // Donnée YAML
	protected Plugin plugin; // Plugin associé pour chargé
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param plugin Le plugin pour récupérer le dossier des données
	 * @param file le fichier à charger
	 * @throws IOException
	 * @date 27/11/2019
	 */
	public ConfigLoader(Plugin plugin, String file) throws IOException {
		Validate.notNull(plugin);
		File dataFolder = plugin.getDataFolder();
		if(!dataFolder.exists()) {
			dataFolder.createNewFile();
		}
		File yamlFile = new File(dataFolder, file+".yml");
		if(!yamlFile.exists()) {
			plugin.getLogger().log(Level.WARNING, "Fichier inconnu, création...");
			yamlFile.createNewFile();
			plugin.getLogger().log(Level.FINE, "Fichier créé!");
		}
		this.yaml = YamlConfiguration.loadConfiguration(yamlFile);
		this.plugin = plugin;
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param path
	 * @return La section de config lié au path
	 * @date 29/11/2019
	 */
	protected ConfigurationSection getConfigSection(String path) {
		return this.yaml.getConfigurationSection(path);
	}
}