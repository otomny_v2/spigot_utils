package net.spigotutils.config.savable;

import net.mongoutils.Documentable;


/**
 * Représente un objet pouvant être sauvegarder dans une config '.yml'
 * Besoin d'implémenter un constructeur avec un Document
 * <pre>
 * 
 * public ConfigSavable(Document document)
 * 
 * </pre>
 * @author ComminQ_Q (Computer)
 *
 * @date 07/11/2019
 */
public interface ConfigSavable extends Documentable{

	/**
	 * Renvoie la classe qui devra être instancier
	 * @author ComminQ_Q (Computer)
	 *
	 * @return
	 * @date 07/11/2019
	 */
	public Class<? extends ConfigSavable> getSavableClass();
	
}
