package net.spigotutils.config;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public abstract class ConfigFile {

	protected YamlConfiguration configuration;
	protected Plugin plugin;
	protected String filename;
	
	public ConfigFile(Plugin plugin, String filenamee, YamlConfiguration configuration) {
		this.configuration = configuration;
		this.plugin = plugin;
		if(!filenamee.contains(".yml")) {
			filenamee +=".yml";
		}
		this.filename = filenamee;
	}
	
	public abstract void load();
	
	public void reload() {
		this.load();
	}
	
	public abstract void save();
	
	public Plugin getPlugin() {
		return plugin;
	}
	
	public String getFileName() {
		return filename;
	}

	public YamlConfiguration getConfiguration() {
		return configuration;
	}
}