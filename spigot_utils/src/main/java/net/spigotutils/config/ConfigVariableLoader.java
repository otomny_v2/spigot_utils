package net.spigotutils.config;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * Loader du fichier de config de Core-Otomny
 * @author ComminQ_Q (Computer)
 *
 */
public class ConfigVariableLoader {
	
	protected Plugin plugin;
	protected String configFilename;
	
	public ConfigVariableLoader(Plugin plugin, String configFilename) {
		this.plugin = plugin;
		this.configFilename = configFilename;
		ConfigManager.getInstance().loadBasicConfigFile(this.plugin, this.configFilename);
	}
	
	public ConfigVariableLoader(Plugin plugin) {
		this(plugin, "config");
	}
	
	public void init() {
		setDefaultIfMissing();
	}
	
	/**
	 * Si un champ est manquant on se rabat sur la config par défaut.
	 * @author Dorian-PRO
	 * @date 03/05/2020
	 */
	private void setDefaultIfMissing() {
		YamlConfiguration defaultConfig = new PluginConfigFile(this.plugin, this.configFilename, null).getConfiguration();
		YamlConfiguration currentConfig = getYmlConfig();
		
		for (String key : defaultConfig.getKeys(true)) {
			if (!currentConfig.isSet(key)) {
				currentConfig.set(key, defaultConfig.get(key));
				this.plugin.getLogger().warning(
						String.format("La valeur %s est manquante dans le fichier de config. Par défaut: %s",
								key,
								defaultConfig.get(key)));
			}
		}
	}
	
	public void hasConfig(String... keysName) throws Exception{
		for(String keyName : keysName) {
			if (!this.has(keyName)) {
				throw new Exception("La config ne contient pas le champ \""+keyName+"\"");
			}
		}
	}
	
	public boolean has(String keyName) {
		return getYmlConfig().contains(keyName);
	}
	
	public <T> void write(T value, String keyName){
		getYmlConfig().set(keyName, value);
	}
	
	public <T> T read(String keyName) {
		return read(keyName, null);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T read(String keyName, T defaultValue) {
		T value = (T) getYmlConfig().get(keyName);
		return value == null ? defaultValue : value;
	}
	
	public void save() {
		getConfig().save();
	}
	
	public YamlConfiguration getYmlConfig() {
		return getConfig().getConfiguration();
	}
	
	public ConfigFile getConfig() {
		return ConfigManager.getInstance().getConfigFile(this.configFilename, this.plugin);
	}
}
