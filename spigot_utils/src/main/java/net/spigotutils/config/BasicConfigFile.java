package net.spigotutils.config;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class BasicConfigFile extends ConfigFile {
	
	private File file;

	public BasicConfigFile(Plugin plugin, String filename, YamlConfiguration configuration) {
		super(plugin, filename, configuration);
		this.load();
	}

	@Override
	public void load() {

		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		
		this.file = new File(plugin.getDataFolder(), this.filename);
		if (!this.file.exists()) {
			plugin.getLogger().warning("Fichier de configuration introuvable fallback sur les paramètres par défaut");
			try {
				this.file.createNewFile();
				plugin.saveResource(this.filename, true);
				this.file = new File(plugin.getDataFolder(), this.filename);
				this.configuration = YamlConfiguration.loadConfiguration(this.file);
			} catch (Exception e) {
				plugin.getLogger().severe("Impossible de créer le fichier de configuration.");
				this.configuration = new PluginConfigFile(this.plugin, this.filename, null).getConfiguration();
				e.printStackTrace();
			}
		} else this.configuration = YamlConfiguration.loadConfiguration(this.file);
	}
	
	@Override
	public void save() {
		try {
			this.configuration.save(this.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
