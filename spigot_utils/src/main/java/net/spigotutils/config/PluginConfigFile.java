package net.spigotutils.config;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class PluginConfigFile extends ConfigFile {

	public PluginConfigFile(Plugin plugin, String filename, YamlConfiguration configuration) {
		super(plugin, filename, configuration);
		this.load();
	}

	@Override
	public void load() {
		
		Reader stream = null;
		try {
			stream = new InputStreamReader(this.plugin.getClass().getResourceAsStream("/"+this.filename), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (stream != null) {
			this.configuration = YamlConfiguration.loadConfiguration(stream);
			this.configuration.options().copyDefaults(true);
		}
	}
	
	@Override
	public void save() {}
}
