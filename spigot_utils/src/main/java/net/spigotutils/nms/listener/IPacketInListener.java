package net.spigotutils.nms.listener;

import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ServerGamePacketListener;

public interface IPacketInListener<T extends Packet<ServerGamePacketListener>> extends IPacketListener<T>{

	boolean in(T packet);
	
}
