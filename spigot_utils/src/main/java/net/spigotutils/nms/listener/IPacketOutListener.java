package net.spigotutils.nms.listener;

import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;

public interface IPacketOutListener<T extends Packet<ClientGamePacketListener>> extends IPacketListener<T>{

	boolean out(T packet);
}
