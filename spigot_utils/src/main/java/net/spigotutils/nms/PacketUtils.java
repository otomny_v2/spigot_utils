package net.spigotutils.nms;

import java.util.Collection;

import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundDisconnectPacket;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 18/11/2019
 */
public class PacketUtils {
	
	private PacketUtils() {}

	public static void setLocation(
		Entity sPlayer, double x, double y, double z, 
			float pitch, float yaw){
		sPlayer.setPos(x,y,z);
		sPlayer.setXRot(pitch);
		sPlayer.setYRot(yaw);
	}

	public static void setLocation(
		ServerPlayer sPlayer, double x, double y, double z, 
			float pitch, float yaw){
		sPlayer.setPos(x,y,z);
		sPlayer.setXRot(pitch);
		sPlayer.setYRot(yaw);
	}

	public static byte toRawYaw(float yaw) {
		return (byte) (yaw * 256F / 360F);
	}

	public static short rawLocation(double old, double neww){
		return (short) ((neww * 32 - old * 32) * 128);
	}

	public static int toRawLocation(double pos) {
		return Mth.floor((pos * 32.0));
	}

	public static short toRawLocationShort(double pos) {
		return (short) Mth.floor((pos * 32.0));
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @return
	 * @date 18/11/2019
	 */
	public static ServerGamePacketListenerImpl playerConnection(Player player) {
		return ((CraftPlayer) player).getHandle().connection;
	}

	/**
	 * 
	 * @param player
	 * @return
	 */
	public static Connection networkManager(Player player){
		return playerConnection(player).getConnection();
	}
	
	/**
	 * 
	 * @param player
	 * @return
	 */
	public static Channel channel(Player player){
		return networkManager(player).channel;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @param message
	 * @date 18/11/2019
	 */
	public static void kickPlayerAsync(Player player, String message) {
		sendPacket(player, new ClientboundDisconnectPacket(ChatSerializerUtils.toComponent(message)));
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @param packet
	 * @date 18/11/2019
	 */
	public static void sendPacket(Player player, Packet<?> packet) {
		playerConnection(player).send(packet);
	}
	
	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @param packet
	 * @date 18/11/2019
	 */
	public static void sendPacket(Player player, Collection<? extends Packet<?>> packets) {
		packets.stream().forEach(packet -> sendPacket(player, packet));
	}


}
