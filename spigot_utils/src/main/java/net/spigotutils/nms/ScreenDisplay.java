package net.spigotutils.nms;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.api.process.TaskDispatcher;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.protocol.game.ClientboundRemoveEntitiesPacket;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.network.protocol.game.ClientboundTeleportEntityPacket;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.spigotutils.chat.ColorUtils;
import net.spigotutils.utils.number.NumberUtils;
import net.spigotutils.utils.number.TimeFunction;

/**
 * In french and IDC
 */
public class ScreenDisplay {

	/**
	 * @param player
	 *          Le joueur
	 * @param content
	 *          Le contenu du texte a afficher
	 * @param plugin
	 *          Le plugin
	 */
	public static void displayOnScreen(Player player,
			String content, Plugin plugin) {
		displayOnScreen(player, 1.75, 2, 0.1, 20, content, 0.5,
				plugin, 0L);
	}

	/**
	 * @param player
	 *          Le joueur
	 * @param distance
	 *          La distance entre la position et l'écran
	 * @param width
	 * @param height
	 * @param duration
	 * @param content
	 * @param y
	 * @param plugin
	 * @param delay
	 */
	public static void displayOnScreen(Player player,
			double distance, double width, double height,
			long duration, String content, double y, Plugin plugin,
			long delay) {
		var rayTrace = player.rayTraceBlocks(distance);
		if (rayTrace != null && rayTrace.getHitBlock() != null) {
			final double oldDistance = distance;
			distance = rayTrace.getHitPosition()
					.distance(player.getEyeLocation().toVector());
			distance = Math.min(distance, oldDistance);
		}

		var location = player.getEyeLocation();
		final Location randomLocation = createOnScreen(location,
				distance, width, height);

		show(randomLocation, content, player, duration, y, plugin,
				delay);
	}

	/**
	 * @param location
	 *          Position du texte
	 * @param content
	 *          Contenu du texte
	 * @param player
	 *          Le joueur à qui afficher le texte
	 * @param duration
	 *          la durée (en ticks) du texte
	 * @param y
	 *          Hauteur ajouté au texte à la fin de l'animation
	 * @param plugin
	 *          Plugin
	 * @param delay
	 *          Le délai (en ticks)
	 */
	public static void show(Location location, String content,
			Player player, long duration, double y, Plugin plugin,
			long delay) {

		TaskDispatcher.run(() -> {
			final Location textLocation = location;
			var craftWorld = (CraftWorld) location.getWorld();
			var level = craftWorld.getHandle();

			final int id = NumberUtils.random(30000, 60000);
			ArmorStand armor = new ArmorStand(level, 0, 0, 0);

			armor.setPos(textLocation.getX(), textLocation.getY(),
					textLocation.getZ());
			armor.setNoBasePlate(true);
			armor.setSmall(true);
			armor.setMarker(true);

			armor.setCustomName(CraftChatMessage
					.fromStringOrNull(ColorUtils.color(content)));

			armor.setCustomNameVisible(true);
			armor.setInvisible(true);
			armor.setNoGravity(true);
			armor.setId(id);

			var addEntity = new ClientboundAddEntityPacket(armor);
			var teleport = new ClientboundTeleportEntityPacket(armor);
			var setData = new ClientboundSetEntityDataPacket(id,
					armor.getEntityData(), true);
			PacketUtils.sendPacket(player, addEntity);
			PacketUtils.sendPacket(player, teleport);
			PacketUtils.sendPacket(player, setData);

			new BukkitRunnable() {
				double localDuration = 0;

				@Override
				public void run() {
					if (localDuration > duration) {
						PacketUtils.sendPacket(player,
								new ClientboundRemoveEntitiesPacket(id));
						cancel();

					} else {
						double endDuration = duration;

						double animationProgression = TimeFunction
								.easeOutQuart(NumberUtils.map(localDuration, 0,
										endDuration, 0D, 1D));

						armor.setPos(textLocation.getX(),
								textLocation.getY() + animationProgression * y,
								textLocation.getZ());
						PacketUtils.sendPacket(player,
								new ClientboundTeleportEntityPacket(armor));
						localDuration++;
					}
				}
			}.runTaskTimerAsynchronously(plugin, delay, 1);
		});
	}

	/**
	 * Génère une position aléatoire sur un écran d'une
	 * certaines taillesà une distance d'un joueur
	 *
	 * @param location
	 *          Position (avec la rotation)
	 * @param distance
	 *          La distance entre la position et l'écran
	 * @param width
	 *          La largeur de l'écran
	 * @param height
	 *          La hauteur de l'écran
	 * @return La position générée
	 */
	public static Location createOnScreen(Location location,
			double distance, double width, double height) {

		double theta = Math.atan(0.5 * height / distance);
		double omega = Math.atan(0.5 * width / distance);

		float randomTheta = (float) NumberUtils.random(-theta,
				theta);
		float randomOmega = (float) NumberUtils.random(-omega,
				omega);

		// float positionYaw = location.getYaw(); // y rot
		// float positionPitch = location.getPitch(); // x rot

		Vector relative = location.getDirection().clone()
				.rotateAroundX(randomTheta).rotateAroundY(randomOmega)
				.normalize().multiply(distance);

		// float textYaw = positionYaw + randomOmega;
		// float textPitch = positionPitch + randomTheta;

		// double x = Math.sin(Math.toRadians(textYaw)) * distance +
		// location.getX(); double y =
		// Math.sin(Math.toRadians(textPitch)) *
		// distance + location.getY(); double z =
		// Math.cos(Math.toRadians(textYaw))
		// * distance + location.getZ();

		return location.clone().add(relative);
	}
}
