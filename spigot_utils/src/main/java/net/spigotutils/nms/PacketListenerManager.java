package net.spigotutils.nms;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ServerGamePacketListener;
import net.spigotutils.nms.ionetty.InHandlerNetty;
import net.spigotutils.nms.ionetty.OutHandlerNetty;
import net.spigotutils.nms.listener.IPacketInListener;
import net.spigotutils.nms.listener.IPacketOutListener;

public class PacketListenerManager {

	private static PacketListenerManager instance;

	public static PacketListenerManager getInstance() {
		if (instance == null) {
			instance = new PacketListenerManager();
		}
		return instance;
	}

	public static <T extends Packet<ServerGamePacketListener>> void registerIn(
		Player player,
		Class<T> clazz,
		IPacketInListener<T> in) {
		registerIn(player, clazz, "def", in);
	}
	
	public static <T extends Packet<ServerGamePacketListener>> void registerIn(
		Player player,
		Class<T> clazz,
		String key,
		IPacketInListener<T> in) {
		UUID uuid = player.getUniqueId();
		if (instance.inListeners.containsKey(uuid)) {
			if(instance.inListeners.get(uuid).containsKey(clazz)) {
				instance.inListeners.get(uuid).get(clazz).put(key, in);
			}else {
				Map<String, IPacketInListener<?>> map = new ConcurrentHashMap<>();
				map.put(key, in);
				instance.inListeners.get(uuid).put(clazz, map);
			}
		} else {
			Map<Class<? extends Packet<ServerGamePacketListener>>, Map<String, IPacketInListener<?>>> map = new ConcurrentHashMap<>();
			Map<String, IPacketInListener<?>> smap = new ConcurrentHashMap<>();
			smap.put(key, in);
			map.put(clazz, smap);
			instance.inListeners.put(uuid, map);
			// Enregistrement d'un tiers pour écouter le flux de packet
			InHandlerNetty inNetty = new InHandlerNetty(uuid);
			Channel channel = PacketUtils.channel(player);
			instance.nettyInHandler.put(uuid, inNetty);
			channel.pipeline().addBefore("packet_handler", "packet_r_in", inNetty);
		}
	}

	public static <T extends Packet<ClientGamePacketListener>> void registerOut(
		Player player,
		Class<T> clazz,
		IPacketOutListener<T> out) {
		registerOut(player, clazz, "def", out);
	}
	
	public static <T extends Packet<ClientGamePacketListener>> void registerOut(
		Player player,
		Class<T> clazz,
		String key,
		IPacketOutListener<T> out) {
		UUID uuid = player.getUniqueId();
		if (instance.outListeners.containsKey(uuid)) {
			if(instance.outListeners.get(uuid).containsKey(clazz)) {
				instance.outListeners.get(uuid).get(clazz).put(key, out);
			}else {
				Map<String, IPacketOutListener<?>> map = new ConcurrentHashMap<>();
				map.put(key, out);
				instance.outListeners.get(uuid).put(clazz, map);
			}
		} else {
			Map<Class<? extends Packet<ClientGamePacketListener>>, Map<String, IPacketOutListener<?>>> map = new ConcurrentHashMap<>();
			Map<String, IPacketOutListener<?>> smap = new ConcurrentHashMap<>();
			smap.put(key, out);
			map.put(clazz, smap);
			instance.outListeners.put(uuid, map);
			// Enregistrement d'un tiers pour écouter le flux de packet
			OutHandlerNetty outNetty = new OutHandlerNetty(uuid);
			Channel channel = PacketUtils.channel(player);
			instance.nettyOutHandler.put(uuid, outNetty);
			channel.pipeline().addBefore("packet_handler", "packet_r_out", outNetty);
		}
	}

	public static void disconnect(Player player) {
		instance.inListeners.remove(player.getUniqueId());
		instance.nettyInHandler.remove(player.getUniqueId());
		instance.outListeners.remove(player.getUniqueId());
		instance.nettyOutHandler.remove(player.getUniqueId());
	}

	public static void clearIn(Player player) {
		instance.inListeners.remove(player.getUniqueId());

		InHandlerNetty nettyIn = instance.nettyInHandler.remove(player.getUniqueId());
		Channel channel = PacketUtils.channel(player);
		channel.pipeline().remove(nettyIn);
	}

	public static void clearOut(Player player) {
		instance.outListeners.remove(player.getUniqueId());

		OutHandlerNetty nettyOut = instance.nettyOutHandler.remove(player.getUniqueId());
		Channel channel = PacketUtils.channel(player);
		channel.pipeline().remove(nettyOut);
	}

	public static void clearIn(Player player, Class<? extends Packet<ServerGamePacketListener>> clazz) {
		UUID uuid = player.getUniqueId();
		if (instance.inListeners.containsKey(uuid)) {
			instance.inListeners.get(uuid).remove(clazz);
		}
	}

	public static void clearOut(Player player, Class<? extends Packet<ClientGamePacketListener>> clazz) {
		UUID uuid = player.getUniqueId();
		if (instance.outListeners.containsKey(uuid)) {
			instance.outListeners.get(uuid).remove(clazz);
		}
	}

	public static void clearIn(Player player, String key, Class<? extends Packet<ServerGamePacketListener>> clazz) {
		UUID uuid = player.getUniqueId();
		if (instance.inListeners.containsKey(uuid)) {
			instance.inListeners.get(uuid).get(clazz).remove(key);
		}
	}

	public static void clearOut(Player player, String key, Class<? extends Packet<ClientGamePacketListener>> clazz) {
		UUID uuid = player.getUniqueId();
		if (instance.outListeners.containsKey(uuid)) {
			instance.outListeners.get(uuid).get(clazz).get(key);
		}
	}
	
	public static boolean handleOut(UUID uuid, Packet<ClientGamePacketListener> outPacket) {
		return instance._handleOut(uuid, outPacket);
	}

	public static boolean handleIn(UUID uuid, Packet<ServerGamePacketListener> inPacket) {
		return instance._handleIn(uuid, inPacket);
	}

	private Map<UUID, Map<Class<? extends Packet<ServerGamePacketListener>>, Map<String, IPacketInListener<?>>>> inListeners;
	private Map<UUID, Map<Class<? extends Packet<ClientGamePacketListener>>, Map<String, IPacketOutListener<?>>>> outListeners;

	private Map<UUID, InHandlerNetty> nettyInHandler;
	private Map<UUID, OutHandlerNetty> nettyOutHandler;

	public PacketListenerManager() {
		this.inListeners = new ConcurrentHashMap<>();
		this.outListeners = new ConcurrentHashMap<>();
		this.nettyInHandler = new ConcurrentHashMap<>();
		this.nettyOutHandler = new ConcurrentHashMap<>();
	}

	private boolean _handleOut(UUID uuid, Packet<ClientGamePacketListener> outPacket) {
		boolean value = true;
		Map<Class<? extends Packet<ClientGamePacketListener>>, Map<String, IPacketOutListener<?>>> out = this.outListeners
			.get(uuid);
		if (!out.containsKey(outPacket.getClass())) {
			return value;
		}
		Map<String, IPacketOutListener<?>> listeners = out.get(outPacket.getClass());

		return listeners
			.entrySet()
			.stream()
			.map(entry -> ((IPacketOutListener<Packet<ClientGamePacketListener>>) entry.getValue()).out(outPacket))
			.reduce(true, (total, val) -> val && total);
	}

	private boolean _handleIn(UUID uuid, Packet<ServerGamePacketListener> inPacket) {
		boolean value = true;
		Map<Class<? extends Packet<ServerGamePacketListener>>, Map<String, IPacketInListener<?>>> in = this.inListeners
			.get(uuid);
		if (!in.containsKey(inPacket.getClass())) {
			return value;
		}
		Map<String, IPacketInListener<?>> listeners = in.get(inPacket.getClass());

		return listeners
			.entrySet()
			.stream()
			.map(entry -> ((IPacketInListener<Packet<ServerGamePacketListener>>) entry.getValue()).in(inPacket))
			.reduce(true, (total, val) -> val && total);
	}

}
