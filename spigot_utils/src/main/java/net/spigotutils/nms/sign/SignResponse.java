package net.spigotutils.nms.sign;

import lombok.Getter;

public class SignResponse {

	public static SignResponse text(String text) {
		return new SignResponse(text);
	}
	
	public static SignResponse close() {
		return new SignResponse(null);
	}
	
	@Getter private String error;

	private SignResponse(String error) {
		super();
		this.error = error;
	}
	
	
	
}
