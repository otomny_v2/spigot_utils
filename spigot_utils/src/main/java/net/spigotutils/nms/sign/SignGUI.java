package net.spigotutils.nms.sign;

import java.util.function.Consumer;
import java.util.function.Function;

import org.bukkit.craftbukkit.v1_19_R1.block.CraftSign;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundBlockUpdatePacket;
import net.minecraft.network.protocol.game.ClientboundOpenSignEditorPacket;
import net.minecraft.network.protocol.game.ServerboundSignUpdatePacket;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.spigotutils.nms.PacketListenerManager;
import net.spigotutils.nms.PacketUtils;
import net.spigotutils.runnable.SpigotTask;

public class SignGUI {

	private Player player;
	private Function<String, SignResponse> complete;
	private Consumer<String> consum;
	private Plugin plugin;
	private String value;

	public SignGUI(Player player,
			Function<String, SignResponse> complete,
			Consumer<String> consum, Plugin plugin, String value) {
		this.player = player;
		this.complete = (s) -> {
			final BlockPos blockPosition = new BlockPos(
					player.getLocation().getBlockX(), 1,
					player.getLocation().getBlockZ());
			ClientboundBlockUpdatePacket packet = new ClientboundBlockUpdatePacket(
					blockPosition, Blocks.AIR.defaultBlockState());
			sendPacket(packet);
			return complete.apply(s);
		};
		this.consum = consum;
		this.plugin = plugin;
		this.value = value;
	}

	public void sendSignUpdate(String ligne1, String ligne2) {
		sendSignUpdate(ligne1, ligne2, "");
	}

	public void sendSignUpdate(String ligne1, String ligne2,
			String error) {
		packetSend(ligne1, ligne2, error);

		PacketListenerManager.registerIn(this.player,
				ServerboundSignUpdatePacket.class, "signui",
				updateSign -> {

					try {
						String[] updateLines = updateSign.getLines();
						String userInput = updateLines[0];
						SignResponse response = this.complete
								.apply(userInput);
						if (response.getError() == null) {
							PacketListenerManager.clearIn(this.player,
									"signui", ServerboundSignUpdatePacket.class);
							SpigotTask.runSync(this.plugin, () -> {
								this.consum.accept(userInput);
							});
							return false;
						} else {
							int maxLength = 20;
							if (response.getError().length() < maxLength) {
								maxLength = response.getError().length();
							}
							String cleanResponse = response.getError()
									.substring(0, maxLength);
							packetSend(ligne1, ligne2, cleanResponse);
						}
						return false;

					} catch (Exception e) {
						e.printStackTrace();
					}
					return false;
				});
	}

	private void packetSend(String ligne1, String ligne2,
			String error) {
		String[] lines = new String[4];
		lines[0] = this.value;
		lines[1] = ligne1;
		lines[2] = ligne2;
		lines[3] = "§c" + error;

		final BlockPos blockPosition = new BlockPos(
				player.getLocation().getBlockX(), 1,
				player.getLocation().getBlockZ());

		ClientboundBlockUpdatePacket packet = new ClientboundBlockUpdatePacket(
				blockPosition,
				Blocks.BIRCH_WALL_SIGN.defaultBlockState());
		sendPacket(packet);

		Component[] components = CraftSign.sanitizeLines(lines);
		SignBlockEntity sign = new SignBlockEntity(blockPosition,
				Blocks.BIRCH_WALL_SIGN.defaultBlockState());

		// Tricks
		System.arraycopy(components, 0, sign.messages, 0,
				sign.messages.length);
		sendPacket(sign.getUpdatePacket());

		ClientboundOpenSignEditorPacket openSignEditor = new ClientboundOpenSignEditorPacket(
				blockPosition);
		sendPacket(openSignEditor);
	}

	public void sendPacket(Packet<?> packet) {
		PacketUtils.sendPacket(this.player, packet);
	}

	public static class Builder {

		private String firstLine;
		private String secondLine;
		private String value;
		private Function<String, SignResponse> complete;
		private Consumer<String> consum;
		private Plugin plugin;

		public Builder(Plugin plugin) {
			this.plugin = plugin;
		}

		public Builder firstLine(String line) {
			this.firstLine = line;
			return this;
		}

		public Builder value(String value) {
			this.value = value;
			return this;
		}

		public Builder secondLine(String line) {
			this.secondLine = line;
			return this;
		}

		public Builder end(Consumer<String> consum) {
			this.consum = consum;
			return this;
		}

		public Builder onComplete(
				Function<String, SignResponse> complete) {
			this.complete = complete;
			return this;
		}

		public void open(Player player) {
			SignGUI gui = new SignGUI(player, complete, consum, plugin,
					value);
			player.closeInventory();
			gui.sendSignUpdate(firstLine, secondLine);
		}
	}

}
