package net.spigotutils.nms.fakeentity;

import org.bukkit.entity.Player;

public interface FakePlayerEffect {

  /**
   * Fonction executé chaque tick sur chaque entrée de l'entité
   * @param player
   */
  void effect(Player player);
}
