package net.spigotutils.nms.fakeentity;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import net.minecraft.network.protocol.game.ServerboundEntityTagQuery;
import net.minecraft.network.protocol.game.ServerboundInteractPacket;
import net.mongoutils.MongoUtils;
import net.spigotutils.nms.PacketListenerManager;
import net.spigotutils.nms.fakeentity.blocks.FakeBlock;
import net.spigotutils.nms.fakeentity.hologram.Hologram;
import net.spigotutils.nms.fakeentity.player.FakePlayer;
import net.spigotutils.nms.fakeentity.player.MultilineFakePlayer;
import net.spigotutils.nms.listener.IPacketInListener;
import net.spigotutils.runnable.SpigotTask;
import net.spigotutils.utils.reflection.ReflectionUtils;
import net.spigotutils.utils.stream.StreamUtils;

public class FakeEntityManager {

	private static final String COLLECTION = "fakeentity";
	private static double DISTANCE_THRESHOLD = 60.5f;
	private static FakeEntityManager instance;

	public static FakeEntityManager getInstance() {
		return instance;
	}

	private int beginId;
	@Getter
	private List<FakeEntity> fakeEntities = new CopyOnWriteArrayList<>();
	private Plugin plugin;

	public FakeEntityManager(Plugin plugin) {
		instance = this;
		this.beginId = 9999; // Pour pas qu'on envoie une erreur au
													// niveau du packet

		// Chargement des données depuis la BDD
		load();

		new BukkitRunnable() {
			@Override
			public void run() {
				FakeEntityManager.this.checkPlayers();
			}
		}.runTaskTimerAsynchronously(plugin, 0, 10L);
		new BukkitRunnable() {
			@Override
			public void run() {
				FakeEntityManager.this.playerEffects();
			}
		}.runTaskTimerAsynchronously(plugin, 0, 2L);
		this.plugin = plugin;
	}

	public void fillEntities(List<Integer> entities, int size) {
		for (int i = 0; i < size; i++) {
			entities.add(askEntity());
		}
	}

	public int askEntity() {
		if(this.beginId > 99999999)
			this.beginId = 9999;
		return this.beginId++;
	}

	public List<Hologram> getHolograms() {
		return StreamUtils.filterAndMap(this.fakeEntities,
				Hologram.class::isInstance, Hologram.class::cast);
	}

	public List<FakePlayer> getFakePlayers() {
		return StreamUtils.filterAndMap(this.fakeEntities,
				FakePlayer.class::isInstance, FakePlayer.class::cast);
	}

	public void playerJoin(Player player) {
		PacketListenerManager.registerIn(player,
				ServerboundInteractPacket.class, "femanager",
				new IPacketInListener<>() {
					int count; // minecraft send 4 packets in a row if it's
											// not attack

					@Override
					public boolean in(ServerboundInteractPacket packet) {
						int entityId = ReflectionUtils.getFieldValue("a",
								packet);
						String actionType = ((Object) ReflectionUtils
								.getFieldValue("b", packet)).toString();

						var fakeEntity = FakeEntityManager.this.fakeEntities
								.stream().filter((f) -> f.is(entityId))
								.findFirst().orElse(null);
						if (fakeEntity != null) {
							// if (!actionType.contains("$1")) {
							// 	count++;
							// 	if (count >= 4) {
							// 		count = 0;
							// 	} else {
							// 		return false;
							// 	}
							// }
							if (fakeEntity instanceof FakeBlock fakeBlock) {
								if (entityId == fakeBlock.getEntityId()) {
									if (fakeBlock.getClickListener() != null) {
										fakeBlock.getClickListener().accept(player);
									}
								}
							} else if (fakeEntity instanceof FakePlayer) {
								FakePlayer fakePlayer = (FakePlayer) fakeEntity;
								if (entityId == fakePlayer.getEntityId()) {
									if (fakePlayer.getClickListener() != null) {
										fakePlayer.getClickListener().accept(player);
									}
								}
							} else if (fakeEntity instanceof Hologram) {
								Hologram hologram = (Hologram) fakeEntity;
								if (hologram.getEntities().contains(entityId)) {
									if (hologram.getClickListener() != null) {
										hologram.getClickListener().accept(player);
									} else if (hologram.isInteractible()) {
										String cmd = hologram.getExecutedCommand();
										if (cmd.startsWith("/")) {
											cmd = hologram.getExecutedCommand()
													.substring(1);
										}
										final String cpy = cmd;
										SpigotTask.runSync(plugin,
												() -> player.performCommand(cpy));
									}
								}
							} else if (fakeEntity instanceof MultilineFakePlayer mfp) {
								if (mfp.getFakePlayer().getEntityId() == entityId
										|| mfp.getLines().getEntities()
												.contains(entityId)) {
									if (mfp.getOnClick() != null) {
										mfp.getOnClick().accept(player);
									}
								}
							}
						}
						return true;
					}
				});
	}

	public void playerDisconnect(Player player) {
		for (FakeEntity fakeEntity : this.fakeEntities) {
			fakeEntity.getTrackerEntry().remove(player);
		}
		PacketListenerManager.clearIn(player, "femanager",
				ServerboundEntityTagQuery.class);
	}

	public void add(FakeEntity fakeEntity) {
		this.fakeEntities.add(fakeEntity);
		fakeEntity.update();
		if (fakeEntity.isInDatabase()) {
			MongoUtils.insertInDb(System.getProperty("dbName"),
					COLLECTION, fakeEntity);
		}
	}

	public void delete(FakeEntity fakeEntity) {
		for (Player player : fakeEntity.getTrackerEntry()) {
			fakeEntity.killHologram(player);
		}
		this.fakeEntities.remove(fakeEntity);
		if (fakeEntity.isInDatabase()) {
			MongoUtils.remove(System.getProperty("dbName"), COLLECTION,
					fakeEntity.getUniqueID(fakeEntity));
		}
	}

	public void update() {
		for (var fakeEntity : this.fakeEntities) {
			update(fakeEntity);
		}
	}

	public void update(FakeEntity fakeEntity) {
		if (fakeEntity.isInDatabase()) {
			MongoUtils.update(System.getProperty("dbName"), COLLECTION,
					fakeEntity.getUniqueID(fakeEntity), fakeEntity);
		}
	}

	public void load() {
		MongoUtils.loadFrom(System.getProperty("dbName"), COLLECTION,
				FakeEntity.class).forEach(this.fakeEntities::add);
	}

	public void playerEffects() {
		this.fakeEntities.parallelStream()
				.filter(fEntity -> fEntity instanceof FakePlayerEffect)
				.forEach(fEntity -> {
					var playerEffect = (FakePlayerEffect) fEntity;
					for (Player player : fEntity.trackerEntry) {
						if (fEntity instanceof FakePlayerFilter filter) {
							// FakeEntity est implémente l'interface filter
							if (!filter.canSee(player))
								// On nous dit que le joueur ne peux pas voir
								// cette entité
								// Donc on skip !!
								continue;
						}
						playerEffect.effect(player);
					}
				});
	}

	/**
	 * Mets à jour ou pas la visualisation de l'hologramme
	 *
	 * @author Fabien CAYRE (Computer)
	 * @date 19/01/2021
	 */
	public void checkPlayers() {

		Bukkit.getOnlinePlayers().parallelStream()
				.forEach(player -> {
					double distance = 0.0d;
					for (FakeEntity fakeEntity : this.fakeEntities) {
						if (fakeEntity instanceof FakePlayerFilter filter) {
							if (!filter.canSee(player)) {
								if (fakeEntity.getTrackerEntry()
										.contains(player)) {
									fakeEntity.killHologram(player);
									fakeEntity.getTrackerEntry().remove(player);
								}
								continue;
							}
						}
						distance = fakeEntity.distance(player);
						// On check s'il sort de la range de l'hologramme
						if (fakeEntity.getTrackerEntry().contains(player)
								&& (distance == -1
										|| distance > DISTANCE_THRESHOLD)) {
							fakeEntity.killHologram(player);
							fakeEntity.getTrackerEntry().remove(player);
							continue;
						}
						// On check s'il est entré dans la range d'un nouvel
						// hologramme
						if (distance != -1 && distance <= DISTANCE_THRESHOLD
								&& !fakeEntity.getTrackerEntry()
										.contains(player)) {
							fakeEntity.getTrackerEntry().add(player);
							fakeEntity.spawnHologram(player);
						}
					}
				});
	}
}
