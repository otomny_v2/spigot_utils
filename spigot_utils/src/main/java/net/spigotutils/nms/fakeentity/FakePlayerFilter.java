package net.spigotutils.nms.fakeentity;

import org.bukkit.entity.Player;

public interface FakePlayerFilter {
    
    boolean canSee(Player player);

}
