package net.spigotutils.nms.fakeentity.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;

import org.bson.Document;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.craftbukkit.v1_19_R1.CraftParticle;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_19_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.mojang.datafixers.util.Pair;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.protocol.game.ClientboundLevelParticlesPacket;
import net.minecraft.network.protocol.game.ClientboundRemoveEntitiesPacket;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.network.protocol.game.ClientboundSetEquipmentPacket;
import net.minecraft.network.protocol.game.ClientboundTeleportEntityPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.spigotutils.chat.ColorUtils;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.nms.PacketUtils;
import net.spigotutils.nms.fakeentity.FakeEntity;
import net.spigotutils.nms.fakeentity.FakeEntityManager;
import net.spigotutils.nms.fakeentity.FakePlayerEffect;
import net.spigotutils.nms.fakeentity.FakePlayerFilter;
import net.spigotutils.utils.number.NumberUtils;

public class FakeBlock
    extends FakeEntity implements FakePlayerFilter, FakePlayerEffect {

  private static final Random R_PARTICLE = new Random(69420L);

  @Getter
  @Setter
  @GuiTransformField(fieldName = "Bloc affiché")
  private Material displayBlock;
  @Getter
  @Setter
  @GuiTransformField(fieldName = "Particules")
  private List<Particle> particles;

  @Getter @Setter private transient int entityId;

  @Getter @Setter private transient ArmorStand armorStand;

  @Getter @Setter private String displayName;

  @Getter @Setter private Function<Player, Boolean> playerFilter;

  @Getter
  @Setter
  @GuiTransformField(fieldName = "Tourne")
  private boolean rotating;

  @Getter @Setter private Consumer<Player> clickListener;

  public FakeBlock(Location location, boolean inDatabase) {
    this(location, inDatabase, p -> true);
  }

  public FakeBlock(Location location, boolean inDatabase,
                   Function<Player, Boolean> playerFilter) {
    super(location, inDatabase);
    this.playerFilter = playerFilter;
  }

  public FakeBlock(Document document) { super(document); }

  public FakeBlock(Builder builder) {
    super(builder.loc, builder.db);
    this.displayBlock = builder.material;
    this.particles = builder.particles;
    this.playerFilter = builder.playerFilter;
    this.clickListener = builder.clickListener;
    this.rotating = builder.rotating;
    this.displayName = builder.displayName;
  }

  @Override
  protected void updateId() {
    this.entityId = FakeEntityManager.getInstance().askEntity();
    Location copy = this.location.clone();
    ServerLevel level = ((CraftWorld)this.location.getWorld()).getHandle();

    ArmorStand armor = new ArmorStand(level, 0, 0, 0);

    armor.setPos(copy.getX(), copy.getY(), copy.getZ());
    armor.setNoBasePlate(true);
    armor.setInvisible(true);
    armor.setItemSlot(EquipmentSlot.HEAD,
                  CraftItemStack.asNMSCopy(new ItemStack(this.displayBlock)),
                  true);
    armor.setId(this.entityId);

    if (this.displayName != null) {
      armor.setCustomNameVisible(true);
      armor.setCustomName(CraftChatMessage.fromStringOrNull(
          ColorUtils.color(this.displayName)));
    }

    this.armorStand = armor;
  }

  @Override
  public void spawnHologram(Player player) {
    if (this.entityId == -1) {
      updateId();
    }
    if (this.armorStand == null) {
      updateId();
      if (armorStand == null) {
        throw new IllegalStateException(
            "ERROR armorStand is null even with hard reload");
      }
    }

    PacketUtils.sendPacket(player,
                           new ClientboundAddEntityPacket(this.armorStand));

    PacketUtils.sendPacket(
        player, new ClientboundTeleportEntityPacket(this.armorStand));
    // Send metadata
    PacketUtils.sendPacket(
        player, new ClientboundSetEntityDataPacket(
                    this.entityId, this.armorStand.getEntityData(), true));
    // Send equipment slot info
    PacketUtils.sendPacket(
        player,
        new ClientboundSetEquipmentPacket(
            this.entityId, List.of(new Pair<>(EquipmentSlot.HEAD,
                                              this.armorStand.getItemBySlot(
                                                  EquipmentSlot.HEAD)))));
  }

  @Override
  public void killHologram(Player player) {
    // Entity destroy
    PacketUtils.sendPacket(player,
                           new ClientboundRemoveEntitiesPacket(this.entityId));
  }

  @Override
  public boolean canSee(Player player) {
    return this.playerFilter.apply(player);
  }

  int yaw = 0;

  @Override
  public void effect(Player player) {
    for (Particle particle : this.particles) {
      ClientboundLevelParticlesPacket packet =
          new ClientboundLevelParticlesPacket(
              CraftParticle.toNMS(particle),
              true, // ?
              (float)this.location.getX() +
                  NumberUtils.map(R_PARTICLE.nextDouble(), 0, 1, -0.5,
                                  0.5), // X
              (float)this.location.getY() + 2 +
                  NumberUtils.map(R_PARTICLE.nextDouble(), 0, 1, -0.5,
                                  0.5), // Y
              (float)this.location.getZ() +
                  NumberUtils.map(R_PARTICLE.nextDouble(), 0, 1, -0.5, 1), // Z
              (float)0, // offSetX
              (float)0, // offSetY
              (float)0, // offSetZ
              (float)0, // extra
              1);       // nombre de particules
      PacketUtils.sendPacket(player, packet);
    }
    this.armorStand.setPosRaw(this.location.getX(),
                              this.location.getY() +
                                  Math.sin(Math.toRadians(yaw)) * 0.4,
                              this.location.getZ());
    this.armorStand.setYRot(yaw);
    ClientboundTeleportEntityPacket p =
        new ClientboundTeleportEntityPacket(this.armorStand);
    yaw = yaw += 5 % 360;
    PacketUtils.sendPacket(player, p);
  }

  public static class Builder {

    private Location loc;
    private Material material = Material.STONE;
    private boolean db = false;
    private List<Particle> particles = new ArrayList<>();
    private Function<Player, Boolean> playerFilter = p -> true;
    private Consumer<Player> clickListener = p -> {};
    private boolean rotating = false;
    private String displayName;

    public Builder() {}

    public Builder location(Location loc) {
      this.loc = loc;
      return this;
    }

    public Builder clickListener(Consumer<Player> clickListener) {
      this.clickListener = clickListener;
      return this;
    }

    public Builder displayName(String displayName) {
      this.displayName = displayName;
      return this;
    }

    public Builder material(Material material) {
      this.material = material;
      return this;
    }

    public Builder db(boolean db) {
      this.db = db;
      return this;
    }

    public Builder particles(List<Particle> particles) {
      this.particles = particles;
      return this;
    }

    public Builder playerFilter(Function<Player, Boolean> playerFilter) {
      this.playerFilter = playerFilter;
      return this;
    }

    public Builder rotating(boolean rotating) {
      this.rotating = rotating;
      return this;
    }

    public FakeBlock build() { return new FakeBlock(this); }
  }

  @Override
  protected boolean is(int id) {
    return this.entityId == id;
  }
}
