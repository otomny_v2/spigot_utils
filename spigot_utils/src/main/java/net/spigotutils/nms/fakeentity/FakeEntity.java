package net.spigotutils.nms.fakeentity;

import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.mongoutils.Adaptator;
import net.spigotutils.gui.objectedit.GuiTransform;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.location.LocationUtils;

@GuiTransform
public abstract class FakeEntity implements Adaptator<FakeEntity> {

	@Getter @Setter
	protected UUID uuid;
	@Getter @Setter @GuiTransformField(fieldName = "position")
	protected Location location;
	@Getter @Setter
	protected boolean inDatabase;
	@Getter
	protected ConcurrentLinkedQueue<Player> trackerEntry;
	@Getter
	protected String world = "world";
	
	public FakeEntity(Document document) {
		this.uuid = UUID.fromString(document.getString("_id"));
		Document locDocument = document.get("location", Document.class);
		// On recup la clé "world" et si jamais ya rien, valeur par défaut = "world"
		this.world = locDocument.get("world", "world");
		if(this.world.equals("spawn")){
			this.world = "world";
		}
		this.location = LocationUtils.fromDocument(locDocument);
		World w = Bukkit.getWorld(this.world);
		if(this.location.getWorld() == null){
			this.location.setWorld(w);
		}

		this.trackerEntry = new ConcurrentLinkedQueue<>();
		this.inDatabase = true;
	}
	
	public FakeEntity(
		Location location, boolean inDatabase) {
		this.inDatabase = inDatabase;
		this.uuid = UUID.randomUUID();
		this.trackerEntry = new ConcurrentLinkedQueue<>();
		this.location = location;
		if(this.location.getWorld() != null){
			this.world = this.location.getWorld().getName();
		}
	}
	
	@Override
	public String getUniqueID(FakeEntity fakeEntity) {
		return fakeEntity.uuid.toString();
	}

	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param hologram
	 * @return
	 * @date 20/01/2021
	 */
	@Override
	public Document adapt(FakeEntity fakeEntity) {
		return new Document()
				.append("_id", getUniqueID(fakeEntity))
				.append("type", getClass().getCanonicalName())
				.append("location", LocationUtils.toDocument(this.location))
				.append("world", this.world);
	}
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @date 20/01/2021
	 */
	public void update() {
		for(Player player : this.trackerEntry) {
			killHologram(player);
		}
		// Update les nouveaux IDs
		this.updateId();
		for(Player player : this.trackerEntry) {
			spawnHologram(player);
		}
	}

	public void remove(){
		for(Player player : this.trackerEntry) {
			killHologram(player);
		}
		// Update les nouveaux IDs
		this.updateId();
	}
	
	protected abstract void updateId();

	protected abstract boolean is(int id);

	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param player
	 * @return
	 * @date 20/01/2021
	 */
	public double distance(Player player) {
		return this.location.getWorld() != player.getLocation().getWorld() ? -1 : this.location.distance(player.getLocation());
	}

	public abstract void killHologram(Player player);

	public abstract void spawnHologram(Player player);

	@Override
	public String toString() {
		return "FakeEntity {location: "+this.location+"}";
	}

}
