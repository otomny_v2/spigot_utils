package net.spigotutils.nms.fakeentity.hologram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.protocol.game.ClientboundRemoveEntitiesPacket;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.network.protocol.game.ClientboundTeleportEntityPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.spigotutils.chat.ColorUtils;
import net.spigotutils.gui.objectedit.GuiTransform;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.gui.objectedit.GuiTransformType;
import net.spigotutils.nms.PacketUtils;
import net.spigotutils.nms.fakeentity.FakeEntity;
import net.spigotutils.nms.fakeentity.FakeEntityManager;
import net.spigotutils.placeholder.PlaceholderManager;

@GuiTransform(name = "Hologramme")
public class Hologram extends FakeEntity {

  @Getter
  @Setter
  @GuiTransformField(fieldName = "Hauteur de ligne")
  private double lineHeight;
  @GuiTransformField(fieldName = "Lignes", type = GuiTransformType.STRING_LIST)
  @Getter
  @Setter
  private List<String> content;
  @GuiTransformField(fieldName = "Commande éxécutée ('none' pour rien)")
  @Getter
  @Setter
  private String executedCommand;

  /**
   * Données temporaire
   */
  @Getter private List<Integer> entities;
  @Getter @Setter private Consumer<Player> clickListener;

  /**
   * Constructeur
   *
   * @author Fabien CAYRE (Computer)
   * @param location
   * @date 19/01/2021
   */
  public Hologram(Location location, boolean inDatabase) {
    super(location, inDatabase);
    this.lineHeight = 1.0F;
    this.content = new ArrayList<>();

    this.entities = new ArrayList<>();
  }

  /**
   * Constructeur BDD
   *
   * @author Fabien CAYRE (Computer)
   * @param doc
   * @date 19/01/2021
   */
  public Hologram(Document doc) {
    super(doc);
    this.lineHeight = doc.getDouble("lineHeight");
    this.content = doc.getList("content", String.class);
    this.executedCommand = doc.get("executedCommand", "none");
    this.entities = new ArrayList<>();
    this.inDatabase = true;
  }

  /**
   * Génère tous les paquets à transmettre
   *
   * @author Fabien CAYRE (Computer)
   * @param plugin
   * @date 19/01/2021
   */
  @Override
  public void spawnHologram(Player player) {
    // Pour chaque
    if (this.entities.isEmpty()) {
      FakeEntityManager.getInstance().fillEntities(this.entities,
                                                   this.content.size());
    }
    Location copy = this.location.clone();
    ServerLevel level = ((CraftWorld)this.location.getWorld()).getHandle();
    for (int i = this.content.size() - 1; i >= 0; i--) {
      String line = this.content.get(i);
      if (ColorUtils.strip(line).isBlank()) {
        copy.add(0, this.lineHeight, 0);
        continue;
      }
      int realId = this.getEntities().get(i);
      ArmorStand armor = new ArmorStand(level, 0, 0, 0);

      armor.setPos(copy.getX(), copy.getY(), copy.getZ());
      armor.setNoBasePlate(true);
      armor.setSmall(true);

      armor.setCustomName(CraftChatMessage.fromStringOrNull(ColorUtils.color(
          PlaceholderManager.getInstance().compute(player, line))));

      armor.setCustomNameVisible(true);
      armor.setInvisible(true);
      armor.setNoGravity(true);
      armor.setId(realId);

      PacketUtils.sendPacket(player, new ClientboundAddEntityPacket(armor));
      PacketUtils.sendPacket(player,
                             new ClientboundTeleportEntityPacket(armor));
      PacketUtils.sendPacket(player, new ClientboundSetEntityDataPacket(
                                         realId, armor.getEntityData(), true));

      copy.add(0, this.lineHeight, 0);
    }
  }

  @Override
  public void updateId() {
    this.entities.clear();
    FakeEntityManager.getInstance().fillEntities(this.entities,
                                                 this.content.size());
  }

  public boolean isInteractible() {
    return this.clickListener != null ||
        (this.executedCommand != null && !this.executedCommand.equals("none") &&
         !this.executedCommand.equals("null"));
  }

  /**
   * @author Fabien CAYRE (Computer)
   * @param player
   * @date 20/01/2021
   */
  @Override
  public void killHologram(Player player) {
    PacketUtils.sendPacket(
        player,
        new ClientboundRemoveEntitiesPacket(
            this.entities.stream().mapToInt(Integer::intValue).toArray()));
  }

  /**
   * @author Fabien CAYRE (Computer)
   * @param hologram
   * @return
   * @date 20/01/2021
   */
  @Override
  public Document adapt(FakeEntity fakeEntity) {
    return super.adapt(fakeEntity)
        .append("content", this.content)
        .append("lineHeight", this.lineHeight)
        .append("executedCommand", this.executedCommand);
  }

  @Override
  public String toString() {
    return "Hologram [lineHeight=" + lineHeight + ", content=" + content +
        ", executedCommand=" + executedCommand + "]";
  }

  public static class Builder {

    private Location location;
    private double lineHeight;
    private List<String> content;
    private String executedCommand;
    private boolean dataBase;

    public Builder() {
      this.location = null;
      this.lineHeight = 0;
      this.content = new ArrayList<>();
      this.executedCommand = "none";
      this.dataBase = false;
    }

    public Builder location(Location location) {
      this.location = location;
      return this;
    }

    public Builder lineHeight(double lineHeight) {
      this.lineHeight = lineHeight;
      return this;
    }

    public Builder content(String... strings) {
      Arrays.asList(strings).forEach(this.content::add);
      return this;
    }

    public Builder executedCommand(String command) {
      this.executedCommand = command;
      return this;
    }

    public Builder dataBase(boolean dataBase) {
      this.dataBase = dataBase;
      return this;
    }

    public Hologram build() {
      Hologram hologram = new Hologram(this.location, this.dataBase);
      hologram.setLineHeight(lineHeight);
      hologram.getContent().addAll(this.content);
      hologram.setExecutedCommand(this.executedCommand);
      return hologram;
    }
  }

  @Override
  protected boolean is(int id) {
    return this.getEntities().contains(id);
  }
}
