package net.spigotutils.nms.fakeentity.player;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.datafixers.util.Pair;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.network.protocol.game.ClientboundAddPlayerPacket;
import net.minecraft.network.protocol.game.ClientboundAnimatePacket;
import net.minecraft.network.protocol.game.ClientboundMoveEntityPacket;
import net.minecraft.network.protocol.game.ClientboundPlayerInfoPacket;
import net.minecraft.network.protocol.game.ClientboundRemoveEntitiesPacket;
import net.minecraft.network.protocol.game.ClientboundRotateHeadPacket;
import net.minecraft.network.protocol.game.ClientboundSetEquipmentPacket;
import net.minecraft.network.protocol.game.ClientboundTeleportEntityPacket;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.spigotutils.location.LocationUtils;
import net.spigotutils.nms.PacketUtils;
import net.spigotutils.nms.fakeentity.FakeEntity;
import net.spigotutils.nms.fakeentity.FakeEntityManager;
import net.spigotutils.nms.fakeentity.FakePlayerEffect;

public class FakePlayer extends FakeEntity implements FakePlayerEffect {

  public static final EquipmentSlot[] SLOTS = new EquipmentSlot[] {
      EquipmentSlot.HEAD, EquipmentSlot.CHEST,    EquipmentSlot.LEGS,
      EquipmentSlot.FEET, EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND};

  @Getter @Setter private int entityId;
  @Getter private String displayName;
  @Getter @Setter private FakePlayerSkin skin;
  @Getter @Setter private Consumer<Player> clickListener;
  @Getter @Setter private Map<EquipmentSlot, ItemStack> equipement;

  private ServerPlayer entityPlayer;

  public FakePlayer(Document document) {
    super(document);
    this.entityId = -1;
    this.equipement = new EnumMap<>(EquipmentSlot.class);
  }

  public FakePlayer(Location location) {
    super(location, false);
    this.entityId = -1;
    this.equipement = new EnumMap<>(EquipmentSlot.class);
  }

  /**
   * Met un item dans le slot d'item
   *
   * @author Fabien CAYRE (Computer)
   *
   * @param slot
   * @param itemStack
   * @date 26/01/2021
   */
  public void setSlot(EquipmentSlot slot,
                      org.bukkit.inventory.ItemStack itemStack) {
    this.equipement.put(slot, CraftItemStack.asNMSCopy(itemStack));
  }

  /**
   * @param displayName the displayName to set
   */
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
    if (this.entityPlayer != null && this.entityId != -1) {
      updateId(this.entityId);
    }
  }

  private void updateId(int setId) {

    if (this.location == null) {
      return;
    }
    if (this.location.getWorld() == null) {
      this.location.setWorld(Bukkit.getWorld(this.world));
    }

    ServerPlayer serverPlayer =
        new ServerPlayer(((CraftServer)Bukkit.getServer()).getServer(),
                         ((CraftWorld)location.getWorld()).getHandle(),
                         new GameProfile(this.uuid, this.displayName), null);

    PacketUtils.setLocation(serverPlayer, this.location.getX(),
                            this.location.getY(), this.location.getZ(),
                            this.location.getYaw(), this.location.getPitch());

    serverPlayer.setId(setId);
    serverPlayer.sentListPacket = false;
    // Textures
    if (this.skin != null) {
      serverPlayer.setUUID(this.skin.getUUID());
      serverPlayer.getGameProfile().getProperties().removeAll("textures");
      serverPlayer.getGameProfile().getProperties().put(
          "textures", new Property("textures", this.skin.getValue(),
                                   this.skin.getSignature()));
    }

    this.entityPlayer = serverPlayer;
  }

  @Override
  protected void updateId() {
    this.entityId = FakeEntityManager.getInstance().askEntity();
    updateId(this.entityId);
  }

  @Override
  public void spawnHologram(Player player) {
    try {

      if (this.entityId == -1) {
        updateId();
      }
      if (this.entityPlayer == null) {
        updateId();
        if (this.entityPlayer == null) {
          throw new IllegalStateException(
              "ERROR entityPlayer is null even with hard reload");
        }
      }

      // Spawn info
      PacketUtils.sendPacket(player,
                             new ClientboundPlayerInfoPacket(
                                 ClientboundPlayerInfoPacket.Action.ADD_PLAYER,
                                 this.entityPlayer));

      // Entity spawns
      PacketUtils.sendPacket(player,
                             new ClientboundAddPlayerPacket(this.entityPlayer));

      // Location
      PacketUtils.sendPacket(
          player, new ClientboundTeleportEntityPacket(this.entityPlayer));

      // Rotation
      PacketUtils.sendPacket(
          player,
          new ClientboundMoveEntityPacket.Rot(
              this.entityId, PacketUtils.toRawYaw(this.location.getYaw()),
              PacketUtils.toRawYaw(this.location.getPitch()), true));

      // Packet de merde à cause de mojang
      PacketUtils.sendPacket(
          player,
          new ClientboundRotateHeadPacket(
              this.entityPlayer, PacketUtils.toRawYaw(this.location.getYaw())));

      // Animation
      PacketUtils.sendPacket(
          player,
          new ClientboundAnimatePacket(
              this.entityPlayer, ClientboundAnimatePacket.SWING_MAIN_HAND));

      // Equipements

      List<Pair<EquipmentSlot, ItemStack>> list = new ArrayList<>();

      for (EquipmentSlot slot : EquipmentSlot.values()) {
        if (this.equipement.containsKey(slot)) {
          list.add(new Pair<>(slot, equipement.get(slot)));
        } else {
          list.add(Pair.of(slot, new ItemStack((ItemLike)null)));
        }
      }

      PacketUtils.sendPacket(
          player, new ClientboundSetEquipmentPacket(this.entityId, list));

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void killHologram(Player player) {
    // Destroy info
    if (this.entityPlayer != null) {
      PacketUtils.sendPacket(
          player, new ClientboundPlayerInfoPacket(
                      ClientboundPlayerInfoPacket.Action.REMOVE_PLAYER,
                      this.entityPlayer));
    }
    // Entity destroy
    PacketUtils.sendPacket(player,
                           new ClientboundRemoveEntitiesPacket(this.entityId));
  }

  @Override
  public String toString() {
    return "FakePlayer [entityId=" + entityId + ", displayName=" + displayName +
        "]";
  }

  public static class Builder {

    private Location location;
    private String displayName;
    private FakePlayerSkin skin;
    private Consumer<Player> clickListener;
    private Map<EquipmentSlot, ItemStack> equipement;

    public Builder() {
      this.location = null;
      this.displayName = "";
      this.clickListener = p -> {};
      this.equipement =
          new EnumMap<EquipmentSlot, ItemStack>(EquipmentSlot.class);
    }

    public Builder location(Location location) {
      this.location = location;
      return this;
    }

    public Builder displayName(String displayName) {
      this.displayName = displayName;
      return this;
    }

    public Builder skin(FakePlayerSkin skin) {
      this.skin = skin;
      return this;
    }

    public Builder equipement(EquipmentSlot slot,
                              org.bukkit.inventory.ItemStack itemStack) {
      return this.equipement(slot, CraftItemStack.asNMSCopy(itemStack));
    }

    public Builder equipement(EquipmentSlot slot, ItemStack itemStack) {
      this.equipement.put(slot, itemStack);
      return this;
    }

    public Builder clickListener(Consumer<Player> clickListener) {
      this.clickListener = clickListener;
      return this;
    }

    public FakePlayer build() {
      FakePlayer fakePlayer = new FakePlayer(this.location);
      fakePlayer.setDisplayName(displayName);
      fakePlayer.setSkin(skin);
      fakePlayer.setClickListener(clickListener);
      fakePlayer.setEquipement(equipement);
      return fakePlayer;
    }
  }

  @Override
  protected boolean is(int id) {
    return this.entityId == id;
  }

  @Override
  public void effect(Player player) {
    if (this.entityId != -1 && this.entityPlayer != null) {
      var directionVector = player.getLocation().toVector().subtract(
          this.location.clone().toVector());
      var yawPitch = LocationUtils.getYawPitchFromVector(directionVector);
      var yaw = yawPitch.get1();
      var pitch = yawPitch.get2();

      // Rotation
      PacketUtils.sendPacket(player,
                             new ClientboundMoveEntityPacket.Rot(
                                 this.entityId, PacketUtils.toRawYaw(yaw),
                                 PacketUtils.toRawYaw(pitch), true));

      // Packet de merde à cause de mojang
      PacketUtils.sendPacket(
          player, new ClientboundRotateHeadPacket(this.entityPlayer,
                                                  PacketUtils.toRawYaw(yaw)));
    }
  }
}
