package net.spigotutils.nms.fakeentity;

import java.lang.reflect.InvocationTargetException;

import org.bson.Document;

import net.mongoutils.Loader;

public class FakeEntityLoader implements Loader<FakeEntity>{

	@Override
	public Class<FakeEntity> getAnalyzedClass() {
		return FakeEntity.class;
	}

	@Override
	public FakeEntity loadFrom(Document doc) {
		String type = doc.getString("type");
		try {
			return (FakeEntity) Class.forName(type).getConstructor(Document.class).newInstance(doc);
		} catch (
			InstantiationException
			| IllegalAccessException
			| IllegalArgumentException
			| InvocationTargetException
			| NoSuchMethodException
			| SecurityException
			| ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
