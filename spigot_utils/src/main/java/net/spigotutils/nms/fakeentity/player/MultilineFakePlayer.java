package net.spigotutils.nms.fakeentity.player;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.spigotutils.chat.ColorUtils;
import net.spigotutils.nms.fakeentity.FakeEntity;
import net.spigotutils.nms.fakeentity.FakePlayerEffect;
import net.spigotutils.nms.fakeentity.hologram.Hologram;
import net.spigotutils.utils.StringUtils;

public class MultilineFakePlayer
    extends FakeEntity implements FakePlayerEffect {

  @Getter private FakePlayer fakePlayer;
  @Getter private Hologram lines;
  @Getter @Setter private Supplier<List<String>> linesSuppliers;
  @Getter private FakePlayerSkin skin;
  @Getter private Consumer<Player> onClick;
  @Getter private Map<EquipmentSlot, ItemStack> equipement;

  public MultilineFakePlayer(Location location) {
    super(location, false);
    this.skin = FakePlayerSkin.CHINESE_MAN;
    this.onClick = p -> {};
    this.linesSuppliers = List::of; // Empty list
    this.equipement = new EnumMap<>(EquipmentSlot.class);
  }

  /**
   * @param onClick
   *          the onClick to set
   */
  public void setOnClick(Consumer<Player> onClick) {
    this.onClick = onClick;
    if (this.fakePlayer != null) {
      this.fakePlayer.setClickListener(onClick);
    }
  }

  /**
   * @param onClick
   *          the onClick to set
   */
  public void setClickListener(Consumer<Player> onClick) {
    this.onClick = onClick;
    if (this.fakePlayer != null) {
      this.fakePlayer.setClickListener(onClick);
    }
  }

  /**
   * @param skin
   *          the skin to set
   */
  public void setSkin(FakePlayerSkin skin) {
    this.skin = skin;
    if (this.fakePlayer != null) {
      this.fakePlayer.setSkin(skin);
    }
  }

  @Override
  protected void updateId() {
    if (this.location == null) {
      return;
    }
    if (this.location.getWorld() == null) {
      this.location.setWorld(Bukkit.getWorld(this.world));
    }

    if (this.fakePlayer == null) {
      initFakePlayer();
    }

    if (this.lines == null) {
      initFakeLines();
    }
  }

  private void initFakePlayer() {
    var fakePlayerBuilder = new FakePlayer.Builder()
                                .location(this.location)
                                .displayName("{empty}")
                                .skin(this.skin)
                                .clickListener(this.onClick);

    for (Map.Entry<EquipmentSlot, ItemStack> entry :
         this.equipement.entrySet()) {
      fakePlayerBuilder.equipement(entry.getKey(), entry.getValue());
    }

    this.fakePlayer = fakePlayerBuilder.build();
  }

  private void initFakeLines() {
    this.lines = new Hologram.Builder()
                     .location(this.location.clone().add(0, 1.1, 0))
                     .content("{empty}", "{empty}", "{empty}", "{empty}",
                              "{empty}", "{empty}", "{empty}", "{empty}")
                     .dataBase(false)
                     .lineHeight(0.30)
                     .build();
  }

  @Override
  public void killHologram(Player player) {
    this.lines.killHologram(player);
    this.fakePlayer.killHologram(player);
  }

  @Override
  public void spawnHologram(Player player) {
    try {
      List<String> actualLines = this.linesSuppliers.get();
      if (actualLines != null && !actualLines.isEmpty()) {
        if (actualLines.size() > 1) {
          synchronized (this.lines) {
            this.lines.setContent(
                actualLines.subList(0, actualLines.size() - 1));
            this.lines.spawnHologram(player);
          }
        }
        synchronized (this.fakePlayer) {
          this.fakePlayer.setDisplayName(StringUtils.clamp(
              ColorUtils.color(actualLines.get(actualLines.size() - 1)), 16));
          this.fakePlayer.spawnHologram(player);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static class Builder {

    private Location location;
    private Supplier<List<String>> linesSupplier = List::of;
    private FakePlayerSkin skin;
    private Consumer<Player> clickListener = p -> {};
    private EnumMap<EquipmentSlot, ItemStack> equipement =
        new EnumMap<>(EquipmentSlot.class);

    public Builder(Location location) { this.location = location; }

    public Builder linesSupplier(Supplier<List<String>> lSupplier) {
      this.linesSupplier = lSupplier;
      return this;
    }

    public Builder skin(FakePlayerSkin skin) {
      this.skin = skin;
      return this;
    }

    public Builder clickListener(Consumer<Player> clickListener) {
      this.clickListener = clickListener;
      return this;
    }

    public Builder equipement(EquipmentSlot slot, ItemStack itemStack) {
      this.equipement.put(slot, itemStack);
      return this;
    }

    public Builder equipement(EquipmentSlot slot,
                              org.bukkit.inventory.ItemStack itemStack) {
      this.equipement.put(slot, CraftItemStack.asNMSCopy(itemStack));
      return this;
    }

    public MultilineFakePlayer build() {
      MultilineFakePlayer fakePlayer = new MultilineFakePlayer(this.location);
      fakePlayer.equipement = this.equipement;
      fakePlayer.inDatabase = false;
      fakePlayer.linesSuppliers = this.linesSupplier;
      fakePlayer.onClick = this.clickListener;
      fakePlayer.skin = this.skin;
      return fakePlayer;
    }
  }

  @Override
  protected boolean is(int id) {
    return this.fakePlayer.getEntityId() == id ||
        this.getLines().getEntities().contains(id);
  }

  @Override
  public void effect(Player player) {
    if (this.fakePlayer != null)
      this.fakePlayer.effect(player);
  }
}
