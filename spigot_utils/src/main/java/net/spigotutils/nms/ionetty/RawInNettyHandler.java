package net.spigotutils.nms.ionetty;

import java.util.function.Consumer;

import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ServerGamePacketListener;
import net.spigotutils.nms.PacketUtils;

public class RawInNettyHandler extends ChannelInboundHandlerAdapter {

    public static RawInNettyHandler register(String key, Player player,
            Consumer<Packet<ServerGamePacketListener>> consumer) {
        RawInNettyHandler inNetty = new RawInNettyHandler(consumer);
        Channel channel = PacketUtils.channel(player);
        channel.pipeline().addBefore("packet_handler", key, inNetty);
        return inNetty;
    }

    public static void cleanPipeline(String key, Player player){
        PacketUtils.channel(player).pipeline().remove(key);
    }

    private Consumer<Packet<ServerGamePacketListener>> consumer;

    public RawInNettyHandler(Consumer<Packet<ServerGamePacketListener>> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof Packet) {
            Packet<ServerGamePacketListener> p = (Packet<ServerGamePacketListener>) msg;
            consumer.accept(p);
            super.channelRead(ctx, msg);
        }
    }

}
