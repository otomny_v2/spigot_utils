package net.spigotutils.nms.ionetty;

import java.util.UUID;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ServerGamePacketListener;
import net.spigotutils.nms.PacketListenerManager;

@AllArgsConstructor
@Getter
public class InHandlerNetty extends ChannelInboundHandlerAdapter{

	private UUID uuid;
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		if(msg instanceof Packet) {
			Packet<ServerGamePacketListener> p = (Packet<ServerGamePacketListener>) msg;
			if(PacketListenerManager.handleIn(this.uuid, p)) {
				super.channelRead(ctx, msg);
			}
		}
	}
	
	
}
