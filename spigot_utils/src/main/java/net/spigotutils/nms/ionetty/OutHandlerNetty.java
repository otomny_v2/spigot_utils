package net.spigotutils.nms.ionetty;

import java.util.UUID;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.spigotutils.nms.PacketListenerManager;

@AllArgsConstructor
@Getter
public class OutHandlerNetty extends ChannelOutboundHandlerAdapter{

	private UUID uuid;
	
	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		if(msg instanceof Packet) {
			Packet<ClientGamePacketListener> p = (Packet<ClientGamePacketListener>) msg;
			if(PacketListenerManager.handleOut(this.uuid, p)) {
				super.write(ctx, msg, promise);
			}
		}
	}
	
}
