package net.spigotutils.nms.ionetty;

import java.util.function.Consumer;

import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.spigotutils.nms.PacketUtils;

public class RawOutNettyHandler extends ChannelOutboundHandlerAdapter {

    public static RawOutNettyHandler register(String key, Player player,
            Consumer<Packet<ClientGamePacketListener>> consumer) {
        var outNetty = new RawOutNettyHandler(consumer);
        Channel channel = PacketUtils.channel(player);
        channel.pipeline().addBefore("packet_handler", key, outNetty);
        return outNetty;
    }

    public static void cleanPipeline(String key, Player player) {
        PacketUtils.channel(player).pipeline().remove(key);
    }

    private Consumer<Packet<ClientGamePacketListener>> consumer;

    public RawOutNettyHandler(Consumer<Packet<ClientGamePacketListener>> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof Packet) {
            Packet<ClientGamePacketListener> p = (Packet<ClientGamePacketListener>) msg;
            consumer.accept(p);
            super.write(ctx, msg, promise);
        }
    }

}
