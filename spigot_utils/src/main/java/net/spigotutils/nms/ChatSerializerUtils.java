package net.spigotutils.nms;

import org.bukkit.craftbukkit.v1_19_R1.util.CraftChatMessage;

import net.minecraft.network.chat.Component;


public class ChatSerializerUtils {

	public static Component toComponent(String string) {
		return CraftChatMessage.fromStringOrNull(string);
	}

	public static Component toComponent_NMS(String string) {
		return Component.nullToEmpty(string);
	}

	
}
