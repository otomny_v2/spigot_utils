package net.spigotutils.nms;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import net.spigotutils.utils.AStar;

public class PathDisplay {

  public static PathDisplay display(Player player, Location start,
                                    Location end) {
    var pathDisplay = new PathDisplay(player, start, end);
    // init stuff
    pathDisplay.init();
    return pathDisplay;
  }

  public static PathDisplay display(Player player, Location end) {
    return display(player, player.getLocation(), end);
  }

  private Player player;
  private Location start;
  private Location end;
  private List<Location> nodes;

  private PathDisplay(Player player, Location start, Location end) {
    this.player = player;
    this.start = start;
    this.end = end;
  }

  public void init() {

    var astar = new AStar(start, end, 10000, true, 2);

    var paths = astar.getPath();
    if (paths.length == 0) {
      throw new RuntimeException(
          "Erreur: astar.getPath a renvoyé un tableau vide...");
    }

    this.nodes = List.of(paths);
  }

  public void showNodes() {
    var dust = new Particle.DustOptions(Color.YELLOW, 3f);
    for (int i = 0; i < nodes.size(); i++) {
      if (i % 3 == 0) {
        var l = nodes.get(i);
        player.spawnParticle(Particle.REDSTONE, l.getX(), l.getY()+1, l.getZ(), 1, 0, 0,
                             0, 0, dust);
      }
    }
  }
}
