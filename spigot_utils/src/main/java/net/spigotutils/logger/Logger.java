package net.spigotutils.logger;

public class Logger {

	private static final boolean DEBUG = true;
	
	/**
	 * Debug le code si le debuggage est actif
	 * @author ComminQ_Q (Computer)
	 *
	 * @param string
	 * @date 20/10/2019
	 */
	public static void debug(String string) {
		if(DEBUG) {
			System.out.println(string);
		}
	}
	
	/**
	 * Debug le code si le debuggage est actif
	 * @author ComminQ_Q (Computer)
	 *
	 * @param string
	 * @date 20/10/2019
	 */
	public static void debug(String string, Object... objects) {
		if(DEBUG) {
			System.out.println(String.format(string, objects));
		}
	}
	
}
