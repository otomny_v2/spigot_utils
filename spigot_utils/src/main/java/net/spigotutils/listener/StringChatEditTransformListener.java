package net.spigotutils.listener;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import lombok.Getter;

public class StringChatEditTransformListener implements Listener {

  @Getter
  private static Map<Player, Consumer<String>> map = new HashMap<>();

  @EventHandler(priority = EventPriority.LOWEST)
  public void onChat(AsyncPlayerChatEvent event) {
    if (map.containsKey(event.getPlayer())) {
      map.get(event.getPlayer()).accept(event.getMessage());
      map.remove(event.getPlayer());
      event.setCancelled(true);
    }
  }

}