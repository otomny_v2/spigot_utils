package net.spigotutils.schematics;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bson.Document;
import org.bukkit.plugin.Plugin;

import net.spigotutils.utils.stream.StreamUtils;

/**
 * Compression selon Run-length encoding (RLE) + GZIP + VarInt
 * 30 octets de meta data
 */
public class FastSchematicV1 implements FastSchematicReadWrite {

	public static LinkedList<Document> blocksSegments(FastSchematic obj) {
		LinkedList<Document> blocksSegments = new LinkedList<>();

		String precedent = null;
		for (int y = 0; y < obj.getHeight(); y++) {
			for (int x = 0; x < obj.getWidth(); x++) {
				for (int z = 0; z < obj.getLength(); z++) {
					int index = y * obj.getLength() * obj.getWidth() + x * obj.getLength() + z;
					Document lastSegment = blocksSegments.peekLast();
					String current = obj.getBlocks()[index];
					if (current.contains("minecraft:air")) {
						// PAR DEFAULT Skip Air
						continue;
					}
					if (!precedent.equals(current)) {
						// On doit ajouter ce document au segments de blocs
						precedent = current;
						Document newSegment = new Document()
								.append("index", index)
								.append("type", current)
								.append("count", 1);
						blocksSegments.addLast(newSegment);
					} else {
						lastSegment.append("count", lastSegment.getInteger("count", 0) + 1);
					}
				}
			}
		}

		return blocksSegments;
	}

	@Override
	public FastSchematic from(Plugin plugin, byte[] data) {
		try {
			var byteArrayInputStream = new ByteArrayInputStream(data);
			var dataInputStream = new DataInputStream(new GZIPInputStream(byteArrayInputStream));

			// pour décaler le curseur, on skip le tag version
			dataInputStream.skipBytes(1);
			int width = StreamUtils.readVarInt(dataInputStream);
			int height = StreamUtils.readVarInt(dataInputStream);
			int length = StreamUtils.readVarInt(dataInputStream);

			int uniqueBlockDataSize = dataInputStream.readShort();
			List<String> uniqueBlockData = new ArrayList<>(uniqueBlockDataSize);
			for (int i = 0; i < uniqueBlockDataSize; i++) {
				uniqueBlockData.add(dataInputStream.readUTF());
			}

			String[] blockDatas = new String[width * height * length];
			Arrays.fill(blockDatas, "minecraft:air");

			int segmentCount = dataInputStream.readInt();
			for (int i = 0; i < segmentCount; i++) {
				int index = StreamUtils.readVarInt(dataInputStream);
				int count = StreamUtils.readVarInt(dataInputStream);
				int materialIndex = StreamUtils.readVarInt(dataInputStream);
				for (int blockIndex = index; blockIndex < index + count; blockIndex++) {
					blockDatas[blockIndex] = uniqueBlockData.get(materialIndex);
				}
			}

			int offsetX, offsetY, offsetZ;
			offsetX = StreamUtils.readVarInt(dataInputStream);
			offsetY = StreamUtils.readVarInt(dataInputStream);
			offsetZ = StreamUtils.readVarInt(dataInputStream);

			return new FastSchematic(width, height, length,
					blockDatas, offsetX, offsetY, offsetZ);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte[] to(Plugin server, FastSchematic obj) {
		try {
			var byteArrayOutputStream = new ByteArrayOutputStream();
			var dataOutputStream = new DataOutputStream(new GZIPOutputStream(byteArrayOutputStream));

			// First, version
			dataOutputStream.writeByte(getVersion()); // 1 octets de version
			// On encode respectivement width, height et length avec varint
			StreamUtils.writeVarInt(dataOutputStream, obj.getWidth());
			StreamUtils.writeVarInt(dataOutputStream, obj.getHeight());
			StreamUtils.writeVarInt(dataOutputStream, obj.getLength());

			var uniqueMaterials = Stream.of(obj.getBlocks()).distinct()
					.filter(mat -> !mat.contains("minecraft:air"))
					.toList();

			// palette des blocks
			// Nombre de blocks dans la palette
			// 2 octets nombre de blocs dans la palette
			dataOutputStream.writeShort(uniqueMaterials.size());
			// On écrit à la suite le nombre de material
			uniqueMaterials.forEach(material -> {
				try {
					dataOutputStream.writeUTF(material.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

			// On récupère les segments
			var segments = blocksSegments(obj);
			// 4 octets pour le nombre de segments
			dataOutputStream.writeInt(segments.size());

			var blocks = new ByteArrayOutputStream(segments.size() * 3);

			segments.forEach(segment -> {

				// - startIndex (varint)
				try {
					StreamUtils.writeVarInt(blocks, segment.getInteger("index", 0));
					StreamUtils.writeVarInt(blocks, segment.getInteger("count", 0));
					// - length (varint)
					// - type (varint)
					String mat = segment.get("type", "minecraft:bedrock");
					StreamUtils.writeVarInt(blocks, uniqueMaterials.indexOf(mat));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			byte[] blocksVarInt = blocks.toByteArray();
			dataOutputStream.write(blocksVarInt);

			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetX()); // 4 octets
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetY()); // 4 octets
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetZ()); // 4 octets
			// 12 octets pour le offset

			// 30 octets de base sans segments
			byte[] dataArray = byteArrayOutputStream.toByteArray();
			return dataArray;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte getVersion() {
		return 1;
	}

}
