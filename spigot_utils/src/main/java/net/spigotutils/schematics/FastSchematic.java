package net.spigotutils.schematics;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import lombok.Getter;
import lombok.Setter;
import net.spigotutils.nbt.Paste;

/**
 * Helper class for schematic serialization as byte array
 * This class must contains :
 * Blocks, Entities and TileEntities
 *
 * Done:
 * Blocks (compressed) with BlockData
 *
 * Missing:
 * Entities
 * TileEntities
 * BlockData (vegetables)
 *
 * Extension : OCS
 *
 * Otomny Compressed Schematics
 */
@Getter
@Setter
public class FastSchematic {

  public static List<String> listSchematics(Plugin plugin) {
    File schematicsFolder = new File(plugin.getDataFolder(), "fastschematics");
    if (!schematicsFolder.exists()) {
      try {
        schematicsFolder.mkdir();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return Arrays.asList(schematicsFolder.list());
  }

  public static FastSchematic saveFile(Location start, Location end,
                                       Location offset) {
    // Calculer width, height et length,
    // Récupérer les différents blocks (de manière distincte)
    // Calculer le nombre de bits qu'il faut pour coder les différentes blocs

    int minX = Math.min(start.getBlockX(), end.getBlockX());
    int minY = Math.min(start.getBlockY(), end.getBlockY());
    int minZ = Math.min(start.getBlockZ(), end.getBlockZ());

    int maxX = Math.max(start.getBlockX(), end.getBlockX());
    int maxY = Math.max(start.getBlockY(), end.getBlockY());
    int maxZ = Math.max(start.getBlockZ(), end.getBlockZ());

    int width = (maxX - minX) + 1;
    int height = (maxY - minY) + 1;
    int length = (maxZ - minZ) + 1;

    String[] blocks = new String[length * height * width];
    Arrays.fill(blocks, "minecraft:air");
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        for (int z = 0; z < length; z++) {
          int relativeX = (x + minX);
          int relativeY = (y + minY);
          int relativeZ = (z + minZ);
          int index = y * length * width + x * length + z;
          if (index >= blocks.length) {
            System.err.println(
                "FastSchematic matrix index = length * height * width (" +
                length + " x " + height + " x " + width + ")");
            System.err.println("FastSchematic maxX - minX = (" + maxX + " - " +
                               minX + " = " + (maxX - minX) + ")");
            System.err.println("FastSchematic maxY - minY = (" + maxY + " - " +
                               minY + " = " + (maxY - minY) + ")");
            System.err.println("FastSchematic maxZ - minZ = (" + maxZ + " - " +
                               minZ + " = " + (maxZ - minZ) + ")");
            System.err.println("FastSchematic relative = (x=" + relativeX +
                               ",y=" + relativeY + ",z=" + relativeZ + ")");
            throw new IllegalStateException(
                "Error while creating fastschematic (index=" + index +
                ", size=" + blocks.length + ", x=" + x + ", y=" + y +
                ", z=" + z + ")");
          }
          blocks[index] = offset.getWorld()
                              .getBlockAt(relativeX, relativeY, relativeZ)
                              .getBlockData()
                              .getAsString()
                              .toLowerCase();
        }
      }
    }

    Location relativeOffset = offset.clone().subtract(
        new Location(start.getWorld(), minX, minY, minZ));

    return new FastSchematic(width, height, length, blocks,
                             relativeOffset.getX(), relativeOffset.getY(),
                             relativeOffset.getZ());
  }

  public static FastSchematic importSchematic(Plugin plugin, byte[] data) {
    try {
      var byteArrayInputStream = new ByteArrayInputStream(data);
      var dataInputStream =
          new DataInputStream(new GZIPInputStream(byteArrayInputStream));

      byte version = dataInputStream.readByte();
      return switch (version) {
                                case 1 -> new FastSchematicV1().from(plugin, data);
				case 2 -> new FastSchematicV2().from(plugin, data);
				case 3 -> new FastSchematicV3().from(plugin, data);
				default -> throw new IllegalStateException("Unknown byte version " + version);
			};
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void saveToFile(FastSchematic obj, Plugin plugin, String fileName) throws IOException {
		saveToFile(obj, plugin, fileName, new FastSchematicV1());
	}

	public static void saveToFile(FastSchematic obj, Plugin plugin, String fileName, FastSchematicReadWrite version)
			throws IOException {
		byte[] rawByteFile = exportSchematic(plugin, obj, version);
		File schematicsFolder = new File(plugin.getDataFolder(), "fastschematics");
		if (!schematicsFolder.exists()) {
			try {
				schematicsFolder.mkdir();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		File linkedFile = new File(schematicsFolder, fileName + ".ocs");
		if (linkedFile.exists()) {
			throw new FileAlreadyExistsException(fileName + ".ocs");
		}
		RandomAccessFile newFile = new RandomAccessFile(linkedFile, "rw");
		newFile.write(rawByteFile);
		newFile.close();
	}

	public static FastSchematic loadFromFile(Plugin plugin, String fileName) throws IOException {
		File schematicsFolder = new File(plugin.getDataFolder(), "fastschematics");
		if (!schematicsFolder.exists()) {
			try {
				schematicsFolder.mkdir();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		File linkedFile = new File(schematicsFolder, fileName);
		byte[] fullFileBytes = Files.readAllBytes(linkedFile.toPath());
		return importSchematic(plugin, fullFileBytes);
	}

	public static byte[] base64Encode(Plugin plugin, FastSchematic obj) {
		return Base64.getEncoder().encode(exportSchematic(plugin, obj));
	}

	public static byte[] base64Encode(Plugin plugin, FastSchematic obj, FastSchematicReadWrite version) {
		return Base64.getEncoder().encode(exportSchematic(plugin, obj, version));
	}

	public static byte[] exportSchematic(Plugin plugin, FastSchematic obj) {
		return exportSchematic(plugin, obj, new FastSchematicV1());
	}

	public static byte[] exportSchematic(Plugin plugin, FastSchematic obj, FastSchematicReadWrite version) {
		return version.to(plugin, obj);
	}

	public static FastSchematic base64Decode(Plugin plugin, byte[] b64) {
		return importSchematic(plugin, Base64.getDecoder().decode(b64));
	}

	private int width;
	private int height;
	private int length;
	private String[] blocks;
	private double relativeOffSetX;
	private double relativeOffSetY;
	private double relativeOffSetZ;

	public FastSchematic(int width, int height, int length, String[] blocks, double relativeOffSetX,
			double relativeOffSetY, double relativeOffSetZ) {
		this.width = width;
		this.height = height;
		this.length = length;
		this.blocks = blocks;
		this.relativeOffSetX = relativeOffSetX;
		this.relativeOffSetY = relativeOffSetY;
		this.relativeOffSetZ = relativeOffSetZ;
	}

	public Paste asPaste() {
		return new Paste(this);
	}

}
