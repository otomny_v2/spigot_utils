package net.spigotutils.schematics;

import org.bukkit.plugin.Plugin;

public interface FastSchematicReadWrite {

  byte getVersion();

  FastSchematic from(Plugin plugin, byte[] data);

  byte[] to(Plugin plugin, FastSchematic fastSchematic);


}

