package net.spigotutils.schematics;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.Material;
import org.bukkit.plugin.Plugin;

import net.spigotutils.utils.stream.StreamUtils;

/**
 * Stockage des données sous forme d'un mix entre du RLE (Run Length Encoding)
 * et matrix Puis compression format GZIP
 * 
 * Data compressed with a mix between RLE and pure matrix encoding.
 * Then this byte stream is compressed with GZIP
 */
public class FastSchematicV3 implements FastSchematicReadWrite {

	public static final int RLE = 1;
	public static final int MATRIX = 2;

	@Override
	public byte getVersion() {
		return 3;
	}

	@Override
	public FastSchematic from(Plugin plugin, byte[] data) {
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
			DataInputStream dataInputStream = new DataInputStream(new GZIPInputStream(inputStream));

			// pour décaler le curseur, on skip le tag version

			// We skip the version indicator
		 	dataInputStream.readByte();
			int width = StreamUtils.readVarInt(dataInputStream);
			int height = StreamUtils.readVarInt(dataInputStream);
			int length = StreamUtils.readVarInt(dataInputStream);

			int materialCount = dataInputStream.readShort();
			List<String> blockDatas = new ArrayList<>();

			for (int i = 0; i < materialCount; i++) {
				blockDatas.add(dataInputStream.readUTF());
			}

			// yes i keep all sysout and ?
			//░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░
			//░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░
			//░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░
			//░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░
			//░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░
			//█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█
			//█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█
			//░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░
			//░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░
			//░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░
			//░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░
			//░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░
			//░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░
			//░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░
			//░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░

			//System.out.println("FastSchematicV3 Loading matrix index = length * height * width (" + length + " x " + height
			//		+ " x " + width + ")");
			//System.out.println("Material count " + materialCount);
			//System.out.println("Materials " + materials);

			String[] matrix = new String[width * height * length];
			Arrays.fill(matrix, "minecraft:air");

			String toAssign = null;
			int skipTo = -1;
			int currentMode = -1;

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					for (int z = 0; z < length; z++) {
						int index = y * length * width + x * length + z;
						if (toAssign != null) {
							if (index < skipTo) {
								matrix[index] = toAssign;
								continue;
							} else {
								toAssign = null;
							}
						}
						int rawDataValue = StreamUtils.readVarInt(dataInputStream);
						if (isMode(rawDataValue, blockDatas)) {
							if (mode(rawDataValue, blockDatas) == RLE) {
								// RLE
								// Encoded as
								//System.out.println("RLE Mode toggled");
								int materialIndex = StreamUtils.readVarInt(dataInputStream);
								int count = StreamUtils.readVarInt(dataInputStream);
								skipTo = index + count;
								toAssign = blockDatas.get(materialIndex);
								matrix[index] = toAssign;
								currentMode = RLE;
							} else {
								int materialIndex = StreamUtils.readVarInt(dataInputStream);
								matrix[index] = blockDatas.get(materialIndex);
								currentMode = MATRIX;
							}
						} else {
							if (currentMode == MATRIX) {
								matrix[index] = blockDatas.get(rawDataValue);
							} else if (currentMode == RLE) {
								int materialIndex = rawDataValue;
								int count = StreamUtils.readVarInt(dataInputStream);
								skipTo = index + count;
								toAssign = blockDatas.get(materialIndex);
								matrix[index] = toAssign; // WE DO A BIT OF TROLLING
							}
						}

					}
				}
			}

			//System.out.println("FastSchematicV3 load skip to " + skipTo);
			//System.out.println("FastSchematicV3 load material toAssing " + toAssign);

			int relativeX = StreamUtils.readVarInt(dataInputStream);
			int relativeY = StreamUtils.readVarInt(dataInputStream);
			int relativeZ = StreamUtils.readVarInt(dataInputStream);

			FastSchematic fastSchematic = new FastSchematic(width, height, length,
					matrix, relativeX, relativeY, relativeZ);

			return fastSchematic;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte[] to(Plugin plugin, FastSchematic obj) {
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(32);
			DataOutputStream dataOutputStream = new DataOutputStream(new GZIPOutputStream(byteArrayOutputStream));

			// First, version
			dataOutputStream.writeByte(getVersion());
			// 1 octets pour la version

			// 1 byte for version

			// On encode respectivement width, height et length

			// We code respectivly, width height and length
			StreamUtils.writeVarInt(dataOutputStream, obj.getWidth());
			StreamUtils.writeVarInt(dataOutputStream, obj.getHeight());
			StreamUtils.writeVarInt(dataOutputStream, obj.getLength());

			// palette des blocks
			// blocks palette
			var uniqueBlockData = Stream.of(obj.getBlocks()).distinct().toList();

			// Nombre de blocks dans la palette
			// Block count inside the palette

			// 2 octets nombre de blocs dans la palette
			// 2 byte for it
			dataOutputStream.writeShort(uniqueBlockData.size());

			// On écrit à la suite le nombre de material
			// we write all the materials with UTF
			uniqueBlockData.forEach(material -> {
				try {
					dataOutputStream.writeUTF(material);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

			//System.out.println(
			//		"FastSchematicV3 Loading matrix index = length * height * width (" + obj.getLength() + " x " + obj.getHeight()
			//				+ " x " + obj.getWidth() + ")");
			//System.out.println("Material count " + uniqueMaterials.size());
			//System.out.println("Materials " + uniqueMaterials);

			// we generate the markers
			var markers = getMarkers(obj);
			// System.out.println("Markers: " + markers);
			int previousMode = -1;

			int skipTo = -1;
			for (int y = 0; y < obj.getHeight(); y++) {
				for (int x = 0; x < obj.getWidth(); x++) {
					for (int z = 0; z < obj.getLength(); z++) {
						int index = y * obj.getLength() * obj.getWidth() + x * obj.getLength() + z;
						int materialIndex = uniqueBlockData.indexOf(obj.getBlocks()[index]);
						if (index < skipTo)
							continue;
						if (markers.containsKey(index)) {
							// index is marked as a differents mode (RLE/MATRIX)
							// we need to check
							MarkerData markerData = markers.get(index);
							if (markerData.mode == MATRIX) {
								if (previousMode != MATRIX) {
									StreamUtils.writeVarInt(dataOutputStream, uniqueBlockData.size() + MATRIX);
									previousMode = MATRIX;
								}
								StreamUtils.writeVarInt(dataOutputStream, materialIndex);
							} else if (markerData.mode == RLE) {
								if (previousMode != RLE) {
									StreamUtils.writeVarInt(dataOutputStream, uniqueBlockData.size() + RLE);
									previousMode = RLE;
								}
								StreamUtils.writeVarInt(dataOutputStream, materialIndex);
								StreamUtils.writeVarInt(dataOutputStream, markerData.count);
								skipTo = index + markerData.count;

								//System.out.println("RLE Encode material: " + materialIndex);
								//System.out.println("RLE Encode skipTo: " + skipTo);
								//System.out.println("RLE Encode count: " + markerData.count);
							}
						} else {
							StreamUtils.writeVarInt(dataOutputStream, materialIndex);
						}
					}
				}
			}

			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetX());
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetY());
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetZ());
			// 12 octets pour le offset
			// 15 + 12 = 27 octets de base sans segments
			dataOutputStream.close();

			byte[] dataArray = byteArrayOutputStream.toByteArray();
			return dataArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new byte[] {};
	}

	public <E> int mode(int result, List<E> materials) {
		return result - materials.size();
	}

	public <E> boolean isMode(int result, List<E> materials) {
		return mode(result, materials) == MATRIX || mode(result, materials) == RLE;
	}

	public static Map<Integer, MarkerData> getMarkers(FastSchematic schem) {
		final int consecutiveSameElements = 3;

		Map<Integer, MarkerData> markers = new HashMap<>();
		int height = schem.getHeight();
		int width = schem.getWidth();
		int length = schem.getLength();

		String previousMaterial = null;
		int previousCount = 0;
		int previousIndex = 0;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				for (int z = 0; z < length; z++) {
					int index = y * length * width + x * length + z;
					String material = schem.getBlocks()[index];

					if (previousMaterial == null || !previousMaterial.equalsIgnoreCase(material)) {
						// Le type de block a changé
						if (previousCount > consecutiveSameElements) {
							// Il y a eu plus de type de bloc similaire que (consecutiveSameElements)

							// S'il y a eu un marqueur de type MATRIX, on va le remplacer avec cette fonction
							markers.put(previousIndex, new MarkerData(RLE, previousCount, previousMaterial));

							markers.put(index, new MarkerData(MATRIX));
						} else if (previousMaterial == null) {
							markers.put(index, new MarkerData(MATRIX));
						}

						// On remet les compteurs a zero vu qu'on vient de passer un segment de blocs
						// similaire
						previousMaterial = material;
						previousCount = 1;
						previousIndex = index;
					} else {
						previousCount++;
						if (index == (height * width * length) - 1) {
							// Dernier bloc du schematic
							if (previousCount > consecutiveSameElements) {
								// On est arrivé à la fin,
								// On a beaucoup d'élèments consécutif qui se suivent
								// Et on a pas marqué l'index précèdent en tant que RLE
								markers.put(previousIndex, new MarkerData(RLE, previousCount, previousMaterial));
							}
						}
					}
				}
			}
		}

		return markers;
	}

	public record MarkerData(int mode, int count, String material) {
		public MarkerData(int mode, int count) {
			this(mode, count, "minecraft:stone");
		}

		public MarkerData(int mode) {
			this(mode, 0);
		}

		public Material materialRaw(){
			return Material.valueOf(this.material.split("\\:")[1].toUpperCase());
		}
	}

}
