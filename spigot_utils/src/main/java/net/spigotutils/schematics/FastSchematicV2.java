package net.spigotutils.schematics;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.plugin.Plugin;

import net.spigotutils.utils.stream.StreamUtils;

/**
 * Stockage des matériaux sous forme de matrice + varint
 * Compression GZIP après le stockage
 * 
 * 27 octets de metadata
 */
public class FastSchematicV2 implements FastSchematicReadWrite {

	@Override
	public byte getVersion() {
		return 2;
	}

	@Override
	public FastSchematic from(Plugin plugin, byte[] data) {
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
			DataInputStream dataInputStream = new DataInputStream(new GZIPInputStream(inputStream));

			// pour décaler le curseur, on skip le tag version
			dataInputStream.skipBytes(1);
			int width = StreamUtils.readVarInt(dataInputStream);
			int height = StreamUtils.readVarInt(dataInputStream);
			int length = StreamUtils.readVarInt(dataInputStream);

			int materialCount = dataInputStream.readShort();
			List<String> blockDatas = new ArrayList<>();

			for (int i = 0; i < materialCount; i++) {
				blockDatas.add(dataInputStream.readUTF());
			}

			String[] matrix = new String[width * height * length];
			Arrays.fill(matrix, "minecraft:air");

			//System.out.println("FastSchematic Loading matrix index = length * height * width (" + length + " x " + height
			//		+ " x " + width + ")");
			//System.out.println("Material count " + materialCount);
			//System.out.println("Materials " + materials);

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					for (int z = 0; z < length; z++) {
						int index = y * length * width + x * length + z;
						int materialIndex = StreamUtils.readVarInt(dataInputStream);
						matrix[index] = blockDatas.get(materialIndex);
					}
				}
			}

			int relativeX = StreamUtils.readVarInt(dataInputStream);
			int relativeY = StreamUtils.readVarInt(dataInputStream);
			int relativeZ = StreamUtils.readVarInt(dataInputStream);

			FastSchematic fastSchematic = new FastSchematic(width, height, length,
					matrix, relativeX, relativeY, relativeZ);

			return fastSchematic;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public byte[] to(Plugin plugin, FastSchematic obj) {
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(32);
			DataOutputStream dataOutputStream = new DataOutputStream(new GZIPOutputStream(byteArrayOutputStream));

			// First, version
			dataOutputStream.writeByte(getVersion());
			// 1 octets pour la version
			// On encode respectivement width, height et length
			StreamUtils.writeVarInt(dataOutputStream, obj.getWidth());
			StreamUtils.writeVarInt(dataOutputStream, obj.getHeight());
			StreamUtils.writeVarInt(dataOutputStream, obj.getLength());
			var uniqueBlockData = Stream.of(obj.getBlocks()).distinct().toList();

			// palette des blocks
			// Nombre de blocks dans la palette
			// 2 octets nombre de blocs dans la palette
			dataOutputStream.writeShort(uniqueBlockData.size());
			// On écrit à la suite le nombre de material
			uniqueBlockData.forEach(blockData -> {
				try {
					dataOutputStream.writeUTF(blockData);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			// 15 octets de bases

			// On code les matériaux sur une simple matrice
			for (int y = 0; y < obj.getHeight(); y++) {
				for (int x = 0; x < obj.getWidth(); x++) {
					for (int z = 0; z < obj.getLength(); z++) {
						int index = y * obj.getLength() * obj.getWidth() + x * obj.getLength() + z;
						String mat = obj.getBlocks()[index];
						int blockId = uniqueBlockData.indexOf(mat);
						if (blockId == -1) {
							throw new IllegalStateException("ERROR, trying to write undefined material (blockId=" + blockId +
									", material = " + mat + ", index=" + index + ", materialMatrix=" + Arrays.toString(obj.getBlocks())
									+ ")");
						}
						StreamUtils.writeVarInt(dataOutputStream, blockId);
					}
				}
			}

			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetX());
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetY());
			StreamUtils.writeVarInt(dataOutputStream, (int) obj.getRelativeOffSetZ());
			// 12 octets pour le offset
			// 15 + 12 = 27 octets de base sans segments
			dataOutputStream.close();

			byte[] dataArray = byteArrayOutputStream.toByteArray();
			return dataArray;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
