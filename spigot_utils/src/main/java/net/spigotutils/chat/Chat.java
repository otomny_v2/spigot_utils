package net.spigotutils.chat;

import org.bukkit.entity.Player;

import net.spigotutils.locale.I18N;
import net.spigotutils.placeholder.PlaceholderManager;

public class Chat {

	private Chat() {
	}

	/**
	 * Shortcut for the {@link Chat#msg(Player, String)} method
	 * 
	 * @return The message fully transformed
	 */
	public static String c(Player p, String k) {
		return msg(p, k);
	}

	/**
	 * Apply all differents placeholder managements and
	 * translations To get correct text to display to user
	 * 
	 * @param player
	 *          The player
	 * @param translation
	 *          The key of the translation
	 * @return The message fully transformed
	 */
	public static String msg(Player player, String key) {
		String lang = I18N.getInstance().getLangProvider()
				.apply(player);
		String translation = I18N.t(lang, key);

		String msg = PlaceholderManager.getInstance().compute(player,
				translation);

		return ColorUtils.color(msg);
	}

}
