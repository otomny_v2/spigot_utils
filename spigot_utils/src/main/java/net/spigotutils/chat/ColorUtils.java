package net.spigotutils.chat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

import net.spigotutils.utils.stream.StreamUtils;

/**
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 12/07/2020
 */
public class ColorUtils {

	/**
	 * 
	 * @param text
	 * @return
	 */
	public static ChatColor lastColorOfString(String text){
		String transform = ChatColor.translateAlternateColorCodes('&', text);
		for(int i = transform.length() - 1; i >= 0; i--){
			if(transform.charAt(i) == '§' && i < transform.length() - 1){
				return ChatColor.getByChar(transform.charAt(i+1));
			}
		}
		return null;
	}

	/**
	 * Transforme les codes couleurs '&' en vrais codes couleurs
	 * @author ComminQ_Q (Computer)
	 *
	 * @param base La chaîne de caractères à convertir
	 * @return La chaîne de caracères transformée
	 * @date 12/07/2020
	 */
	public static String color(String base) {
		if(base == null)
			return "§c§lNULL";
		return ChatColor.translateAlternateColorCodes('&', base);
	}
	
	/**
	 * Transforme les codes couleurs '&' en vrais codes couleurs
	 * sur une liste de chaîne de caractères
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param base Les chaînes de caractères à convertir
	 * @return Les chaînes de caractères transformées
	 * @date 20/12/2020
	 */
	public static List<String> color(List<String> base){
		if(base == null)
			return new ArrayList<>(){{
				add("§c§lNULL");
			}};
		return StreamUtils.map(base, s -> ChatColor.translateAlternateColorCodes('&', s));
	}
	
	/**
	 * Retire les codes couleurs d'une chaîne de caractères
	 * @author ComminQ_Q (Computer)
	 *
	 * @param base La chaîne de caracères à convertir
	 * @return La chaîne de caracères sans couleur
	 * @date 12/07/2020
	 */
	public static String strip(String base) {
		return ChatColor.stripColor(base);
	}
	
}
