package net.spigotutils.timings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CustomTimings {

	public static int MAX_REGISTERED_TIMINGS = 100;
	public static Map<String, LinkedList<TimingsObject>> timings = new HashMap<>();
	public static boolean ENABLE_TIMINGS = true;
	
	public static void doTimings(String paramString) {
		if(!ENABLE_TIMINGS) return;
		if(!timings.containsKey(paramString)) {
			timings.put(paramString,
					new LinkedList<>(Arrays.asList(new TimingsObject(System.currentTimeMillis(), -1))));
			return;
		}
		LinkedList<TimingsObject> timingsObjectList = timings.get(paramString);
		if(timingsObjectList.isEmpty()) {
			removeTiming(paramString);
			return;
		}
		TimingsObject timingsObject = timingsObjectList.getLast();
		if(timingsObjectList.size() >= MAX_REGISTERED_TIMINGS) {
			timingsObjectList.pollFirst();
		}
		if(timingsObject.isLast()) {
			timingsObject.setEnd(System.currentTimeMillis());
		}else {
			timingsObjectList.addLast(new TimingsObject(System.currentTimeMillis(), -1));
		}
		return;
	}
	
	public static Map<String, LinkedList<TimingsObject>> getTimings() {
		return timings;
	}
	
	public static void reloadTimings() {
		timings.clear();
	}
	
	public static void removeTiming(String paramString) {
		timings.remove(paramString);
	}
	
	
	public static String recapTimings(String paramString) {
		if(!ENABLE_TIMINGS) return "Aucun";
		List<TimingsObject> timingsObject = timings.get(paramString);
		int count = timingsObject.size();
		double E = timingsObject.stream()
				.filter(tO -> !tO.isLast())
				.mapToLong(timingObject -> timingObject.getDuration()).sum() / count;
		StringBuilder builder = new StringBuilder();
		builder.append("["+paramString+"] Moyenne : "+(E<=0 ? "⩽0" : E)+"ms, Appel : "+count);
		builder.append("\n");
		builder.append("15 dernier timings effectué:").append("\n");
		getTimings().get(paramString)
			.stream()
			.filter(tO -> !tO.isLast())
			.sorted()
			.limit(15)
			.forEach(to -> builder.append(to.toString()+", "));
		builder.replace(builder.length()-2, builder.length(), "");
		return builder.toString();
	}
	
	
	public static class TimingsObject implements Comparable<TimingsObject>{

		private long start;
		private long end;
		private boolean async;
		
		public TimingsObject(long start, long end) {
			this(false, start, end);
		}
		
		public TimingsObject(boolean async, long start, long end) {
			this.async = async;
			this.start = start;
			this.end = end;
		}

		public long getStart() {
			return start;
		}
		
		public long getEnd() {
			return end;
		}
		
		public void setStart(long start) {
			this.start = start;
		}
		
		public void setEnd(long end) {
			this.end = end;
		}
		
		public boolean isLast() {
			return this.end == -1;
		}
		
		public boolean isAsync() {
			return async;
		}
		
		public long getDuration() {
			return end - start;
		}
		
		@Override
		public String toString() {
			return "Durée : "+this.getDuration()+"ms";
		}

		@Override
		public int compareTo(TimingsObject o) {
			return (int) (o.start - this.start);
		}
		
		
	}

	
}
