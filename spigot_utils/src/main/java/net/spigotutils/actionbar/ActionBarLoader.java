package net.spigotutils.actionbar;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 * Représente une barre d'action qui se charge
 * @author ComminQ_Q (Computer)
 *
 * @date 11/11/2019
 */
public class ActionBarLoader {
	
	/**
	 * La ligne représentant la barre de chargement
	 */
	protected static final String LINE = "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";

	private double seconds;
	private ChatColor color;
	private Predicate<Player> cancel;
	private BiConsumer<Player, Boolean> end;
	private Consumer<Player> start;
	private BukkitTask task;
	private Player player;
	private boolean displayTime;
	private long startTimestamp;
	private boolean reverse;
	private String name;
	private int size;
	
	/**
	 * Le constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @param loaderBuilder
	 * @param player
	 * @date 11/11/2019
	 */
	public ActionBarLoader(ActionBarLoaderBuilder loaderBuilder, Player player) {
		this.seconds = loaderBuilder.seconds;
		this.color = loaderBuilder.color;
		this.cancel = loaderBuilder.cancel;
		this.end = loaderBuilder.end;
		this.start = loaderBuilder.start;
		this.player = player;
		this.displayTime = loaderBuilder.displayTime;
		this.reverse = loaderBuilder.reverse;
		this.size = loaderBuilder.size;
		this.name = loaderBuilder.name;
	}
	
	public void cancel() {
		this.task.cancel();
	}
	
	@SuppressWarnings("deprecation")
	public void send() {
		this.startTimestamp = System.currentTimeMillis();
		this.task = new BukkitRunnable() {
			
			double currentSecond;
			
			@Override
			public void run() {
				currentSecond+=0.05;
				if(ActionBarLoader.this.player == null || !ActionBarLoader.this.player.isOnline()) {
					cancel();
					LoaderUtils.getInstance().clear(ActionBarLoader.this);
					return;
				}
				boolean canceled = ActionBarLoader.this.cancel.test(ActionBarLoader.this.player);
				if(currentSecond > ActionBarLoader.this.seconds || canceled) {
					ActionBarLoader.this.end.accept(ActionBarLoader.this.player, canceled);
					ActionBarLoader.this.player.sendActionBar("§f");
					cancel();
					LoaderUtils.getInstance().clear(ActionBarLoader.this);
					return;
				}
				ActionBarLoader.this.player.sendActionBar(loader(currentSecond));
			}
		}.runTaskTimer(LoaderUtils.getInstance().getPlugin(), 0, 1);
		LoaderUtils.getInstance().add(this);
	}

	private String loader(double currentSecond) {
		final String NEW_LINE = LINE.substring(0, this.size);
		
		double relativeTime = Double.valueOf(Double.valueOf((getSupposedEndTime() - System.currentTimeMillis())) / 50);
		double percentage = relativeTime / Double.valueOf(getTimeInTicks());
		int amount = 0;
		if(reverse) {
			amount = (int) Math.round(NEW_LINE.length() * percentage);
		}else {
			amount = NEW_LINE.length() - amount;
			
		}
		String value = this.color + NEW_LINE.substring(0, amount) + "§f" + NEW_LINE.substring(amount);
		if(this.displayTime) {
			value+=" "+this.color+"§l("+String.format("%.1f", (this.seconds - currentSecond))+"s)";
		}
		if(amount < 0) {
			amount = 0;
		}
		return this.name+" "+value;
	}

	
	private double getTimeInTicks() {
		return this.seconds*20;
	}

	private long getSupposedEndTime() {
		return (long) (this.startTimestamp + 1000 * seconds);
	}

	public double getSeconds() {
		return seconds;
	}

	public ChatColor getColor() {
		return color;
	}

	public Predicate<Player> getCancel() {
		return cancel;
	}

	public BiConsumer<Player, Boolean> getEnd() {
		return end;
	}

	public Consumer<Player> getStart() {
		return start;
	}

	public BukkitTask getTask() {
		return task;
	}

	public Player getPlayer() {
		return player;
	}
	
	
	
	
}
