package net.spigotutils.actionbar;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * La classe builder pouvant construire une barre de chargement
 * @author ComminQ_Q (Computer)
 *
 * @date 11/11/2019
 */
public class ActionBarLoaderBuilder {

	protected double seconds; // Le temps
	protected boolean displayTime; // Si on doit afficher le temps restant
	protected ChatColor color; // La couleur
	protected Predicate<Player> cancel; // Si on doit annuler le chargement ou pas
	protected BiConsumer<Player, Boolean> end; // Quand on fini
	protected Consumer<Player> start; // Quand on commence le timer
	protected boolean reverse; // Pour l'affichage
	protected int size = 16; // La taille de la barre
	protected String name = ""; // Le nom de la barre de chargement
	
	/**
	 * Le constructeur
	 * @author ComminQ_Q (Computer)
	 *
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder() {
		this.cancel = player -> false;
		this.end = (player, canceled) -> {};
		this.start = player -> {};
		this.seconds = 5;
		this.displayTime = true;
		this.color = ChatColor.GOLD;
		this.reverse = false;
	}
	
	/**
	 * La méthode qui doit être appelée quand le timer démarre
	 * @author ComminQ_Q (Computer)
	 *
	 * @param onStart
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder start(Consumer<Player> onStart) {
		this.start = onStart;
		return this;
	}
	
	/**
	 * Le nom de la barre d'action
	 * @author ComminQ_Q (Computer)
	 *
	 * @param name
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder name(String name) {
		this.name = name;
		return this;
	}
	
	/**
	 * La taille de la barre d'action
	 * @author ComminQ_Q (Computer)
	 *
	 * @param size
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder size(int size) {
		if(size < 1 || size > ActionBarLoader.LINE.length()) size = ActionBarLoader.LINE.length();
		this.size = size;
		return this;
	}
	
	/**
	 * Le temps de la barre
	 * @author ComminQ_Q (Computer)
	 *
	 * @param seconds
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder seconds(double seconds) {
		if(seconds < 1) seconds = 1;
		this.seconds = seconds;
		return this;
	}
	
	/**
	 * Si on doit afficher le temps restant ou pas
	 * @author ComminQ_Q (Computer)
	 *
	 * @param time
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder displayTime(boolean time) {
		this.displayTime = time;
		return this;
	}
	
	/**
	 * Renverse l'affichage de la barre
	 * @author ComminQ_Q (Computer)
	 *
	 * @param reverse
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder reverse(boolean reverse) {
		this.reverse = reverse;
		return this;
	}
	
	/**
	 * La couleur de la barre
	 * @author ComminQ_Q (Computer)
	 *
	 * @param color
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder color(ChatColor color) {
		this.color = color;
		return this;
	}
	
	/**
	 * Quel évènement provoque l'arrêt du timer
	 * @author ComminQ_Q (Computer)
	 *
	 * @param withCancellable
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder cancel(Predicate<Player> withCancellable) {
		this.cancel = withCancellable;
		return this;
	}
	
	/**
	 * La méthode à appelée quand le timer est fini
	 * @author ComminQ_Q (Computer)
	 *
	 * @param onEnd
	 * @return
	 * @date 11/11/2019
	 */
	public ActionBarLoaderBuilder end(BiConsumer<Player, Boolean> onEnd) {
		this.end = onEnd;
		return this;
	}
	
	/**
	 * Démarre le timer avec des joueurs
	 * @author ComminQ_Q (Computer)
	 *
	 * @param players
	 * @date 11/11/2019
	 */
	public void start(Player... players) {
		if(this.color == null) {
			this.color = ChatColor.GOLD;
		}
		Validate.notNull(this.cancel);
		Validate.notNull(this.end);
		Validate.notNull(this.start);
		for(Player player : players) {
			new ActionBarLoader(this, player).send();
		}
	}
	
}
