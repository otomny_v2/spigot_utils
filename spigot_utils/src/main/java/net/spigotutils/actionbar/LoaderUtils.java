package net.spigotutils.actionbar;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class LoaderUtils {

	private static LoaderUtils instance;
	
	protected static synchronized LoaderUtils getInstance() {
		return instance;
	}
	
	public static synchronized LoaderUtils getInstance(Plugin plugin) {
		if(instance == null)instance = new LoaderUtils(plugin);
		return instance;
	}
	
	private Map<UUID, ActionBarLoader> loaders;
	private Plugin plugin;
	
	public LoaderUtils(Plugin plugin) {
		this.plugin = plugin;
		this.loaders = new HashMap<>();
	}
	
	public Plugin getPlugin() {
		return plugin;
	}

	public void clear(ActionBarLoader loader) {
		Player player = loader.getPlayer();
		if(this.loaders.containsKey(player.getUniqueId())) {
			this.loaders.remove(player.getUniqueId());
		}
	}
	
	public void add(ActionBarLoader loader) {
		Player player = loader.getPlayer();
		if(this.loaders.containsKey(player.getUniqueId())) {
			this.loaders.get(player.getUniqueId()).cancel();
			this.loaders.remove(player.getUniqueId());
		}
		this.loaders.put(player.getUniqueId(), loader);
	}

	
}
