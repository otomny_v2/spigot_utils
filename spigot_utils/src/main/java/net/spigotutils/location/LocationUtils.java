package net.spigotutils.location;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

import net.spigotutils.utils.Pair;

/**
 *
 * @author ComminQ_Q (Computer)
 *
 * @date 22/10/2019
 */
public class LocationUtils {

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param location
   * @return le document représentant la location
   * @date 22/10/2019
   */
  public static Document toDocument(Location location) {
    if (location == null)
      return null;
    return new Document()
        .append("world", location.getWorld().getName())
        .append("x", location.getX())
        .append("y", location.getY())
        .append("z", location.getZ())
        .append("pitch", location.getPitch())
        .append("yaw", location.getYaw());
  }

  /**
   *
   * @author ComminQ_Q (Computer)
   *
   * @param document
   * @return la location issu du document
   * @date 22/10/2019
   */
  public static Location fromDocument(Document document) {
    if (document == null)
      return null;
    try {
      double x, y, z;
      x = document.getDouble("x");
      y = document.getDouble("y");
      z = document.getDouble("z");
      World world = Bukkit.getWorld(document.getString("world"));
      float yaw, pitch;
      yaw = document.getDouble("yaw").floatValue();
      pitch = document.getDouble("pitch").floatValue();
      return new Location(world, x, y, z, yaw, pitch);
    } catch (Exception e) {
      double x, y, z;
      try {
        x = document.getInteger("x");
      } catch (Exception ee) {
        x = document.getDouble("x");
      }
      try {
        y = document.getInteger("y");
      } catch (Exception ee) {
        y = document.getDouble("y");
      }
      try {
        z = document.getInteger("z");
      } catch (Exception ee) {
        z = document.getDouble("z");
      }
      World world = Bukkit.getWorld(document.getString("world"));
      float yaw, pitch;
      try {
        yaw = document.getDouble("yaw").floatValue();
      } catch (Exception ee) {
        yaw = document.getInteger("yaw").floatValue();
      }
      try {
        pitch = document.getDouble("pitch").floatValue();
      } catch (Exception ee) {
        pitch = document.getInteger("pitch").floatValue();
      }
      return new Location(world, x, y, z, yaw, pitch);
    }
  }

	/**
	 * Renvoie la valeur de pitch & yaw en degrés a partir d'un vecteur
	 * @param vector
	 * @return Un pair<Yaw, Pitch>
	 */
  public static Pair<Float, Float> getYawPitchFromVector(Vector vector) {
    float pitch = 0;
    float yaw = 0;

    final double _2PI = 2 * Math.PI;
    final double x = vector.getX();
    final double z = vector.getZ();

    if (x == 0 && z == 0) {
      pitch = vector.getY() > 0 ? -90 : 90;
      return Pair.of(yaw, pitch);
    }

    double theta = Math.atan2(-x, z);
    yaw = (float)Math.toDegrees((theta + _2PI) % _2PI);

    double x2 = NumberConversions.square(x);
    double z2 = NumberConversions.square(z);
    double xz = Math.sqrt(x2 + z2);
    pitch = (float)Math.toDegrees(Math.atan(-vector.getY() / xz));

    return Pair.of(yaw, pitch);
  }
}
