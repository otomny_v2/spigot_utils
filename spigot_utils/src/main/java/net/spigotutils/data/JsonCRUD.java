package net.spigotutils.data;

import org.bson.Document;

import net.spigotutils.data.exceptions.DataAlreadyExists;
import net.spigotutils.data.exceptions.DataNotFound;

/**
 * Json modeling manager respecting CRUD rules
 * (Create, Read, Update and Delete)
 *
 */
public interface JsonCRUD {

  /**
   * Handle startup of the backend
   */
  void startup();

  /**
   * Handle shutdown of the backend
   */
  void shutdown();

  /**
   * Insert object in the JsonCRUD data backend
   * @param <T> Data type
   * @param collection Collection name
   * @param data Data object
   * @return True if object was inserted
   * @throws DataAlreadyExists
   */
  <T extends Identifiable &Adaptable> boolean create(String collection,
                                                        T data)
      throws DataAlreadyExists;

  <T extends Identifiable &Adaptable &Loadable> T read(String collection,
                                                          String id)
      throws DataNotFound;

  <T extends Identifiable &Adaptable> boolean update(String collection,
                                                        T data)
      throws DataNotFound;

  boolean delete(String collection, String identifier)throws DataNotFound;

  <T extends Identifiable &Adaptable> boolean delete(String collection,
                                                        T data)
      throws DataNotFound;

  boolean updatePartial(String collection, String identifier, Document doc)
      throws DataNotFound;
}
