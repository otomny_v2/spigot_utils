package net.spigotutils.data;

import org.bson.Document;

import net.spigotutils.data.exceptions.DataAlreadyExists;
import net.spigotutils.data.exceptions.DataNotFound;

public class MongoCRUD implements JsonCRUD {

  @Override
  public <T extends Identifiable & Adaptable> boolean create(String collection,
                                                             T data)
      throws DataAlreadyExists {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public <T extends Identifiable & Adaptable & Loadable>
      T read(String collection, String id) throws DataNotFound {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <T extends Identifiable & Adaptable> boolean update(String collection,
                                                             T data)
      throws DataNotFound {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean delete(String collection, String identifier)
      throws DataNotFound {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public <T extends Identifiable & Adaptable> boolean delete(String collection,
                                                             T data)
      throws DataNotFound {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean updatePartial(String collection, String identifier,
                               Document doc) throws DataNotFound {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void startup() {
    // TODO Auto-generated method stub
  }

  @Override
  public void shutdown() {
    // TODO Auto-generated method stub
  }
}
