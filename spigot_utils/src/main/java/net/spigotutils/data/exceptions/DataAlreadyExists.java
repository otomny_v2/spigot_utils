package net.spigotutils.data.exceptions;

import net.spigotutils.data.Adaptable;
import net.spigotutils.data.Identifiable;

public class DataAlreadyExists extends Exception {

  /**
   * @param message
   */
  public <T extends Identifiable & Adaptable>
  DataAlreadyExists(String collection, T data) {
    super("The data (id: '" + data.getDatabaseId() +
          "', class: " + data.getClass().getSimpleName() +
          ") is already present in collection '" + collection + "'");
  }
}
