package net.spigotutils.data.exceptions;

import net.spigotutils.data.Adaptable;
import net.spigotutils.data.Identifiable;

public class DataNotFound extends Exception {

  /**
   * @param message
   */
  public <T extends Identifiable & Adaptable> DataNotFound(String collection,
                                                              T data) {
    super("The data (id: '" + data.getDatabaseId() +
          "', class: " + data.getClass().getSimpleName() +
          ") was not found in collection '" + collection + "'");
  }
}
