package net.spigotutils.data;

import org.bson.Document;

/**
 * Represent a loadable object.
 * 
 * Must implement a constructor.
 * <pre>
 * public Loadable(org.bson.Document)
 * </pre>
 */
public interface Loadable {
	
	default boolean supportAutoUpdate(){
		return true;
	}

	default void update(Document document){}

}
