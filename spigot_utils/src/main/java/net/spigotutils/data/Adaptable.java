package net.spigotutils.data;

import org.bson.Document;

public interface Adaptable {
	
	Document adapt();

}
