package net.spigotutils.data;

/**
 * 
 */
public interface Identifiable {
	
	/**
	 * 
	 * @return A unique database identifier
	 */
	String getDatabaseId();

}
