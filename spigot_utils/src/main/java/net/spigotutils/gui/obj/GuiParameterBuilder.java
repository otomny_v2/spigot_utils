package net.spigotutils.gui.obj;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bukkit.plugin.Plugin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import net.spigotutils.gui.objectedit.GuiTransformer;
/**
 * UTILISER {@link GuiTransformer}
 * @author ComminQ_Q (Computer)
 *
 * @param <T>
 * @date 12/07/2020
 */
@Deprecated()
public class GuiParameterBuilder<T> {
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public enum ParameterType{
		
		STRING,
		LONG_STRING,
		MATERIAL_STRING,
		MATERIAL,
		ENUM,
		INTEGER,
		DOUBLE,
		BOOLEAN;
		
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	@AllArgsConstructor
	@ToString
	public static class GuiParameterField {
		
		@Getter private String fieldName;
		@Getter private ParameterType type;
		@Getter private String infoLine;
		
	}
	
	@Getter private List<GuiParameterField> fields;
	@Getter private T object;
	@Getter private Consumer<T> consumer;
	@Getter private Plugin plugin;
	@Getter private String name;
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public GuiParameterBuilder(T object) {
		this.fields = new ArrayList<GuiParameterBuilder.GuiParameterField>();
		this.object = object;
		this.name = "Paramètre de "+object.getClass().getSimpleName();
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public GuiParameterBuilder<T> field(String fieldName, ParameterType type, String infoLine) {
		this.fields.add(new GuiParameterField(fieldName, type, infoLine));
		return this;
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public GuiParameterBuilder<T> onEnd(Consumer<T> consumer){
		this.consumer = consumer;
		return this;
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public GuiParameterBuilder<T> name(String name){
		this.name = name;
		return this;
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	public GuiParameterBuilder<T> plugin(Plugin plugin){
		this.plugin = plugin;
		return this;
	}
	
	public GuiParameter<T> build() {
		return new GuiParameter<T>(this);
	}
	
	
	
}
