package net.spigotutils.gui.obj;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Predicates;

import net.spigotutils.gui.Gui;
import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.GuiListener;
import net.spigotutils.gui.GuiType;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.obj.GuiParameterBuilder.GuiParameterField;
import net.spigotutils.gui.objectedit.GuiTransformer;
import net.spigotutils.item.ItemStackBuilder;
import net.spigotutils.nms.sign.SignGUI;
import net.spigotutils.nms.sign.SignResponse;
import net.spigotutils.utils.reflection.ReflectionUtils;
import net.wesjd.anvilgui.AnvilGUI;

/**
 * UTILISER {@link GuiTransformer}
 * @author ComminQ_Q (Computer)
 *
 * @param <T>
 * @date 12/07/2020
 */
@Deprecated()
public class GuiParameter<T> {

	private T data;
	private Class<T> clazzData;
	private List<GuiParameterField> fields;
	private Consumer<T> end;
	private Plugin plugin;
	private String name;
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	@SuppressWarnings("unchecked")
	public GuiParameter(GuiParameterBuilder<T> guiParameterBuilder) {
		this.data = guiParameterBuilder.getObject();
		this.clazzData = (Class<T>) guiParameterBuilder.getObject().getClass();
		this.fields = guiParameterBuilder.getFields();
		this.plugin = guiParameterBuilder.getPlugin();
		this.end = guiParameterBuilder.getConsumer();
		this.name = guiParameterBuilder.getName();
	}
	/**
	 * UTILISER {@link GuiTransformer}
	 * @author ComminQ_Q (Computer)
	 *
	 * @param <T>
	 * @date 12/07/2020
	 */
	@Deprecated()
	private void createItem(Player player, GuiParameterField currentField) throws Exception {

		Field field = this.clazzData.getDeclaredField(currentField.getFieldName());
		if (!field.isAccessible()) {
			field.setAccessible(true);
		}
		String fieldString = StringUtils.capitalise(field.get(this.data).toString());
		switch (currentField.getType()) {
		case DOUBLE:
			displaySign(player, "Entrez un nombre flotant", currentField, text -> {
				try {
					Double.parseDouble(text);
					return true;
				} catch (Exception e) {
					return false;
				}
			}, text -> {
				double value = Double.parseDouble(text);
				try {
					field.setDouble(this.data, value);
				} catch (Exception e) {
					e.printStackTrace();
				}
			},fieldString);
			break;
		case BOOLEAN:
			final boolean fieldValue = field.getBoolean(this.data);
			new GuiBuilder()
				.type(GuiType.HOPPER)
				.item(2, new GuiItemBuilder()
							.icon(fieldValue ? Material.GREEN_CONCRETE : Material.RED_CONCRETE)
							.name(fieldString)
							.description("",
									     "§7Cliquez pour mettre à jour la valeur booléenne à "+(!fieldValue))
							.clickHandler(new GuiItem.ClickHandler() {
								
								@Override
								public boolean onClick(Gui panel, Player player, ClickType clickType, int slot) {
									try {
										field.set(GuiParameter.this.data, !fieldValue);
										open(player);
									} catch (IllegalArgumentException | IllegalAccessException e) {
										e.printStackTrace();
									}
									return true;
								}
							})
							.build())
				.item(0, new GuiBackItem(p ->  {
					open(p);
				}))
				.build()
				.open(player);
			break;
		case INTEGER:
			displaySign(player, "Entrez un nombre entier", currentField, text -> {
				try {
					Integer.parseInt(text);
					return true;
				} catch (Exception e) {
					return false;
				}
			}, text -> {
				int value = Integer.parseInt(text);
				try {
					field.setDouble(this.data, value);
				} catch (Exception e) {
					e.printStackTrace();
				}
			},fieldString);
			break;
		case STRING:
			displaySign(player, "Entrez du texte", currentField, Predicates.alwaysTrue(), text -> {
				try {
					field.set(this.data, text);
				} catch (Exception e) {
					e.printStackTrace();
				}
			},fieldString);
			break;
		case LONG_STRING:
			longString(currentField, field, player, fieldString);
			break;
		case MATERIAL_STRING:
			new GuiBuilder()
					.type(GuiType.HOPPER)
					.item(0, new GuiBackItem(this::open))
					.item(
							2,
							new GuiItemBuilder()
									.icon(Material.PAPER)
									.name("§f")
									.description(
											"",
											"§fCliquez sur un item de votre",
											"§finventaire pour appliquer le matériel")
									.build())
					.listener(new GuiListener() {

						@Override
						public void setup() {
						}

						@Override
						public void onInventoryClose(InventoryCloseEvent event) {
						}

						@Override
						public void onInventoryClick(Player user, InventoryClickEvent event) {
							int slot = event.getRawSlot();
							if (slot > 4) {
								if (event.getCurrentItem() != null) {
									event.setCancelled(true);
									Material type = event.getCurrentItem().getType();
									String typeToString = type.toString().toLowerCase();
									try {
										field.set(GuiParameter.this.data, typeToString);
										open(player);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}
					})
					.build()
					.open(player);
			break;
		case ENUM:
			Class<?> holdClass = field.getType();
			if (holdClass.isEnum()) {
				Object[] values = ReflectionUtils.enumList(holdClass);
				GuiBuilder builder = new GuiBuilder();
				builder.name(currentField.getInfoLine());
				for (Object enumValue : values) {
					String objName = StringUtils.capitalize(enumValue.toString());
					builder
							.item(
									new GuiItemBuilder()
											.icon(Material.ITEM_FRAME)
											.name("§f" + objName)
											.description("", "§7Cliquez pour sélectionner")
											.clickHandler(new GuiItem.ClickHandler() {
												@Override
												public boolean onClick(
														Gui panel,
														Player player,
														ClickType clickType,
														int slot) {
													try {
														field.set(GuiParameter.this.data, enumValue);
														open(player);
													} catch (IllegalArgumentException | IllegalAccessException e) {
														e.printStackTrace();
													} catch (Exception e) {
														e.printStackTrace();
													}
													return true;
												}
											})
											.build());
				}
				builder.build().open(player);
				return;
			}
			open(player);
			break;
		case MATERIAL:
			new GuiBuilder()
					.type(GuiType.HOPPER)
					.item(0, new GuiBackItem(this::open))
					.item(
							2,
							new GuiItemBuilder()
									.icon(Material.PAPER)
									.name("§f")
									.description(
											"",
											"§fCliquez sur un item de votre",
											"§finventaire pour appliquer le matériel")
									.build())
					.listener(new GuiListener() {

						@Override
						public void setup() {
						}

						@Override
						public void onInventoryClose(InventoryCloseEvent event) {
						}

						@Override
						public void onInventoryClick(Player user, InventoryClickEvent event) {
							int slot = event.getRawSlot();
							if (slot > 4) {
								if (event.getCurrentItem() != null) {
									event.setCancelled(true);
									Material type = event.getCurrentItem().getType();
									try {
										field.set(GuiParameter.this.data, type);
										open(player);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}
					})
					.build()
					.open(player);
			break;
		}
	}

	public void longString(GuiParameterField currentField, Field field, Player player, String value) {
		new AnvilGUI.Builder()
				.item(new ItemStackBuilder(Material.PAPER).get())
				.text(value)
				.title(currentField.getInfoLine())
				.onClose(p -> {
				})
				.onComplete((p, text) -> {
					try {
						field.set(this.data, text);
						open(player);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return AnvilGUI.Response.close();
				})
				.plugin(this.plugin)
				.open(player);
	}

	public void displaySign(
			Player player,
			String textDisplay,
			GuiParameterField field,
			Predicate<String> validate,
			Consumer<String> consumer,
			String value) {
		new SignGUI.Builder(this.plugin)
				.value(value)
				.firstLine(textDisplay)
				.secondLine(field.getInfoLine())
				.end(text -> {
					consumer.accept(text);
					try {
						open(player);
					} catch (Exception e) {
						e.printStackTrace();
					}
				})
				.onComplete(text -> {
					if (validate.test(text)) {
						return SignResponse.close();
					}
					return SignResponse.text("Erreur");
				})
				.open(player);
	}

	public void open(Player player) {
		
		GuiBuilder builder = new GuiBuilder();
		builder
			.name(this.name)
			.fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE, 1))
			.size(3*9)
			.item(new GuiBackItem(p -> {
				this.end.accept(this.data);
			}));
		
		for(GuiParameterField fields : this.fields) {
			builder
				.item(new GuiItemBuilder()
						.icon(Material.NAME_TAG)
						.name("§e"+fields.getInfoLine())
						.description("§cCliquez pour éditer ce champ")
						.clickHandler(new GuiItem.ClickHandler() {
							
							@Override
							public boolean onClick(Gui panel, Player player, ClickType clickType, int slot) {
								try {
									createItem(player, fields);
								} catch (Exception e) {
									e.printStackTrace();
								}
								return true;
							}
						})
						.build());
		}
		builder.build().open(player);
	}

}
