package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.spigotutils.utils.TriConsumer;

public abstract class EditTransform {

	protected Field field;
	protected TriConsumer<Field, Object, Object> setter;
	protected String name;
	protected Object object;
	protected String[] description;
	@Getter @Setter private Runnable backToGui;
	
	public EditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		this.field = field;
		this.setter = setter;
		this.object = object;
	}
	
	public EditTransform descData(String name, String[] description) {
		this.description = description;
		this.name = name;
		return this;
	}

	public abstract void openGui(Player player);
	
	
	
}
