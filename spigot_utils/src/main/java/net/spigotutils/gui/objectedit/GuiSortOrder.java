package net.spigotutils.gui.objectedit;

public enum GuiSortOrder {
	
	ASC,
	DESC,
	ALPHABETIC,
	ALPHABETIC_REVERSE,
	NO_SORT;

}
