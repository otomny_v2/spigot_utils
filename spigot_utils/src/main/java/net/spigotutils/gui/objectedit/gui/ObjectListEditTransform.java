package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.Guictify;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.gui.objectedit.GuiTransformer;
import net.spigotutils.utils.StringUtils;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;

@SuppressWarnings("unchecked")
public class ObjectListEditTransform extends EditTransform {

  private Class<?> parameterType;

  public ObjectListEditTransform(Field field, Object object,
                                 TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
    this.parameterType = ReflectionUtils.getTypeOfListField(field);
  }

  @Override
  public void openGui(Player player) {
    openGui(player, 0);
  }

  public void openGui(Player player, int page) {

    final int ITEM_PAGE_COUNT = 3 * 7;
    try {
      this.field.setAccessible(true);
      List<Object> list = (List<Object>)this.field.get(this.object);
      this.field.setAccessible(false);

      int maxPageCount = list.size() / ITEM_PAGE_COUNT;
      if (page < 0) {
        openGui(player, 0);
      } else if (page > maxPageCount) {
        openGui(player, maxPageCount);
      }
      int startPage = page * ITEM_PAGE_COUNT;
      int endPage = Math.min(list.size(), (page + 1) * ITEM_PAGE_COUNT);

      GuiBuilder guiBuilder =
          new GuiBuilder()
              .name("§e" + strip(this.name))
              .size(5 * 9)
              .fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

      for (int i = startPage; i < endPage; i++) {
        final int index = i;
        var objectAt = list.get(index);
        String toString = objectAt == null
                              ? "null"
                              : StringUtils.clamp(objectAt.toString(), 60);

        GuiItemBuilder guiItemBuilder =
            objectAt instanceof Guictify
                ? ((Guictify)objectAt).item()
                : new GuiItemBuilder().icon(
                      new ItemStack(Material.PAPER, i + 1));

        guiBuilder.item(
            guiItemBuilder.name("§eÉlèment n°" + (i + 1))
                .description("", "§7Contenu : " + toString,
                             "§c- Clique droit pour supprimer",
                             "§a- Clique gauche pour éditer")
                .clickHandler((gui, pl, click, slot) -> {
                  if (click == ClickType.LEFT) {
                    // Editer
                    GuiTransformer
                        .of(list.get(index), () -> { openGui(player, page); })
                        .openGUI(player);
                    ;
                    return true;
                  }
                  if (click == ClickType.RIGHT) {
                    // Supprimer
                    GuiAcceptItem.openAccept("§cSupprimer cet élément ?",
                                             player,
                                             ()
                                                 ->
                                             // Cancel
                                             openGui(player),
                                             () -> {
                                               // Accept
                                               list.remove(index);
                                               openGui(player, page);
                                             });
                    return true;
                  }
                  return true;
                })
                .build());
      }

      GuiTransformField fieldData =
          field.getAnnotation(GuiTransformField.class);
      if (!fieldData.lockAdd()) {
        guiBuilder.item(
            new GuiItemBuilder()
                .name("§aAjouter un élément")
                .icon(Material.LIME_CONCRETE)
                .description("", "§7Cliquez pour ajouter un élément")
                .clickHandler((gui, pl, click, slot) -> {
                  // Ajouter
                  try {
                    final Object createdElement =
                        this.parameterType.getConstructor().newInstance();
                    GuiTransformer
                        .of(createdElement,
                            () -> {
                              list.add(createdElement);
                              openGui(player, page);
                            })
                        .openGUI(player);
                  } catch (InstantiationException | IllegalAccessException |
                           IllegalArgumentException |
                           InvocationTargetException | NoSuchMethodException |
                           SecurityException e) {
                    e.printStackTrace();
                  };
                  return true;
                })
                .build());
      }

      if (page > 0) {
        guiBuilder.item(39, new GuiItemBuilder()
                                .name("§7Page précèdente")
                                .icon(Material.ARROW)
                                .clickHandler(() -> openGui(player, page - 1))
                                .build());
      }
      if (page < maxPageCount) {
        guiBuilder.item(41, new GuiItemBuilder()
                                .icon(Material.ARROW)
                                .name("§7Page suivante")
                                .clickHandler(() -> openGui(player, page + 1))
                                .build());
      }

      guiBuilder.item(0, new GuiBackItem(this.getBackToGui()));

      guiBuilder.build().open(player);
    } catch (Exception e) {
      e.printStackTrace();
      this.getBackToGui().run();
    }
  }
}
