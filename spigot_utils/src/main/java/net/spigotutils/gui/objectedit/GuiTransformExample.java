package net.spigotutils.gui.objectedit;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * Classe exemple pour le système de GuiTransform automatique
 * 
 * L'annotation @GuiTransform spécifie que cette classe peut être transformée en
 * Gui Elle contient l'attribut name, qui est par défaut le nom de classe dans
 * la JVM On peut utiliser les codes couleurs avec '&' à l'intérieur de la
 * valeur
 * 
 * Le titre de l'inventaire créer est
 * 
 * <pre>
 * "Edition de §cExample";
 * </pre>
 * 
 * Les champs qui n'ont pas l'annotation @GuiTransformField ne seront pas
 * affiché dans l'inventaire
 * 
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 12/07/2020
 */
@GuiTransform(name = "§cExample")
@SuppressWarnings("unused")
public class GuiTransformExample {

	/**
	 * Marche également avec les autres annotations
	 * 
	 */
	@Getter
	@Setter
	@GuiTransformField(fieldName = "Nom de l'exemple", description = {
			"§eChanger le nom", "§ede l'exemple"
	})
	private String name;

	private int count;

	private double money;

	/**
	 * Force le type du champ
	 */
	@Getter
	@Setter
	@GuiTransformField(fieldName = "Type affiché", type = GuiTransformType.MATERIAL_STRING)
	private String display;

}
