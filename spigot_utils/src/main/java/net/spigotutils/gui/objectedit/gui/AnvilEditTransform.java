package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;
import net.wesjd.anvilgui.AnvilGUI;

@SuppressWarnings("deprecation")
public class AnvilEditTransform extends EditTransform {

	public AnvilEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		try {
			this.field.setAccessible(true);
			String currValue = (String) this.field.get(this.object);
			this.field.setAccessible(false);
			new AnvilGUI.Builder()
					.plugin(CustomBoards.getInstance().getPlugin())
					.title("§e" + strip(this.name))
					.text(currValue == null ? "undefined" : currValue)
					.item(new ItemStack(Material.PAPER))
					.onClose(x -> {
						this.getBackToGui().run();
					})
					.onComplete((p, string) -> {
						this.field.setAccessible(true);
						this.setter.accept(this.field, this.object, string);
						this.field.setAccessible(false);
						return AnvilGUI.Response.close();
					})
					.open(player);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
