package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.listener.StringChatEditTransformListener;
import net.spigotutils.runnable.SpigotTask;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;

public class StringChatEditTransform extends EditTransform {

  public StringChatEditTransform(Field field, Object object,
                                 TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
  }

  @Override
  public void openGui(Player player) {
    player.closeInventory();
    try {
      this.field.setAccessible(true);
      Object tempVal = this.field.get(this.object);
      String strValue = tempVal == null ? "Non-défini" : tempVal.toString();
      this.field.setAccessible(false);
      player.sendMessage("§aValeur actuelle : " + strValue);
      player.sendMessage("§aRentrez la valeur dans le chat :");
      player.sendMessage("§e§oTappez 'cancel' pour annuler");

      StringChatEditTransformListener.getMap().put(player, (val) -> {
        if (!val.equals("cancel")) {
          this.field.setAccessible(true);
          this.setter.accept(this.field, this.object, val);
          this.field.setAccessible(false);
          SpigotTask.runSync(CustomBoards.getInstance().getPlugin(),
                             () -> this.getBackToGui().run());
        }
      });
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}
