package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.CustomKeyProvider;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.utils.TriConsumer;

public class SetListMultipleChoiceTransform
		extends EditTransform {

	private Class<? extends CustomKeyProvider> keyProvider;
	private CustomKeyProvider keyProviderInstance;

	public SetListMultipleChoiceTransform(Field field,
			Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
		this.keyProvider = field
				.getAnnotation(GuiTransformField.class).customList();
		try {
			this.keyProviderInstance = this.keyProvider
					.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openGui(Player player) {
		openGui(player, 0);
	}

	public void openGui(Player player, int page) {

		int itemPerPage = 3 * 7;

		try {
			this.keyProviderInstance.init(player);
			List<String> strings = this.keyProviderInstance
					.getKeyChoose();
			this.field.setAccessible(true);
			
			@SuppressWarnings("unchecked")
			List<Object> objs = (List<Object>) this.field
					.get(this.object);
			this.field.setAccessible(false);
			List<String> selected = objs.stream()
					.map(this.keyProviderInstance::keyToString).toList();

			int start = page * itemPerPage;
			int end = Math.min(strings.size(),
					(page + 1) * itemPerPage);
			int maxPageCount = (int) Math
					.floor(Double.valueOf(strings.size())
							/ Double.valueOf(itemPerPage))
					+ 1;
			int maxPage = maxPageCount - 1;

			GuiBuilder gui = new GuiBuilder().name("§eChoix")
					.fillSide(
							new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))
					.size(5 * 9);

			for (int i = start; i < end; i++) {
				String string = strings.get(i);
				Object obj = this.keyProviderInstance
						.stringToKey(string);
				var guiItem = this.keyProviderInstance.create(string);
				boolean contains = selected.contains(string);

				if (contains) {
					guiItem.icon(Material.DIAMOND).description(
							"§aSelectionné", "§7Cliquez pour déselectionner");
				} else {
					guiItem.description("",
							"§7Cliquez pour selectionner cette valeur");
				}

				gui.item(guiItem.clickHandler(() -> {
					if (contains) {
						objs.remove(obj);
					} else {
						objs.add(obj);
					}
					openGui(player, page);
				}).build());
			}

			if (page > 0) {
				gui.item(47,
						new GuiItemBuilder().icon(Material.ARROW)
								.name("§ePage précèdente")
								.clickHandler(() -> openGui(player, page - 1))
								.build());
			}
			if (page < maxPage) {
				gui.item(51,
						new GuiItemBuilder().icon(Material.ARROW)
								.name("§ePage suivante")
								.clickHandler(() -> openGui(player, page + 1))
								.build());
			}

			gui.item(9,
					new GuiBackItem(z -> this.getBackToGui().run()));

			gui.build().open(player);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
