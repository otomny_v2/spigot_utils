package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiSortOrder;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.utils.StringUtils;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;

public class EnumEditTransform extends EditTransform {

	public EnumEditTransform(Field field, Object object,
			TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		openGui(player, 0);
	}

	private void openGui(Player player, int page) {
		GuiTransformField fieldData = this.field
				.getAnnotation(GuiTransformField.class);
		Class<?> hold = this.field.getType();
		try {
			Enum[] enumValues = (Enum[]) ReflectionUtils
					.enumList(hold);

			List<Enum> enumValuesList = Arrays.asList(enumValues);
			if (fieldData.sort() == GuiSortOrder.ASC) {
				Collections.sort(enumValuesList, (o1, o2) -> {

					return o1.ordinal() - o2.ordinal();
				});
			} else {
				Collections.sort(enumValuesList, (o1, o2) -> {
					return o1.toString().compareTo(o2.toString());
				});
			}
			final int ITEM_PAGE_COUNT = 4 * 7;
			int maxPageCount = (int) Math
					.floor(Double.valueOf(enumValues.length)
							/ Double.valueOf(ITEM_PAGE_COUNT))
					+ 1;

			var guiBuilder = new GuiBuilder()
					.name("§eChoix liste " + name + " (page " + page + ")")
					.size(6 * 9)
					.fillSide(Material.BLUE_STAINED_GLASS_PANE);

			if (page < 0) {
				openGui(player, 0);
			} else if (page > maxPageCount) {
				openGui(player, maxPageCount);
			}
			int startPage = page * ITEM_PAGE_COUNT;
			int endPage = Math.min(enumValues.length,
					(page + 1) * ITEM_PAGE_COUNT);

			for (int i = startPage; i < endPage; i++) {
				Object enumValue = enumValuesList.get(i);
				guiBuilder.item(new GuiItemBuilder()
						.name(StringUtils
								.capitalize("§e" + enumValue.toString()))
						.icon(Material.NAME_TAG)
						.clickHandler((g, p, clickType, slot) -> {
							this.field.setAccessible(true);
							this.setter.accept(this.field, this.object,
									enumValue.toString());
							this.field.setAccessible(false);
							this.getBackToGui().run();
							return true;
						}).build());
			}

			if (page > 0) {
				guiBuilder.item(48,
						new GuiItemBuilder().name("§7Page précèdente")
								.icon(Material.ARROW)
								.clickHandler(() -> openGui(player, page - 1))
								.build());
			}
			if (page < maxPageCount) {
				guiBuilder.item(50,
						new GuiItemBuilder().icon(Material.ARROW)
								.name("§7Page suivante")
								.clickHandler(() -> openGui(player, page + 1))
								.build());
			}

			guiBuilder.item(9, new GuiBackItem(this.getBackToGui()));

			guiBuilder.build().open(player);

		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
