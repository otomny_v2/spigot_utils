package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;
import net.wesjd.anvilgui.AnvilGUI;

public class StringMapKeyValueTransform extends EditTransform {

  public StringMapKeyValueTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
  }

  @Override
  public void openGui(Player player) {
    openGui(player, 0);
  }

  @SuppressWarnings("unchecked")
  private void openGui(Player player, int page) {

    final int ITEM_PAGE_COUNT = 3 * 7;

    try {
      this.field.setAccessible(true);
      Map<String, String> map = (Map<String, String>) this.field.get(this.object);

      this.field.setAccessible(false);

      int maxPageCount = map.keySet().size() / ITEM_PAGE_COUNT;
      if (page < 0) {
        openGui(player, 0);
      } else if (page > maxPageCount) {
        openGui(player, maxPageCount);
      }
      int startPage = page * ITEM_PAGE_COUNT;
      int endPage = Math.min(map.size(), (page + 1) * ITEM_PAGE_COUNT);

      List<Entry<String, String>> list = new ArrayList<>(map.entrySet());

      GuiBuilder guiBuilder = new GuiBuilder()
          .name("§e" + strip(this.name))
          .size(5 * 9)
          .fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

      for (int i = startPage; i < endPage; i++) {
        String key = list.get(i).getKey();
        String value = map.get(key);
        guiBuilder
            .item(
                new GuiItemBuilder()
                    .icon(Material.PAPER)
                    .name("§eClé : " + key)
                    .description(
                        "",
                        "§6Valeur: " + value,
                        "",
                        "§e- Clic droit : supprimer",
                        "§e- Clic gauche : modifier la valeur")
                    .clickHandler((gui, pl, click, slot) -> {
                      if (click == ClickType.LEFT) {
                        // Editer la valeur
                        try {

                          new AnvilGUI.Builder()
                              .plugin(CustomBoards.getInstance().getPlugin())
                              .title("§eRentrez la valeur")
                              .text(value)
                              .itemLeft(new ItemStack(Material.PAPER))
                              .onClose(x -> {
                                openGui(player, page);
                              })
                              .onComplete((p, value_) -> {
                                map.replace(key, value_);
                                return AnvilGUI.Response.close();
                              })
                              .open(player);
                        } catch (
                            IllegalArgumentException
                            | SecurityException e) {
                          e.printStackTrace();
                        }
                        return true;
                      }
                      if (click == ClickType.RIGHT) {
                        // Supprimer
                        GuiAcceptItem
                            .openAccept(
                                "§cSupprimer cet élément ?",
                                player,
                                () -> {
                                  // Cancel
                                  openGui(player, page);
                                },
                                () -> {
                                  // Accept
                                  map.remove(key);
                                  openGui(player, page);
                                });
                        return true;
                      }
                      return true;
                    })
                    .build());
      }

      if (page == maxPageCount) {
        guiBuilder
            .item(
                new GuiItemBuilder()
                    .name("§aAjouter un élément")
                    .icon(Material.LIME_CONCRETE)
                    .description("", "§7Cliquez pour ajouter un élément")
                    .clickHandler((gui, pl, click, slot) -> {
                      // Ajouter
                      try {
                        new AnvilGUI.Builder()
                            .plugin(CustomBoards.getInstance().getPlugin())
                            .title("§e Rentrez la clé")
                            .text("i")
                            .itemLeft(new ItemStack(Material.PAPER))
                            .onClose(x -> {
                              openGui(player, page);
                            })
                            .onComplete((p, key) -> {
                              map.put(key, "EMPTY");
                              return AnvilGUI.Response.close();
                            })
                            .open(player);
                      } catch (
                          IllegalArgumentException
                          | SecurityException e) {
                        e.printStackTrace();
                      }
                      return true;
                    })
                    .build());
      }

      if (page > 0) {
        guiBuilder.item(39, new GuiItemBuilder()
            .name("§7Page précèdente")
            .icon(Material.ARROW)
            .clickHandler(() -> openGui(player, page - 1))
            .build());
      }
      if (page < maxPageCount) {
        guiBuilder.item(41, new GuiItemBuilder()
            .icon(Material.ARROW)
            .name("§7Page suivante")
            .clickHandler(() -> openGui(player, page + 1))
            .build());
      }

      guiBuilder.item(0, new GuiBackItem(z -> this.getBackToGui().run()));

      guiBuilder
          .build()
          .open(player);

    } catch (IllegalArgumentException | IllegalAccessException
        | SecurityException e) {
      e.printStackTrace();
      this.getBackToGui().run();
    }
  }

}
