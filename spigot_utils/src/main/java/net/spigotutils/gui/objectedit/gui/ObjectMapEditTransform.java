package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.gui.objectedit.GuiTransformer;
import net.spigotutils.gui.objectedit.StringToKeyTransform;
import net.spigotutils.nms.sign.SignGUI;
import net.spigotutils.nms.sign.SignResponse;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;


public class ObjectMapEditTransform extends EditTransform {

  private Class<?> parameterType;

  public ObjectMapEditTransform(Field field, Object object,
                                TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
    this.parameterType = ReflectionUtils.getValueTypeOfMapField(field);
  }

  @Override
  public void openGui(Player player) {
    openGui(player, 0);
  }

  @SuppressWarnings("unchecked")
  private void openGui(Player player, int page) {

    final int ITEM_PAGE_COUNT = 3 * 7;

    try {
      this.field.setAccessible(true);
      Map<Object, Object> map =
          (Map<Object, Object>)this.field.get(this.object);

      Class<? extends StringToKeyTransform> clazz =
          this.field.getAnnotation(GuiTransformField.class).keyTransform();
      StringToKeyTransform keyTransform = clazz.getConstructor().newInstance();
      this.field.setAccessible(false);

      int maxPageCount = map.keySet().size() / ITEM_PAGE_COUNT;
      if (page < 0) {
        openGui(player, 0);
      } else if (page > maxPageCount) {
        openGui(player, maxPageCount);
      }
      int startPage = page * ITEM_PAGE_COUNT;
      int endPage = Math.min(map.size(), (page + 1) * ITEM_PAGE_COUNT);

      List<Entry<Object, Object>> list = new ArrayList<>(map.entrySet());

      GuiBuilder guiBuilder =
          new GuiBuilder()
              .name("§e" + strip(this.name))
              .size(5 * 9)
              .fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

      for (int i = startPage; i < endPage; i++) {
        Object key = list.get(i).getKey();

        guiBuilder.item(
            new GuiItemBuilder()
                .icon(Material.PAPER)
                .name("§eClé : " + key.toString())
                .description("", "§7Contenu : ")
                .description(map.get(key).toString().split("\\n"))
                .description("§c- Clique droit pour supprimer",
                             "§a- Clique molette pour dupliquer",
                             "§a- Clique gauche pour éditer")
                .clickHandler((gui, pl, click, slot) -> {
                  if (click == ClickType.LEFT) {
                    // Editer
                    GuiTransformer
                        .of(map.get(key), () -> { openGui(player, page); })
                        .openGUI(player);
                    return true;
                  }
                  if (click == ClickType.MIDDLE) {
                    Object newKey = keyTransform.duplicate(key);
                    Object toClone = map.get(key);
                    if (toClone instanceof Cloneable) {
                      try {
                        Object clonedInstance = ReflectionUtils.callMethod(
                            parameterType, toClone, "clone", new Class<?>[] {},
                            new Object[] {});
                        map.put(newKey, clonedInstance);
                      } catch (Exception e) {
                        e.printStackTrace();
                      }
                    }
                    openGui(player, page);
                    return true;
                  }
                  if (click == ClickType.RIGHT) {
                    // Supprimer
                    GuiAcceptItem.openAccept("§cSupprimer cet élément ?",
                                             player,
                                             ()
                                                 ->
                                             // Cancel
                                             openGui(player),
                                             () -> {
                                               // Accept
                                               map.remove(key);
                                               openGui(player, page);
                                             });
                    return true;
                  }
                  return true;
                })
                .build());
      }

      if (page == maxPageCount) {
        guiBuilder.item(
            new GuiItemBuilder()
                .name("§aAjouter un élément")
                .icon(Material.LIME_CONCRETE)
                .description("", "§7Cliquez pour ajouter un élément")
                .clickHandler((gui, pl, click, slot) -> {
                  // Ajouter
                  try {
                    final Object createdElement =
                        this.parameterType.getConstructor().newInstance();

                    new SignGUI.Builder(CustomBoards.getInstance().getPlugin())
                        .firstLine("Rentrez la clé")
                        .secondLine("")
                        .end(s -> {
                          // On entre la clé
                          Object createdKey = keyTransform.key(s);
                          GuiTransformer
                              .of(createdElement,
                                  () -> {
                                    map.put(createdKey, createdElement);
                                    openGui(player, page);
                                  })
                              .openGUI(player);
                        })
                        .onComplete(s -> {
                          try {
                            Object key = keyTransform.key(s);
                            if (map.containsKey(key)) {
                              return SignResponse.text("§cClé Existante");
                            }
                            return SignResponse.close();
                          } catch (Exception e) {
                            return SignResponse.text("§aErreur transformation");
                          }
                        })
                        .open(player);

                  } catch (InstantiationException | IllegalAccessException |
                           IllegalArgumentException |
                           InvocationTargetException | NoSuchMethodException |
                           SecurityException e) {
                    e.printStackTrace();
                  }
                  return true;
                })
                .build());
      }

      if (page > 0) {
        guiBuilder.item(39, new GuiItemBuilder()
                                .name("§7Page précèdente")
                                .icon(Material.ARROW)
                                .clickHandler(() -> openGui(player, page - 1))
                                .build());
      }
      if (page < maxPageCount) {
        guiBuilder.item(41, new GuiItemBuilder()
                                .icon(Material.ARROW)
                                .name("§7Page suivante")
                                .clickHandler(() -> openGui(player, page + 1))
                                .build());
      }

      guiBuilder.item(0, new GuiBackItem(z -> this.getBackToGui().run()));

      guiBuilder.build().open(player);

    } catch (IllegalArgumentException | IllegalAccessException |
             InstantiationException | InvocationTargetException |
             NoSuchMethodException | SecurityException e) {
      e.printStackTrace();
      this.getBackToGui().run();
    }
  }
}