package net.spigotutils.gui.objectedit;

public class StringToIntKeyTransform implements StringToKeyTransform {

	@Override
	public Object key(String value) {
		return Integer.parseInt(value);
	}

	@Override
	public Object duplicate(Object key) {
		return ((int) key) + 1;
	}

}
