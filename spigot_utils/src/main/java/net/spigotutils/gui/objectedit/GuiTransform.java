package net.spigotutils.gui.objectedit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface GuiTransform {

	String name() default "ClassName";
	
}
