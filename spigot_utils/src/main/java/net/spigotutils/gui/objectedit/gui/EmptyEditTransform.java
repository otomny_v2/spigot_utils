package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.utils.TriConsumer;

public class EmptyEditTransform extends EditTransform {

  public EmptyEditTransform(Field field, Object object,
                            TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
  }

  @Override
  public void openGui(Player player) {
    getBackToGui().run();
  }
}
