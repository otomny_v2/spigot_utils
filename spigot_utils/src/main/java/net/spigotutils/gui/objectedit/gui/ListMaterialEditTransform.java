package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.GuiListener;
import net.spigotutils.gui.GuiType;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.utils.TriConsumer;

public class ListMaterialEditTransform extends EditTransform {

  public ListMaterialEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
  }

  @Override
  public void openGui(Player player) {

    try {
      this.field.setAccessible(true);
      List<Material> materials_ = (List<Material>) this.field.get(this.object);
      this.field.setAccessible(false);
      if (materials_ == null) {
        materials_ = new ArrayList<>();
      }
      final var materials = materials_;
      GuiBuilder guiBuilder = new GuiBuilder()
        .name("§eEditions matériaux")
        .size(5*9)
        .fillSide(Material.YELLOW_STAINED_GLASS_PANE);
      
  
      for (int i = 0; i < materials.size(); i++) {
        final int index = i;
        Material mat = materials.get(i);
        guiBuilder
        .item(new GuiItemBuilder()
          .name(mat.toString())
          .icon(mat)
          .clickHandler(() -> openEdit(materials, index, player))
          .build());
      }
  
      guiBuilder
        .item(new GuiItemBuilder()
          .name("§aAjouter un matériel")
          .icon(Material.LIME_CONCRETE)
          .clickHandler(() -> openEdit(materials, materials.size(), player))
          .build());
  
      guiBuilder
        .item(9, new GuiBackItem(this.getBackToGui()));
  
      
      guiBuilder.build()
        .open(player);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public void openEdit(List<Material> materials, int index, Player player) {
    Material display = Material.PAPER;
    if(materials.size() > index){
      display = materials.get(index);
    }
    new GuiBuilder()
        .type(GuiType.HOPPER)
        .name("§e" + super.name)
        .item(0, new GuiBackItem(this::openGui))
        .item(
            2,
            new GuiItemBuilder()
                .icon(display)
                .name("§f")
                .description("", "§fCliquez sur un item de votre", "§finventaire pour appliquer le matériel")
                .build())
        .listener(new GuiListener() {

          @Override
          public void setup() {
          }

          @Override
          public void onInventoryClose(InventoryCloseEvent event) {
          }

          @Override
          public void onInventoryClick(Player user, InventoryClickEvent event) {
            int slot = event.getRawSlot();
            if (slot > 4) {
              if (event.getCurrentItem() != null) {
                Material type = event.getCurrentItem().getType();
                try {
                  ListMaterialEditTransform.this.field.setAccessible(true);
                  if(!materials.contains(type)){
                    if(index >= materials.size()){
                      materials.add(type);
                    }else{
                      materials.set(index, type);
                    }
                  }
                  ListMaterialEditTransform.this.field.setAccessible(false);
                  event.setCancelled(true);
                  openGui(player);
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            }
          }
        })
        .build()
        .open(player);
  }

  /*
   * Class<?> materialClazz = Class.forName("org.bukkit.Material");
   * Method valueOf = materialClazz.getMethod("valueOf", String.class);
   * Object material = valueOf.invoke(null, ((String) string).toUpperCase());
   * field.set(obj, material);
   */
}
