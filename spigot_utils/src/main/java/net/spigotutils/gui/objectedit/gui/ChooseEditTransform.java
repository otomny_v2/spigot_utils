package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.GuiListener;
import net.spigotutils.gui.GuiType;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.utils.TriConsumer;

public class ChooseEditTransform extends EditTransform {

	public ChooseEditTransform(Field field, Object object,
			TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {

		Material display = Material.PAPER;
		this.field.setAccessible(true);
		try {
			if (this.field.get(this.object) != null) {
				display = Material
						.valueOf(this.field.get(this.object).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.field.setAccessible(false);

		new GuiBuilder().type(GuiType.HOPPER).name("§e" + super.name)
				.item(0, new GuiBackItem(p -> this.getBackToGui().run()))
				.item(2, new GuiItemBuilder().icon(display).name("§f")
						.description("", "§fCliquez sur un item de votre",
								"§finventaire pour appliquer le matériel")
						.build())
				.listener(new GuiListener() {

					@Override
					public void setup() {
					}

					@Override
					public void onInventoryClose(
							InventoryCloseEvent event) {
					}

					@Override
					public void onInventoryClick(Player user,
							InventoryClickEvent event) {
						try {
							int slot = event.getRawSlot();
							if (slot > 4) {
								if (event.getCurrentItem() != null) {
									Material type = event.getCurrentItem()
											.getType();
									String typeToString = type.toString()
											.toLowerCase();
									ChooseEditTransform.this.field
											.setAccessible(true);
									ChooseEditTransform.this.setter.accept(
											ChooseEditTransform.this.field,
											ChooseEditTransform.this.object,
											typeToString);
									ChooseEditTransform.this.field
											.setAccessible(false);
									event.setCancelled(true);
									openGui(player);

								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).build().open(player);
	}

}
