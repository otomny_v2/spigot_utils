package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;
import net.wesjd.anvilgui.AnvilGUI;

@SuppressWarnings("unchecked")
public class StringListEditTransform extends EditTransform {

	public StringListEditTransform(Field field, Object object,
			TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		try {
			this.field.setAccessible(true);
			List<String> string = (List<String>) this.field
					.get(this.object);
			this.field.setAccessible(false);
			GuiBuilder guiBuilder = new GuiBuilder()
					.name("§e" + strip(this.name)).size(5 * 9).fillSide(
							new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

			for (int i = 0; i < string.size(); i++) {
				final int index = i;
				guiBuilder.item(new GuiItemBuilder()
						.icon(new ItemStack(Material.PAPER, i + 1))
						.name("§eLigne n°" + (i + 1))
						.description("", "§7Contenu : " + string.get(i),
								"§c- Clique droit pour supprimer",
								"§a- Clique gauche pour éditer")
						.clickHandler((gui, pl, click, slot) -> {
							if (click == ClickType.LEFT) {
								// Editer
								new AnvilGUI.Builder()
										.plugin(
												CustomBoards.getInstance().getPlugin())
										.text(string.get(index))
										.title("§7Edition ligne n°" + (index + 1))
										.onComplete((p, res) -> {
											string.set(index, res);
											this.field.setAccessible(true);
											this.setter.accept(this.field, this.object,
													string);
											this.field.setAccessible(false);
											openGui(player);
											return AnvilGUI.Response.close();
										}).open(player);
								return true;
							}
							if (click == ClickType.RIGHT) {
								// Supprimer
								GuiAcceptItem.openAccept(
										"§cSupprimer cette ligne ?", player,
										() -> openGui(player), () -> {
											// Accept
											string.remove(index);
											this.field.setAccessible(true);
											this.setter.accept(this.field, this.object,
													string);
											this.field.setAccessible(false);
											openGui(player);
										});
								return true;
							}
							return true;
						}).build());
			}

			guiBuilder.item(new GuiItemBuilder()
					.name("§aAjouter une ligne")
					.icon(Material.LIME_CONCRETE)
					.description("", "§7Cliquez pour ajouter une ligne")
					.clickHandler((gui, pl, click, slot) -> {
						// Ajouter
						new AnvilGUI.Builder()
								.plugin(CustomBoards.getInstance().getPlugin())
								.text("").title("§7Ajouter une ligne")
								.onComplete((p, res) -> {
									string.add(res);
									this.field.setAccessible(true);
									this.setter.accept(this.field, this.object,
											string);
									this.field.setAccessible(false);
									openGui(player);
									return AnvilGUI.Response.close();
								}).open(player);
						return true;
					}).build());

			guiBuilder.item(0,
					new GuiBackItem(z -> this.getBackToGui().run()));

			guiBuilder.build().open(player);
		} catch (Exception e) {
			e.printStackTrace();
			this.getBackToGui().run();
		}
	}

}
