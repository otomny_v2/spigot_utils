package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.utils.TriConsumer;

public class LocationEditTransform extends EditTransform {

	public LocationEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		try {
			this.field.setAccessible(true);
			this.field.set(this.object, player.getLocation());
			player.sendMessage("§aVous avez modifié les données selon votre position.");
			this.field.setAccessible(false);
			this.getBackToGui().run();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
