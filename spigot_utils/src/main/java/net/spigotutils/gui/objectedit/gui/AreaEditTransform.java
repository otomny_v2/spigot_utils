package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.nbt.Area;
import net.spigotutils.nbt.AreaSelector;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;

public class AreaEditTransform extends EditTransform {

  public AreaEditTransform(Field field, Object object,
                           TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
  }

  @Override
  public void openGui(Player player) {
    player.closeInventory();
    AreaSelector.Builder(CustomBoards.getInstance().getPlugin())
        .complete((l1, l2) -> {
          try {
            field.setAccessible(true);
            field.set(this.object, new Area(l1, l2));
            field.setAccessible(false);
          } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
          }
          getBackToGui().run();
        })
        .end(() -> {
          // Quand annulé
          player.sendMessage("§cOpération annulée");
          getBackToGui().run();
        })
        .build(player);
  }
}
