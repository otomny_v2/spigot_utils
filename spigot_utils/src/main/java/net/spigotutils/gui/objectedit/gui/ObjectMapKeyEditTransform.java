package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.gui.objectedit.GuiTransformer;
import net.spigotutils.utils.StringUtils;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;

@SuppressWarnings("unchecked")
public class ObjectMapKeyEditTransform extends EditTransform {

	private static final int ITEM_PER_PAGE = 20;
	private Class<?> valueType;
	private Class<?> keyType;

	public ObjectMapKeyEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
		this.valueType = ReflectionUtils.getValueTypeOfMapField(field);
		this.keyType = ReflectionUtils.getKeyTypeOfMapField(field);
	}

	@Override
	public void openGui(Player player) {
		try {
			this.field.setAccessible(true);
			Map<Object, Object> map = (Map<Object, Object>) this.field.get(this.object);
			this.field.setAccessible(false);
			
			GuiBuilder guiBuilder = new GuiBuilder()
				.name("§e" + strip(this.name))
				.size(5 * 9)
				.fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));
			
			
			for(Object key : map.keySet()) {
				guiBuilder
				.item(
					new GuiItemBuilder()
						.icon(Material.PAPER)
						.name("§eClé : "+key.toString())
						.description(
							"",
							"§7Contenu : " + map.get(key).toString(),
							"§c- Clique droit pour supprimer",
							"§a- Clique gauche pour éditer")
						.clickHandler((gui, pl, click, slot) -> {
							if (click == ClickType.LEFT) {
								// Editer
								GuiTransformer.of(map.get(key), () -> {
									openGui(player);
								}).openGUI(player);
								return true;
							}
							if (click == ClickType.RIGHT) {
								// Supprimer
								GuiAcceptItem
									.openAccept(
										"§cSupprimer cet élément ?",
										player,
										(z) -> {
											// Cancel
											openGui(player);
										},
										(z) -> {
											// Accept
											map.remove(key);
											openGui(player);
										});
								return true;
							}
							return true;
						})
						.build());
			}
			
			guiBuilder
			.item(
				new GuiItemBuilder()
					.name("§aAjouter un élément")
					.icon(Material.LIME_CONCRETE)
					.description("", "§7Cliquez pour ajouter un élément")
					.clickHandler((gui, pl, click, slot) -> {
						// Ajouter
						enumEdit(player, 1, enumValue -> {
							try {
								Object createdValue = this.valueType.getConstructor().newInstance();
								GuiTransformer.of(createdValue,() -> {
									map.put(enumValue, createdValue);
									this.getBackToGui().run();
								}).openGUI(player);
							} catch (
								InstantiationException
								| IllegalAccessException
								| IllegalArgumentException
								| InvocationTargetException
								| NoSuchMethodException
								| SecurityException e) {
								e.printStackTrace();
							}
						});
						return true;
					})
					.build());

		guiBuilder.item(0, new GuiBackItem(z -> this.getBackToGui().run()));

		guiBuilder
			.build()
			.open(player);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			this.getBackToGui().run();
		}
		
	}
	
	private void enumEdit(Player player, int page, Consumer<Object> accept) {
		GuiTransformField fieldData = this.field.getAnnotation(GuiTransformField.class);
		// Map<Enum, ObjectEdit>
		
		try {
			Object[] enumValues = ReflectionUtils.enumList(this.keyType);
			
			List<Object> enumValuesList = Arrays.asList(enumValues);
			Collections.sort(enumValuesList, (o1, o2) -> {
				return o1.toString().compareTo(o2.toString());
			});
			
			if(enumValues.length > ITEM_PER_PAGE) {
				int minPagePlus = (int) Math.floor(enumValues.length / ITEM_PER_PAGE) + 1;
				if(page < 1) {
					enumEdit(player, page+1, accept);
				}else if(page > minPagePlus) {
					enumEdit(player, page-1, accept);
				}
				String name = fieldData.fieldName();
				GuiBuilder gui = new GuiBuilder().name("§eChoix liste " + name+" (page "+page+")").size(4 * 9);
				int minDisplay = Math.min(page*ITEM_PER_PAGE, enumValues.length);
				for(int i = (page-1)*ITEM_PER_PAGE; i < minDisplay; i++) {
					Object enumValue = enumValuesList.get(i);
					gui
					.item(
							new GuiItemBuilder()
									.name(StringUtils.capitalize("§e" + enumValue.toString()))
									.icon(Material.NAME_TAG)
									.clickHandler((g, p, clickType, slot) -> {
										accept.accept(enumValue);
										return true;
									})
									.build());
				}

				if(page > 1) {
					gui.item(39, new GuiItemBuilder().name("§7Précédent").icon(Material.ARROW).clickHandler((a,b,c,d) -> {
						enumEdit(player, page-1, accept);
						return true;
					}).build());
				}
				if(page < minPagePlus) {
					gui.item(41, new GuiItemBuilder().name("§7Suivant").icon(Material.ARROW).clickHandler((a,b,c,d) -> {
						enumEdit(player, page+1, accept);
						return true;
					}).build());
				}
				
				gui.item(36, new GuiBackItem(p -> this.getBackToGui().run()));
				
				gui.build().open(player);
			}else {
				String name = fieldData.fieldName();
				GuiBuilder gui = new GuiBuilder().name("§eChoix liste " + name).size(4 * 9);

				for (Object enumValue : enumValuesList) {
					gui
							.item(
									new GuiItemBuilder()
											.name(StringUtils.capitalize("§e" + enumValue.toString()))
											.icon(Material.NAME_TAG)
											.clickHandler((g, p, clickType, slot) -> {
												accept.accept(enumValue);
												return true;
											})
											.build());
				}
				gui.item(36, new GuiBackItem(p -> this.getBackToGui().run()));
				gui.build().open(player);
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
