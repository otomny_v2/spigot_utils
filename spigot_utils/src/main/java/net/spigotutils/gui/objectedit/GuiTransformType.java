package net.spigotutils.gui.objectedit;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import lombok.Getter;
import net.spigotutils.gui.objectedit.gui.ActionPlayerTransform;
import net.spigotutils.gui.objectedit.gui.AnvilEditTransform;
import net.spigotutils.gui.objectedit.gui.AreaEditTransform;
import net.spigotutils.gui.objectedit.gui.BooleanEditTransform;
import net.spigotutils.gui.objectedit.gui.ChooseEditTransform;
import net.spigotutils.gui.objectedit.gui.CustomKeyEditTransform;
import net.spigotutils.gui.objectedit.gui.EditTransform;
import net.spigotutils.gui.objectedit.gui.EnumEditTransform;
import net.spigotutils.gui.objectedit.gui.ListMaterialEditTransform;
import net.spigotutils.gui.objectedit.gui.LocationEditTransform;
import net.spigotutils.gui.objectedit.gui.LocationListEditTransform;
import net.spigotutils.gui.objectedit.gui.ObjectEditTransform;
import net.spigotutils.gui.objectedit.gui.ObjectListEditTransform;
import net.spigotutils.gui.objectedit.gui.ObjectMapEditTransform;
import net.spigotutils.gui.objectedit.gui.ObjectMapKeyEditTransform;
import net.spigotutils.gui.objectedit.gui.SetEnumMultipleChoiceTransform;
import net.spigotutils.gui.objectedit.gui.SetListMultipleChoiceTransform;
import net.spigotutils.gui.objectedit.gui.SignEditTransform;
import net.spigotutils.gui.objectedit.gui.StringChatEditTransform;
import net.spigotutils.gui.objectedit.gui.StringListEditTransform;
import net.spigotutils.gui.objectedit.gui.StringMapKeyValueTransform;
import net.spigotutils.gui.objectedit.gui.StringSchematicEditTransform;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;


public enum GuiTransformType {

  AUTO_DETECT(null, null),
  STRING(AnvilEditTransform.class, (field, obj, string) -> {
    try {
      field.set(obj, string);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  ACTION(ActionPlayerTransform.class, null),
  INT(SignEditTransform.class, (field, obj, string) -> {
    try {
      if (field.getType() == int.class) {
        field.setInt(obj, Integer.parseInt((String)string));
      } else if (field.getType() == Integer.class) {
        field.set(obj, Integer.parseInt((String)string));
      } else {
        field.setLong(obj, Long.parseLong((String)string));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }),
  DOUBLE(SignEditTransform.class, (field, obj, string) -> {
    try {
      field.setDouble(obj, Double.parseDouble((String)string));
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  MATERIAL_STRING(ChooseEditTransform.class, (field, obj, string) -> {
    try {
      field.set(obj, string);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  AREA(AreaEditTransform.class, TriConsumer.none()),
  MATERIAL_LIST(ListMaterialEditTransform.class, TriConsumer.none()),
  MATERIAL(ChooseEditTransform.class, (field, obj, string) -> {
    try {
      Class<?> materialClazz = Class.forName("org.bukkit.Material");
      Method valueOf = materialClazz.getMethod("valueOf", String.class);
      Object material = valueOf.invoke(null, ((String)string).toUpperCase());
      field.set(obj, material);
    } catch (ClassNotFoundException | NoSuchMethodException |
             SecurityException | IllegalAccessException |
             IllegalArgumentException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }),
  ENUM(EnumEditTransform.class, (field, obj, string) -> {
    Class<?> ennum = field.getType();
    if (ennum.isEnum()) {
      try {
        Object[] values = ReflectionUtils.enumList(ennum);
        for (Object enumValue : values) {
          if (enumValue.toString().equals(string)) {
            field.set(obj, enumValue);
            break;
          }
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        e.printStackTrace();
      }
    }
  }),
  ENUM_MULTIPLE_CHOICE(SetEnumMultipleChoiceTransform.class,
                       TriConsumer.none()),
	LIST_MULTIPLE_CHOICE(SetListMultipleChoiceTransform.class, TriConsumer.none()),
  STRING_LIST(StringListEditTransform.class, (field, obj, list) -> {
    try {
      field.set(obj, list);
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  BOOLEAN(BooleanEditTransform.class, (field, obj, string) -> {
    final boolean val = Boolean.valueOf((String)string);
    try {
      field.setAccessible(true);
      if (field.getType() == boolean.class) {
        field.setBoolean(obj, val);
      } else {
        field.set(obj, Boolean.valueOf(val));
      }
      field.setAccessible(false);
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  EDIT_LIST_LOCATION(LocationListEditTransform.class, (a, b, c) -> {}),
  EDIT_SCHEMATIC(StringSchematicEditTransform.class, (a, b, c) -> {}),
  EDIT_LIST(ObjectListEditTransform.class, (field, obj, data) -> {}),
  EDIT_MAP(ObjectMapEditTransform.class, (a, b, c) -> {}),
  EDIT_OBJ(ObjectEditTransform.class, (a, b, c) -> {}),
  EDIT_MAP_ENUM(ObjectMapKeyEditTransform.class, (a, b, c) -> {}),
  MAP_STR_STR(StringMapKeyValueTransform.class, TriConsumer.none()),
	@Deprecated
  CUSTOM_KEY(CustomKeyEditTransform.class, (a, b, c) -> {}),
  LIST_CHOICE(CustomKeyEditTransform.class, (a, b, c) -> {}),
  CUSTOM(null, TriConsumer.none()),
  LOCATION(LocationEditTransform.class, (field, obj, location) -> {
    try {
      field.set(obj, location);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }),
  STRING_CHAT(StringChatEditTransform.class, (field, obj, c) -> {
    try {
      field.set(obj, c);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
  });

  @Getter public Class<? extends EditTransform> edit;
  @Getter public TriConsumer<Field, Object, Object> setter;

  GuiTransformType(Class<? extends EditTransform> edit,
                   TriConsumer<Field, Object, Object> setter) {
    this.edit = edit;
    this.setter = setter;
  }
}
