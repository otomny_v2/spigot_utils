package net.spigotutils.gui.objectedit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.bukkit.Material;

import net.spigotutils.gui.objectedit.gui.EditTransform;
import net.spigotutils.gui.objectedit.gui.EmptyEditTransform;


@Retention(RetentionPolicy.RUNTIME)
public @interface GuiTransformField {

  Material display() default Material.BOOK;

  String fieldName() default "";

  String[] description() default {};

  GuiTransformType type() default GuiTransformType.AUTO_DETECT;

  GuiSortOrder sort() default GuiSortOrder.ALPHABETIC;

  boolean lockAdd() default false;

  Class<? extends StringToKeyTransform>
  keyTransform() default StringToIntKeyTransform.class;

  /**
   * Provide a custom GUI edition for the field type
   * @return
   */
  Class<? extends EditTransform> custom() default EmptyEditTransform.class;

  /**
   * Provide a specific value mapping 
   * @return
   */
  Class<? extends CustomKeyProvider> customList() default EmptyKeyProvider
      .class;

  Class<? extends ActionOnClickProvider> action() default EmptyAction.class;

  String db() default "";

  String collection() default "";
}
