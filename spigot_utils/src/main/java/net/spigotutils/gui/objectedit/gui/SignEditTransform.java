package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.nms.sign.SignGUI;
import net.spigotutils.nms.sign.SignResponse;
import net.spigotutils.runnable.SpigotTask;
import net.spigotutils.scoreboard.CustomBoards;
import net.spigotutils.utils.TriConsumer;

public class SignEditTransform extends EditTransform {

	public SignEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		new SignGUI.Builder(CustomBoards.getInstance().getPlugin())
				.firstLine(strip(this.name))
				.secondLine(String.join(" ", this.description))
				.end(s -> {
					this.field.setAccessible(true);
					this.setter.accept(this.field, this.object, s);
					this.field.setAccessible(false);
					SpigotTask.runSync(CustomBoards.getInstance().getPlugin(), () -> {
						this.getBackToGui().run();
					});
				}).onComplete(s -> {
					return SignResponse.close();
				})
				.open(player);
	}

}
