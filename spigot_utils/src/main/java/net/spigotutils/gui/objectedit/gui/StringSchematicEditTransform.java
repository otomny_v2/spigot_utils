package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.nbt.Schematic;
import net.spigotutils.utils.TriConsumer;

public class StringSchematicEditTransform extends EditTransform {

	private static final int ITEM_PER_PAGE = 21;

	public StringSchematicEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		// open(player, 0);
		var guiBuilder = new GuiBuilder()
				.name("§eChoix schématic")
				.size(5 * 9)
				.fillSide(Material.YELLOW_STAINED_GLASS_PANE);

		for (String schematicNameSpace : Schematic.listSchematicDBSorted(null).keySet()) {
			guiBuilder.item(new GuiItemBuilder()
					.icon(Material.CHEST)
					.name(schematicNameSpace)
					.description("§eCliquez pour sélectionner")
					.clickHandler(() -> open(player, 0, schematicNameSpace))
					.build());
		}

		guiBuilder.build()
				.open(player);
	}

	public void open(Player player, int page, String namespace) {
		GuiBuilder gui = new GuiBuilder()
				.name("§eAdministration des niveaux de blocs")
				.fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))
				.size(6 * 9);

		List<String> allBlockLevel = Schematic.listSchematicDBSorted(null).get(namespace);

		int blockLevelCount = allBlockLevel.size();
		int indexStart = page * ITEM_PER_PAGE;
		int indexEnd = (page + 1) * ITEM_PER_PAGE;
		if (indexStart >= blockLevelCount) {
			open(player, page - 1, namespace);
			return;
		}
		indexEnd = Math.min(indexEnd, blockLevelCount);

		for (int index = indexStart; index < indexEnd; index++) {
			String schem = allBlockLevel.get(index);
			gui
					.item(
							new GuiItemBuilder()
									.icon(Material.IRON_PICKAXE)
									.name("§e" + schem)
									.description("", "§aCliquez pour choisir ce schematic")
									.clickHandler((g, p, clickType, slot) -> {
										this.field.setAccessible(true);
										try {
											this.field.set(this.object, schem);
										} catch (IllegalArgumentException | IllegalAccessException e) {
											e.printStackTrace();
										}
										this.field.setAccessible(false);
										this.getBackToGui().run();
										return true;
									})
									.build());
		}
		if (page > 0) {

			// Ajout page précèdente
			gui
					.item(
							47,
							new GuiItemBuilder()
									.icon(Material.ARROW)
									.name("§7§oPage précédente")
									.clickHandler((w, x, y, z) -> {
										open(player, page - 1, namespace);
										return true;
									})
									.build());
		}
		if (page < Math.round((double) blockLevelCount / (double) ITEM_PER_PAGE)) {
			// Ajout page suivante
			gui
					.item(
							51,
							new GuiItemBuilder()
									.icon(Material.ARROW)
									.name("§7§oPage suivante")
									.clickHandler((w, x, y, z) -> {
										open(player, page + 1, namespace);
										return true;
									})
									.build());
		}

		gui.item(9, new GuiBackItem(() -> openGui(player)));

		gui.build().open(player);
	}

}
