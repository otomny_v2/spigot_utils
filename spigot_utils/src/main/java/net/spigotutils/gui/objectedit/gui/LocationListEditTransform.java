package net.spigotutils.gui.objectedit.gui;

import static net.spigotutils.chat.ColorUtils.strip;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiAcceptItem;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.utils.TriConsumer;

@SuppressWarnings("unchecked")
public class LocationListEditTransform extends EditTransform {

	public LocationListEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		try {
			this.field.setAccessible(true);
			List<Location> list = (List<Location>) this.field.get(this.object);
			this.field.setAccessible(false);
			GuiBuilder guiBuilder = new GuiBuilder()
				.name("§e" + strip(this.name))
				.size(5 * 9)
				.fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

			for (int i = 0; i < list.size(); i++) {
				final int index = i;
				Location l = list.get(i);
				guiBuilder
					.item(
						new GuiItemBuilder()
							.icon(new ItemStack(Material.PAPER, i+1))
							.name("§eLocation n°" + (i + 1))
							.description(
								"",
								"§7Contenu : §e" + l.getBlockX() + " / " + l.getBlockY() + " / " + l.getBlockZ(),
								"§c- Clique droit pour supprimer",
								"§a- Clique gauche pour changer la position")
							.clickHandler((gui, pl, click, slot) -> {
								if (click == ClickType.LEFT) {
									// Editer
									Location loc = list.get(index);
									loc.setX(player.getLocation().getX());
									loc.setY(player.getLocation().getY());
									loc.setZ(player.getLocation().getZ());
									loc.setYaw(player.getLocation().getYaw());
									loc.setPitch(player.getLocation().getPitch());
									openGui(player);
									return true;
								}
								if (click == ClickType.RIGHT) {
									// Supprimer
									GuiAcceptItem
										.openAccept(
											"§cSupprimer cet élément ?",
											player,
											(z) -> {
												// Cancel
												openGui(player);
											},
											(z) -> {
												// Accept
												list.remove(index);
												openGui(player);
											});
									return true;
								}
								return true;
							})
							.build());
			}

			guiBuilder
				.item(
					new GuiItemBuilder()
						.name("§aAjouter un élément")
						.icon(Material.LIME_CONCRETE)
						.description("", "§7Cliquez pour ajouter une position")
						.clickHandler((gui, pl, click, slot) -> {
							// Ajouter
							Location loc = player.getLocation();
							list.add(loc);
							openGui(player);
							return true;
						})
						.build());

			guiBuilder.item(0, new GuiBackItem(z -> this.getBackToGui().run()));

			guiBuilder
				.build()
				.open(player);
		} catch (Exception e) {
			e.printStackTrace();
			this.getBackToGui().run();
		}
	}
}
