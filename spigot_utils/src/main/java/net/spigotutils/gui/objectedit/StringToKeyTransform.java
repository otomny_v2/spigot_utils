package net.spigotutils.gui.objectedit;

public interface StringToKeyTransform {

	public Object key(String value);
	
	public Object duplicate(Object key);
	
}
