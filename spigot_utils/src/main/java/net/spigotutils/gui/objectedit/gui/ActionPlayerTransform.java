package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Player;

import net.spigotutils.gui.objectedit.ActionOnClickProvider;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.utils.TriConsumer;

public class ActionPlayerTransform extends EditTransform{

	private Class<? extends ActionOnClickProvider> action;
	private ActionOnClickProvider actionInstance;

	public ActionPlayerTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
		this.action = field.getAnnotation(GuiTransformField.class).action();
		try {
			this.actionInstance = this.action.getConstructor().newInstance();
		} catch (
			InstantiationException
			| IllegalAccessException
			| IllegalArgumentException
			| InvocationTargetException
			| NoSuchMethodException
			| SecurityException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openGui(Player player) {
		this.actionInstance.click(player, this.field, this.object, getBackToGui());
	}

}
