package net.spigotutils.gui.objectedit;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.spigotutils.gui.item.GuiItemBuilder;

public interface CustomKeyProvider {

  default void init(Player player) {}

  default GuiItemBuilder create(String string) {
    Material type = Material.PAPER;
    return new GuiItemBuilder().name(string).icon(type).description();
  }

  List<String> getKeyChoose();

  String keyToString(Object key);

  Object stringToKey(String key);
}
