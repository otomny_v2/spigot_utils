package net.spigotutils.gui.objectedit;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class EmptyKeyProvider implements CustomKeyProvider{

	@Override
	public void init(Player player) {}

	@Override
	public List<String> getKeyChoose() {
		return new ArrayList<>();
	}

	@Override
	public Object stringToKey(String key) {
		return new Object();
	}

	@Override
	public String keyToString(Object key) {
		return "";
	}

}
