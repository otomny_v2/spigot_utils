package net.spigotutils.gui.objectedit;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.Updatetable;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.gui.EditTransform;
import net.spigotutils.gui.objectedit.gui.EmptyEditTransform;
import net.spigotutils.nbt.Area;
import net.spigotutils.utils.StringUtils;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;

public class GuiTransformer {

  public static GuiTransformer of(Object data, Runnable onClose) {
    return new GuiTransformer(data, onClose);
  }

  public static void fast(Object data, Player player) {
    of(data, () -> {}).openGUI(player);
  }

  public static void fast(Object data, Player player, Runnable onClose) {
    of(data, onClose).openGUI(player);
  }

  private Object data;
  private Runnable onClose;

  private GuiTransformer(Object data, Runnable onClose) {
    if (!isCorrectClass(data)) {
      throw new IllegalStateException("ERROR : La classe " + data.getClass() +
                                      " n'a pas l'annotation '@GuiTransform'");
    }
    this.data = data;
    this.onClose = onClose;
  }

  public void openGUI(Player player) {
    GuiTransform clazzData =
        this.data.getClass().getAnnotation(GuiTransform.class);
    String name = clazzData.name().equals("ClassName")
                      ? this.data.getClass().getSimpleName()
                      : clazzData.name();
    GuiBuilder gui =
        new GuiBuilder()
            .name("§eÉdition " + name)
            .size(4 * 9)
            .fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE));

    gui.item(new GuiBackItem(p -> {
      p.closeInventory();
      if (this.data instanceof Updatetable update) {
        update.update();
      }
      this.onClose.run();
    }));

    searchForGuiField(data).forEach(field -> {
      GuiTransformType type = findType(field);
      GuiTransformField fieldData =
          field.getAnnotation(GuiTransformField.class);
      String fieldName = fieldData.fieldName();
      if (fieldName.equals("")) {
        fieldName = StringUtils.capitalize(field.getName());
      }
      String[] desc = fieldData.description();

      GuiItemBuilder itemBuilder =
          new GuiItemBuilder()
              .icon(fieldData.display())
              .name(fieldName)
              .description(
                  Arrays.asList(desc).stream().map(s -> "§e" + s).toList());

      field.setAccessible(true);
      try {
        Object fValue = field.get(this.data);
        if (fValue == null) {
          itemBuilder.description("§7§oLa valeur n'est pas définie");
        } else if (List.class.isAssignableFrom(field.getType())) {
          List<?> fList = (List<?>)fValue;
          itemBuilder.description("§7§oValeur(s): ");
          if (fList.size() > 7) {
            fList.subList(0, 3).forEach(
                obj
                -> itemBuilder.description(
                    "§7§o- §6" + StringUtils.clamp(obj.toString(), 40)));
            itemBuilder.description("§7..");
            fList.subList(fList.size() - 3, fList.size())
                .forEach(
                    obj
                    -> itemBuilder.description(
                        "§7§o- §6" + StringUtils.clamp(obj.toString(), 40)));
          } else {
            fList.forEach(
                obj
                -> itemBuilder.description(
                    "§7§o- §6" + StringUtils.clamp(obj.toString(), 40)));
          }

        } else if (Map.class.isAssignableFrom(field.getType())) {
          Map<?, ?> fMap = (Map<?, ?>)fValue;
          itemBuilder.description("§7§oValeur(s): ");
          if (fMap.size() > 7) {
            int i = 0;
            int d = 0;
            for (Entry<?, ?> entry : fMap.entrySet()) {
              if (i < 3 || i > fMap.size() - 4) {
                itemBuilder.description(
                    "§7§o- §6" + entry.getKey().toString() + " : " +
                    StringUtils.clamp(entry.getValue().toString(), 40));
              } else if (d == 0) {
                itemBuilder.description("§7..");
                d = 1;
              }
              i++;
            }
          } else {
            fMap.forEach((key, value) -> {
              itemBuilder.description("§7§o- §6" + key.toString() + " : " +
                                      StringUtils.clamp(value.toString(), 40));
            });
          }

        } else {
          itemBuilder.description("§7§oValeur : §6" +
                                  StringUtils.clamp(fValue.toString(), 40));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      field.setAccessible(false);

      final String _fieldName = fieldName;

      gui.item(itemBuilder
                   .clickHandler((g, p, clickType, slot) -> {
                     try {
                       var editKlass = type == GuiTransformType.CUSTOM
                                           ? fieldData.custom()
                                           : type.getEdit();
                       EditTransform edit =
                           editKlass
                               .getConstructor(Field.class, Object.class,
                                               TriConsumer.class)
                               .newInstance(field, data, type.setter)
                               .descData(_fieldName, desc);
                       edit.setBackToGui(() -> { openGUI(p); });
                       edit.openGui(player);
                     } catch (Exception e) {
                       e.printStackTrace();
                     }
                     return true;
                   })
                   .build());
    });
    gui.build().open(player);
  }

  private GuiTransformType findType(Field field) {
    if (!isCorrectField(field)) {
      throw new IllegalStateException(
          "ERROR : Le champ n'a pas l'annotation '@GuiTransformField'");
    }
    GuiTransformField fieldData = field.getAnnotation(GuiTransformField.class);
    if (fieldData.type() == GuiTransformType.AUTO_DETECT) {
      if (fieldData.customList() != EmptyKeyProvider.class) {
        return GuiTransformType.CUSTOM_KEY;
      }
      if (fieldData.custom() != EmptyEditTransform.class) {
        return GuiTransformType.CUSTOM;
      }
      if (fieldData.action() != EmptyAction.class) {
        return GuiTransformType.ACTION;
      }
      Class<?> type = field.getType();
      if (type == double.class) {
        return GuiTransformType.DOUBLE;
      }
      if (type == int.class || type == long.class || type == Integer.class ||
          type == Long.class) {
        return GuiTransformType.INT;
      }
      if (type == String.class) {
        return GuiTransformType.STRING;
      }
      if (type == Material.class) {
        return GuiTransformType.MATERIAL;
      }
      if (type == Boolean.class || type == boolean.class) {
        return GuiTransformType.BOOLEAN;
      }
      if (type.isEnum()) {
        return GuiTransformType.ENUM;
      }
      if (type == Location.class) {
        return GuiTransformType.LOCATION;
      }
      if (type == Area.class) {
        return GuiTransformType.AREA;
      }
      if (type.isAssignableFrom(Map.class)) {
        Class<?> typeValue = ReflectionUtils.getValueTypeOfMapField(field);
        Class<?> keyType = ReflectionUtils.getKeyTypeOfMapField(field);
        if (typeValue == String.class && keyType == String.class) {
          return GuiTransformType.MAP_STR_STR;
        }
        if (typeValue.isAnnotationPresent(GuiTransform.class)) {
          if (keyType.isEnum()) {
            return GuiTransformType.EDIT_MAP_ENUM;
          }
          return GuiTransformType.EDIT_MAP;
        }
      }
      if (type.isAssignableFrom(List.class)) {
        Class<?> typeList = ReflectionUtils.getTypeOfListField(field);
        if (typeList == Material.class) {
          return GuiTransformType.MATERIAL_LIST;
        }
        if (typeList == Location.class) {
          return GuiTransformType.EDIT_LIST_LOCATION;
        } else if (typeList == String.class) {
          return GuiTransformType.STRING_LIST;
        } else if (typeList.isAnnotationPresent(GuiTransform.class)) {
          return GuiTransformType.EDIT_LIST;
        }
      }
      if (type.isAssignableFrom(Collection.class)) {
        Class<?> typeList = ReflectionUtils.getTypeOfListField(field);
        if (typeList.isEnum()) {
          return GuiTransformType.ENUM_MULTIPLE_CHOICE;
        }
      }
      if (type.isAnnotationPresent(GuiTransform.class)) {
        return GuiTransformType.EDIT_OBJ;
      }
      throw new IllegalStateException(
          "ERROR : Le champ " + field.getName() +
          " ne correspond à aucun Type connu [AUTODETECT]");
    }
    return fieldData.type();
  }

  private boolean isCorrectClass(Object data) {
    return data.getClass().isAnnotationPresent(GuiTransform.class);
  }

  private boolean isCorrectField(Field field) {
    return field.isAnnotationPresent(GuiTransformField.class);
  }

  private List<Field> searchForGuiField(Object data) {
    Class<? extends Object> clazz = data.getClass();
    List<Field> fields = new ArrayList<Field>();

    for (Field field : clazz.getDeclaredFields()) {
      if (isCorrectField(field)) {
        fields.add(field);
      }
    }

    Class<? extends Object> superr = clazz.getSuperclass();

    do {
      for (Field field : superr.getDeclaredFields()) {
        if (isCorrectField(field)) {
          fields.add(field);
        }
      }
      superr = superr.getSuperclass();
    } while (superr != null);

    return fields;
  }
}
