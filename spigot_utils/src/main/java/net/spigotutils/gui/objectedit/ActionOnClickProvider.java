package net.spigotutils.gui.objectedit;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

public interface ActionOnClickProvider {

	void click(Player player, Field field, Object data, Runnable onClose);
	
}
