package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import net.spigotutils.gui.objectedit.GuiTransformer;
import net.spigotutils.utils.TriConsumer;

public class ObjectEditTransform extends EditTransform {

	public ObjectEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		try {
			super.field.setAccessible(true);
			GuiTransformer
				.of(super.field.get(super.object), () -> {
					super.field.setAccessible(false);
					this.getBackToGui().run();
				})
				.openGUI(player);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}