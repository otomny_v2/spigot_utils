package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.CustomKeyProvider;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;


public class CustomKeyEditTransform extends EditTransform {

  private Class<? extends CustomKeyProvider> keyProvider;
  private CustomKeyProvider keyProviderInstance;

  public CustomKeyEditTransform(Field field, Object object,
                                TriConsumer<Field, Object, Object> setter) {
    super(field, object, setter);
    this.keyProvider =
        field.getAnnotation(GuiTransformField.class).customList();
    try {
      this.keyProviderInstance =
          this.keyProvider.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException |
             IllegalArgumentException | InvocationTargetException |
             NoSuchMethodException | SecurityException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void openGui(Player player) {
    openGui(player, 0);
  }

  public void openGui(Player player, int page) {

    int itemPerPage = 3 * 7;

    try {
      this.keyProviderInstance.init(player);
      Object keyVal = ReflectionUtils.getFieldValue(this.field, this.object);
      List<String> strings = this.keyProviderInstance.getKeyChoose();

      int start = page * itemPerPage;
      int end = Math.min(strings.size(), (page + 1) * itemPerPage);
      int maxPageCount = (int)Math.floor(Double.valueOf(strings.size()) /
                                         Double.valueOf(itemPerPage)) +
                         1;
      int maxPage = maxPageCount - 1;

      GuiBuilder gui =
          new GuiBuilder()
              .name("§eChoix")
              .fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))
              .size(5 * 9);

      for (int i = start; i < end; i++) {
        String string = strings.get(i);
        var guiItem = this.keyProviderInstance.create(string);

        if (keyVal != null) {
          String keyString = this.keyProviderInstance.keyToString(keyVal);
          if (string.equals(keyString)) {
            guiItem.icon(Material.DIAMOND).description("§aSelectionné");
          } else {
            guiItem.description("", "§7Cliquez pour selectionner cette valeur");
          }
        } else {
          guiItem.description("", "§7Cliquez pour selectionner cette valeur");
        }

        gui.item(guiItem
                     .clickHandler(() -> {
                       ReflectionUtils.setFieldValue(
                           field, object,
                           this.keyProviderInstance.stringToKey(string));
                       openGui(player, page);
                     })
                     .build());
      }

      if (page > 0) {
        gui.item(47, new GuiItemBuilder()
                         .icon(Material.ARROW)
                         .name("§ePage précèdente")
                         .clickHandler(() -> openGui(player, page - 1))
                         .build());
      }
      if (page < maxPage) {
        gui.item(51, new GuiItemBuilder()
                         .icon(Material.ARROW)
                         .name("§ePage suivante")
                         .clickHandler(() -> openGui(player, page + 1))
                         .build());
      }

      gui.item(9, new GuiBackItem(z -> this.getBackToGui().run()));

      gui.build().open(player);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
