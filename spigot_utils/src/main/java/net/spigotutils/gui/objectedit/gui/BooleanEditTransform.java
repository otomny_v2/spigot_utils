package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.GuiType;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.utils.TriConsumer;

public class BooleanEditTransform extends EditTransform{

	public BooleanEditTransform(Field field, Object object, TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		boolean fv = false;
		try {
			this.field.setAccessible(true);
			if(field.getType() == boolean.class) {
				fv = this.field.getBoolean(this.object);
			}else {
				fv = (Boolean) this.field.get(this.object);
			}
			this.field.setAccessible(false);
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		final boolean fieldValue = fv;
		new GuiBuilder()
			.type(GuiType.HOPPER)
			.name("§e"+super.name)
			.item(2, new GuiItemBuilder()
						.icon(fieldValue ? Material.GREEN_CONCRETE : Material.RED_CONCRETE)
						.name(this.name)
						.description("",
								     "§7Cliquez pour mettre à jour la valeur booléenne à "+(!fieldValue))
						.clickHandler((panel, p, clickType, slot) -> {
							try {
								this.field.setAccessible(true);
								this.setter.accept(this.field, this.object, Boolean.valueOf(!fieldValue).toString());
								this.field.setAccessible(false);
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							}
							openGui(player);
							return true;
						})
						.build())
			.item(0, new GuiBackItem(p ->  {
				this.getBackToGui().run();
			}))
			.build()
			.open(player);
	}

	
}
