package net.spigotutils.gui.objectedit.gui;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.gui.objectedit.GuiSortOrder;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.utils.StringUtils;
import net.spigotutils.utils.TriConsumer;
import net.spigotutils.utils.reflection.ReflectionUtils;

public class SetEnumMultipleChoiceTransform extends EditTransform {

	public static final int ITEM_PER_PAGE = 4 * 7;

	public SetEnumMultipleChoiceTransform(Field field, Object object,
			TriConsumer<Field, Object, Object> setter) {
		super(field, object, setter);
	}

	@Override
	public void openGui(Player player) {
		openGui(player, 1);
	}

	private void openGui(Player player, int page) {
		GuiTransformField fieldData = this.field
				.getAnnotation(GuiTransformField.class);
		Class<?> hold = this.field.getType();
		try {
			Enum[] enumValues = (Enum[]) ReflectionUtils
					.enumList(hold);

			List<Enum> enumValuesList = Arrays.asList(enumValues);
			if (fieldData.sort() == GuiSortOrder.ASC) {
				Collections.sort(enumValuesList, (o1, o2) -> {

					return o1.ordinal() - o2.ordinal();
				});
			} else {
				Collections.sort(enumValuesList, (o1, o2) -> {
					return o1.toString().compareTo(o2.toString());
				});
			}

			if (enumValues.length > ITEM_PER_PAGE) {
				int minPagePlus = (int) Math
						.floor(enumValues.length / ITEM_PER_PAGE) + 1;
				if (page < 1) {
					openGui(player, page + 1);
				} else if (page > minPagePlus) {
					openGui(player, page - 1);
				}
				String name = fieldData.fieldName();
				GuiBuilder gui = new GuiBuilder()
						.name(
								"§eChoix multiple " + name + " (page " + page + ")")
						.size(4 * 9)
						.fillSide(Material.BLUE_STAINED_GLASS_PANE);
				int minDisplay = Math.min(page * ITEM_PER_PAGE,
						enumValues.length);
				for (int i = (page - 1)
						* ITEM_PER_PAGE; i < minDisplay; i++) {
					Object enumValue = enumValuesList.get(i);
					gui.item(new GuiItemBuilder()
							.name(StringUtils
									.capitalize("§e" + enumValue.toString()))
							.icon(Material.NAME_TAG)
							.clickHandler((g, p, clickType, slot) -> {
								select(enumValue, field);
								this.getBackToGui().run();
								return true;
							}).build());
				}

				if (page > 1) {
					gui.item(39,
							new GuiItemBuilder().name("§7Précédent")
									.icon(Material.ARROW)
									.clickHandler((a, b, c, d) -> {
										openGui(player, page - 1);
										return true;
									}).build());
				}
				if (page < minPagePlus) {
					gui.item(41,
							new GuiItemBuilder().name("§7Suivant")
									.icon(Material.ARROW)
									.clickHandler((a, b, c, d) -> {
										openGui(player, page + 1);
										return true;
									}).build());
				}

				gui.item(36,
						new GuiBackItem(p -> this.getBackToGui().run()));

				gui.build().open(player);
			} else {
				String name = fieldData.fieldName();
				GuiBuilder gui = new GuiBuilder()
						.name("§eChoix liste " + name).size(4 * 9);

				for (Object enumValue : enumValuesList) {
					gui.item(new GuiItemBuilder()
							.name(StringUtils
									.capitalize("§e" + enumValue.toString()))
							.icon(Material.NAME_TAG)
							.clickHandler((g, p, clickType, slot) -> {
								select(enumValue, field);
								this.getBackToGui().run();
								return true;
							}).build());
				}
				gui.item(36,
						new GuiBackItem(p -> this.getBackToGui().run()));
				gui.build().open(player);
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void select(Object enumValue, Field field){
		this.field.setAccessible(true);
		try {
			Collection collection = (Collection<?>) field.get(this.object);
			if(collection.contains(enumValue)){
				collection.remove(enumValue);
			}else{
				collection.add(enumValue);
			}
		} catch (IllegalArgumentException
				| IllegalAccessException e) {
			e.printStackTrace();
		}
		
		this.field.setAccessible(false);
	}
}
