package net.spigotutils.gui;

import java.util.SortedMap;
import java.util.TreeMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.item.GuiItem;

public class GuiBuilder {

	private String name;
	private final SortedMap<Integer, GuiItem> items = new TreeMap<>();
	private int size;
	private Player player;
	private GuiListener listener;
	private GuiType type = GuiType.INVENTORY;
	private boolean fillSide;
	private ItemStack fillSideItem;
	private boolean closable = true;
	@SuppressWarnings("unused")
	private boolean page;

	public GuiBuilder name(String name) {
		this.name = ChatColor.translateAlternateColorCodes('&', name);
		return this;
	}

	/**
	 * Add item to the panel in the last slot.
	 * 
	 * @param item - Panel item
	 * @return PanelBuilder
	 */
	public GuiBuilder item(GuiItem item) {
		// Do not add null items
		if (item == null) {
			return this;
		}
		return item(nextSlot(), item);
	}

	public GuiBuilder closable(boolean close) {
		this.closable = close;
		return this;
	}

	/**
	 * Add item into a specific slot. If it is already occupied, it will be
	 * replaced.
	 * 
	 * @param slot - slot
	 * @param item - Panel item
	 * @return PanelBuilder
	 */
	public GuiBuilder item(int slot, GuiItem item) {
		// Do not add null items
		if (item == null) {
			return this;
		}
		items.put(slot, item);
		return this;
	}

	
	/**
	 * Active l'option de remplir par un item sur les bordures
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param fillSize
	 * @return
	 * @date 06/10/2019
	 */
	public GuiBuilder fillSide(Material toFill) {
		return fillSide(new ItemStack(toFill));
	}

	/**
	 * Active l'option de remplir par un item sur les bordures
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param fillSize
	 * @return
	 * @date 06/10/2019
	 */
	public GuiBuilder fillSide(ItemStack toFill) {
		this.fillSide = true;
		if (this.fillSide) {
			this.fillSideItem = toFill;
		}
		return this;
	}

	/**
	 * Désactive l'option de remplir par un item sur les bordures
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param fillSize
	 * @return
	 * @date 06/10/2019
	 */
	public GuiBuilder fillSide() {
		this.fillSide = false;
		this.fillSideItem = null;
		return this;
	}

	/**
	 * Forces panel to be a specific number of slots.
	 * 
	 * @param size - size to be
	 * @return PanelBuilder - PanelBuilder
	 */
	public GuiBuilder size(int size) {
		this.size = size;
		return this;
	}

	/**
	 * Sets the user who will get this panel. This will open it immediately when it
	 * is built
	 * 
	 * @param user - the User
	 * @return PanelBuilder
	 */
	public GuiBuilder user(Player user) {
		this.player = user;
		return this;
	}

	/**
	 * Sets which PanelListener will listen for clicks
	 * 
	 * @param listener - listener for this panel
	 * @return PanelBuilder
	 */
	public GuiBuilder listener(GuiListener listener) {
		this.listener = listener;
		return this;
	}

	/**
	 * Sets which Panel.Type will be used. Defaults to {@link GuiType#INVENTORY}.
	 * 
	 * @param type - Panel.Type for this panel.
	 * @return PanelBuilder
	 * @since 1.7.0
	 */
	public GuiBuilder type(GuiType type) {
		this.type = type;
		return this;
	}

	
	public GuiBuilder multiPage(boolean multiPage){
		this.page = multiPage;
		return this;
	}

	/**
	 * Get the next free slot number after the largest slot.
	 * 
	 * @return next slot number, or -1 in case none has been found.
	 */
	public int nextSlot() {
		if (!this.fillSide) {
			return items.isEmpty() ? 0 : items.lastKey() + 1;
		}
		if (items.isEmpty()) {
			return 10;
		}
		int nextSlot = items.lastKey() + 1;
		if (nextSlot % 9 == 0) {
			nextSlot += 1;
		}
		if ((nextSlot + 1) % 9 == 0) {
			nextSlot += 2;
		}
		return nextSlot;
	}

	/**
	 * Checks if a slot is occupied in the panel or not
	 * 
	 * @param slot to check
	 * @return true or false
	 */
	public boolean slotOccupied(int slot) {
		return items.containsKey(slot);
	}


	/**
	 * Build the panel
	 * 
	 * @return Panel
	 */
	public Gui build() {
		// items.lastKey() is a slot position, so the panel size is this value + 1
		return new Gui(
			name,
			items,
			Math.max(size, items.isEmpty() ? size : items.lastKey() + 1),
			this.player,
			listener,
			type,
			this.fillSide,
			this.fillSideItem,
			this.closable);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the items
	 */
	public SortedMap<Integer, GuiItem> getItems() {
		return items;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @return the listener
	 */
	public GuiListener getListener() {
		return listener;
	}

	/**
	 * @return the panelType
	 * @since 1.7.0
	 */
	public GuiType getPanelType() {
		return type;
	}

	public boolean isFillSide() {
		return fillSide;
	}

	public ItemStack getFillSideItem() {
		return fillSideItem;
	}
	
	public boolean isClosable() {
		return this.closable;
	}

}
