package net.spigotutils.gui;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.item.GuiBackItem;
import net.spigotutils.gui.item.GuiItem;
import net.spigotutils.gui.item.GuiItemBuilder;
import net.spigotutils.utils.Pair;
import net.spigotutils.utils.head.HeadGetter;
import net.spigotutils.utils.head.HeadRequester;

/**
 * Représente un GUI
 * 
 * @author Fabien
 */
public class Gui implements InventoryHolder, HeadRequester {

	/**
	 * @param <T>
	 *          Le type de donnée affichée
	 * @param guiBuilder
	 *          Le GUI
	 * @param elements
	 *          les élèments
	 * @param page
	 *          le numéro de la page sur laquelle le joueur se
	 *          trouve
	 * @param itemPerPage
	 *          le nombre d'item affiché par page
	 * @param itemFunction
	 *          la fonction permettant de transformer un element
	 *          en item à afficher
	 * @param reOpen
	 *          la fonction permettant de ré-ouvrir le GUI
	 * @param onClose
	 *          la fonction appelée lorsque le joueur ferme le
	 *          GUI
	 * @return un tuple de deux élèment content le gui créé
	 *         ainsi que le nombre d'item qui est affiché
	 */
	public static <T> Pair<Gui, Integer> multiPageGui(
			GuiBuilder guiBuilder, List<T> elements, int page,
			int itemPerPage, Function<T, GuiItem> itemFunction,
			Consumer<Integer> reOpen, Runnable onClose) {
		return multiPageGui(guiBuilder, elements, page, itemPerPage,
				itemFunction, reOpen, (gui, itemOnPage) -> {
				}, onClose);
	}

	/**
	 * @param <T>
	 *          Le type de donnée affichée
	 * @param guiBuilder
	 *          Le GUI
	 * @param elements
	 *          les élèments
	 * @param page
	 *          le numéro de la page sur laquelle le joueur se
	 *          trouve
	 * @param itemPerPage
	 *          le nombre d'item affiché par page
	 * @param itemFunction
	 *          la fonction permettant de transformer un element
	 *          en item à afficher
	 * @param reOpen
	 *          la fonction permettant de ré-ouvrir le GUI
	 * @param beforeNewItem
	 *          la fonction rajoutant des items (du styles ajout
	 *          d'elements etc... avant que les items de
	 *          pagination arrivent) (arg1: GuiBuilder, arg2:
	 *          itemOnPage)
	 * @param onClose
	 *          la fonction appelée lorsque le joueur ferme le
	 *          GUI
	 * @return un tuple de deux élèment content le gui créé
	 *         ainsi que le nombre d'item qui est affiché
	 */
	public static <T> Pair<Gui, Integer> multiPageGui(
			GuiBuilder guiBuilder, List<T> elements, int page,
			int itemPerPage, Function<T, GuiItem> itemFunction,
			Consumer<Integer> reOpen,
			BiConsumer<GuiBuilder, Integer> beforeNewItem,
			Runnable onClose) {
		return multiPageGui(guiBuilder, elements, page, itemPerPage,
				(arg, index) -> itemFunction.apply(arg), reOpen,
				beforeNewItem, onClose);
	}

	/**
	 * @param <T>
	 *          Le type de donnée affichée
	 * @param guiBuilder
	 *          Le GUI
	 * @param elements
	 *          les élèments
	 * @param page
	 *          le numéro de la page sur laquelle le joueur se
	 *          trouve
	 * @param itemPerPage
	 *          le nombre d'item affiché par page
	 * @param itemFunction
	 *          la fonction permettant de transformer un element
	 *          en item à afficher
	 * @param reOpen
	 *          la fonction permettant de ré-ouvrir le GUI
	 * @param beforeNewItem
	 *          la fonction rajoutant des items (du styles ajout
	 *          d'elements etc... avant que les items de
	 *          pagination arrivent) (arg1: GuiBuilder, arg2:
	 *          itemOnPage)
	 * @param onClose
	 *          la fonction appelée lorsque le joueur ferme le
	 *          GUI
	 * @return un tuple de deux élèment content le gui créé
	 *         ainsi que le nombre d'item qui est affiché
	 */
	public static <T> Pair<Gui, Integer> multiPageGui(
			GuiBuilder guiBuilder, List<T> elements, int page,
			int itemPerPage,
			BiFunction<T, Integer, GuiItem> itemFunction,
			Consumer<Integer> reOpen,
			BiConsumer<GuiBuilder, Integer> beforeNewItem,
			Runnable onClose) {

		int elementCount = elements.size();
		int indexStart = page * itemPerPage;
		int indexEnd = (page + 1) * itemPerPage;
		int itemOnPage = (indexEnd - indexStart);
		indexEnd = Math.min(indexEnd, elementCount);

		if (indexStart > elementCount) {
			reOpen.accept(page - 1);
			return null;
		}

		for (int index = indexStart; index < indexEnd; index++) {
			T elementAt = elements.get(index);
			GuiItem item = itemFunction.apply(elementAt, index);
			guiBuilder.item(item);
		}

		if (onClose != null) {
			guiBuilder.item(9,
					new GuiBackItem(close -> onClose.run()));
		}

		beforeNewItem.accept(guiBuilder, itemOnPage);

		if (page > 0) {
			// Ajout page précèdente
			guiBuilder.item(47, new GuiItemBuilder()
					.icon(Material.ARROW).name("§7§oPage précédente")
					.clickHandler(() -> reOpen.accept(page - 1)).build());
		}
		if (page < Math
				.round((double) elementCount / (double) itemPerPage)) {
			// Ajout page suivante
			guiBuilder.item(51, new GuiItemBuilder()
					.icon(Material.ARROW).name("§7§oPage suivante")
					.clickHandler(() -> reOpen.accept(page + 1)).build());
		}

		return new Pair<>(guiBuilder.build(), itemOnPage);
	}

	private Inventory inventory;
	private Map<Integer, GuiItem> items;
	private GuiListener listener;
	private Player player;
	private String name;
	private boolean fillSide;
	private ItemStack fillSideItem;
	private boolean closable;

	public Gui() {
	}

	public Gui(String name, Map<Integer, GuiItem> items, int size,
			Player player, GuiListener listener) {
		this(name, items, size, player, listener, GuiType.INVENTORY);
	}

	/**
	 * @since 1.7.0
	 */
	public Gui(String name, Map<Integer, GuiItem> items, int size,
			Player player, GuiListener listener, GuiType type) {
		makePanel(name, items, size, player, listener, type, true);
	}

	public Gui(String name, SortedMap<Integer, GuiItem> items,
			int size, Player player, GuiListener listener,
			GuiType type, boolean fillSide, ItemStack fillSideItem,
			boolean closable) {
		this.makePanel(name, items,
				Math.max(size,
						items.isEmpty() ? size : items.lastKey() + 1),
				player, listener, type, fillSide, fillSideItem,
				closable);
	}

	protected void makePanel(String name,
			Map<Integer, GuiItem> items, int size, Player player,
			GuiListener listener, GuiType type, boolean fillSide,
			ItemStack fillSideItem, boolean closable) {
		this.fillSide = fillSide;
		this.fillSideItem = fillSideItem;
		this.makePanel(name, items, size, player, listener, type,
				closable);
	}

	protected void makePanel(String name,
			Map<Integer, GuiItem> items, int size, Player player,
			GuiListener listener) {
		this.makePanel(name, items, size, player, listener,
				GuiType.INVENTORY, true);
	}

	/**
	 * @since 1.7.0
	 */
	@SuppressWarnings("deprecation")
	protected void makePanel(String name,
			Map<Integer, GuiItem> items, int size, Player Player,
			GuiListener listener, GuiType type, boolean closable) {
		this.name = name;
		this.items = items;
		this.closable = closable;

		// Create panel
		switch (type) {
		case INVENTORY:
			inventory = Bukkit.createInventory(null, fixSize(size),
					name);
			break;
		case HOPPER:
			inventory = Bukkit.createInventory(null,
					InventoryType.HOPPER, name);
			break;
		case DROPPER:
			inventory = Bukkit.createInventory(null,
					InventoryType.DROPPER, name);
			break;
		}
		if (this.fillSide) {
			fillOnSide();
		}
		// Fill the inventory and return
		for (Map.Entry<Integer, GuiItem> en : items.entrySet()) {
			if (en.getKey() < 54) {
				inventory.setItem(en.getKey(), en.getValue().getItem());
				// Get player head in async
				if (en.getValue().isPlayerHead()) {
					HeadGetter.getHead(en.getValue(), this);
				}
			}
		}
		this.listener = listener;
		// If the listener is defined, then run setup
		if (listener != null)
			listener.setup();

		// If the Player is defined, then open panel immediately
		this.player = Player;
		if (Player != null)
			this.open(this.player);
	}

	private void fillOnSide() {
		if (this.inventory.getType() == InventoryType.CHEST) {
			var meta = this.fillSideItem.getItemMeta();
			if (meta != null) {
				meta.setDisplayName("§e");
				meta.setLocalizedName("§e"); // Localized name cannot
																			// be overridden by the
																			// player using an
																			// anvils
				this.fillSideItem.setItemMeta(meta);
			}
			for (int i = 0; i < inventory.getSize(); i++) {
				inventory.setItem(i, this.fillSideItem.clone());
			}
			for (int i = 0; i < inventory.getSize(); i++) {
				if (i > 9 && i < inventory.getSize() - 9) {
					if ((i % 9) >= 1 && (i % 9) <= 7) {
						inventory.setItem(i, null);
					}
				}
			}
		}

	}

	private int fixSize(int size) {
		// If size is undefined (0) then use the number of items
		if (size == 0) {
			size = items.keySet().size();
		}
		/*
		 * TEST sur ça prcq ça merde if (this.fillSide) { size += 9;
		 * }
		 */
		if (size > 0) {
			// Make sure size is a multiple of 9 and is 54 max.
			size = size + 8;
			size -= (size % 9);
			if (size > 54)
				size = 54;
		} else {
			return 9;
		}
		return size;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	public Map<Integer, GuiItem> getItems() {
		return items;
	}

	/**
	 * @return the listener
	 */
	public Optional<GuiListener> getListener() {
		return Optional.ofNullable(listener);
	}

	public Optional<Player> getPlayer() {
		return Optional.ofNullable(player);
	}

	public void open(Player... players) {
		for (Player player : players) {
			player.openInventory(inventory);
			GuiListenerManager.getOpenPanels()
					.put(player.getUniqueId(), this);
		}
	}

	/**
	 * @param inventory
	 *          the inventory to set
	 */
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	/**
	 * @param items
	 *          the items to set
	 */
	public void setItems(Map<Integer, GuiItem> items) {
		this.items = items;
	}

	/**
	 * @param listener
	 *          the listener to set
	 */
	public void setListener(GuiListener listener) {
		this.listener = listener;
	}

	public boolean isClosable() {
		return this.closable;
	}

	/**
	 * @param Player
	 *          - the Player the Player to set
	 */
	public void setPlayer(Player Player) {
		this.player = Player;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	@Override
	public void setHead(GuiItem item) {
		// Update the panel item
		// Find panel item index in items and replace it once more
		// in inventory to
		// update it.
		this.items.entrySet().stream()
				.filter(entry -> entry.getValue() == item)
				.mapToInt(Map.Entry::getKey).findFirst()
				.ifPresent(index ->
				// Update item inside inventory to change icon only if
				// item is inside panel.
				this.inventory.setItem(index, item.getItem()));
	}
}