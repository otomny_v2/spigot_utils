package net.spigotutils.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public interface GuiListener {

    /**
     * This is called when the panel is first setup
     */
    void setup();

    void onInventoryClose(InventoryCloseEvent event);

    void onInventoryClick(Player user, InventoryClickEvent event);

    /**
     * Called after a user has clicked on a panel item.
     * Used to refresh the panel in its entirety
     *
     * @since 1.6.0
     */
    default void refreshPanel() {
    }


}
