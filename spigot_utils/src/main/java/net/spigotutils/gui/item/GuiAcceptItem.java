package net.spigotutils.gui.item;

import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import net.spigotutils.gui.Gui;
import net.spigotutils.gui.GuiBuilder;

public class GuiAcceptItem extends GuiItem{

	public static boolean openAccept(String name, Player player, Runnable cancel, Runnable accept) {
		return openAccept(name, player, z -> cancel.run(), z -> accept.run());
	}

	/**
	 * 
	 * @deprecated Better use {@link GuiAcceptItem#openAccept(String, Player, Runnable, Runnable)
	 * @param name
	 * @param player
	 * @param cancel
	 * @param accept
	 * @return
	 */
	@Deprecated
	public static boolean openAccept(String name, Player player, Consumer<Player> cancel, Consumer<Player> accept) {
		GuiBuilder guiBuilder = new GuiBuilder()
			.name(name)
			.fillSide(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE, 1))
			.size(3 * 9);

		// Item annulation
		GuiItem cancel_ = new GuiItemBuilder()
			.icon(Material.RED_CONCRETE)
			.name("§cAnnuler")
			.description("", "§7Revenir en arrière")
			.clickHandler((p, pl, cl, sl) -> {
				cancel.accept(player);
				return true;
			})
			.build();

		// Item validation
		GuiItem accept_ = new GuiItemBuilder()
			.icon(Material.GREEN_CONCRETE)
			.name("§aValider")
			.description("", "§7Accepter")
			.clickHandler((p, pl, cl, sl) -> {
				accept.accept(player);
				return true;
			})
			.build();

		for (int i = 10; i < 13; i++) guiBuilder.item(i, cancel_);
		for (int i = 14; i < 17; i++) guiBuilder.item(i, accept_);

		guiBuilder.size(3 * 9).build().open(player);
		return true;
	}
	
	private Consumer<Player> cancel;
	private Consumer<Player> accept;

	public GuiAcceptItem(GuiItemBuilder builtItem) {
		super(builtItem);
	}
	
	public GuiAcceptItem(GuiItemBuilder item, Consumer<Player> cancel, Consumer<Player> accept) {
		this(item);
		this.setClickHandler(this::openAccept);
		this.cancel = cancel;
		this.accept = accept;
	}
	
	public boolean openAccept(Gui gui, Player player, ClickType clickType, int slot) {
		return openAccept(this.getName(), player, this.cancel, this.accept);
			
	}

}
