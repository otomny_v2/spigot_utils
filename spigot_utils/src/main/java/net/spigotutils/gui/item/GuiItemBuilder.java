package net.spigotutils.gui.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.mongodb.lang.Nullable;

import lombok.Getter;

public class GuiItemBuilder implements Cloneable {

	private ItemStack icon = new ItemStack(Material.AIR);
	private @Nullable String name = "§e";
	private List<String> description = new ArrayList<>();
	private boolean glow = false;
	private GuiItem.ClickHandler clickHandler;
	private boolean playerHead;
	private boolean invisible;
	private String playerHeadName;
	@Getter
	private boolean hideEnchant = true;

	public GuiItemBuilder icon(Material icon) {
		this.icon = new ItemStack(icon);
		return this;
	}

	public GuiItemBuilder icon(ItemStack icon) {
		this.icon = icon;
		return this;
	}

	/**
	 * Set icon to player's head
	 *
	 * @param playerName
	 *          - player's name
	 * @return PanelItemBuilder
	 */
	public GuiItemBuilder icon(String playerName) {
		this.icon = new ItemStack(Material.PLAYER_HEAD, 1);
		this.name = playerName;
		this.playerHead = true;
		this.playerHeadName = playerName;
		return this;
	}

	public GuiItemBuilder name(@Nullable String name) {
		this.name = name != null
				? ChatColor.translateAlternateColorCodes('&', name)
				: null;
		return this;
	}

	/**
	 * Adds a list of strings to the descriptions
	 *
	 * @param description
	 *          - List of strings
	 * @return PanelItemBuilder
	 */
	public GuiItemBuilder description(List<String> description) {
		description.forEach(this::description);
		return this;
	}

	/**
	 * @param hideEnchant
	 *          hide enchantment lore
	 * @return GuiItemBuilder
	 */
	public GuiItemBuilder hideEnchant(boolean hideEnchant) {
		this.hideEnchant = hideEnchant;
		return this;
	}

	/**
	 * Add any number of lines to the description
	 *
	 * @param description
	 *          strings of lines
	 * @return PanelItemBuilder
	 */
	public GuiItemBuilder description(String... description) {
		List<String> additions = Arrays.asList(description);
		ArrayList<String> updatableList = new ArrayList<>();
		updatableList.addAll(this.description);
		updatableList.addAll(additions);
		this.description = updatableList;
		return this;
	}

	/**
	 * 
	 * @param condition
	 * @param description
	 * @return
	 */
	public GuiItemBuilder description(boolean condition,
			String... description) {
		if (condition)
			description(description);
		return this;
	}

	/**
	 * Adds a line to the description
	 *
	 * @param description
	 *          - string
	 * @return PanelItemBuilder
	 */
	public GuiItemBuilder description(String description) {
		Collections.addAll(this.description,
				description.split("\n"));
		return this;
	}

	public GuiItemBuilder glow(boolean glow) {
		this.glow = glow;
		return this;
	}

	public GuiItemBuilder invisible(boolean invisible) {
		this.invisible = invisible;
		return this;
	}

	public GuiItemBuilder clickHandler(Runnable runnable) {
		return this.clickHandler((a, b, c, d) -> {
			runnable.run();
			return true;
		});
	}

	public GuiItemBuilder clickHandler(
			GuiItem.SecondaryClickHandler clickHandler) {
		return this.clickHandler((gui, player, clickType, slot) -> {
			clickHandler.onClick(gui, player, clickType, slot);
			return true;
		});
	}

	public GuiItemBuilder clickHandler(
			GuiItem.ClickHandler clickHandler) {
		this.clickHandler = clickHandler;
		return this;
	}

	public GuiItem build() {
		return new GuiItem(this);
	}

	/**
	 * @return the icon
	 */
	public ItemStack getIcon() {
		return icon;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public List<String> getDescription() {
		return description;
	}

	/**
	 * @return the glow
	 */
	public boolean isGlow() {
		return glow;
	}

	/**
	 * @return the clickHandler
	 */
	public GuiItem.ClickHandler getClickHandler() {
		return clickHandler;
	}

	/**
	 * @return the playerHead
	 */
	public boolean isPlayerHead() {
		return playerHead;
	}

	/**
	 * @return the invisible
	 */
	public boolean isInvisible() {
		return invisible;
	}

	@Override
	public GuiItemBuilder clone() {
		GuiItemBuilder clonee = new GuiItemBuilder();
		clonee.clickHandler = this.clickHandler;
		clonee.description = new ArrayList<String>(this.description);
		clonee.glow = this.glow;
		clonee.icon = this.icon.clone();
		clonee.invisible = this.invisible;
		clonee.name = this.name;
		clonee.playerHead = this.playerHead;
		return clonee;
	}

	public String getPlayerHeadName() {
		return this.playerHeadName;
	}
}
