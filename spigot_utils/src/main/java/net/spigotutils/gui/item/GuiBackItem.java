package net.spigotutils.gui.item;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

public class GuiBackItem extends GuiItem{

    public GuiBackItem(GuiItemBuilder builtItem) {
    	super(builtItem);
    }
    
    public GuiBackItem(Runnable end) {
    	super(new GuiItemBuilder()
  				.name("§7Retour")
  				.icon(Material.ARROW)
  				.clickHandler(end)
  				.glow(true)
  				.description("", "§7Cliquez pour retourner à l'écran précédent"));
    }
    
    public GuiBackItem(Consumer<Player> end) {
    	super(new GuiItemBuilder()
    				.name("§7Retour")
    				.icon(Material.ARROW)
    				.clickHandler((a,b,c,d) -> {
    					end.accept(b);
    					return true;
    				})
    				.glow(true)
    				.description("", "§7Cliquez pour retourner à l'écran précédent"));
    }
    
    

	
}
