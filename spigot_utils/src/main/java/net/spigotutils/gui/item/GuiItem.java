package net.spigotutils.gui.item;

import java.util.List;
import java.util.Optional;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.spigotutils.gui.Gui;

@SuppressWarnings("deprecation")
public class GuiItem {

  public static GuiItem empty() { return new GuiItemBuilder().build(); }

  private ItemStack icon;
  private GuiItem.ClickHandler clickHandler;
  private List<String> description;
  private String name;
  private boolean glow;
  private ItemMeta meta;
  private final boolean playerHead;
  private String playerHeadName;
  private boolean invisible;
  private boolean hideEnchant;

  public GuiItem(GuiItemBuilder builtItem) {
    this.icon = builtItem.getIcon();
    this.playerHead = builtItem.isPlayerHead();
    // Get the meta
    meta = icon.getItemMeta();
    this.hideEnchant = builtItem.isHideEnchant();
    if (meta != null) {
      // Set flags to neaten up the view
      meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
      meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
      meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
      if (builtItem.isHideEnchant() || builtItem.isGlow()) {
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
      }
      meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
      icon.setItemMeta(meta);
    }

    this.playerHeadName = builtItem.getPlayerHeadName();

    this.clickHandler = builtItem.getClickHandler();

    // Create the final item
    setName(builtItem.getName());
    setDescription(builtItem.getDescription());
    setGlow(builtItem.isGlow());
    setInvisible(builtItem.isInvisible());
  }

  public ItemStack getItem() { return icon; }

  public List<String> getDescription() { return description; }

  public void setDescription(List<String> description) {
    this.description = description;
    if (meta != null) {
      meta.setLore(description);
      icon.setItemMeta(meta);
    }
  }

  public String getName() { return name; }

  public void setName(String name) {
    this.name = name;
    if (meta != null) {
      meta.setDisplayName(name);
      meta.setLocalizedName(name); // Localized name cannot
                                   // be overridden by the
                                   // player using an
                                   // anvils
      icon.setItemMeta(meta);
    }
  }

  public boolean isInvisible() { return invisible; }

  public void setInvisible(boolean invisible) {
    this.invisible = invisible;
    if (meta != null) {
      if (invisible) {
        meta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
        meta.removeItemFlags(ItemFlag.HIDE_ENCHANTS);
        icon.setItemMeta(meta);
      } else {
        meta.removeEnchant(Enchantment.VANISHING_CURSE);
        if (this.hideEnchant) {
          meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        icon.setItemMeta(meta);
      }
    }
  }

  public Optional<ClickHandler> getClickHandler() {
    return Optional.ofNullable(clickHandler);
  }

  /**
   * @param clickHandler
   *            the clickHandler to set
   * @since 1.6.0
   */
  public void setClickHandler(ClickHandler clickHandler) {
    this.clickHandler = clickHandler;
  }

  public boolean isGlow() { return glow; }

  public void setGlow(boolean glow) {
    this.glow = glow;
    if (meta != null && glow) {
      meta.addEnchant(Enchantment.ARROW_DAMAGE, 0, glow);
      icon.setItemMeta(meta);
    }
  }

  /**
   * @return the playerHead
   */
  public boolean isPlayerHead() { return playerHead; }

  /**
   * Click handler interface
   */
  public interface SecondaryClickHandler {
    /**
     * This is executed when the icon is clicked
     *
     * @param panel
     *            - the panel that is being clicked
     * @param player
     *            - the User
     * @param clickType
     *            - the click type
     * @param slot
     *            - the slot that was clicked
     */
    void onClick(Gui panel, Player player, ClickType clickType, int slot);
  }

  /**
   * Click handler interface
   */
  public interface ClickHandler {
    /**
     * This is executed when the icon is clicked
     *
     * @param panel
     *            - the panel that is being clicked
     * @param player
     *            - the User
     * @param clickType
     *            - the click type
     * @param slot
     *            - the slot that was clicked
     * @return true if the click event should be cancelled
     */
    boolean onClick(Gui panel, Player player, ClickType clickType, int slot);
  }

  public void setHead(ItemStack itemStack) {
    this.icon = itemStack;
    // Get the meta
    meta = icon.getItemMeta();
    if (meta != null) {
      // Set flags to neaten up the view
      meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
      meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
      meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
      if (this.hideEnchant || this.glow) {
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
      }
      icon.setItemMeta(meta);
    }
    // Create the final item
    setName(name);
    setDescription(description);
    setGlow(glow);
  }

  public String getPlayerHeadName() { return this.playerHeadName; }
}
