package net.spigotutils.runnable;

import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 * Classe helper pour tourner des tâches facilement
 * @author fabien
 *
 */
public class SpigotTask {
	
	/**
	 * @author fabien
	 * @param plugin L'instance à utilisé pour tourner la tâche
	 * @param call La fonction à éxécuté
	 */
	public static void runSync(Plugin plugin, Consumer<Void> call) {
		Bukkit.getScheduler().runTask(plugin, () -> call.accept(null));
	}
	
	/**
	 * @author fabien
	 * @param plugin L'instance à utilisé pour tourner la tâche
	 * @param call La fonction à éxécuté
	 */
	public static void runSync(Plugin plugin, Runnable call) {
		Bukkit.getScheduler().runTask(plugin, call);
	}

	/**
	 * 
	 * @param plugin Plugin instance
	 * @param call Function call
	 * @param delayTick Delay in tick
	 */
	public static void runSync(Plugin plugin, Runnable call, int delayTick){
		Bukkit.getScheduler().runTaskLater(plugin, call, delayTick);
	}
	
	/**
	 * @author fabien
	 * @param plugin L'instance à utilisé pour tourner la tâche
	 * @param call La fonction à éxécuté
	 */
	public static void runAsync(Plugin plugin, Consumer<Void> call) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> call.accept(null));
	}
	
	/**
	 * @author fabien
	 * @param plugin L'instance à utilisé pour tourner la tâche
	 * @param call La fonction à éxécuté
	 */
	public static void runAsync(Plugin plugin, Runnable call) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, call);
	}
	
}
