package net.spigotutils.examples;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import net.spigotutils.gui.objectedit.GuiTransform;
import net.spigotutils.gui.objectedit.GuiTransformField;
import net.spigotutils.gui.objectedit.GuiTransformer;

/**
 * This class provide example for GuiObject classes
 * 
 * It help you build UI from Java Object, in order to change their values
 * 
 * Mostly used in In-Game configuration system, like 
 * rank system, lobby parameters etc...
 * 
 * For example, in Otomny, we use this package to update, remove or add quests
 * 
 */
public class GuiObjectExample {
  
  @GuiTransform
  @AllArgsConstructor
  public class Example{

    @GuiTransformField()
    private int count;

  }

  /**
   * This code display an inventory to the player
   * where he can interact with the object field
   * such as count
   * 
   * When he clicks, he need to input a integer (via Sign), and then the input
   * will replace the value of the field "count", in the "ex" object
   * 
   * 
   * @param player the player to show the gui to
   */
  public void example(Player player){
    Example ex = new Example(5);
    GuiTransformer.fast(ex, player, () -> {
      // This function is called after the player closes the inventory

      // You could update your database to keep it coherent whenever the player change the value
    });
  }

}
