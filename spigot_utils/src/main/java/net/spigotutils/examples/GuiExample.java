package net.spigotutils.examples;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import net.spigotutils.gui.GuiBuilder;
import net.spigotutils.gui.item.GuiItemBuilder;

/**
 * This class provide an example for Gui classes
 * 
 * It help you build UI faster than usual bukkit API call
 * And also make it easier
 * 
 */
public class GuiExample {

  /**
   * This code display an inventory to the player
   * where he can interact with an hopper, and each time he clicks
   * it send "Hello!" to the player
   * 
   * The gui is kept in memory while the player is inside
   * 
   * as soon as the player quit the inventory, it is deleted
   * (then the GC might remove it from memory)
   * 
   * @param player The player to open the GUI
   */
  public void example(Player player) {
    new GuiBuilder()
        .size(3 * 9)
        .fillSide(Material.ORANGE_STAINED_GLASS_PANE)
        .item(13, new GuiItemBuilder()
            .icon(Material.HOPPER)
            .name("§eClick me !")
            .clickHandler(() -> player.sendMessage("Hello!"))
            .build())
        .build()
        .open(player);
  }

}
