package net.spigotutils.player;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Permet de géré l'envoie des messages
 * 
 * @author ComminQ_Q (Computer)
 *
 * @date 09/10/2019
 */
public class MessageUtil {

	private static Map<String, String> prefixes = new HashMap<>();

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param prefix
	 * @param value
	 * @date 14/10/2019
	 */
	public static void addPrefix(String prefix, String value) {
		prefixes.put(prefix, value);
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param prefix
	 * @return
	 * @date 14/10/2019
	 */
	public static String getPrefix(String prefix) {
		return prefixes.get(prefix);
	}

	/**
	 * Envoie un message à un joueur
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param player
	 * @param message
	 * @date 09/10/2019
	 */
	public static void sendMessage(Player player, String message) {
		player.sendMessage(message);
	}

	/**
	 * Envoie un message aux joueurs validant un predicat
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param predicate
	 * @param message
	 * @date 09/10/2019
	 */
	public static void sendMessage(Predicate<Player> predicate, String message) {
		Bukkit.getOnlinePlayers()
				.stream()
				.filter(predicate)
				.forEach(player -> player.sendMessage(message));
	}

	/**
	 * Envoie un message aux joueurs validant un predicat avec un prefix
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param predicate
	 * @param message
	 * @date 09/10/2019
	 */

	public static void sendMessageWithPrefix(Predicate<Player> predicate, String prefix, String message) {
		Bukkit.getOnlinePlayers()
				.stream()
				.filter(predicate)
				.forEach(player -> player.sendMessage(prefixes.get(prefix) + " " + message));
	}

	public static void sendActionbar(JavaPlugin plugin, Player player, String message, int seconds) {
		new BukkitRunnable() {
			int time = 0;
			@Override
			public void run() {
				if (time == seconds) {
					cancel();
				} else {
					time++;
					player.sendActionBar(message);
				}
			}
		}.runTaskTimer(plugin, 0, 20L);
	}

}
