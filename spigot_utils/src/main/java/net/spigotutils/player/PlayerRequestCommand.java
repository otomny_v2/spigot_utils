package net.spigotutils.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import lombok.Getter;
import net.spigotutils.commands.CustomCommand;
import net.spigotutils.commands.arguments.IntegerCommandArgument;
import net.spigotutils.commands.arguments.StringCommandArgument;

public class PlayerRequestCommand extends CustomCommand {

  @Getter private static PlayerRequestCommand instance = null;
  private static final Map<UUID, PlayerRequest> requests = new HashMap<>();

  public static Map<UUID, PlayerRequest> getRequests() { return requests; }

  public static void complete(UUID uuid) { requests.remove(uuid); }

  public PlayerRequestCommand() {
    super("prc");
    instance = this;
    super.setNeedArgument(true);
    super.registerCommandComponant(0, new StringCommandArgument("uuid", false));
    super.registerCommandComponant(1, new IntegerCommandArgument("ac", false));
    init();
  }

  @Override
  public boolean execute(CommandSender sender, Map<Integer, Object> args) {
    Player player = player(sender);
    if (player == null)
      return true;
    if (args.size() != 2)
      return true;
    String reqUuid = (String)args.get(0);
    int reqCmd = (int)args.get(1);
    UUID uuid = null;
    try {
      uuid = UUID.fromString(reqUuid);
    } catch (Exception e) {
      return true;
    }
    if (!requests.containsKey(uuid))
      return true;
    PlayerRequest request = requests.get(uuid);
    if (player != request.getTo())
      return true;
    if (reqCmd == 0) {
      request.accept();
    } else {
      request.cancel();
    }
    complete(uuid);
    return true;
  }
}
