package net.spigotutils.player;

import java.util.UUID;

import org.bukkit.entity.Player;

import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;


public class PlayerRequest {

	@Getter 
	private Player from;
	@Getter 
	private Player to;
	private Runnable accept;
	private Runnable deny;
	@Getter 
	private UUID uuid;
	private String message;
	
	public PlayerRequest(Player from, Player to, Runnable accept, Runnable deny, String message) {
		super();
		this.from = from;
		this.to = to;
		this.accept = accept;
		this.deny = deny;
		this.uuid = UUID.randomUUID();
		this.message = message;
	}
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 * 
	 * Envoie le message a from (donc la target, le receveur)
	 * Utilise la commande prc, 0 = accepté, 1 = refusé
	 * 
	 *
	 * @date 30/11/2020
	 */
	public void send() {
		TextComponent accept = new TextComponent("§aAccepter");
		accept.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("§aAccepter cette requête").create()));
		accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/prc "+this.uuid+" 0"));
		
		TextComponent deny = new TextComponent("§cRefuser");
		deny.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("§cRefuser cette requête").create()));
		deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/prc "+this.uuid+" 1"));
		
		TextComponent total = new TextComponent(accept, new TextComponent(" §7/ "), deny);
		
		this.to.sendMessage(this.message);
		this.to.sendMessage(total);
		PlayerRequestCommand.getRequests().put(this.uuid, this);
	}

	public void accept() {
		this.accept.run();
	}
	
	public void cancel() {
		this.deny.run();
	}

	public static class Builder {
		
		private Player from;
		private Player to;
		private Runnable accept;
		private Runnable deny;
		private String message;
		
		public Builder(String message) {
			this.message = message;
		}

		public Builder fromTo(Player from, Player to) {
			this.from = from;
			this.to = to;
			return this;
		}

		public Builder accept(Runnable accept) {
			this.accept = accept;
			return this;
		}
		
		public Builder deny(Runnable deny) {
			this.deny = deny;
			return this;
		}
		
		public void send() {
			build().send();
		}
		
		public PlayerRequest build() {
			return new PlayerRequest(from, to, accept, deny, message);
		}
		
	}
	
	
}
