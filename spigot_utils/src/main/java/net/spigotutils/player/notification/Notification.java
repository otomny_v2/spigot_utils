package net.spigotutils.player.notification;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.api.process.TaskDispatcher;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ImpossibleTrigger;
import net.minecraft.network.protocol.game.ClientboundUpdateAdvancementsPacket;
import net.minecraft.resources.ResourceLocation;
import net.spigotutils.nms.PacketUtils;
import net.spigotutils.utils.Ex;
import net.spigotutils.utils.StringUtils;

/**
 * 
 * Create little notification using Advancement system on Minecraft
 * 
 * @author ComminQ_Q (Computer)<br>
 *         Example:
 * 
 *         <pre>
 *         new Notification(plugin).chatColor(ChatColor.GOLD).title("Connexion !").icon(IconType.ENDER_EYE).send(player);
 *         </pre>
 * 
 * @date 09/10/2019
 */
public class Notification {

	private static int MEM = 0;

	private String title; // The title
	private IconType icon; // The icon
	private ChatColor chatColor; // Color
	private Plugin plugin; // The plugin linked to

	/**
	 * Constructor
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param plugin
	 * @date 09/10/2019
	 */
	public Notification(Plugin plugin) {
		this.plugin = plugin;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param title Notification title
	 * @return
	 * @date 09/10/2019
	 */
	public Notification title(String title) {
		this.title = title;
		return this;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param icon Notification icon
	 * @return
	 * @date 09/10/2019
	 */
	public Notification icon(IconType icon) {
		this.icon = icon;
		return this;
	}

	/**
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param chatColor The color
	 * @return
	 * @date 09/10/2019
	 */
	public Notification chatColor(ChatColor chatColor) {
		this.chatColor = chatColor;
		return this;
	}

	/**
	 * Send notification to player(s)
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param players
	 * @deprecated 
	 *  Better use {@link #send(Player...)},
	 * 	Otherwise, Bukkit store advancement on file system
	 *  AND it's pretty slow, locking the main thread (boring af)
	 * 
	 * @date 09/10/2019
	 */
	@Deprecated
	public void sendBukkit(Player... players) {
		String json = createJsonString(this);
		for (Player player : players) {
			// long start = System.currentTimeMillis();
			NamespacedKey nk = new NamespacedKey(this.plugin, StringUtils.alphabet(MEM++));

			Bukkit.getUnsafe().loadAdvancement(nk, json);
			Advancement advancement = Bukkit.getAdvancement(nk);
			AdvancementProgress progress = player.getAdvancementProgress(advancement);
			if (!progress.isDone()) {
				progress.awardCriteria("impossible");
			}
			// System.out.println("Nofication#sendBukkit part 1 :  "+(System.currentTimeMillis()-start)+"ms.");
			Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
				// long start_ = System.currentTimeMillis();
				Advancement advancement2 = Bukkit.getAdvancement(nk);
				AdvancementProgress progress2 = player.getAdvancementProgress(advancement2);
				progress2.revokeCriteria("impossible");
				boolean done = progress2.isDone();
				if (done) {
					Bukkit.getLogger().log(Level.SEVERE, "Erreur : l'advancement fictif n'a pas été revoqué");
				}

				
				// System.out.println("Nofication#sendBukkit part 2 :  "+(System.currentTimeMillis()-start_)+"ms.");
			}, 5L);
		}
	}

	/**
	 * Send notification to players
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param players
	 * @date 09/10/2019
	 */
	public void send(Collection<? extends Player> players) {
		send(players.toArray(Player[]::new));
	}

	/**
	 * Send notification to player(s)
	 * 
	 * @author ComminQ_Q (Computer)
	 *
	 * @param players
	 * @date 09/10/2019
	 */
	public void send(Player... players) {
		// Async scheduling inside Thread Pool

		// If there is error there, just
		// tweak it with something like
		/*
		new BukkitRunnable(){

			...	

		}.runTaskAsynchronously(...);
		*/
		/**
		 * 
		 * PacketUtils.sendPacket =
		 * ((CraftPlayer) player).getHandle().connection.send(packet)
		 * With Mojang mapping of course, idk with Spigot Mapping
		 * 
		 */
		TaskDispatcher.run(() -> {
			Ex.grab(() -> {
				// Info from decompiler
				/*
				 * private final boolean reset; <= var0 private final Map<ResourceLocation,
				 * Advancement.Builder> added; <= var1 private final Set<ResourceLocation>
				 * removed; <= var2 private final Map<ResourceLocation, AdvancementProgress>
				 * progress; <= var3
				 */
	
				// First : send the player he has achievement
				var advancement = createAdvancement(this);
	
				var progress = new net.minecraft.advancements.AdvancementProgress();
	
				ImpossibleTrigger impossibleTrigger = new ImpossibleTrigger();
				Criterion criterion = new Criterion(impossibleTrigger.createInstance(null, null));
					
				var d2Array = new String[][] { { "impossible" } };
	
				progress.update(Map.of("impossible", criterion), d2Array);
				progress.grantProgress("impossible");
	
				var packet = new ClientboundUpdateAdvancementsPacket(false, List.of(advancement), Set.of(),
						Map.of(advancement.getId(), progress));
				// arg0 : reset (idk)
				// arg1 : added
				// arg2 : removed
				// arg3 : progress update
	
				for (Player player : players)
					PacketUtils.sendPacket(player, packet); // Send packet to players
				// Then : remove this same achievement
	
				var removePacket = new ClientboundUpdateAdvancementsPacket(false, List.of(), Set.of(advancement.getId()),
						new HashMap<>());
	
				TaskDispatcher.run(() -> {
					for (Player player : players)
						PacketUtils.sendPacket(player, removePacket); // Send packet to players
	
				}, 250);
			});
		});
	}

	public ChatColor getChatColor() {
		return chatColor;
	}

	public IconType getIcon() {
		return icon;
	}

	public String getTitle() {
		return title;
	}
	
	// From there, it's only hax

	private net.minecraft.advancements.Advancement createAdvancement(Notification notification) {
		ResourceLocation nameKey = new ResourceLocation(this.plugin.getName().toLowerCase(), StringUtils.alphabet(MEM++).toLowerCase());

		return net.minecraft.advancements.Advancement.Builder.fromJson(createJson(notification), null).build(nameKey);
		// var adv = new net.minecraft.advancements.Advancement(nameKey, null,
		// advancementdisplay, advancementrewards, map, astring)
	}

	private String createJsonString(Notification notification) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(createJson(notification));
	}

	private JsonObject createJson(Notification notification) {
		JsonObject json = new JsonObject();
		JsonObject icon = new JsonObject();
		icon.addProperty("item", notification.getIcon().getId());
		JsonObject titleJson = new JsonObject();
		titleJson.addProperty("text", notification.getTitle());
		titleJson.addProperty("color", notification.getChatColor().name().toLowerCase());
		JsonObject display = new JsonObject();
		display.add("icon", icon);
		display.add("title", titleJson);
		display.addProperty("background", "minecraft:textures/gui/advancements/backgrounds/adventure.png");
		display.addProperty("frame", "task");
		display.addProperty("description", " ");
		display.addProperty("announce_to_chat", false);
		display.addProperty("show_toast", true);
		display.addProperty("hidden", true);
		JsonObject criteria = new JsonObject();
		JsonObject trigger = new JsonObject();
		trigger.addProperty("trigger", "minecraft:impossible");
		criteria.add("impossible", trigger);
		json.add("criteria", criteria);
		json.add("display", display);
		return json;
	}

}
