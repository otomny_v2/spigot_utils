package net.spigotutils.player.notification;

public enum IconType{
	
	DIAMOND_SWORD("minecraft:diamond_sword"),
	IRON_SWORD("minecraft:iron_sword"),
	GOLD_SWORD("minecraft:golden_sword"),
	WOOD_SWORD("minecraft:wooden_sword"),
	DIAMOND_PICKAXE("minecraft:diamond_pickaxe"),
	IRON_PICKAXE("minecraft:iron_pickaxe"),
	GOLD_PICKAXE("minecraft:golden_pickaxe"),
	WOOD_PICKAXE("minecraft:wooden_pickaxe"),
	CHEST("minecraft:chest"),
	FURNACE("minecraft:furnace"),
	BARRIER("minecraft:barrier"),
	GRASS_BLOCK("minecraft:grass_block"),
	ENDER_EYE("minecraft:ender_eye"),
	GOLDEN_APPLE("minecraft:golden_apple");
	
	private String id;
	
	private IconType(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
}
