package net.spigotutils.player;

import java.util.function.BiFunction;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;

public class Permissions {

	@Getter @Setter private static BiFunction<Player, String, String> checker;
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param player
	 * @param permission
	 * @return
	 * @date 28/04/2021
	 */
	public static String getValue(Player player, String permission) {
		return checker.apply(player, permission);
	}
	
	/**
	 * 
	 * @author Fabien CAYRE (Computer)
	 *
	 * @param player
	 * @param permission
	 * @return
	 * @date 28/04/2021
	 */
	public static boolean check(Player player, String permission) {
		return getValue(player, permission) != null;
	}
	
}
