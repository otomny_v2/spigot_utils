package net.spigotutils.commands;

public interface ICommand {

	String getName();
	
	String getUsage();
	
}
