package net.spigotutils.commands.arguments;

import java.util.Arrays;
import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class BooleanCommandArgument extends CommandArgument<Boolean> {

  public BooleanCommandArgument() {
    super("Booléen");
  }

  public BooleanCommandArgument(String placeholder) {
    super(placeholder);
  }

  public BooleanCommandArgument(boolean optional) {
    super("Booléen", optional);
  }

  public BooleanCommandArgument(String placeholder, boolean optional) {
    super(placeholder, optional);
  }

  @Override
  public Boolean getValue(String string) {
    Boolean dbl = null;
    try {
      dbl = Boolean.parseBoolean(string);
    } catch (Exception e) {
    }
    return dbl;
  }

  @Override
  public boolean isComponant(String string) {
    boolean isComponant = true;
    try {
      Boolean.parseBoolean(string);
    } catch (Exception e) {
      isComponant = false;
    }
    return isComponant;
  }

  @Override
  public List<String> getAcceptedValues() {
    return Arrays.asList("true", "false");
  }

}
