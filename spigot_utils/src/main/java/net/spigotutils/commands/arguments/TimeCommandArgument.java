package net.spigotutils.commands.arguments;

import java.util.Arrays;
import java.util.List;

import net.spigotutils.commands.CommandArgument;
import net.spigotutils.utils.TimeParser;

public class TimeCommandArgument extends CommandArgument<Long>{

	private static final List<String> ACCEPTEDS_VALUES = Arrays.asList(
			"1y", "1y1M", "1y1M1w", "1y1M1w1h", "1y1M1w1h1m", "1y1M1w1h1m1s");
	
	public TimeCommandArgument() {
		this(false);
	}
	
	public TimeCommandArgument(boolean optional) {
		super("Horodatage", optional);
	}

	@Override
	public Long getValue(String string) {
		return TimeParser.toMillis(string);
	}

	@Override
	public List<String> getAcceptedValues() {
		return ACCEPTEDS_VALUES;
	}

	@Override
	public boolean isComponant(String string) {
		return TimeParser.toMillis(string) != -1;
	}

}
