package net.spigotutils.commands.arguments;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import net.spigotutils.commands.CommandArgument;

public class MultipleStringCommandArgument extends CommandArgument<String>{

	private List<String> acceptedValues;
	
	public MultipleStringCommandArgument(String placeHolder, String... acceptedValues) {
		super(placeHolder);
		this.acceptedValues = Arrays.asList(acceptedValues).stream().collect(Collectors.toList());
	}
	
	public MultipleStringCommandArgument(String placeHolder, boolean optional, String... acceptedValues) {
		super(placeHolder, optional);
		this.acceptedValues = Arrays.asList(acceptedValues).stream().collect(Collectors.toList());
	}

	@Override
	public String getValue(String string) {
		return string;
	}

	@Override
	public boolean isComponant(String string) {
		return acceptedValues.contains(string);
	}

	@Override
	public List<String> getAcceptedValues() {
		return this.acceptedValues;
	}

}
