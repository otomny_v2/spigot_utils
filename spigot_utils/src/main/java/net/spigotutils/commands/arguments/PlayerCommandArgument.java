package net.spigotutils.commands.arguments;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.spigotutils.commands.CommandArgument;

public class PlayerCommandArgument extends CommandArgument<Player>{

	public PlayerCommandArgument() {
		super("Joueur");
	}
	
	public PlayerCommandArgument(boolean optional) {
		super("Joueur", optional);
	}

	@Override
	public Player getValue(String string) {
		return Bukkit.getPlayer(string);
	}

	@Override
	public boolean isComponant(String string) {
		return Bukkit.getPlayer(string) != null;
	}

	@Override
	public List<String> getAcceptedValues() {
		return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
	}

}
