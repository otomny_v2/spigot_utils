package net.spigotutils.commands.arguments;

import java.util.Arrays;
import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class IntegerCommandArgument extends CommandArgument<Integer>{

	public IntegerCommandArgument(String placeHolder) {
		super(placeHolder);
	}
	
	public IntegerCommandArgument(String placeHolder, boolean optional) {
		super(placeHolder, optional);
	}

	@Override
	public Integer getValue(String string) {
		Integer val = null;
		try {
			val = Integer.parseInt(string);
		} catch (Exception e) {}
		return val;
	}

	@Override
	public boolean isComponant(String string) {
		boolean isComponant = true;
		try {Double.parseDouble(string);}catch (Exception e) {
			isComponant = false;
		}
		return isComponant;
	}

	@Override
	public List<String> getAcceptedValues() {
		return Arrays.asList("0", "-1", "1");
	}

}
