package net.spigotutils.commands.arguments;

import java.util.ArrayList;
import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class StringCommandArgument extends CommandArgument<String>{

	public StringCommandArgument(String placeHolder) {
		super(placeHolder);
	}
	
	public StringCommandArgument(String placeHolder, boolean optional) {
		super(placeHolder, optional);
	}

	@Override
	public String getValue(String string) {
		return string;
	}

	@Override
	public List<String> getAcceptedValues() {
		return new ArrayList<>();
	}

	@Override
	public boolean isComponant(String string) {
		return string != null;
	}

}
