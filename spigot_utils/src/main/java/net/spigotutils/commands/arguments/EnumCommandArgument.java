package net.spigotutils.commands.arguments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.spigotutils.commands.CommandArgument;
import net.spigotutils.utils.reflection.ReflectionUtils;

/**
 * 
 */
public class EnumCommandArgument extends CommandArgument<Enum<?>> {

  private Class<? extends Enum<?>> enumClass;
  private Map<String, Enum<?>> enumConstants;

  public EnumCommandArgument(Class<? extends Enum<?>> klass) {
    this(klass, klass.getSimpleName());
  }

  public EnumCommandArgument(Class<? extends Enum<?>> klass, String placeHolder) {
    this(klass, placeHolder, false);
  }

  public EnumCommandArgument(Class<? extends Enum<?>> klass,
      String placeHolder, boolean optional) {
    super(placeHolder, optional);
    this.enumClass = klass;
    this.enumConstants = new HashMap<>();
    try {
      List.of(ReflectionUtils.enumList(this.enumClass))
          .stream()
          .peek(enumConstant -> {
            // Register the enum with it's value while creating accepted values
            this.enumConstants.put(enumConstant.toString(), enumConstant);
          });

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Override
  public Enum<?> getValue(String string) {
    return this.enumConstants.get(string);
  }

  @Override
  public List<String> getAcceptedValues() {
    return new ArrayList<>(this.enumConstants.keySet());
  }

  @Override
  public boolean isComponant(String string) {
    return this.enumConstants.keySet().contains(string);
  }

}
