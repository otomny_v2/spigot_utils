package net.spigotutils.commands.arguments;

import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class SentenceCommandArgument extends CommandArgument<String>{

    public SentenceCommandArgument(String placeHolder) {
        super(placeHolder);
    }

    @Override
    public String getValue(String string) {
        return string;
    }

    @Override
    public List<String> getAcceptedValues() {
        return List.of();
    }

    @Override
    public boolean isComponant(String string) {
        return true;
    }
    
}
