package net.spigotutils.commands.arguments;

import java.util.List;
import java.util.function.Function;

import org.bukkit.command.CommandSender;

import net.spigotutils.commands.CommandArgument;

public abstract class ReactiveCommandArgument<T>
		extends CommandArgument<T> {

	private Function<CommandSender, List<String>> acceptedValuesFunction;

	public ReactiveCommandArgument(String placeHolder,
			boolean optional,
			Function<CommandSender, List<String>> acceptedValuesFunction) {
		super(placeHolder, optional);
		this.acceptedValuesFunction = acceptedValuesFunction;
	}

	public ReactiveCommandArgument(String placeHolder,
			Function<CommandSender, List<String>> acceptedValuesFunction) {
		super(placeHolder);
		this.acceptedValuesFunction = acceptedValuesFunction;
	}

	public List<String> getAccceptedValues(CommandSender sender) {
		return this.acceptedValuesFunction.apply(sender);
	}

	public abstract T getValue(String string,
			CommandSender sender);

	public abstract boolean isComponant(String string,
			CommandSender sender);

	@Override
	@Deprecated
	public boolean isComponant(String string) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@Deprecated
	public T getValue(String string) {
		return null;
	}

	@Override
	@Deprecated
	public List<String> getAcceptedValues() {
		return null;
	}

}
