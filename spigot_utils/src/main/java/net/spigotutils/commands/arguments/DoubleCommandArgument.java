package net.spigotutils.commands.arguments;

import java.util.Arrays;
import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class DoubleCommandArgument extends CommandArgument<Double>{
	
	public DoubleCommandArgument(String placeHolder) {
		super(placeHolder);
	}
	
	public DoubleCommandArgument(String placeHolder, boolean optional) {
		super(placeHolder, optional);
	}

	@Override
	public Double getValue(String string) {
		Double dbl = null;
		try {
			dbl = Double.parseDouble(string);
		} catch (Exception e) {}
		return dbl;
	}

	@Override
	public boolean isComponant(String string) {
		boolean isComponant = true;
		try {Double.parseDouble(string);}catch (Exception e) {
			isComponant = false;
		}
		return isComponant;
	}

	@Override
	public List<String> getAcceptedValues() {
		return Arrays.asList("0.0", "-1.0", "1.0");
	}

}
