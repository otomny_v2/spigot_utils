package net.spigotutils.commands.arguments;

import java.util.ArrayList;
import java.util.List;

import net.spigotutils.commands.CommandArgument;

public class OptionsCommandArgument extends CommandArgument<String>{
	
	public OptionsCommandArgument(String placeHolder) {
		super(placeHolder);
	}
	
	public OptionsCommandArgument(String placeHolder, boolean optional) {
		super("-"+placeHolder, optional);
	}

	@Override
	public String getValue(String string) {
		return string;
	}

	@Override
	public List<String> getAcceptedValues() {
		return new ArrayList<>();
	}

	@Override
	public boolean isComponant(String string) {
		try {
			Integer.parseInt(string);
			return false;
		}catch (Exception e) {}
		try {
			Double.parseDouble(string);
			return false;
		} catch (Exception e) {}
		return string.startsWith("-");
	}
}
