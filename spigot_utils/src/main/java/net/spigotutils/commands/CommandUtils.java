package net.spigotutils.commands;

import java.lang.reflect.Constructor;
import java.util.Set;

import net.spigotutils.plugin.AdvancedPlugin;
import net.spigotutils.utils.reflection.ReflectionUtils;

public class CommandUtils {

	public static void loadCommand(ClassLoader loader, String packageName, String prefix, AdvancedPlugin plugin) {
		
		Set<Class<? extends CustomCommand>> set;
		
		if(loader == null) set = ReflectionUtils.findClassBySuperclass(packageName, CustomCommand.class);
		else set = ReflectionUtils.findClassBySuperclass(loader, packageName, CustomCommand.class);
			
		set.forEach(clazz -> {

			Class<? extends CustomCommand> commandClass = (Class<? extends CustomCommand>) clazz;
			try {
				plugin.getLogger().fine("[Launching] ["+prefix+"] Enregistrement de la commande " + clazz.getSimpleName());
				Constructor<? extends CustomCommand> constructor = commandClass.getConstructor(plugin.getClass());
				plugin.loadCommands(constructor.newInstance(plugin));
			} catch (NoSuchMethodException e) {
				// Il n'y a pas de constructeur nécessitant la classe Main
				try {
					plugin.getLogger().info("[Launching] ["+prefix+"] Enregistrement de la commande " + clazz.getSimpleName());
					Constructor<? extends CustomCommand> constructor = commandClass.getConstructor();
					plugin.loadCommands(constructor.newInstance());

				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	public static void loadCommand(String packageName, String prefix, AdvancedPlugin plugin) {
		loadCommand(null, packageName, prefix, plugin);		
	}
}
