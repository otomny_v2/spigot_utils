package net.spigotutils.commands;

public abstract class CommandComponant {

	private String placeholder;
	private boolean optional;
	
	public CommandComponant(String placeHolder) {
		this(placeHolder, false);
	}
	
	public CommandComponant(String placeHolder, boolean optional) {
		this.placeholder = placeHolder;
		this.optional = optional;
	}

	public abstract boolean isComponant(String string);
	
	public String getPlaceholder() {
		return placeholder;
	}

	protected void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	
	public boolean isOptional() {
		return this.optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}
	
}
