package net.spigotutils.commands;

import java.util.List;

public abstract class CommandArgument<T> extends CommandComponant {


	public CommandArgument(String placeHolder) {
		this(placeHolder, false);
	}
	
	public CommandArgument(String placeHolder, boolean optional) {
		super(placeHolder, optional);
	}

	public abstract T getValue(String string);
	
	public abstract List<String> getAcceptedValues();
	
	
	
	
}
