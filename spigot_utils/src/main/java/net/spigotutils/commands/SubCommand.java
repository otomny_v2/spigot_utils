package net.spigotutils.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;

import net.spigotutils.commands.arguments.ReactiveCommandArgument;
import net.spigotutils.commands.arguments.SentenceCommandArgument;
import net.spigotutils.player.Permissions;

/**
 * Représente une sous commande
 * 
 * @author fabien
 * @date 19/10/2019
 *
 */
public abstract class SubCommand extends CommandComponant implements ICommand {

	private Map<Integer, List<CommandComponant>> componants;
	private ICommand parentCommand;
	private String permission;
	private String usage;
	private boolean needArgument;

	public SubCommand(String placeHolder) {
		super(placeHolder);
		this.componants = new HashMap<>();
		this.permission = "";
		this.needArgument = false;
	}

	@Override
	public String getName() {
		return this.getPlaceholder();
	}

	protected void link(ICommand command) {
		this.parentCommand = command;
	}

	public void setNeedArgument(boolean needArgument) {
		this.needArgument = needArgument;
	}

	@Override
	public boolean isComponant(String string) {
		return this.getPlaceholder().equals(string);
	}

	/**
	 * Enregistre un composant pour la commande (Arg ou Subcommand)
	 * 
	 * 
	 * @author fabien
	 * @date 02/12/2022	 * 
	 * @param slot
	 * @param cc
	 */
	public void reg(int slot, CommandComponant cc){
		this.registerCommandComponant(slot, cc);
	}

	/**
	 * Enregistre un composant pour la commande (Arg ou Subcommand)
	 * 
	 * @author fabien
	 * @date 19/10/2019
	 * @param componant
	 */
	public void registerCommandComponant(int slot, CommandComponant componant) {
		if (!this.componants.containsKey(slot)) {
			List<CommandComponant> componants = new ArrayList<>();
			componants.add(componant);
			this.componants.put(slot, componants);
		} else {
			this.componants.get(slot).add(componant);
		}
		if (componant instanceof SubCommand) {
			((SubCommand) componant).link(this);
		}
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public boolean findSubCommandRespect(CommandSender sender, Predicate<SubCommand> subCommand) {
		return findSubCommand(sender).anyMatch(subCommand);
	}

	public Stream<SubCommand> findSubCommand(CommandSender sender) {
		return this.componants.values().stream().flatMap(List::stream).filter(cc -> cc instanceof SubCommand)
				.map(cc -> (SubCommand) cc).filter(subCommand -> {
					if (!subCommand.getPermission().isEmpty() && sender instanceof Player) {
						return Permissions.check((Player) sender, subCommand.getPermission());
					}
					return true;
				});
	}

	public boolean hasSubCommand(CommandSender sender, List<CommandComponant> list) {
		if (list.isEmpty())
			return false;
		return list.stream().anyMatch(cc -> cc instanceof SubCommand);
	}

	public SubCommand isSubCommand(CommandSender sender, String string) {
		return findSubCommand(sender).filter(subCommand -> subCommand.getPlaceholder().equals(string))
				.filter(subCommand -> {
					if (!subCommand.getPermission().isEmpty() && sender instanceof Player) {
						return Permissions.check((Player) sender, subCommand.getPermission());
					}
					return true;
				}).findFirst().orElse(null);
	}

	public List<CommandArgument<?>> getFirstCommandArguments() {
		LinkedList<CommandArgument<?>> args = new LinkedList<>();
		if (!this.componants.containsKey(0)) {
			return args;
		}
		return this.componants.get(0).stream().filter(cc -> cc instanceof CommandArgument)
				.map(cc -> (CommandArgument<?>) cc).collect(Collectors.toList());
	}

	public List<CommandArgument<?>> getCommandArguments(int i) {
		LinkedList<CommandArgument<?>> args = new LinkedList<>();
		if (!this.componants.containsKey(i)) {
			return args;
		}
		return this.componants.get(i).stream().filter(cc -> cc instanceof CommandArgument)
				.map(cc -> (CommandArgument<?>) cc).collect(Collectors.toList());
	}

	public CommandArgument<?> findCommandArgumentRespect(int i, Predicate<CommandArgument<?>> predicate) {
		return getCommandArguments(i).stream().filter(predicate).findFirst().orElse(null);
	}

	public List<CommandArgument<?>> findCommandArgumentRespect(int i) {
		return this.componants.get(i).stream().filter(cc -> cc instanceof CommandArgument<?>)
				.map(cc -> (CommandArgument<?>) cc).collect(Collectors.toList());
	}

	public boolean existCommandArgumentRespect(int i, Predicate<CommandArgument<?>> predicate) {
		return findCommandArgumentRespect(i, predicate) != null;
	}

	protected void buildUsage() {
		StringBuilder totalUsage = new StringBuilder();
		StringBuilder allUsage = new StringBuilder();
		for (Integer slot : this.componants.keySet()) {
			List<CommandComponant> componants = this.componants.get(slot);
			StringBuilder currentArg = new StringBuilder();
			currentArg.append("<");
			for (CommandComponant componant : componants) {
				if (componant.isOptional()) {
					currentArg.append("(");
				}
				currentArg.append(componant.getPlaceholder());
				if (componant.isOptional()) {
					currentArg.append(")");
				}
				if (componants.indexOf(componant) != componants.size() - 1) {
					currentArg.append("|");
				}
			}
			currentArg.append(">");
			allUsage.append(currentArg);
			allUsage.append(" ");
		}
		String parentUsage = this.parentCommand.getUsage();
		totalUsage.append(parentUsage);
		totalUsage.append(" ");
		totalUsage.append(this.getName());
		totalUsage.append(" ");
		totalUsage.append(allUsage);
		this.usage = totalUsage.toString();

		if (!this.componants.isEmpty()) {
			this.componants
					.values()
					.parallelStream()
					.flatMap(List::stream)
					.filter(comnpt -> comnpt instanceof SubCommand)
					.map(compnt -> (SubCommand) compnt)
					.forEach(SubCommand::buildUsage);
		}
	}

	@Override
	public String getUsage() {
		return this.usage;
	}

	public abstract boolean execute(CommandSender sender, Map<Integer, Object> obj);

	public boolean execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
			if (this.needArgument) {
				sender.sendMessage(this.usage);
				return true;
			}
		}
		Map<Integer, Object> values = new TreeMap<>();
		for (int i = 0; i < args.length; i++) {
			String argument = args[i];
			CommandArgument<?> cmdArg = findCommandArgumentRespect(i, cmndArgs -> {
				if(cmndArgs instanceof ReactiveCommandArgument<?> reacCmdArgs){
					return reacCmdArgs.isComponant(argument, sender);
				}else{
					return cmndArgs.isComponant(argument);
				}
			});
			if (cmdArg != null) {
				Object value = null;
				if (cmdArg instanceof SentenceCommandArgument sentencArg) {
					var builder = new StringBuilder();
					for (int j = i; j < args.length; j++) {
						builder.append(args[j] + " ");
					}
					builder.replace(builder.length() - 1, builder.length(), "");
					value = cmdArg.getValue(builder.toString());
				} else if (cmdArg instanceof ReactiveCommandArgument) {
					value = ((ReactiveCommandArgument<?>) cmdArg).getValue(argument, sender);
				} else {
					value = cmdArg.getValue(argument);
				}

				if (value != null) {
					values.put(i, value);
				}
			}
		}
		if (values.isEmpty()) {
			if (this.needArgument) {
				sender.sendMessage(this.usage);
				return true;
			}
		}

		if (!existCommandArgumentRespect(0, Predicates.alwaysTrue())) {
			if (args.length == 0) {
				if (!this.needArgument) {
					return this.execute(sender, ImmutableMap.of());
				} else if (!hasSubCommand(sender, this.componants.get(0))) {
					boolean result = execute(sender, new HashMap<>());
					if (!result) {
						sender.sendMessage(this.usage);
					}
					return true;
				}

			}
			// TODO un argument passé en paramêtre
			SubCommand subCommand = isSubCommand(sender, args[0]);
			if (subCommand != null) {
				subCommand.execute(sender, Arrays.copyOfRange(args, 1, args.length));
				return true;
			}
		}
		return execute(sender, values);
	}

	public List<String> tabComplete(CommandSender sender, String[] copyOfRange, String previousArgument) {
		List<String> array = new ArrayList<>();
		int currentArgIndex = 0;
		if(copyOfRange.length > 0){
			currentArgIndex = copyOfRange.length - 1;
		}

		if(currentArgIndex > 0){
			SubCommand currentSubCommand = isSubCommand(sender, copyOfRange[0]);
			if (currentSubCommand != null) {
				String[] copy = new String[copyOfRange.length - 1];
				System.arraycopy(copyOfRange, 1, copy, 0, copyOfRange.length - 1);
				array.addAll(currentSubCommand.tabComplete(sender, copy, this.getName()));
			}
		}
		

		List<CommandComponant> cmdComps = this.getCommandComponants(currentArgIndex);
		String currentArgValue = copyOfRange[currentArgIndex];
		Predicate<String> startWith = str -> str.toLowerCase().startsWith(currentArgValue.toLowerCase());
		for (CommandComponant commandComp : cmdComps) {
			if (commandComp instanceof CommandArgument) {
				List<String> acceptedValues = new ArrayList<>();
				if (commandComp instanceof ReactiveCommandArgument) {
					acceptedValues.addAll(((ReactiveCommandArgument<?>) commandComp).getAccceptedValues(sender));
				} else {
					acceptedValues.addAll(((CommandArgument<?>) commandComp).getAcceptedValues());
				}
				array.addAll(acceptedValues.stream().filter(startWith).collect(Collectors.toList()));
			} else {
				array.add(commandComp.getPlaceholder());
			}
		}

		return array;
	}

	public List<CommandComponant> getCommandComponants(int argsIndex) {
		List<CommandComponant> list = this.componants.get(argsIndex);
		return list == null ? new ArrayList<>() : list;
	}

}
