package net.spigotutils.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.base.Predicates;

import net.spigotutils.commands.arguments.ReactiveCommandArgument;
import net.spigotutils.commands.arguments.SentenceCommandArgument;
import net.spigotutils.player.Permissions;

/**
 * Représente une classe helper pour construire des
 * commandes plus facilement
 *
 * @author fabien
 * @date 19/10/2019
 */
public abstract class CustomCommand extends Command implements ICommand {

  public static final String NO_PERMISSION =
      ChatColor.RED + "Vous n'avez pas la permission de faire cette commande";

  /**
   * @author ComminQ_Q (Computer)
   * @param sender
   * @return
   * @date 21/10/2019
   */
  public static Player player(CommandSender sender) {
    if (sender instanceof Player) {
      return ((Player)sender).getPlayer();
    }
    return null;
  }

  /**
   Liste
   des
   composants
   de
   commandes
   (arg
   ou
   subcommand)
 */
  private Map<Integer, List<CommandComponant>> componants;
  private boolean needArgument; // S'il faut un argument à saisir
                                // pour executer la commande

  /**
   * Constructeur
   *
   * @author fabien
   * @date 19/10/2019
   * @param name
   */
  public CustomCommand(String name) {
    super(name);
    this.componants = new HashMap<>();
    this.setPermission("");
  }

  /**
   * Constructeur
   *
   * @author fabien
   * @date 19/10/2019
   * @param name
   * @param description
   * @param aliases
   */
  public CustomCommand(String name, String description, List<String> aliases) {
    this(name, description, "", aliases);
  }

  /**
   * Constructeur
   *
   * @author fabien
   * @date 19/10/2019
   * @param name
   * @param description
   * @param usageMessage
   * @param aliases
   */
  public CustomCommand(String name, String description, String usageMessage,
                       List<String> aliases) {
    super(name, description, usageMessage, aliases);
    this.componants = new HashMap<>();
    this.setPermission("");
  }

  /**
   * Enregistre un composant pour la commande (Arg ou Subcommand)
   *
   *
   * @author fabien
   * @date 02/12/2022	 *
   * @param slot
   * @param cc
   */
  public void reg(int slot, CommandComponant cc) {
    this.registerCommandComponant(slot, cc);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param slot
   * @param componant
   * @date 26/04/2020
   */
  public void registerCommandComponant(int slot, CommandComponant componant) {
    if (!this.componants.containsKey(slot)) {
      List<CommandComponant> componants = new ArrayList<>();
      componants.add(componant);
      this.componants.put(slot, componants);
    } else {
      this.componants.get(slot).add(componant);
    }
    if (componant instanceof SubCommand) {
      ((SubCommand)componant).link(this);
    }
  }

  /**
   * Intilialise la construction de l'usage (A faire a la fin
   * du constructeur implémenté)
   *
   * @author fabien
   * @date 19/10/2019
   */
  public void init() { buildUsage(); }

  /**
   * @author ComminQ_Q (Computer)
   * @param sender
   * @param subCommand
   * @return
   * @date 26/04/2020
   */
  public boolean findSubCommandRespect(CommandSender sender,
                                       Predicate<SubCommand> subCommand) {
    return findSubCommand(sender).anyMatch(subCommand);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param sender
   * @return
   * @date 26/04/2020
   */
  public Stream<SubCommand> findSubCommand(CommandSender sender) {
    return this.componants.values()
        .stream()
        .flatMap(List::stream)
        .filter(cc -> cc instanceof SubCommand)
        .map(cc -> (SubCommand)cc)
        .filter(subCommand -> {
          if (!subCommand.getPermission().isEmpty() && sender instanceof
                                                           Player) {
            return Permissions.check((Player)sender,
                                     subCommand.getPermission());
          }
          return true;
        });
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param sender
   * @param list
   * @return
   * @date 26/04/2020
   */
  public boolean hasSubCommand(CommandSender sender,
                               List<CommandComponant> list) {
    if (list == null) {
      return false;
    }
    if (list.isEmpty())
      return false;
    return list.stream().anyMatch(cc -> cc instanceof SubCommand);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param sender
   * @param string
   * @return
   * @date 26/04/2020
   */
  public SubCommand isSubCommand(CommandSender sender, String string) {
    return findSubCommand(sender)
        .filter(subCommand -> subCommand.getPlaceholder().equals(string))
        .filter(subCommand -> {
          if (!subCommand.getPermission().isEmpty() && sender instanceof
                                                           Player) {
            return Permissions.check((Player)sender,
                                     subCommand.getPermission());
          }
          return true;
        })
        .findFirst()
        .orElse(null);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @return
   * @date 26/04/2020
   */
  public List<CommandArgument<?>> getFirstCommandArguments() {
    LinkedList<CommandArgument<?>> args = new LinkedList<>();
    if (!this.componants.containsKey(0)) {
      return args;
    }
    return this.componants.get(0)
        .stream()
        .filter(cc -> cc instanceof CommandArgument)
        .map(cc -> (CommandArgument<?>)cc)
        .collect(Collectors.toList());
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param i
   * @return
   * @date 26/04/2020
   */
  public List<CommandComponant> getCommandComponants(int i) {
    List<CommandComponant> args = new ArrayList<>();
    if (!this.componants.containsKey(i)) {
      return args;
    }
    return this.componants.get(i);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param i
   * @return
   * @date 26/04/2020
   */
  public List<CommandArgument<?>> getCommandArguments(int i) {
    LinkedList<CommandArgument<?>> args = new LinkedList<>();
    if (!this.componants.containsKey(i)) {
      return args;
    }
    return this.componants.get(i)
        .stream()
        .filter(cc -> cc instanceof CommandArgument)
        .map(cc -> (CommandArgument<?>)cc)
        .collect(Collectors.toList());
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param i
   * @param predicate
   * @return
   * @date 26/04/2020
   */
  public CommandArgument<?>
  findCommandArgumentRespect(int i, Predicate<CommandArgument<?>> predicate) {
    return getCommandArguments(i).stream().filter(predicate).findFirst().orElse(
        null);
  }

  /**
   * @author ComminQ_Q (Computer)
   * @param i
   * @return
   * @date 26/04/2020
   */
  public List<CommandArgument<?>> findCommandArgumentRespect(int i) {
    return this.componants.get(i)
        .stream()
        .filter(cc -> cc instanceof CommandArgument<?>)
        .map(cc -> (CommandArgument<?>)cc)
        .collect(Collectors.toList());
  }

  /**
   * Renvoie l'usage de la commande
   *
   * @author fabien
   * @date 19/10/2019
   * @return
   */
  public String getUsageMessage() { return this.usageMessage; }

  /**
   * Recherche parmis les premier arguments de commande <br>
   * un argument de commande qui matche avec le prédicat
   *
   * @author fabien
   * @date 19/10/2019
   * @param predicate
   * @return
   */
  public CommandArgument<?>
  findCommandArgumentRespect(Predicate<CommandArgument<?>> predicate) {
    return getFirstCommandArguments()
        .stream()
        .filter(predicate)
        .findFirst()
        .orElse(null);
  }

  /**
   * Vérifie la présence un argument de commande<br>
   * qui matche un prédicat
   *
   * @author fabien
   * @date 19/10/2019
   * @param predicate
   * @return
   */
  public boolean
  existCommandArgumentRespect(Predicate<CommandArgument<?>> predicate) {
    return findCommandArgumentRespect(predicate) != null;
  }

  /**
   * Vérifie la présence un argument de commande<br>
   * qui matche un prédicat
   *
   * @author fabien
   * @date 19/10/2019
   * @param predicate
   * @return
   */
  public boolean existCommandComponant(Predicate<CommandComponant> predicate) {
    return this.componants.get(0).stream().filter(predicate).findFirst().orElse(
               null) != null;
  }

  /**
   * Construit l'usage avec les arguments de commandes
   *
   * @author fabien
   * @date 19/10/2019
   */
  private void buildUsage() {
    String usageMessage = "§cUtilisation: /" + this.getName();
    StringBuilder totalUsage = new StringBuilder(usageMessage).append(" ");
    for (List<CommandComponant> liste : this.componants.values()) {
      StringBuilder currentArg = new StringBuilder();
      currentArg.append("<");
      for (CommandComponant carg : liste) {
        if (carg.isOptional()) {
          currentArg.append("(");
        }
        currentArg.append(carg.getPlaceholder());
        if (carg.isOptional()) {
          currentArg.append(")");
        }
        currentArg.append("|");
      }
      currentArg.replace(currentArg.length() - 1, currentArg.length(), "");
      currentArg.append(">");
      totalUsage.append(currentArg).append(" ");
    }

    this.usageMessage = totalUsage.toString();
    if (!this.componants.isEmpty()) {
      this.componants.values()
          .parallelStream()
          .flatMap(List::stream)
          .filter(comnpt -> comnpt instanceof SubCommand)
          .map(compnt -> (SubCommand)compnt)
          .forEach(SubCommand::buildUsage);
    }
  }

  /**
   * Execute la commande implémenté dans la sous-classe
   *
   * @author fabien
   * @date 19/10/2019
   * @param sender
   * @param args
   * @return
   */
  public abstract boolean execute(CommandSender sender,
                                  Map<Integer, Object> args);

  @Override
  public boolean execute(CommandSender sender, String commandLabel,
                         String[] args) {
    Map<Integer, Object> values = new TreeMap<>();
    List<CommandArgument<?>> used = new ArrayList<>();

    if (!getPermission().isEmpty()) {
      if (sender instanceof Player) {
        if (!Permissions.check((Player)sender, getPermission())) {
          sender.sendMessage(CustomCommand.NO_PERMISSION);
          return true;
        }
      }
    }
    int index = args.length - 1;
    if (index >= 1) {
      SubCommand currentSubCommand = this.isSubCommand(sender, args[0]);
      if (currentSubCommand != null) {
        String[] copyOfRange = new String[args.length - 1];
        System.arraycopy(args, 1, copyOfRange, 0, args.length - 1);
        return currentSubCommand.execute(sender, copyOfRange);
      }
    }
    for (int i = 0; i < args.length; i++) {
      final int ii = i;
      CommandArgument<?> cmdArg = findCommandArgumentRespect(i, cmndArgs -> {
        if (cmndArgs instanceof ReactiveCommandArgument<?> reacCmdArgs) {
          return reacCmdArgs.isComponant(args[ii], sender);
        } else {
          return cmndArgs.isComponant(args[ii]);
        }
      });
      if (cmdArg != null && !used.contains(cmdArg)) {
        used.add(cmdArg);
        if (cmdArg instanceof SentenceCommandArgument sentencArg) {
          var builder = new StringBuilder();
          for (int j = i; j < args.length; j++) {
            builder.append(args[j] + " ");
          }
          builder.replace(builder.length() - 1, builder.length(), "");
          values.put(i, cmdArg.getValue(builder.toString()));
          break;
        }
        if (cmdArg instanceof ReactiveCommandArgument<?> reactArg) {
          values.put(i, reactArg.getValue(args[i], sender));
        } else {
          values.put(i, cmdArg.getValue(args[i]));
        }
      }
    }
    if (!existCommandArgumentRespect(Predicates.alwaysTrue())) {
      if (args.length == 0) {
        if (!hasSubCommand(sender, this.componants.get(0))) {
          boolean result = execute(sender, new HashMap<>());
          if (!result) {
            sender.sendMessage(this.usageMessage);
          }
          return true;
        }
      }
    }
    if (args.length > 0) {
      SubCommand subCommand = null;
      try {
        subCommand = isSubCommand(sender, args[0]);
        if (subCommand != null) {
          return subCommand.execute(sender,
                                    Arrays.copyOfRange(args, 1, args.length));
        }
      } catch (Exception e) {
        sender.sendMessage("§cErreur de type " + e.getClass().getSimpleName() +
                           " (Veuillez regarder la console)");
        e.printStackTrace();
        if (subCommand != null) {
          sender.sendMessage(subCommand.getUsage());
        }
        return false;
      }
    }
    if (args.length == 0 || values.isEmpty()) {
      if (needArgument &&
          existCommandArgumentRespect(Predicates.alwaysTrue())) {
        sender.sendMessage(this.usageMessage);
        return true;
      }
    }

    return execute(sender, values);
  }

  @Override
  public List<String> tabComplete(CommandSender sender, String alias,
                                  String[] args)
      throws IllegalArgumentException {
    List<String> array = new ArrayList<>();
    if (!getPermission().isEmpty()) {
      if (sender instanceof Player) {
        if (!Permissions.check((Player)sender, getPermission())) {
          sender.sendMessage(CustomCommand.NO_PERMISSION);
          return array;
        }
      }
    }
    int index = args.length - 1;
    Predicate<String> startWith =
        str -> str.toLowerCase().startsWith(args[index].toLowerCase());
    if (index >= 1) {
      SubCommand currentSubCommand = this.isSubCommand(sender, args[0]);
      if (currentSubCommand != null) {
        String[] copyOfRange = new String[args.length - 1];
        System.arraycopy(args, 1, copyOfRange, 0, args.length - 1);
        array.addAll(
            currentSubCommand.tabComplete(sender, copyOfRange, this.getName()));
      }
    }
    if (index == 0) {
      List<String> placement = findSubCommand(sender)
                                   .map(SubCommand::getPlaceholder)
                                   .filter(startWith)
                                   .collect(Collectors.toList());
      array.addAll(placement);
    }

    List<CommandComponant> cmdComps = this.getCommandComponants(index);
    for (CommandComponant commandComp : cmdComps) {
      if (commandComp instanceof CommandArgument) {
        List<String> acceptedValues = new ArrayList<>();
        if (commandComp instanceof ReactiveCommandArgument) {
          acceptedValues.addAll(((ReactiveCommandArgument<?>)commandComp)
                                    .getAccceptedValues(sender));
        } else {
          acceptedValues.addAll(
              ((CommandArgument<?>)commandComp).getAcceptedValues());
        }
        array.addAll(acceptedValues.stream().filter(startWith).collect(
            Collectors.toList()));
      } else if (!(commandComp instanceof SubCommand)) {
        if (startWith.test(commandComp.getPlaceholder())) {
          array.add(commandComp.getPlaceholder());
        }
      }
    }

    return array;
  }

  @Override
  public String getUsage() {
    return super.getUsage();
  }

  public Map<Integer, List<CommandComponant>> getComponants() {
    return this.componants;
  }

  /**
   * Vérifie s'il y a besoin d'un argument de commande pour
   * qu'elle soit executée
   *
   * @author fabien
   * @date 19/10/2019
   * @return
   */
  public boolean isNeedArgument() { return needArgument; }

  /**
   * Met à jour le besoin d'un argument
   *
   * @author fabien
   * @date 19/10/2019
   * @param needArgument
   */
  public void setNeedArgument(boolean needArgument) {
    this.needArgument = needArgument;
  }
}
