package net.spigotutils.test.gui;

import org.junit.Test;

import lombok.Getter;
import lombok.Setter;
import net.spigotutils.gui.obj.GuiParameterBuilder;
import net.spigotutils.gui.obj.GuiParameterBuilder.ParameterType;

public class TestGuiParameter {

	@Test
	public void testGuiParams() {

		DummyParameter dummy = new DummyParameter();
		GuiParameterBuilder<DummyParameter> builder = new GuiParameterBuilder<DummyParameter>(dummy)
					.field("test", ParameterType.STRING, "Nom test1")
					.field("test2", ParameterType.INTEGER, "Nombre test2");
		
		
	}
	
	
	private class DummyParameter{
		
		@Getter @Setter private String test;
		@Getter @Setter private int test2;
		
	}
}
