package net.spigotutils.test.command.testcommands;

import java.util.Map;
import java.util.function.Consumer;

import org.bukkit.command.CommandSender;

import net.spigotutils.commands.CustomCommand;

public class TestCustomCommand extends CustomCommand{

	public TestCustomCommand() {
		super("test");
		init();
	}
	
	public TestCustomCommand(Consumer<CustomCommand> consumer) {
		super("test");
		consumer.accept(this);
		init();
	}

	@Override
	public boolean execute(CommandSender sender, Map<Integer, Object> args) {
		sender.sendMessage("");
		return true;
	}

}
