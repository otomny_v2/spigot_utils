package net.spigotutils.test.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.kyori.adventure.text.Component;

public class TestCommandSender implements CommandSender {

	private LinkedList<String> message;
	
	public TestCommandSender() {
		this.message = new LinkedList<String>();
	}
	
	@Override
	public boolean isPermissionSet(String name) {
		return true;
	}

	@Override
	public boolean isPermissionSet(Permission perm) {
		return true;
	}

	@Override
	public boolean hasPermission(String name) {
		return true;
	}

	@Override
	public boolean hasPermission(Permission perm) {
		return true;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
		return null;
	}

	@Override
	public void removeAttachment(PermissionAttachment attachment) {

	}

	@Override
	public void recalculatePermissions() {

	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return null;
	}

	@Override
	public boolean isOp() {
		return true;
	}

	@Override
	public void setOp(boolean value) {
	}

	@Override
	public void sendMessage(String message) {
		this.message.add(message);
	}

	@Override
	public void sendMessage(String[] messages) {
		this.message.addAll(Arrays.asList(Objects.requireNonNull(messages)));
	}
	
	public LinkedList<String> getMessage() {
		return message;
	}
	
	public String getLast() {
		return getMessage().getLast();
	}

	@Override
	public Server getServer() {
		return null;
	}

	@Override
	public String getName() {
		return "TestConsoleSender";
	}

	@Override
	public Spigot spigot() {
		return null;
	}

	@Override
	public void sendMessage(@Nullable UUID sender, @NotNull String message) {
		
	}

	@Override
	public void sendMessage(@Nullable UUID sender, @NotNull String... messages) {
		
	}

	@Override
	public @NotNull Component name() {
		return null;
	}

}
