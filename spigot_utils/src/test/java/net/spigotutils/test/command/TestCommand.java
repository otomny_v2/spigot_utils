package net.spigotutils.test.command;

import static org.junit.Assert.assertEquals;

import org.bukkit.ChatColor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.spigotutils.commands.CustomCommand;
import net.spigotutils.commands.arguments.DoubleCommandArgument;
import net.spigotutils.commands.arguments.IntegerCommandArgument;
import net.spigotutils.commands.arguments.MultipleStringCommandArgument;
import net.spigotutils.commands.arguments.PlayerCommandArgument;
import net.spigotutils.test.command.testcommands.TestCustomCommand;

public class TestCommand {

	private TestCommandSender console;
	private CustomCommand command;
	
	@Before
	public void setUp() {
		this.console = new TestCommandSender();
		this.command = new TestCustomCommand();
	}
	
	@After
	public void tearDown() {
		this.command = null;
		this.console = null;
	}
	
	@Test
	public void testCommandNoArgumentNoNeed() {
		this.command.execute(this.console, "test", new String[0]);
		assertEquals("", this.console.getLast());
	}
	
	@Test
	public void testCommandOneArgumentIntegerTypeUsage() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.registerCommandComponant(0, new IntegerCommandArgument("Valeur"));
		});
		assertEquals("Utilisation: /test <Valeur> ", ChatColor.stripColor(this.command.getUsage()));
	}
	
	@Test
	public void testCommandOneArgumentDoubleTypeUsage() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.registerCommandComponant(0, new DoubleCommandArgument("Valeur"));
		});
		assertEquals("Utilisation: /test <Valeur> ", ChatColor.stripColor(this.command.getUsage()));
	}
	
	@Test
	public void testCommandOneArgumentPlayerTypeUsage() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.registerCommandComponant(0, new PlayerCommandArgument());
		});
		assertEquals("Utilisation: /test <Joueur> ", ChatColor.stripColor(this.command.getUsage()));
	}
	
	@Test
	public void testCommandOneArgumentMultiStringTypeUsage() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.registerCommandComponant(0, new MultipleStringCommandArgument("Armure", "Shonai", "Murai"));
		});
		assertEquals("Utilisation: /test <Armure> ", ChatColor.stripColor(this.command.getUsage()));
	}
	
	@Test
	public void testCommandOneArgumentIntegerConsoleReceive() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.setNeedArgument(true);
			customCommand.registerCommandComponant(0, new IntegerCommandArgument("Valeur"));
		});
		this.command.execute(this.console, "test", new String[0]);
		assertEquals("Utilisation: /test <Valeur> ", ChatColor.stripColor(this.console.getLast()));
	}
	
	@Test
	public void testCommandOneArgumentDoubleConsoleReceive() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.setNeedArgument(true);
			customCommand.registerCommandComponant(0, new DoubleCommandArgument("Valeur"));
		});
		this.command.execute(this.console, "test", new String[0]);
		assertEquals("Utilisation: /test <Valeur> ", ChatColor.stripColor(this.console.getLast()));
	}
	
	@Test
	public void testCommandOneArgumentPlayerConsoleReceive() {
		this.command = new TestCustomCommand(customCommand -> {
			customCommand.setNeedArgument(true);
			customCommand.registerCommandComponant(0, new PlayerCommandArgument());
		});
		this.command.execute(this.console, "test", new String[0]);
		assertEquals("Utilisation: /test <Joueur> ", ChatColor.stripColor(this.console.getLast()));
	}

}
