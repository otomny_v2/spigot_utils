package net.spigotutils.test.nbt;

import static org.junit.Assert.assertEquals;

import org.bukkit.Material;
import org.junit.Test;

import net.spigotutils.nbt.BlockData;

public class BlockDataTest {

	@Test
	public void testBlockDataReplaceStairs() {

		var blockData = new BlockData(
				"minecraft:stone_stairs[cock=true]", "zzz", 0, 0, 0);

		assertEquals(blockData.getFastMaterial(),
				Material.STONE_STAIRS);

		blockData.swapMaterial(Material.ACACIA_STAIRS);

		assertEquals(blockData.getFastMaterial(),
				Material.ACACIA_STAIRS);
		assertEquals(blockData.getBlockData(), "minecraft:acacia_stairs[cock=true]");

	}

}
