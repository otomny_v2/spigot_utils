package net.spigotutils.test.components;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;

public class TestKyoriComponent {

	@Test
	public void testComponentToString() {
		String original = "Test un deux trois §aquatre";
		TextComponent component = Component.text("Test un deux trois §aquatre");
	
		assertEquals(original, component.content());
	}

}
