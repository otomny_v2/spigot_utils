package net.spigotutils.test.schematics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.junit.Test;

import net.spigotutils.schematics.FastSchematic;
import net.spigotutils.schematics.FastSchematicV3;

public class NewSchematicTest {

  private String[] toBlockDatas(Material[] mats) {
    String[] s = new String[mats.length];
    for (int i = 0; i < mats.length; i++) {
      s[i] = "minecraft:" + mats[i].toString().toLowerCase();
    }
    return s;
  }

  @Test
  public void testSchematicV3Single_RLE() {
    Material[] matsRaw = new Material[] {
        // premier étage
        Material.GOLD_BLOCK,
        Material.GOLD_BLOCK,
        Material.GOLD_BLOCK,
        Material.GOLD_BLOCK,
    };
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 1, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.RLE, segments.get(0).mode());
    assertEquals(Material.GOLD_BLOCK, segments.get(0).materialRaw());
    assertEquals(4, segments.get(0).count());
    assertEquals(1, segments.size());
  }

  /**
   * Test de codage de deux RLE à la suite mais pour différents type de blocks
   */
  @Test
  public void testSchematicV3Double_RLE() {
    Material[] matsRaw =
        new Material[] {// premier étage
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        // deuxième étage
                        Material.COBBLESTONE, Material.COBBLESTONE,
                        Material.COBBLESTONE, Material.COBBLESTONE};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 2, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.RLE, segments.get(0).mode());
    assertEquals(Material.GOLD_BLOCK, segments.get(0).materialRaw());
    assertEquals(4, segments.get(0).count());

    assertEquals(FastSchematicV3.RLE, segments.get(4).mode());
    assertEquals(Material.COBBLESTONE, segments.get(4).materialRaw());
    assertEquals(4, segments.get(4).count());

    assertEquals(2, segments.size());
  }

  /**
   * Test de codage de trois RLE à la suite mais pour différents type de blocks
   */
  @Test
  public void testSchematicV3Triple_RLE() {
    Material[] matsRaw =
        new Material[] {// premier étage
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        // deuxième étage
                        Material.COBBLESTONE, Material.COBBLESTONE,
                        Material.COBBLESTONE, Material.COBBLESTONE,
                        // troisième étage
                        Material.REDSTONE, Material.REDSTONE, Material.REDSTONE,
                        Material.REDSTONE};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 3, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.RLE, segments.get(0).mode());
    assertEquals(Material.GOLD_BLOCK, segments.get(0).materialRaw());
    assertEquals(4, segments.get(0).count());

    assertEquals(FastSchematicV3.RLE, segments.get(4).mode());
    assertEquals(Material.COBBLESTONE, segments.get(4).materialRaw());
    assertEquals(4, segments.get(4).count());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.REDSTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(3, segments.size());
  }

  /**
   * Test de codage de quatre RLE à la suite mais pour différents type de blocks
   */
  @Test
  public void testSchematicV3Quadruple_RLE() {
    Material[] matsRaw = new Material[] {
        // premier étage
        Material.GOLD_BLOCK, Material.GOLD_BLOCK, Material.GOLD_BLOCK,
        Material.GOLD_BLOCK,
        // deuxième étage
        Material.COBBLESTONE, Material.COBBLESTONE, Material.COBBLESTONE,
        Material.COBBLESTONE,
        // troisième étage
        Material.REDSTONE, Material.REDSTONE, Material.REDSTONE,
        Material.REDSTONE,
        // quatrième étage
        Material.DIAMOND, Material.DIAMOND, Material.DIAMOND, Material.DIAMOND};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 4, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.RLE, segments.get(0).mode());
    assertEquals(Material.GOLD_BLOCK, segments.get(0).materialRaw());
    assertEquals(4, segments.get(0).count());

    assertEquals(FastSchematicV3.RLE, segments.get(4).mode());
    assertEquals(Material.COBBLESTONE, segments.get(4).materialRaw());
    assertEquals(4, segments.get(4).count());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.REDSTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(FastSchematicV3.RLE, segments.get(12).mode());
    assertEquals(Material.DIAMOND, segments.get(12).materialRaw());
    assertEquals(4, segments.get(12).count());

    assertEquals(4, segments.size());
  }

  /**
   * Test de codage, mode RLE, puis mode MATRIX puis mode RLE encore
   */
  @Test
  public void testSchematicV3_RLE_MATRIX_RLE() {
    Material[] matsRaw =
        new Material[] {// premier étage
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        Material.GOLD_BLOCK, Material.GOLD_BLOCK,
                        // deuxième étage
                        Material.GOLD_BLOCK, Material.REDSTONE,
                        Material.REDSTONE, Material.COBBLESTONE,
                        // troisième étage
                        Material.COBBLESTONE, Material.COBBLESTONE,
                        Material.COBBLESTONE, Material.COBBLESTONE};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 3, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.RLE, segments.get(0).mode());
    assertEquals(Material.GOLD_BLOCK, segments.get(0).materialRaw());
    assertEquals(5, segments.get(0).count());

    assertEquals(FastSchematicV3.MATRIX, segments.get(5).mode());
    assertEquals(FastSchematicV3.RLE, segments.get(7).mode());
    assertEquals(3, segments.size());
  }

  /**
   * Test de codage, MATRIX puis RLE
   */
  @Test
  public void testSchematicV3_MATRIX_RLE() {
    Material[] matsRaw =
        new Material[] {// premier étage
                        Material.GOLD_BLOCK, Material.STONE,
                        Material.GOLD_BLOCK, Material.LAPIS_BLOCK,
                        // deuxième étage
                        Material.GOLD_BLOCK, Material.REDSTONE,
                        Material.REDSTONE, Material.OAK_BOAT,
                        // troisième étage
                        Material.COBBLESTONE, Material.COBBLESTONE,
                        Material.COBBLESTONE, Material.COBBLESTONE};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 3, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.MATRIX, segments.get(0).mode());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.COBBLESTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(2, segments.size());
  }

  /**
   * Test de codage, MATRIX puis Double RLE
   */
  @Test
  public void testSchematicV3_MATRIX_Double_RLE() {
    Material[] matsRaw = new Material[] {
        // premier étage
        Material.GOLD_BLOCK, Material.STONE, Material.GOLD_BLOCK,
        Material.LAPIS_BLOCK,
        // deuxième étage
        Material.GOLD_BLOCK, Material.REDSTONE, Material.REDSTONE,
        Material.OAK_BOAT,
        // troisième étage
        Material.COBBLESTONE, Material.COBBLESTONE, Material.COBBLESTONE,
        Material.COBBLESTONE,
        // Quatrième étage
        Material.DIAMOND, Material.DIAMOND, Material.DIAMOND, Material.DIAMOND};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 4, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.MATRIX, segments.get(0).mode());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.COBBLESTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(FastSchematicV3.RLE, segments.get(12).mode());
    assertEquals(Material.DIAMOND, segments.get(12).materialRaw());
    assertEquals(4, segments.get(12).count());

    assertEquals(3, segments.size());
  }

  @Test
  public void testArraySameDim() {

    int minX = 8;
    int minY = 2;
    int minZ = 6;
    int maxX = 16;
    int maxY = 3;
    int maxZ = 18;

    // 16-8: 8
    // 2-2

    int width = (maxX - minX) + 1;
    int height = (maxY - minY) + 1;
    int length = (maxZ - minZ) + 1;

    Material[] array = new Material[length * height * width];
    Arrays.fill(array, Material.AIR);

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        for (int z = 0; z < length; z++) {
          int index = y * length * width + x * length + z;
          array[index] = Material.STONE;
        }
      }
    }

    List<Material> materials = Arrays.asList(array);
    assertTrue(materials.contains(Material.STONE));
    assertFalse(materials.contains(Material.AIR));
  }

  @Test
  public void testSchematicV3_Save_Load() {
    Material[] matsRaw = new Material[] {
        // premier étage
        Material.GOLD_BLOCK, Material.STONE, Material.GOLD_BLOCK,
        Material.LAPIS_BLOCK,
        // deuxième étage
        Material.GOLD_BLOCK, Material.REDSTONE, Material.REDSTONE,
        Material.OAK_BOAT,
        // troisième étage
        Material.COBBLESTONE, Material.COBBLESTONE, Material.COBBLESTONE,
        Material.COBBLESTONE,
        // Quatrième étage
        Material.DIAMOND, Material.DIAMOND, Material.DIAMOND, Material.DIAMOND};
    String[] mats = toBlockDatas(matsRaw);

    FastSchematic schematic = new FastSchematic(2, 4, 2, mats, 0, 0, 0);
    var segments = FastSchematicV3.getMarkers(schematic);

    assertEquals(FastSchematicV3.MATRIX, segments.get(0).mode());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.COBBLESTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(FastSchematicV3.RLE, segments.get(12).mode());
    assertEquals(Material.DIAMOND, segments.get(12).materialRaw());
    assertEquals(4, segments.get(12).count());

    assertEquals(3, segments.size());

    byte[] data =
        FastSchematic.base64Encode(null, schematic, new FastSchematicV3());

    FastSchematic loadFromData = FastSchematic.base64Decode(null, data);

    assertEquals(schematic.getBlocks().length, loadFromData.getBlocks().length);
    for (int i = 0; i < schematic.getBlocks().length; i++) {
      assertEquals(schematic.getBlocks()[i], loadFromData.getBlocks()[i]);
    }

    segments = FastSchematicV3.getMarkers(loadFromData);

    assertEquals(FastSchematicV3.MATRIX, segments.get(0).mode());

    assertEquals(FastSchematicV3.RLE, segments.get(8).mode());
    assertEquals(Material.COBBLESTONE, segments.get(8).materialRaw());
    assertEquals(4, segments.get(8).count());

    assertEquals(FastSchematicV3.RLE, segments.get(12).mode());
    assertEquals(Material.DIAMOND, segments.get(12).materialRaw());
    assertEquals(4, segments.get(12).count());

    assertEquals(3, segments.size());
  }
}
