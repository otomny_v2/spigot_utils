package net.spigotutils.test.item;

import static org.junit.Assert.*;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.Before;
import org.junit.Test;

import net.spigotutils.item.ItemStackBuilder;

/**
 * Test du builder d'item <br>
 * SEULEMENT LES TESTS N'IMPLICANT PAS de {@link ItemMeta} <br>
 * Car nécessiste une instance de {@link Server} (Minecraft) <br>
 * @author ComminQ_Q (Computer)
 *
 * @date 04/10/2019
 */
public class TestItemStackBuilder {

	private ItemStackBuilder builder;
	
	@Before
	public void before() {
		this.builder = new ItemStackBuilder(Material.STONE, 1);
	}
	
	@Test
	public void testItemStackBuilderMaterial() {
		assertEquals(this.builder.get().getType(), Material.STONE);
	}

	@Test
	public void testItemStackBuilderMaterialInt() {
		assertEquals(this.builder.get().getType(), Material.STONE);
		assertEquals(this.builder.get().getAmount(), 1);
	}

	@Test
	public void testItemStackBuilderItemStack() {
		ItemStack itemStack = new ItemStack(Material.STONE, 1);
		ItemStackBuilder builder = new ItemStackBuilder(itemStack);
		assertEquals(builder.get().getType(), Material.STONE);
		assertEquals(builder.get().getAmount(), 1);
	}

}
