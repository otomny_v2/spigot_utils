package net.spigotutils.test.number;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.spigotutils.utils.number.NumberUtils;

public class NumberTest {
    
    // Tous les tests === Combien de bits pour coder ce nombre
    @Test
    public void testCleanBinLog(){
       


        int _1Bits = NumberUtils.fastlog2(2); //1
        int _2Bits = NumberUtils.fastlog2(4); //2
        int _3Bits = NumberUtils.fastlog2(8); //3
        int _4Bits = NumberUtils.fastlog2(16); //4
        int _5Bits = NumberUtils.fastlog2(32); //5
        int _6Bits = NumberUtils.fastlog2(64); //5
        int _7Bits = NumberUtils.fastlog2(128); //5
        int _8Bits = NumberUtils.fastlog2(256); //5
        int _9Bits = NumberUtils.fastlog2(512); //5

        assertEquals(1, _1Bits);
        assertEquals(2, _2Bits);
        assertEquals(3, _3Bits);
        assertEquals(4, _4Bits);
        assertEquals(5, _5Bits);   
        assertEquals(6, _6Bits);   
        assertEquals(7, _7Bits);   
        assertEquals(8, _8Bits);         
        assertEquals(9, _9Bits);   
    }

    @Test
    public void testDirtyBinLog(){

        int test1 = NumberUtils.fastlog2(250); // 2 ^ 8 = 256
        int test2 = NumberUtils.fastlog2(255); // 2 ^ 8 = 256
        int test3 = NumberUtils.fastlog2(257); // 2 ^ 9 = 512

        assertEquals(7, test1);
        assertEquals(7, test2);
        assertEquals(8, test3);
    }

    @Test
    public void testDirtyBinLogSlow(){
        //  0   1   2   3   4   5   6   7   8   9   10      11      12      13
        //  1   2   4   8   16  32  64  128 256 512 1024    2048    4096    8192

        int test1 = NumberUtils.slowlog2(250); // 2 ^ 8 = 256, besoin 
        int test2 = NumberUtils.slowlog2(255); // 2 ^ 8 = 256
        int test3 = NumberUtils.slowlog2(257); // 2 ^ 9 = 512

        assertEquals(7, test1);
        assertEquals(7, test2);
        assertEquals(8, test3);
    }
}
