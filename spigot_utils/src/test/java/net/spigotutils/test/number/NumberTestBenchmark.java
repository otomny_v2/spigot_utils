package net.spigotutils.test.number;

import net.spigotutils.utils.number.NumberUtils;

public class NumberTestBenchmark {
    
    public static void main(String[] args) {

        final int ITERATIONS = 1_000_000;
        
        System.out.println("Pre  benchmark ...");
        long start = System.currentTimeMillis();
        for(int i = 0; i < ITERATIONS; i ++){
            NumberUtils.slowlog2(i);
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("Timings: "+time+"ms for "+ITERATIONS+" iteration | "+(time/ITERATIONS)+"ms per iteration");

        start = System.currentTimeMillis();
        for(int i = 0; i < ITERATIONS; i ++){
            NumberUtils.fastlog2(i);
        }
        time = System.currentTimeMillis() - start;
        System.out.println("Timings: "+time+"ms for "+ITERATIONS+" iteration | "+(time/ITERATIONS)+"ms per iteration");
        
        System.out.println("WarmUP done !");
        start = System.currentTimeMillis();
        for(int i = 0; i < ITERATIONS; i ++){
            NumberUtils.slowlog2(i);
        }
        time = System.currentTimeMillis() - start;
        System.out.println("Timings: "+time+"ms for "+ITERATIONS+" iteration | "+(time/ITERATIONS)+"ms per iteration");

        start = System.currentTimeMillis();
        for(int i = 0; i < ITERATIONS; i ++){
            NumberUtils.fastlog2(i);
        }
        time = System.currentTimeMillis() - start;
        System.out.println("Timings: "+time+"ms for "+ITERATIONS+" iteration | "+(time/ITERATIONS)+"ms per iteration");
    }

}
