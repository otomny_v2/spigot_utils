package net.spigotutils.test.teleportation;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.spigotutils.teleport.Teleportation;

public class TeleporationTest {

	@Test
	public void testDefaultInstant() throws Exception {
		Teleportation tp = Teleportation
					.builder()
					.build();
		
		assertTrue(tp.isInstant());
	}
	
}
